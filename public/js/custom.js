$(document).ready(function(){
    $('#show').click(function() {
      $('.breaks').toggle("slide");
    });
});

/*function startchange() {
  var start = document.getElementById('started').value;
  document.getElementById("startvalue").value = start;
}

function endchange() {
  var end = document.getElementById('ended').value;
  document.getElementById("endvalue").value = end;
}*/

function test(elem) {

var dataId = $(elem).data("id");
var startedAt = $(elem).attr("startedAt");
var endedAt = $(elem).attr("endedAt");

document.getElementById('breakid').value = dataId;
document.getElementById('started').value = startedAt;
document.getElementById('ended').value = endedAt;

}

$(document).ready(function(){
$('#send_form').click(function(e){

  $('#send_form').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
  e.preventDefault();

  var form = document.forms.namedItem("contact_us"); // high importance!, here you need change "yourformname" with the name of yourform
  var formData = new FormData(form); // high importance!
  var id = document.getElementById('breakid').value;
  $.ajax({
  url: "{{route('breaks.update','')}}/"+parseInt(id),
  method: 'post',
  data: formData,
  dataType: "json", // or html if you want...
  contentType: false, // high importance!
  processData: false, // high importance!
  success: function(data){
  $('#send_form').html('Update');
  if(data.status==200){
    $('.message').toggle("slide");
  }    

    setTimeout(function() {
    $('.message').toggle("hide"); 
    }, 3000); // <-- time in milliseconds
 
  }});
  });
});

$(document).ready(function(){
$('#send_form2').click(function(e){

  $('#send_form2').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
  e.preventDefault();

  var form = document.forms.namedItem("contact_us2"); // high importance!, here you need change "yourformname" with the name of yourform
  var formData = new FormData(form); // high importance!

  $.ajax({
  url: "{{route('breaks.add')}}",
  method: 'post',
  data: formData,
  dataType: "json", // or html if you want...
  contentType: false, // high importance!
  processData: false, // high importance!
  success: function(data){
  $('#send_form2').html('Add');
  if(data.status==200){
    $('.message2').toggle("slide");
  }    

    setTimeout(function() {
    $('.message2').toggle("hide"); 
    }, 3000); // <-- time in milliseconds
 
  }});
  });
});

var select = document.getElementById('job_id');
select.onchange = function(){
var id = $(this).val();
getjobs($(this).val());

};

function getjobs(id) {

$.ajax({
method:"get",
url: "{{route('job.data','')}}/"+parseInt(id),
dataType: "json",
contentType: false, // high importance!
processData: false, // high importance!
success:function(result)
{       
if(result)
{
var $jobtypes = $('#jobtypes');
$("#jobtypes").empty();
$("#jobtypes").append(new Option("Select Job Type", "value"));
$.each(result,function(key,value){

var $option = $("<option/>", { value: key,text: value});
$jobtypes.append($option);

});
$('.selecs').reset();
}
}});
}

$(document).ready(function() {
    $('body').bootstrapMaterialDesign();
      $('.datetimepicker').datetimepicker({
        format: 'MM/DD/YYYY h:mm A',
           icons: {
               time: "fa fa-clock-o",
               date: "fa fa-calendar",
               up: "fa fa-chevron-up",
               down: "fa fa-chevron-down",
               previous: 'fa fa-chevron-left',
               next: 'fa fa-chevron-right',
               today: 'fa fa-screenshot',
               clear: 'fa fa-trash',
               close: 'fa fa-remove'
           }
        });

        $('.datepicker').datetimepicker({
           format: 'MM/DD/YYYY',
           icons: {
               time: "fa fa-clock-o",
               date: "fa fa-calendar",
               up: "fa fa-chevron-up",
               down: "fa fa-chevron-down",
               previous: 'fa fa-chevron-left',
               next: 'fa fa-chevron-right',
               today: 'fa fa-screenshot',
               clear: 'fa fa-trash',
               close: 'fa fa-remove'
           }
        });

        $('.timepicker').datetimepicker({
//          format: 'H:mm',    // use this format if you want the 24hours timepicker

           format: 'h:mm A',    //use this format if you want the 12hours timpiecker with AM/PM toggle
           icons: {
               time: "fa fa-clock-o",
               date: "fa fa-calendar",
               up: "fa fa-chevron-up",
               down: "fa fa-chevron-down",
               previous: 'fa fa-chevron-left',
               next: 'fa fa-chevron-right',
               today: 'fa fa-screenshot',
               clear: 'fa fa-trash',
               close: 'fa fa-remove'
           }
        });
});