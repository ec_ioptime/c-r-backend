<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeList extends Model
{
    public $table = "times_list";


    public $fillable = [
        'name'
    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];


}
