<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobConfiguration extends Model
{

     /**
     * Table
     */
    public $table = "job_configuration";

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'job_id',
        'job_type_id',
        'start_time',
        'end_time',
        'overtime',
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday',
        'sunday',
        'start_overtime',
        'end_overtime',
        'overtime_limit',
        'overtime_rate'
    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

     
     /**
     * Job Type Model Linked
     */
    public function types(){

        return $this->belongsTo('App\Types','job_type_id','id');
    }
}
