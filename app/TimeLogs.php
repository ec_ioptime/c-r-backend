<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeLogs extends Model
{

     /**
     * Table
     */
    public $table = "time_logs";

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'user_id',
        'job_id',
        'job_type_id',
        'clock_intime',
        'leave_yardtime',
        'arrive_jobtime',
        'leave_jobtime',
        'arrive_yardtime',
        'clock_outtime',
        'clock_in_location',
        'leave_yard_location',
        'arrive_job_location',
        'leave_job_location',
        'arrive_yard_location',
        'clock_out_location',
        'hourly',
        'overtime_hourly',
        'planttime_hourly',
        'plant_overtime',
        'hourly_lp2',
        'overtime_hourly_lp2',
        'hourly_lp4',
        'overtime_hourly_lp4',
        'plant_hourly_lp2',
        'plant_overtime_lp2',
        'planttime_hourly_lp4',
        'plant_overtime_lp4',
        'fixed_salary',
        'double_time_hourly',
        'lp2_doubletime',
        'lp4_doubletime',
        'plant_doubletime',
        'lp2_plant_doubletime',
        'lp4_plant_doubletime',
        'mechanic_callout',
        'vacation_pay_hourly',
        'sick_pay_hourly',
        'holiday',
        'invoiceNumber',
        'notes',
        'status'
    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];


     /**
     * Jobs Model Linked
     */
    public function jobs(){

        return $this->belongsTo('App\Jobs','job_id','id');
    }

     /**
     * Job Types Model Linked
     */
    public function types(){

        return $this->belongsTo('App\Types','job_type_id','id');
    }

     /**
     * User Model Linked
     */

    public function user(){

        return $this->belongsTo('App\User','user_id','id');
    }


}
