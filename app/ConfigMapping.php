<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfigMapping extends Model
{    

     /**
     * Table
     */
    public $table = "conf_mapping";

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'config_id',
        'mapped_to'
    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

     /**
     * Config Model Linked
     */
    public function jobconfig(){

        return $this->belongsTo('App\JobConfiguration','config_id','id');
    }
    
}
