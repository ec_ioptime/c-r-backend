<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{

     /**
     * Table
     */
    public $table = "jobs";

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'title','on_site'
    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

     
     /**
     * Job Type Model Linked
     */
    public function types(){

        return $this->hasMany('App\Types','job_id','id');
    }
}
