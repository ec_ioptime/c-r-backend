<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Breaks extends Model
{

     /**
     * Table
     */
    public $table = "breaks";

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'time_log_id',
        'startedAt',
        'endedAt',
        'startedAtLocation',
        'endedAtLocation',
        'total_time'
    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

}
