<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    public $fillable = [
        'name','user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    // public function client(){
    //     return $this->belongsTo(Client::class,'client_id');
    // }
    public function reports(){
        return $this->hasMany(Report::class,'property_id');
    }

    public static function getAllAttributes()
    {
        $instance = new Property();
        return $instance->fillable;
    }
}
