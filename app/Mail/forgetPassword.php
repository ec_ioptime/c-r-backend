<?php



namespace App\Mail;

use App\User;



use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Contracts\Queue\ShouldQueue;



class forgetPassword extends Mailable

{

    use Queueable, SerializesModels;



    public $code;

    public  $user;



    /**

     * Create a new message instance.

     *

     * @return void

     */

    public function __construct($code,User $user)

    {

        $this->code=$code;

        $this->user = $user;

    }



    /**

     * Build the message.

     *

     * @return $this

     */

    public function build()

    {

        return $this->from('no-reply@crtimeclock.com')->subject('Reset Password')->markdown('emails.forgetPassword');



    }

}

