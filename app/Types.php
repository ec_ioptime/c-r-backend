<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Types extends Model
{

     /**
     * Table
     */
    public $table = "job_types";

     
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'job_id','name'
    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    

     /**
     * Jobs Model Linked
     */
    public function jobs(){

        return $this->belongsTo('App\Jobs','job_id','id');
    }
}
