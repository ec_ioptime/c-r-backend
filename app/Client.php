<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $fillable=[
    // 'id',
    'first_name',
    'last_name',
    // 'phone_number',
    // 'email',
    // 'address',
    // 'city',
    // 'state',
    // 'zip',
    // 'country',
    // 'date_of_inspection',
    // 'inspection_fee',
    // 'realtor1',
    // 'realtor2',
    // 'realtor1_email',
    // 'realtor2_email',
    // 'weather',
    // 'direction',
    // 'summary',
    // 'image'

    ];

    public function property(){
        return $this->hasOne(Property::class);
    }
}