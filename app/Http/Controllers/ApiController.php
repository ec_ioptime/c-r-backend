<?php

namespace App\Http\Controllers;

use Route;
use App\Jobs;
use App\User;
use App\Types;
use Validator;
use App\Breaks;
use App\TimeLogs;
use Carbon\Carbon;
use App\ConfigMapping;
use GuzzleHttp\Client;
use App\JobConfiguration;
use App\Mail\forgetPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\TimeCalculation;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ApiController extends Controller
{


     /**
     * User Login.
     *
     * @return \Illuminate\Http\Response
     */

    public function userlogin(Request $request)
    {
        Log::info("Logggin in.....");
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password'=>'required'
        ]);
        Log::info("LOGIN INFO",$request->except('password'));

        try{

            if ($validator->fails()){

            return response()->json([
                'status_code' => 422,
                'status_message' => 'Validation Error',
                'message' => $validator->errors(),
            ]);
                Log::info("Validation Error");
            }

        $user = User::where('email',$request->email)->first();

        if(auth()->attempt(['email'=>$request['email'], 'password'=>$request['password']])){

            //$user->device_token=$request->device_token;
            //$user->save();
  Log::info("User Logged in Successfully.", $user->toArray());
            return response()->json([
                'status_code' => 200,
                'status_message' => 'User Login Successfully.',
                'data' => $user,
            ]);

        }else{
            return response()->json([
                'status_code' => 201,
                'status_message' => 'Email/Password not matched'
            ]);
             Log::info("Validation Error");
        }
         } catch (Exception $e) {
             return response()->json([
                'status_code'=>500,
                'status_message' => 'Something Went Wrong!'
            ]);
             Log::info("Something Went Wrong");
        }
    }


     /**
     * Jobs.
     *
     * @return \Illuminate\Http\Response
     */

    public function get_jobs(Request $request){

        try{

        $jobs = Jobs::with('types')->get();

        return response()->json([
            'status_code'=>200,
            'status_message'=>'success',
            'data'=>$jobs
        ]);

        } catch (Exception $e) {
             return response()->json([
                'status_code'=>500,
                'status_message' => 'Something Went Wrong!'
            ]);
        }
    }


     /**
     * Job Types
     *
     * @return \Illuminate\Http\Response
     */

    public function get_jobtypes(Request $request){

        $types = Types::where('job_id',$request->job_id)->with('jobs')->get();


        return response()->json([
            'status'=>200,
            'message'=>'success',
            'data'=>$types
        ]);
    }


     /**
     * Time Logs.
     *
     * @return \Illuminate\Http\Response
     */

     public function timelogs(Request $request){

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'job_id' => 'required',
            'job_type_id' => 'required',
        ]);

        try{

            if ($validator->fails()){

            return response()->json([
                'status_code' => 422,
                'status_message' => 'Validation Error',
                'message' => $validator->errors(),
            ]);

            }

            $Getlogs = TimeLogs::where('user_id',$request->user_id)->whereDay('created_at', now()->day)->get();

            if(sizeof($Getlogs) > 0){

                $GetID = TimeLogs::where('user_id',$request->user_id)->whereDay('created_at', now()->day)->value('id');

                $TimeLogs = TimeLogs::findOrFail($GetID);

               if($request->has('job_id')){

                $TimeLogs->job_id = $request->job_id;

               }

               if($request->has('job_type_id')){

                $TimeLogs->job_type_id = $request->job_type_id;

               }

               if($request->has('clock_in')){

                $TimeLogs->clock_in = $request->clock_in;
                $TimeLogs->clock_intime = Carbon::now();

               }

               if($request->has('leave_yard')){

                $TimeLogs->leave_yard = $request->leave_yard;
                $TimeLogs->leave_yardtime = Carbon::now();

               }

               if($request->has('arrive_job')){

                $TimeLogs->arrive_job = $request->arrive_job;
                $TimeLogs->arrive_jobtime = Carbon::now();

               }

               if($request->has('leave_job')){

                $TimeLogs->leave_job = $request->leave_job;
                $TimeLogs->leave_jobtime = Carbon::now();

               }

               if($request->has('arrive_yard')){

                $TimeLogs->arrive_yard = $request->arrive_yard;
                $TimeLogs->arrive_yardtime = Carbon::now();

               }

               if($request->has('clock_out')){

                $TimeLogs->clock_out = $request->clock_out;
                $TimeLogs->clock_outtime = Carbon::now();

               }

               $TimeLogs->save();

               return response()->json([
                'status_code'=>200,
                'status_message'=>'success',
               ]);
            }

            else{

               $TimeLogs = new TimeLogs;

               $TimeLogs->user_id = $request->user_id;

               $TimeLogs->job_id = $request->job_id;

               $TimeLogs->job_type_id = $request->job_type_id;

               if($request->has('clock_in')){

                $TimeLogs->clock_in = $request->clock_in;
                $TimeLogs->clock_intime = Carbon::now();

               }

               if($request->has('leave_yard')){

                $TimeLogs->leave_yard = $request->leave_yard;
                $TimeLogs->leave_yardtime = Carbon::now();

               }

               if($request->has('arrive_job')){

                $TimeLogs->arrive_job = $request->arrive_job;
                $TimeLogs->arrive_jobtime = Carbon::now();

               }

               if($request->has('leave_job')){

                $TimeLogs->leave_job = $request->leave_job;
                $TimeLogs->leave_jobtime = Carbon::now();

               }

               if($request->has('arrive_yard')){

                $TimeLogs->arrive_yard = $request->arrive_yard;
                $TimeLogs->arrive_yardtime = Carbon::now();

               }

               if($request->has('clock_out')){

                $TimeLogs->clock_out = $request->clock_out;
                $TimeLogs->clock_outtime = Carbon::now();

               }

               $TimeLogs->save();

               return response()->json([
                'status_code'=>200,
                'status_message'=>'success',
               ]);

            }




            } catch (Exception $e) {
             return response()->json([
                'status_code'=>500,
                'status_message' => 'Something Went Wrong!'
            ]);
        }

     }


     /**
     * Time Logs.
     *
     * @return \Illuminate\Http\Response
     */

     public function post_timelogs(Request $request){

        $validator = Validator::make($request->all(), [
            'data' => ''
        ]);

        try{

            if ($validator->fails()){

            return response()->json([
                'status_code' => 422,
                'status_message' => 'Validation Error',
                'message' => $validator->errors(),
            ]);

            }

            $requestJson = $request->all();

            if($request->job_id==""){

            return response()->json([
                'status_code' => 422,
                'status_message' => 'job_id field is required'
            ]);

            }

            $TimeLogs = new TimeLogs;

            $TimeLogs->user_id = $request->user_id;

            $TimeLogs->job_id = $request->job_id;

            if($request->job_type_id!=""){

            $TimeLogs->job_type_id = $request->job_type_id;

            }else{

                $TimeLogs->job_type_id = "0";
            }

            if($request->clock_in!=""){

            $clock_intime = $request->clock_in;
            $TimeLogs->clock_intime = date('Y-m-d H:i:s', $clock_intime/1000);



            }

            if($request->leave_yard!=""){

            $leave_yardtime = $request->leave_yard;
            $TimeLogs->leave_yardtime = date('Y-m-d H:i:s', $leave_yardtime/1000);

            }

            if($request->arrive_job!=""){

            $arrive_jobtime = $request->arrive_job;
            $TimeLogs->arrive_jobtime = date('Y-m-d H:i:s', $arrive_jobtime/1000);

            }

            if($request->leave_job!=""){

            $leave_jobtime = $request->leave_job;
            $TimeLogs->leave_jobtime = date('Y-m-d H:i:s', $leave_jobtime/1000);

            }

            if($request->arrive_yard!=""){

            $arrive_yardtime = $request->arrive_yard;
            $TimeLogs->arrive_yardtime = date('Y-m-d H:i:s', $arrive_yardtime/1000);

            }

            if($request->clock_out!=""){

            $clock_outtime =$request->clock_out;
            $TimeLogs->clock_outtime = date('Y-m-d H:i:s', $clock_outtime/1000);

            }

            if($request->invoiceNumber!=""){

            $TimeLogs->invoiceNumber = $request->invoiceNumber;

            }

            if($request->notes!=""){

            $TimeLogs->notes = $request->notes;

            }

            if($request->clock_in_location!=""){

            $TimeLogs->clock_in_location = $request->clock_in_location;

            }

            if($request->leave_yard_location!=""){

            $TimeLogs->leave_yard_location = $request->leave_yard_location;

            }

            if($request->arrive_job_location!=""){

            $TimeLogs->arrive_job_location = $request->arrive_job_location;

            }

            if($request->leave_job_location!=""){

            $TimeLogs->leave_job_location = $request->leave_job_location;

            }

            if($request->arrive_yard_location!=""){

            $TimeLogs->arrive_yard_location = $request->arrive_yard_location;

            }

            if($request->clock_out_location!=""){

            $TimeLogs->clock_out_location = $request->clock_out_location;

            }

            if($request->holiday=="false" || $request->holiday==false){

            $TimeLogs->holiday = false;

            }else{

                $TimeLogs->holiday = true;
            }

            if($request->job_type_id > 0){

            $timeConfig = JobConfiguration::where('job_id',$request->job_id)
            ->where('job_type_id',$request->job_type_id)
            ->get();

            }else{

            $timeConfig = JobConfiguration::where('job_id',$request->job_id)->get();

            }

            if(sizeof($timeConfig) > 0){

                $TimeLogs->save();

            if($request->breaks!==""){

            $breakJson = json_decode($request->breaks, true);

            if(sizeof($breakJson) > 0){

               foreach ($breakJson as $breaks){

                $Breaks = new Breaks;

                $Breaks->time_log_id = $TimeLogs->id;

                $startedAt = $breaks['startedAt'];
                $Breaks->startedAt = date('Y-m-d H:i:s', $startedAt/1000);

                $endedAt = $breaks['endedAt'];
                $Breaks->endedAt = date('Y-m-d H:i:s', $endedAt/1000);

                $starttime = Carbon::parse(date('Y-m-d H:i:s', $startedAt/1000));
                $endtime = Carbon::parse(date('Y-m-d H:i:s', $endedAt/1000));

                $minutes = $starttime->diffInMinutes($endtime);

                $Breaks->total_time = $this->convertMinuteToHours($minutes);

                if($breaks['startedAtLocation']!=""){

                $Breaks->startedAtLocation = json_encode($breaks['startedAtLocation']);

                }

                if($breaks['endedAtLocation']!=""){

                $Breaks->endedAtLocation = json_encode($breaks['endedAtLocation']);

                }

                $Breaks->save();

               }
              }
            }

            (new TimeCalculation)->calculation($TimeLogs);

            }else{

              return response()->json([
                'status_code'=>500,
                'status_message'=>'JobConfiguration Does not exist!',
               ]);

            }
               return response()->json([
                'status_code'=>200,
                'status_message'=>'success',
               ]);

            } catch (Exception $e) {
             return response()->json([
                'status_code'=>500,
                'status_message' => 'Something Went Wrong!'
            ]);
        }
     }

     public function newtest(Request $request){

        $validator = Validator::make($request->all(), [
            'data' => ''
        ]);

        try{

            if ($validator->fails()){

            return response()->json([
                'status_code' => 422,
                'status_message' => 'Validation Error',
                'message' => $validator->errors(),
            ]);

            }

            $requestJson = $request->all();

            if($request->job_id==""){

            return response()->json([
                'status_code' => 422,
                'status_message' => 'job_id field is required'
            ]);

            }


            $TimeLogs = new TimeLogs;

            $TimeLogs->user_id = $request->user_id;

            $TimeLogs->job_id = $request->job_id;

            if($request->job_type_id!=""){

            $TimeLogs->job_type_id = $request->job_type_id;

            }else{

                $TimeLogs->job_type_id = "0";
            }

            if($request->clock_in!=""){

            $clock_intime = $request->clock_in;
            $TimeLogs->clock_intime = date('Y-m-d H:i:s', $clock_intime/1000);

            }

            if($request->leave_yard!=""){

            $leave_yardtime = $request->leave_yard;
            $TimeLogs->leave_yardtime = date('Y-m-d H:i:s', $leave_yardtime/1000);

            }

            if($request->arrive_job!=""){

            $arrive_jobtime = $request->arrive_job;
            $TimeLogs->arrive_jobtime = date('Y-m-d H:i:s', $arrive_jobtime/1000);

            }

            if($request->leave_job!=""){

            $leave_jobtime = $request->leave_job;
            $TimeLogs->leave_jobtime = date('Y-m-d H:i:s', $leave_jobtime/1000);

            }

            if($request->arrive_yard!=""){

            $arrive_yardtime = $request->arrive_yard;
            $TimeLogs->arrive_yardtime = date('Y-m-d H:i:s', $arrive_yardtime/1000);

            }

            if($request->clock_out!=""){

            $clock_outtime =$request->clock_out;
            $TimeLogs->clock_outtime = date('Y-m-d H:i:s', $clock_outtime/1000);

            }


            if($request->holiday=="false" || $request->holiday==false){

            $TimeLogs->holiday = false;

            }else{

                $TimeLogs->holiday = true;
            }

            $TimeLogs->save();

            if($request->breaks!==""){

            $breakJson = json_decode($request->breaks, true);

            if(sizeof($breakJson) > 0){

               foreach ($breakJson as $breaks){

                $Breaks = new Breaks;

                $Breaks->time_log_id = $TimeLogs->id;

                $startedAt = $breaks['startedAt'];
                $Breaks->startedAt = date('Y-m-d H:i:s', $startedAt/1000);

                $endedAt = $breaks['endedAt'];
                $Breaks->endedAt = date('Y-m-d H:i:s', $endedAt/1000);

                $starttime = Carbon::parse(date('Y-m-d H:i:s', $startedAt/1000));
                $endtime = Carbon::parse(date('Y-m-d H:i:s', $endedAt/1000));

                $minutes = $starttime->diffInMinutes($endtime);

                $Breaks->total_time = $this->convertMinuteToHours($minutes);

                $Breaks->save();

               }
             }
            }

              (new TimeCalculation)->calculation($TimeLogs);

               return response()->json([
                'status_code'=>200,
                'status_message'=>'success',
               ]);

            } catch (Exception $e) {
             return response()->json([
                'status_code'=>500,
                'status_message' => 'Something Went Wrong!'
            ]);
        }
     }

    public function convertMinuteToHours($minutes)
    {
        return $minutes / 60;
    }


     /**
     * Time Logs.
     *
     * @return \Illuminate\Http\Response
     */

     public function testlogs(Request $request){

        $validator = Validator::make($request->all(), [
            'data' => ''
        ]);

        try{

            if ($validator->fails()){

            return response()->json([
                'status_code' => 422,
                'status_message' => 'Validation Error',
                'message' => $validator->errors(),
            ]);

            }


            $data = json_encode($request->getContent(), true);

            dd($data);

            $requestJson = json_decode($request->data, true);

            if($requestJson['job_id']==""){

            return response()->json([
                'status_code' => 422,
                'status_message' => 'job_id field is required'
            ]);

            }

            if($requestJson['job_type_id']==""){

            return response()->json([
                'status_code' => 422,
                'status_message' => 'job_type_id field is required'
            ]);

            }

            $TimeLogs = new TimeLogs;

            $TimeLogs->user_id = $requestJson['user_id'];

            $TimeLogs->job_id = $requestJson['job_id'];

            $TimeLogs->job_type_id = $requestJson['job_type_id'];

            $seconds = time();


            $rounded_seconds = round($seconds / (15 * 60)) * (15 * 60);
            $getdates = date('Y-m-d H:i:s', $rounded_seconds);
dd($getdates);

            $clock_intime = round($requestJson['clock_in']/ 300) * 300;

            //dd($clock_intime);

             $getdate = date('Y-m-d H:i:s', $clock_intime/1000);

             dd($getdate);

             $date = Carbon::parse($getdate);

             $now = $date;
             $next_five = round(time() / 300) * 300;

             dd(time());

            $TimeLogs->clock_intime = date('Y-m-d H:i:s', $clock_intime/1000);



            dd("end");

            if($requestJson['clock_in']!=""){

            $clock_intime = $requestJson['clock_in'];
            $TimeLogs->clock_intime = date('Y-m-d H:i:s', $clock_intime/1000);

            }

            if($requestJson['leave_yard']!=""){

            $leave_yardtime = $requestJson['leave_yard'];
            $TimeLogs->leave_yardtime = date('Y-m-d H:i:s', $leave_yardtime/1000);

            }

            if($requestJson['arrive_job']!=""){

            $arrive_jobtime = $requestJson['arrive_job'];
            $TimeLogs->arrive_jobtime = date('Y-m-d H:i:s', $arrive_jobtime/1000);

            }

            if($requestJson['leave_job']!=""){

            $leave_jobtime = $requestJson['leave_job'];
            $TimeLogs->leave_jobtime = date('Y-m-d H:i:s', $leave_jobtime/1000);

            }

            if($requestJson['arrive_yard']!=""){

            $arrive_yardtime = $requestJson['arrive_yard'];
            $TimeLogs->arrive_yardtime = date('Y-m-d H:i:s', $arrive_yardtime/1000);

            }

            if($requestJson['clock_out']!=""){

            $clock_outtime = $requestJson['clock_out'];
            $TimeLogs->clock_outtime = date('Y-m-d H:i:s', $clock_outtime/1000);

            }

            if($requestJson['holiday']!=""){

            $TimeLogs->holiday = $requestJson['holiday'];

            }

            $TimeLogs->save();

            if(sizeof($requestJson['breaks']) > 0){

               foreach ($requestJson['breaks'] as $breaks){

                $Breaks = new Breaks;

                $Breaks->time_log_id = $TimeLogs->id;

                $startedAt = $breaks['startedAt'];
                $Breaks->startedAt = date('Y-m-d H:i:s', $startedAt/1000);

                $endedAt = $breaks['endedAt'];
                $Breaks->endedAt = date('Y-m-d H:i:s', $endedAt/1000);

                $Breaks->save();

               }
            }

               return response()->json([
                'status_code'=>200,
                'status_message'=>'success',
               ]);

            } catch (Exception $e) {
             return response()->json([
                'status_code'=>500,
                'status_message' => 'Something Went Wrong!'
            ]);
        }

     }



     /**
     * Reset Password.
     *
     * @return \Illuminate\Http\Response
     */

    public function reset(Request $request){

        $validator = Validator::make($request->all(), [
            'oldpassword' => '',
            'password' => 'required',
            'id'=>'required'
        ]);

        try{

            if ($validator->fails()){

            return response()->json([
                'status_code' => 422,
                'status_message' => 'Validation Error',
                'message' => $validator->errors(),
            ]);

            }

        $user = User::where('id', $request->id)->first();

        if($request->has('oldpassword')){

        $authPassword = Hash::check($request->oldpassword, $user->password);

        if($authPassword == false ){

            return response()->json([
            'status_code'=>201,
            'status_message'=>'Old Password is not correct!',
            ]);

         }
        }

            User::where('id',$request->id)->update(['password'=> bcrypt($request->password)]);

            return response()->json([
            'status_code'=>200,
            'status_message'=>'Password reset successfully!',
            ]);

        } catch (Exception $e) {
             return response()->json([
                'status_code'=>500,
                'status_message' => 'Something Went Wrong!'
            ]);
        }
    }


     /**
     * Forgot Password.
     *
     * @return \Illuminate\Http\Response
     */

    public function forgot(Request $request){

     $validator = Validator::make($request->all(), [
               'email' => 'required',
           ]);

     try{

            if ($validator->fails()){

            return response()->json([
                'status_code' => 422,
                'status_message' => 'Validation Error',
                'message' => $validator->errors(),
            ]);

            }

            $user = User::where('email','=', $request->email)->first();

            if($user){

                $num = 4;

                $random = rand(pow(10, $num-1), pow(10, $num)-1);

                $code = 'ioptime@'.$random;

                $digits = bcrypt($code);

                if($user->id!=0){

                    $admin = User::where('id',$user->id)->first();
                }

                User::where('id',$user->id)->update(['password'=> $digits]);

                Mail::to($request->email)->send(new forgetPassword($code,$admin));

                 return response()->json([
                 'status_code'=>200,
                 'status_message'=>'System generated password sent to email !',
                 /*'data'=>['code'=>$digits,
                           'id'=> $user->id]*/
                 ]);
            }
            else{

                return response()->json([
                'status_code'=>404,
                'status_message' => 'No Record found ! '
                ]);
            }


        } catch (Exception $e) {
             return response()->json([
                'status_code'=>500,
                'status_message' => 'Something Went Wrong!'
            ]);
        }
    }
}
