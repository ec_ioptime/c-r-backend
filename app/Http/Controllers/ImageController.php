<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function upload(Request $request)
    {

         if(!$request->hasFile('images')) {
            return response()->json(['errors'=>'Images_not_found'], 200);
        }
        $response = [];
        $responses = [];
        if($request->hasfile('images')) {
            $data = [];
            $images = $request -> file('images');
               foreach($images as $image) {
                  $imageName = time() . '.' . $image->extension();
                  $image->move(public_path('/img'), $imageName);
                  $imageName = url('/img/' . $imageName);
                //   dd($imageName);
                 $data[] = $imageName;
            }
            $responses['images'] = $data;
        }

        $responses['id'] = $request->id;
        foreach ($responses['images'] as $key => $value) {
            $arrr=[];
            $response[$key]= ['id:'=>$responses['id'][$key],'url:'=>$value];
        }

        return $response;
    }
}