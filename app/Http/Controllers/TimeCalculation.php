<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Exception;
use Carbon\Carbon;

use App\User;
use App\Breaks;
use App\TimeLogs;
use App\ConfigMapping;
use App\JobConfiguration;

use App\Http\Controllers\Calculations\Commercial;
use App\Http\Controllers\Calculations\LP2Commercial;
use App\Http\Controllers\Calculations\LP4Commercial;
use App\Http\Controllers\Calculations\LP2Industrial;
use App\Http\Controllers\Calculations\LP4Industrial;
use App\Http\Controllers\Calculations\Industrial;

class TimeCalculation extends Controller
{

    public function convertMinuteToHours($minutes)
    {
        return $minutes / 60;
    }

    public function calculation($timelogs){

        $data = TimeLogs::findorfail($timelogs->id);

        if($data->job_type_id > 0){

        $timeConfig = JobConfiguration::where('job_id',$data->job_id)
        ->where('job_type_id',$data->job_type_id)
        ->value('id');

        }else{

        $timeConfig = JobConfiguration::where('job_id',$data->job_id)->value('id');

        }

        $config  = JobConfiguration::findorfail($timeConfig);

        $mapping = ConfigMapping::where('config_id',$config->id)->value('mapped_to');

        $breaks  = Breaks::where('time_log_id',$data->id)->get();
           
        //Industrial
        if($data->job_type_id == "14" || $data->job_type_id == "15"){

        (new Industrial)->calculations($data,$config,$mapping);
        
        }
        
        //LP2Commercial
        if($mapping == "hourly_lp2_1/2" || $data->job_type_id == "16" || $data->job_type_id == "20"){

        (new LP2Commercial)->calculations($data,$config,$mapping);

        }
        
        //LP4Commercial  
        if($mapping == "hourly_lp4" || $data->job_type_id == "19" || $data->job_type_id == "24"){

        (new LP4Commercial)->calculations($data,$config,$mapping);

        }
        
        //LP2Industrial
        if($data->job_type_id == "22" || $data->job_type_id == "28"){

        (new LP2Industrial)->calculations($data,$config,$mapping);

        }
        
        //LP4Industrial
        if($data->job_type_id == "18" || $data->job_type_id == "23"){

        (new LP4Industrial)->calculations($data,$config,$mapping);

        }

        if($mapping == "mechanic_callout"){

        $hours = $this->calculateAllhours($data,$config);

        if(sizeof($breaks) > 0){

            $break_time = Breaks::where('time_log_id',$data->id)->sum('total_time');

            $total = $this->convertMinuteToHours($hours) - $break_time;

        }else{

            $total = $this->convertMinuteToHours($hours);
        }

        TimeLogs::where('id',$data->id)->update(['mechanic_callout'=> $total]);

        }

        if($mapping == "double_time_hourly"){

        $hours = $this->calculateAllhours($data,$config);

        if(sizeof($breaks) > 0){

            $break_time = Breaks::where('time_log_id',$data->id)->sum('total_time');

            $total = $this->convertMinuteToHours($hours) - $break_time;

        }else{

            $total = $this->convertMinuteToHours($hours);
        }

        TimeLogs::where('id',$data->id)->update(['double_time_hourly'=> $total]);

        }

        if($mapping == "hourly"){
          
          //Commercial
          if($data->job_type_id == "13" ||$data->job_type_id == "25"){

            (new Commercial)->calculations($data,$config,$mapping);

          }else{

           $mins = 15;
           $firstmins = $this->convertMinuteToHours($mins);

           $hours = $this->calculateAllhours($data,$config);

        if(sizeof($breaks) > 0){

            $break_time = Breaks::where('time_log_id',$data->id)->sum('total_time');

            $total = $this->convertMinuteToHours($hours) - $break_time;

        }else{

            $total = $this->convertMinuteToHours($hours);
        }

       if($config->overtime == 1){

        TimeLogs::where('id',$data->id)->update(['hourly'=> $firstmins,'overtime_hourly'=> $total]);

       }else{

        TimeLogs::where('id',$data->id)->update(['hourly'=> $total]);

       }

       }

       }

        if($mapping == "fixed_salary"){

        $hours = $this->calculateAllhours($data,$config);

        if(sizeof($breaks) > 0){

            $break_time = Breaks::where('time_log_id',$data->id)->sum('total_time');

            $total = $this->convertMinuteToHours($hours) - $break_time;

        }else{

            $total = $this->convertMinuteToHours($hours);
        }

        TimeLogs::where('id',$data->id)->update(['fixed_salary'=> $total]);

        }
    }

    public function calculateAllhours($data,$config)
    {
        if($config->overtime == 1){

        $overtime_start = $config->start_overtime;
        $overtime_end = $config->end_overtime;

        $timestart = Carbon::parse($data->clock_intime)->format('H:i');
        $timeend = Carbon::parse($data->clock_outtime)->format('H:i');

        $clock_intime = Carbon::parse($data->clock_intime);
        $clock_outtime = Carbon::parse($data->clock_outtime);   

        if ($timestart >= $overtime_start)
        {

        $firstminutes = 15;    
        $result = $clock_intime->diffInMinutes($clock_outtime) - $firstminutes;

        $total = $result;

        }else{
            $total = 0;
        }

        }else{

        $clock_intime = Carbon::parse($data->clock_intime);
        $clock_outtime = Carbon::parse($data->clock_outtime);    

        $total = $clock_intime->diffInMinutes($clock_outtime);

        }

        return $total; 
    }
}
