<?php

namespace App\Http\Controllers\Calculations;

use App\Http\Controllers\Controller;
use App\TimeLogs;
use Aws\Middleware;
use Carbon\Carbon;

class Commercial extends Controller
{

    public function convertMinuteToHours($minutes)
    {
        return $minutes / 60;
    }

    public function calculations($data, $config, $mapping)
    {

        $holiday = $data->holiday;
        $day = Carbon::parse($data->clock_intime)->format('l');
        $day2 = Carbon::parse($data->clock_outtime)->format('l');

        $overtime_start = $config->start_overtime;
        $overtime_end = $config->end_overtime;
$day3 = Carbon::parse($data->leave_yardtime)->format('l');
        $get_clock_intime = Carbon::parse($data->clock_intime)->format('H:i:s');
        $get_leave_yardtime = Carbon::parse($data->leave_yardtime)->format('H:i:s');
        $get_arrive_jobtime = Carbon::parse($data->arrive_jobtime)->format('H:i:s');
        $get_leave_jobtime = Carbon::parse($data->leave_jobtime)->format('H:i:s');
        $get_arrive_yardtime = Carbon::parse($data->arrive_yardtime)->format('H:i:s');
        $get_clock_outtime = Carbon::parse($data->clock_outtime)->format('H:i:s');

        $ot_end = explode(':', $overtime_end);
        $ot_start = explode(':', $overtime_start);
        $clockintime = explode(':', $get_clock_intime);
        $leaveyardtime = explode(':', $get_leave_yardtime);
        $arrivejobtime = explode(':', $get_arrive_jobtime);
        $leavejobtime = explode(':', $get_leave_jobtime);
        $arriveyardtime = explode(':', $get_arrive_yardtime);
        $clockouttime = explode(':', $get_clock_outtime);
          $arriveYardDate = Carbon::parse($data->arrive_yardtime)->format('Y-m-d');
        $clockIndate = Carbon::parse($data->clock_intime)->format('Y-m-d');
        $clockIN = Carbon::parse($data->clock_intime);
        $clockOUT = Carbon::parse($data->clock_outtime);

        $totaltime = $clockOUT->diffInMinutes($clockIN);

        $overtime = $this->convertMinuteToHours(abs($totaltime));

        //If clockout and clockin days are not same

        $Midnight = explode(':', "00:00:00");

        $minutes33 = ($Midnight[0] * 60.0 + $Midnight[1]);
        $minutes32 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $DBTime = $minutes32 - $minutes33;

        $OTime = $this->convertMinuteToHours(abs($DBTime));

        $MidRegularOvertime = $overtime - $OTime;


        $Midnight2 = explode(':', "24:00:00");

        $minutes34 = ($clockintime[0] * 60.0 + $clockintime[1]);
        $minutes35 = ($Midnight2[0] * 60.0 + $Midnight2[1]);

        $STime = abs($minutes34 - $minutes35);
        $SundayMidTime = $this->convertMinuteToHours(abs($STime));


        if ($get_clock_outtime > $overtime_end) {
            $minutes36 = ($Midnight[0] * 60.0 + $Midnight[1]);
            $minutes37 = ($ot_end[0] * 60.0 + $ot_end[1]);

            $MTime = $minutes36 - $minutes37;

        } else {

            $minutes36 = ($Midnight[0] * 60.0 + $Midnight[1]);
            $minutes37 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

            $MTime = $minutes36 - $minutes37;

        }
        if($get_leave_jobtime < $overtime_end && $leavejobtime > $Midnight && $get_clock_outtime > $overtime_end && $get_clock_intime >= $overtime_end && $clockIndate != $arriveYardDate)
        {
            $minutes38 = ($clockouttime[0] * 60.0 + $clockouttime[1]);
            $minutes39 = ($ot_start[0] * 60.0 + $ot_start[1]);
            $MTime2 = $minutes38 - $minutes39;
            $MTime =abs( $MTime) + $MTime2;
        }
        $MidTime = $this->convertMinuteToHours(abs($MTime));

        if ($get_clock_outtime > $overtime_end) {

            $minutes38 = ($ot_end[0] * 60.0 + $ot_end[1]);
            $minutes39 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

            $RegTime =$minutes39 - $minutes38;
            /* over time done on monday after 3pm  */
            if ($day == 'Sunday' || $day == 'Monday') {
                $overTimeStart = $ot_start[0] * 60.0 + $ot_start[1];
                 $clockOutTime = ($clockouttime[0] * 60.0 + $clockouttime[1]);
                $overTimeOnMonday =$clockOutTime - $overTimeStart;
                $RegTime = $RegTime - $overTimeOnMonday;
            }

        } else {

            $RegTime = 0;

        }

        $AllRegularTime = $this->convertMinuteToHours(abs($RegTime));
        //If ALL TIME logged between ClockInTime and ClockOutTime is Between 3:00 pm and 7:00 am then
        //AllOvertime = True && //Goto CommercialAllOvertime

        if ($get_clock_intime > $overtime_start && $get_clock_outtime < $overtime_end) {


            $clockin = Carbon::parse($data->clock_intime);
            $clockout = Carbon::parse($data->clock_outtime);

            $OnJob = $clockin->diffInMinutes($clockout);

            //TimeOnJob = (ClockOutTime – ClockInTime)

            $TimeOnJob = $this->convertMinuteToHours($OnJob);

            if ($day == "Saturday" && $day2 == "Saturday") {

                TimeLogs::where('id', $data->id)->update(['overtime_hourly' => $overtime]);

            } elseif ($day == "Saturday" && $day2 == "Sunday") {

                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly' => $MidRegularOvertime, 'double_time_hourly' => $OTime]);

            } elseif ($holiday == "1") {

                TimeLogs::where('id', $data->id)->update(['double_time_hourly' => $overtime]);

            } elseif ($day == "Sunday" && $day2 == "Sunday") {

                TimeLogs::where('id', $data->id)->update(['double_time_hourly' => $overtime]);

            } elseif ($day == "Sunday" && $day2 == "Monday") {

                TimeLogs::where('id', $data->id)
                    ->update(['double_time_hourly' => $SundayMidTime, 'overtime_hourly' => $MidTime, 'hourly' => $AllRegularTime]);

                } else if(($day != "Friday" && $day2 != "Saturday") && $day != $day2 &&$day2 !=$day3 && $get_clock_outtime <= $overtime_end && $get_clock_intime >= $overtime_start )

            {
                $minutes39 = ($ot_start[0] * 60.0 + $ot_start[1]);
             $minutes37 = ($ot_end[0] * 60.0 + $ot_end[1]);
                $MTime = $minutes39 - $minutes37;
                $MTime = $this->convertMinuteToHours(abs($MTime));
                $TimeOnJob = $TimeOnJob-$MTime;
                 TimeLogs::where('id', $data->id)->update(['overtime_hourly' => $TimeOnJob,'hourly' => $MTime]);
            }else  {

                /* 15 MINUTES added in houlry if clock intime is in ovetime */
                  $HourlyTime = 15;
                 $Hourly = $this->convertMinuteToHours($HourlyTime);
                 $TimeOnJob = $TimeOnJob - $Hourly;
                TimeLogs::where('id', $data->id)->update(['overtime_hourly' => $TimeOnJob,'hourly' => $Hourly]);

            }

        } elseif ($get_clock_intime < $overtime_end && $get_clock_outtime < $overtime_end && $day == $day2) {
            $clockin = Carbon::parse($data->clock_intime);
            $clockout = Carbon::parse($data->clock_outtime);

            $OnJob = $clockin->diffInMinutes($clockout);
            //TimeOnJob = (ClockOutTime – ClockInTime)

            $TimeOnJob = $this->convertMinuteToHours($OnJob);

            if ($day == "Saturday" && $day2 == "Saturday") {

                TimeLogs::where('id', $data->id)->update(['overtime_hourly' => $overtime]);

            } elseif ($day == "Saturday" && $day2 == "Sunday") {

                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly' => $MidRegularOvertime, 'double_time_hourly' => $OTime]);

            } elseif ($holiday == "1") {

                TimeLogs::where('id', $data->id)->update(['double_time_hourly' => $overtime]);

            } elseif ($day == "Sunday" && $day2 == "Sunday") {

                TimeLogs::where('id', $data->id)->update(['double_time_hourly' => $overtime]);

            } elseif ($day == "Sunday" && $day2 == "Monday") {

                TimeLogs::where('id', $data->id)
                    ->update(['double_time_hourly' => $SundayMidTime, 'overtime_hourly' => $MidTime, 'hourly' => $AllRegularTime]);

            } else {

                TimeLogs::where('id', $data->id)->update(['overtime_hourly' => $TimeOnJob]);

            }

        } elseif ($get_clock_intime > $overtime_end && $get_clock_outtime < $overtime_start && $get_arrive_jobtime < $overtime_start && $get_leave_jobtime < $overtime_start) {

            $clockin = Carbon::parse($data->clock_intime);
            $clockout = Carbon::parse($data->clock_outtime);

            $OnJob = $clockin->diffInMinutes($clockout);

            $HourlyTime = $this->convertMinuteToHours($OnJob);

            if ($day == "Saturday" && $day2 == "Saturday") {

                TimeLogs::where('id', $data->id)->update(['overtime_hourly' => $overtime]);

            } elseif ($day == "Saturday" && $day2 == "Sunday") {

                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly' => $MidRegularOvertime, 'double_time_hourly' => $OTime]);

            } elseif ($holiday == "1") {

                TimeLogs::where('id', $data->id)->update(['double_time_hourly' => $overtime]);

            } elseif ($day == "Sunday" && $day2 == "Sunday") {

                TimeLogs::where('id', $data->id)->update(['double_time_hourly' => $overtime]);

            } elseif ($day == "Sunday" && $day2 == "Monday") {

                TimeLogs::where('id', $data->id)
                    ->update(['double_time_hourly' => $SundayMidTime, 'overtime_hourly' => $MidTime, 'hourly' => $AllRegularTime]);

            } else {
                TimeLogs::where('id', $data->id)->update(['hourly' => $HourlyTime]);

            }

        } else {

            $minutes1 = ($clockintime[0] * 60.0 + $clockintime[1]);
            $minutes2 = ($ot_end[0] * 60.0 + $ot_end[1]);

            $minutes3 = ($ot_start[0] * 60.0 + $ot_start[1]);
            $minutes4 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

            //OverTimeOnJob = time actually spent on the job that is worked between 3:00 pm and 7:00 am.

            $OnJob1 = $minutes4 - $minutes3;

            $OnJob2 = $minutes2 - $minutes1;

            if ($get_clock_intime < $overtime_start && $get_arrive_jobtime > $overtime_start && $get_leave_jobtime > $overtime_start && $get_clock_outtime < $overtime_end) {

                $minutes3 = ($ot_start[0] * 60.0 + $ot_start[1]);
                $minutes4 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                $OTimeOnJob = $minutes3 - $minutes4;
            } elseif ($get_clock_outtime >= $overtime_start && $get_clock_intime > $overtime_start) {

                $clockin = Carbon::parse($data->clock_intime);
                $clockout = Carbon::parse($data->clock_outtime);

                $OTimeOnJob = $clockin->diffInMinutes($clockout);


            } elseif ($get_clock_outtime >= $overtime_start && $get_clock_intime > $overtime_end) {

                $OTimeOnJob = $OnJob1;
                       if ($day != $day2) {

                         $clockINTime = $clockintime[0] * 60.0 + $clockintime[1];
                       $minutes35 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                       $result =abs( $clockINTime - $minutes35);

                       $minutes36 = ($Midnight[0] * 60.0 + $Midnight[1]);
                        $overTimeStart = $ot_end[0] * 60.0 + $ot_end[1];
                        $result2 = abs($minutes36 - $overTimeStart);

                        $OTimeOnJob = abs($result2 + $result) +$OTimeOnJob;

                       }
            } elseif ($get_clock_intime <= $overtime_end && $get_clock_outtime <= $overtime_start) {

                if ($day != $day2) {


                    if ($get_leave_jobtime > $overtime_start) {

                        $minutes4 = ($ot_start[0] * 60.0 + $ot_start[1]);
                        $minutes5 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                        $result = $minutes5 - $minutes4;

                        $leavetime = Carbon::parse($data->leave_jobtime);
                        $clockout = Carbon::parse($data->clock_outtime);

                        $result2 = $leavetime->diffInMinutes($clockout);

                        $total = $result + $result2;

                    } elseif ($get_clock_intime <= $overtime_end && $get_leave_jobtime < $overtime_start && $get_clock_outtime > $overtime_end) {

                        $Midnight = explode(':', "24:00:00");
                        $Midnight2 = explode(':', "00:00:00");

                        $minutes21 = ($ot_start[0] * 60.0 + $ot_start[1]);
                        $minutes22 = ($Midnight[0] * 60.0 + $Midnight[1]);

                        $result = $minutes22 - $minutes21;

                        $minutes23 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                        $minutes24 = ($ot_end[0] * 60.0 + $ot_end[1]);

                        $result2 = $minutes24 - $minutes23;

                        $minutes25 = ($clockintime[0] * 60.0 + $clockintime[1]);
                        $minutes26 = ($ot_end[0] * 60.0 + $ot_end[1]);

                        $result3 = $minutes26 - $minutes25;

                        $total = $result + $result2 + $result3;

                    } else {
                              $OTimeOnJob = $OnJob1;
                      if ($day != $day2) {

                         $clockINTime = $clockintime[0] * 60.0 + $clockintime[1];
                       $minutes35 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                       $result =abs( $clockINTime - $minutes35);

                       $minutes36 = ($Midnight[0] * 60.0 + $Midnight[1]);
                        $overTimeStart = $ot_end[0] * 60.0 + $ot_end[1];
                        $result2 = abs($minutes36 - $overTimeStart);

                        $total = abs($result2 + $result) +$OTimeOnJob;

                       }else {
    $leavetime = Carbon::parse($data->leave_jobtime);
                        $clockout = Carbon::parse($data->clock_outtime);

                        $total = $leavetime->diffInMinutes($clockout);
                       }

                    }

                    $OTimeOnJob = $total;


                } elseif ($get_clock_intime < $overtime_end && $get_leave_jobtime < $overtime_start && $get_clock_outtime <= $overtime_start) {

                      $OTimeOnJob = $OnJob2;


                } else {

                    $OTimeOnJob = 0;

                }


            } elseif ($get_leave_jobtime > $overtime_start && $get_clock_intime > $overtime_end && $get_clock_outtime > $overtime_end && $day != $day2) {

                if ($get_leave_jobtime > $overtime_start) {

                    $minutes4 = ($ot_start[0] * 60.0 + $clockintime[1]);
                    $minutes5 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                    $result = $minutes5 - $minutes4;

                    $leavetime = Carbon::parse($data->leave_jobtime);
                    $clockout = Carbon::parse($data->clock_outtime);

                    $result2 = $leavetime->diffInMinutes($clockout);

                    $total = $result + $result2;

                } else {

                    $leavetime = Carbon::parse($data->leave_jobtime);
                    $clockout = Carbon::parse($data->clock_outtime);

                    $total = $leavetime->diffInMinutes($clockout);

                }

                $OTimeOnJob = $total;
            } else {

                /* friday =  */
                if( $day == "Friday" && $day2 == "Saturday")
                {
                    $minutes1 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes2 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                    $minutes6 = ($ot_start[0] * 60.0 + $ot_start[1]);

                    $notOverTime = $minutes6 - $minutes1;

                    $minutes3 = ($Midnight[0] * 60.0 + $Midnight[1]);
                    $minutes4 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    /* time worked on friday after 3pm */
                    $totalTimeWorkedFriday = $minutes2 - $minutes1;
                    $totaOvertimeOnFriday =$totalTimeWorkedFriday - $notOverTime;

                    // time worked on saturday before clockout
                    $totaTimeonSaturday = $minutes4 + $minutes3;
                    $OTimeOnJob = $totaTimeonSaturday + $totaOvertimeOnFriday;
                //  dd($RegularTime);
                } else
                {
                    $OTimeOnJob = $OnJob2 + $OnJob1;
                }
                    /* time worked between 7am to 4m */


            }

            if ($get_clock_intime < $overtime_end && $get_clock_outtime > $overtime_end && $get_leave_jobtime < $overtime_start) {
                $TotaOverTimeOnJob = $OTimeOnJob - 15;
                // $TotaOverTimeOnJob = $OTimeOnJob ;
            } elseif ($get_clock_intime < $overtime_end && $get_clock_outtime < $overtime_start && $get_leave_jobtime < $overtime_start) {
                    if($day != $day2 && $get_clock_intime < $overtime)
                    {
                        $TotaOverTimeOnJob = $OTimeOnJob - 15;
                    }else
                    {

                        $TotaOverTimeOnJob = $OTimeOnJob;
                    }

            } elseif ($get_clock_intime < $overtime_end) {

                $TotaOverTimeOnJob = $OTimeOnJob - 15;

            } else {
             if($day != $day2 && $get_clock_intime >= $overtime_start)
             {

                 $TotaOverTimeOnJob = $OTimeOnJob - 15;
             }else {

                 $TotaOverTimeOnJob = $OTimeOnJob;
             }

            }
            $OverTimeOnJob = $this->convertMinuteToHours($TotaOverTimeOnJob);

            if ($get_clock_intime < $overtime_end && $get_leave_jobtime < $overtime_start && $get_clock_outtime <= $overtime_start) {

                $clockin = Carbon::parse($data->clock_intime);
                $clockout = Carbon::parse($data->clock_outtime);

                $total = $clockin->diffInMinutes($clockout);
                $RTime = $total - $OTimeOnJob;

            } elseif ($get_clock_intime < $overtime_end && $get_arrive_jobtime < $overtime_start && $get_leave_jobtime < $overtime_start && $get_clock_outtime > $overtime_end) {

                     $clockin = Carbon::parse($data->clock_intime);
                $clockout = Carbon::parse($data->clock_outtime);

                $OnJob = $clockin->diffInMinutes($clockout);
                $RTime = $OnJob - $OTimeOnJob;

                // $minutes27 = ($ot_end[0] * 60.0 + $ot_end[1]);
                // $minutes28 = ($ot_start[0] * 60.0 + $ot_start[1]);

                // $result = $minutes28 - $minutes27;

                // $minutes29 = ($ot_end[0] * 60.0 + $ot_end[1]);
                // $minutes30 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                // $result2 = $minutes30 - $minutes29;

                // $RTime = $result2 + $result;

            } else {
                /* here */
                $clockin = Carbon::parse($data->clock_intime);
                $clockout = Carbon::parse($data->clock_outtime);

                $OnJob = $clockin->diffInMinutes($clockout);


                $RTime = $OnJob - $OTimeOnJob;

            }
            //RegularTime = (ClockOutTime – ClockInTime) - OverTimeOnJob
            if ($get_clock_intime < $overtime_end && $get_clock_outtime > $overtime_end && $get_leave_jobtime < $overtime_start) {

                $TotalRTime = $RTime + 15;
                // $TotalRTime = $RTime;
                // dd($TotalRTime);

            } elseif ($get_clock_intime < $overtime_end && $get_clock_outtime < $overtime_start && $get_leave_jobtime < $overtime_start) {

                if($day != $day2 && $get_clock_intime < $overtime_end)
                {
                    $TotalRTime = $RTime + 15;
                }else
                {
                    $TotalRTime = $RTime;
                }

            } elseif ($get_clock_intime < $overtime_end) {

                $TotalRTime = $RTime + 15;
            } else {

                if($day != $day2 && $get_clock_intime >= $overtime_start)
                    {
                    $TotalRTime = $RTime + 15;
                    }else
                    {
                        $TotalRTime = $RTime;
                    }



            }

            $RegularTime = $this->convertMinuteToHours($TotalRTime);

            if ($day == "Saturday" && $day2 == "Saturday") {

                TimeLogs::where('id', $data->id)->update(['overtime_hourly' => $overtime]);

            } elseif ($day == "Saturday" && $day2 == "Sunday") {

                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly' => $SundayMidTime, 'double_time_hourly' => $OTime]);

            } elseif ($holiday == "1") {

                TimeLogs::where('id', $data->id)->update(['double_time_hourly' => $overtime]);

            } elseif ($day == "Sunday" && $day2 == "Sunday") {

                TimeLogs::where('id', $data->id)->update(['double_time_hourly' => $overtime]);

            } elseif ($day == "Sunday" && $day2 == "Monday") {
                  TimeLogs::where('id', $data->id)
                    ->update(['double_time_hourly' => $SundayMidTime, 'overtime_hourly' => $MidTime, 'hourly' => $AllRegularTime]);
                //    $overTimeStart = $ot_start[0] * 60.0 + $ot_start[1];
                //  $clockOutTime = ($clockouttime[0] * 60.0 + $clockouttime[1]);
                //   $overTimeOnMonday =$clockOutTime - $overTimeStart;
                // $       = $this->convertMinuteToHours(abs($overTimeOnMonday));
                // $MidTime = $MidTime + $overTime;
                // TimeLogs::where('id', $data->id)
                //     ->update(['double_time_hourly' => $SundayMidTime, 'overtime_hourly' => $MidTime, 'hourly' => $AllRegularTime]);

            } elseif ($get_clock_intime == $get_clock_outtime && $day == $day2) {
                TimeLogs::where('id', $data->id)->update(['hourly' => '0.00', 'overtime_hourly' => '0.00']);

            } else {

                TimeLogs::where('id', $data->id)
                    ->update(['hourly' => $RegularTime, 'overtime_hourly' => $OverTimeOnJob]);

            }
        }
    }
}
