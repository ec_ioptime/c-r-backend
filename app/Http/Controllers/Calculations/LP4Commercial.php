<?php

namespace App\Http\Controllers\Calculations;

use App\Http\Controllers\Calculations\MidSundayCalculations;
use App\Http\Controllers\Controller;
use App\TimeLogs;
use Carbon\Carbon;

class LP4Commercial extends Controller
{

    public function convertMinuteToHours($minutes)
    {
        return $minutes / 60;
    }

    public function calculations($data, $config, $mapping)
    {
        $holiday = $data->holiday;
        $day = Carbon::parse($data->clock_intime)->format('l');
        $day2 = Carbon::parse($data->clock_outtime)->format('l');
        $leavejobtimeday = Carbon::parse($data->arrive_yardtime)->format('l');
        $day3 = Carbon::parse($data->leave_yardtime)->format('l');

        $overtime_start = $config->start_overtime;
        $overtime_end = $config->end_overtime;
        $arriveYardDate = Carbon::parse($data->arrive_yardtime)->format('Y-m-d');
        $clockIndate = Carbon::parse($data->clock_intime)->format('Y-m-d');
        $get_clock_intime = Carbon::parse($data->clock_intime)->format('H:i:s');
        $get_leave_yardtime = Carbon::parse($data->leave_yardtime)->format('H:i:s');
        $get_arrive_jobtime = Carbon::parse($data->arrive_jobtime)->format('H:i:s');
        $get_leave_jobtime = Carbon::parse($data->leave_jobtime)->format('H:i:s');
        $get_arrive_yardtime = Carbon::parse($data->arrive_yardtime)->format('H:i:s');
        $get_clock_outtime = Carbon::parse($data->clock_outtime)->format('H:i:s');

        $ot_end = explode(':', $overtime_end);
        $ot_start = explode(':', $overtime_start);
        $clockintime = explode(':', $get_clock_intime);
        $leaveyardtime = explode(':', $get_leave_yardtime);
        $arrivejobtime = explode(':', $get_arrive_jobtime);
        $leavejobtime = explode(':', $get_leave_jobtime);
        $arriveyardtime = explode(':', $get_arrive_yardtime);
        $clockouttime = explode(':', $get_clock_outtime);

        $Midnight = explode(':', "00:00:00");
        $Midnight2 = explode(':', "24:00:00");

        if ($get_clock_intime >= $overtime_start) {

            $arrivejobtime_s = Carbon::parse($data->arrive_jobtime);
            $leavejobtime_s = Carbon::parse($data->leave_jobtime);
            $OverTimeOnJob = $leavejobtime_s->diffInMinutes($arrivejobtime_s);

            $TotalOverTimeOnJob = $OverTimeOnJob;

            $OverTimeHourlyLP = $this->convertMinuteToHours($TotalOverTimeOnJob);

            $clockin = Carbon::parse($data->clock_intime);

            $Sum1 = $clockin->diffInMinutes($arrivejobtime_s);

            $clockout = Carbon::parse($data->clock_outtime);

            $Sum2 = $clockout->diffInMinutes($leavejobtime_s);

            $TotalHourly = $Sum1 + $Sum2 - 15;

            $RegularOvertime = $this->convertMinuteToHours($TotalHourly);
            $HourlyTime = 15;
            /* here */
            $Hourly = $this->convertMinuteToHours($HourlyTime);
        } elseif ($get_clock_intime < $overtime_end && $get_clock_outtime < $overtime_end && $get_arrive_jobtime < $overtime_end && $get_leave_jobtime < $overtime_end) {
            $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
            $minutes2 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

            $OverTimeOnJob = $minutes2 - $minutes1;

            $TotalOverTimeOnJob = $OverTimeOnJob;

            $OverTimeHourlyLP = $this->convertMinuteToHours($TotalOverTimeOnJob);
            $minutes3 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
            $minutes4 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

            $TimeOnJob = $minutes4 - $minutes3;

            $TimeHourlyLP = $this->convertMinuteToHours($TimeOnJob) - $OverTimeHourlyLP;

            if ($get_clock_intime < $overtime_end) {

                $HourlyTime3 = 15;
            }

            $Hourly = $this->convertMinuteToHours($HourlyTime3);

            $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
            $minutes6 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

            $leavetime = Carbon::parse($data->leave_jobtime);
            $clockout = Carbon::parse($data->clock_outtime);

            $result1 = $minutes6 - $minutes5;

            $result2 = $leavetime->diffInMinutes($clockout);

            if ($get_clock_intime < $overtime_end) {

                $TotalHourly = $result1 + $result2 - 15;
            }

            $RegularOvertime = $this->convertMinuteToHours($TotalHourly);
        } else {

            //True
            if ($get_arrive_jobtime <= $overtime_end && $get_leave_jobtime <= $overtime_start) {

                $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes2 = ($ot_end[0] * 60.0 + $ot_end[1]);

                $OverTimeOnJob = $minutes2 - $minutes1;

                //True
            } elseif ($get_arrive_jobtime <= $overtime_end && $get_leave_jobtime >= $overtime_start) {

                $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes2 = ($ot_end[0] * 60.0 + $ot_end[1]);

                $minutes21 = ($ot_start[0] * 60.0 + $ot_start[1]);
                $minutes22 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $Time1 = $minutes2 - $minutes1;
                $Time2 = $minutes22 - $minutes21;

                $OverTimeOnJob = $Time1 + $Time2;

                //True
            } elseif (
                $get_clock_intime <= $overtime_end && $get_arrive_jobtime > $overtime_end &&
                $leavejobtime < $Midnight
            ) {
                $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes2 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $Time1 = $minutes2 - $minutes1;

                $OverTimeOnJob = $Time1;
            } elseif (
                $get_clock_intime <= $overtime_end && $get_arrive_jobtime > $overtime_end &&
                $leavejobtime > $Midnight
            ) {
                $OverTimeOnJob = 0;
                /* if arrive job time is after 3PM */
                if (date("g:i a", strtotime($get_arrive_jobtime)) >= date("g:i a", strtotime($overtime_start))) { /* if leave job time is before 7AM */
                    if (date("g:i a", strtotime($get_leave_jobtime)) <= date("g:i a", strtotime($overtime_end))) {
                        // over time = leave job time - arrive job time
                        $OverTimeOnJob = $leavejobtime[0] * 60.0 + $leavejobtime[1] - $arrivejobtime[0] * 60.0 - $arrivejobtime[1];
                    } else {
                        //over time = 7AM - arrive job time
                        $OverTimeOnJob2 = date("g", strtotime($get_arrive_jobtime)) - date("g", strtotime($overtime_end));
                        $OverTimeOnJob = (str_replace('-', '', $OverTimeOnJob2)+"12") * 60;
                    }
                }
                if ($get_arrive_jobtime >= $overtime_end && $get_leave_jobtime <= $overtime_start) {
                    $OverTimeOnJob = 0;
                }

                // $minutes1 = ($ot_start[0] * 60.0 + $ot_start[1]);
                // $minutes2 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                // $minutes21 = ($Midnight[0] * 60.0 + $Midnight[1]);
                // $minutes22 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                // $Time1 = $minutes2 - $minutes1;
                // $Time2 = $minutes22 - $minutes21;
                // $OverTimeOnJob = $Time1 + $Time2;
                //     $OverTimeOnJob =0;
            } elseif (
                $get_clock_intime > $overtime_end && $get_arrive_jobtime > $overtime_end &&
                $leavejobtime > $Midnight && $get_leave_jobtime < $overtime_end && $get_clock_outtime > $overtime_end
            ) {

                $minutes1 = ($ot_start[0] * 60.0 + $ot_start[1]);
                $minutes2 = ($Midnight2[0] * 60.0 + $Midnight2[1]);

                $minutes21 = ($Midnight[0] * 60.0 + $Midnight[1]);
                $minutes22 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $Time1 = $minutes2 - $minutes1;
                $Time2 = $minutes22 - $minutes21;

                $OverTimeOnJob = $Time1 + $Time2;
            } elseif (
                $get_clock_intime > $overtime_end && $get_arrive_jobtime > $overtime_end &&
                $leavejobtime > $Midnight
            ) {

                $minutes1 = ($ot_start[0] * 60.0 + $ot_start[1]);
                $minutes2 = ($Midnight2[0] * 60.0 + $Midnight2[1]);

                $minutes21 = ($Midnight[0] * 60.0 + $Midnight[1]);
                $minutes22 = ($ot_end[0] * 60.0 + $ot_end[1]);

                $Time1 = $minutes2 - $minutes1;
                $Time2 = $minutes22 - $minutes21;

                $OverTimeOnJob = $Time1 + $Time2;
            } elseif ($get_arrive_jobtime >= $overtime_end && $get_leave_jobtime <= $overtime_start) {

                $OverTimeOnJob = 0;

                //True
            } elseif ($get_arrive_jobtime <= $overtime_start && $get_leave_jobtime >= $overtime_start) {

                $minutes1 = ($ot_start[0] * 60.0 + $ot_start[1]);
                $minutes2 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $OverTimeOnJob = $minutes2 - $minutes1;

                //True
            } elseif ($get_arrive_jobtime >= $overtime_start && $get_leave_jobtime >= $overtime_start) {

                $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes2 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $OverTimeOnJob = $minutes2 - $minutes1;

                //True
            } else {

                $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes2 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $OverTimeOnJob = $minutes2 - $minutes1;
            }

            //OverTimeOnJob = time actually spent on the job that is worked between 3:00 pm and 7:00 am AND is logged between ArriveJobTime and LeaveJobTime

            $TotalOverTimeOnJob = $OverTimeOnJob;

            $OverTimeHourlyLP = $this->convertMinuteToHours($TotalOverTimeOnJob);
            if ($get_arrive_jobtime <= $overtime_end && $get_clock_intime < $overtime_end && $get_leave_jobtime >= $overtime_start) {

                //(LeaveJobTime – ArriveJobTime ) – OvertimeOnJob

                $minutes3 = ($ot_end[0] * 60.0 + $ot_end[1]);
                $minutes4 = ($ot_start[0] * 60.0 + $ot_start[1]);

                $result = $minutes4 - $minutes3;
            } elseif (
                $get_clock_intime <= $overtime_end && $get_arrive_jobtime > $overtime_end &&
                $leavejobtime < $Midnight
            ) {

                $minutes3 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes4 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $Time1 = $minutes4 - $minutes3;

                $result = $Time1;
            } elseif (
                $get_clock_intime <= $overtime_end && $get_arrive_jobtime > $overtime_end &&
                $leavejobtime > $Midnight
            ) {

                // here
                // $minutes3 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                // $minutes4 = ($Midnight2[0] * 60.0 + $Midnight2[1]);

                // $minutes27 = ($Midnight[0] * 60.0 + $Midnight[1]);
                // $minutes28 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                // $Time1 = $minutes4 - $minutes3;
                // $Time2 = $minutes28 - $minutes27;

                // $result = $Time1 + $Time2;
                $result = $leavejobtime[0] * 60.0 + $leavejobtime[1] - $arrivejobtime[0] * 60.0 - $arrivejobtime[1];
                // dd($OverTimeOnJob);
            } elseif (
                $get_clock_intime > $overtime_end && $get_arrive_jobtime > $overtime_end &&
                $leavejobtime > $Midnight
            ) {

                $minutes3 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes4 = ($Midnight2[0] * 60.0 + $Midnight2[1]);

                $minutes27 = ($Midnight[0] * 60.0 + $Midnight[1]);
                $minutes28 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $Time1 = $minutes4 - $minutes3;
                $Time2 = $minutes28 - $minutes27;

                $result = $Time1 + $Time2;
            } else {

                $minutes3 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes4 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $result = $minutes4 - $minutes3;
            }

            $TimeOnJob = $result;
            // dd( $this->convertMinuteToHours($TimeOnJob) );
            $TimeHourlyLP = $this->convertMinuteToHours($TimeOnJob) - $OverTimeHourlyLP;

            //RegularOvertime = all time worked between 3:00 pm and 7:00 am that is NOT logged between ArriveJobTime and LeaveJobTime
            if ($get_leave_jobtime <= $overtime_start && $get_arrive_jobtime <= $overtime_end) {

                if ($get_leave_jobtime < $overtime_start) {

                    $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes6 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                    $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $re1 = $minutes6 - $minutes5;
                    $re2 = $minutes8 - $minutes7;

                    if ($get_clock_outtime < $overtime_start && $get_leave_jobtime < $overtime_start) {

                        $result = $re1;
                    } else {

                        $result = $re1 + $re2;
                    }
                } else {

                    $leavetime = Carbon::parse($data->leave_jobtime);
                    $clockout = Carbon::parse($data->clock_outtime);

                    $result = $leavetime->diffInMinutes($clockout);
                }

                $Regular1 = $result;
            } elseif ($get_leave_jobtime <= $overtime_start && $get_arrive_jobtime >= $overtime_end && $get_clock_outtime >= $overtime_start) {

                if ($get_clock_intime < $overtime_end && $get_leave_jobtime < $overtime_start && $get_clock_outtime >= $overtime_start) {

                    $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes6 = ($ot_end[0] * 60.0 + $ot_end[1]);

                    $result = $minutes6 - $minutes5;

                    $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $result2 = $minutes8 - $minutes7;

                    $Regular1 = $result + $result2;
                } elseif ($get_clock_intime < $overtime_end && $get_arrive_yardtime < $overtime_start && $get_clock_outtime >= $overtime_start) {

                    $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes6 = ($ot_end[0] * 60.0 + $ot_end[1]);

                    $Regular1 = $minutes6 - $minutes5;
                } elseif ($get_clock_outtime <= $overtime_start) {

                    $minutes5 = ($clockouttime[0] * 60.0 + $clockouttime[1]);
                    $minutes6 = ($ot_start[0] * 60.0 + $ot_start[1]);

                    $Regular1 = $minutes6 - $minutes5;
                    /* here */
                } elseif ($get_leave_jobtime < $overtime_start && $get_clock_outtime > $overtime_start) {

                    if ($get_clock_intime < $overtime_end && $get_leave_jobtime < $overtime_start) {

                        $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                        $minutes6 = ($ot_end[0] * 60.0 + $ot_end[1]);

                        $Reg1 = $minutes6 - $minutes5;

                        $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
                        $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                        $Reg2 = $minutes8 - $minutes7;

                        $Regular1 = $Reg1 + $Reg2;
                    } else {

                        $minutes5 = ($clockouttime[0] * 60.0 + $clockouttime[1]);
                        $minutes6 = ($ot_start[0] * 60.0 + $ot_start[1]);

                        $Regular1 = $minutes5 - $minutes6;
                    }
                } else {

                    $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes6 = ($ot_end[0] * 60.0 + $ot_end[1]);

                    $Reg1 = $minutes6 - $minutes5;

                    $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $Reg2 = $minutes8 - $minutes7;

                    $Regular1 = $Reg1 + $Reg2;
                }
            } elseif ($get_leave_jobtime <= $overtime_start && $get_arrive_jobtime <= $overtime_end) {

                $minutes5 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                $minutes6 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                $Regular1 = 0;
            } elseif ($get_leave_jobtime <= $overtime_start && $get_arrive_jobtime >= $overtime_end) {

                if (
                    $get_clock_intime > $overtime_end && $get_arrive_jobtime > $overtime_end &&
                    $leavejobtime > $Midnight && $get_leave_jobtime < $overtime_end && $get_clock_outtime > $overtime_end
                ) {

                    $minutes5 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                    $minutes6 = ($ot_end[0] * 60.0 + $ot_end[1]);

                    $result = $minutes6 - $minutes5;
                } elseif ($get_clock_intime > $overtime_end && $get_clock_outtime < $overtime_start && $get_arrive_jobtime > $overtime_end && $get_leave_jobtime < $overtime_start) {

                    $result = 0;
                } elseif ($get_leave_jobtime <= $overtime_start) {

                    $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes6 = ($ot_end[0] * 60.0 + $ot_end[1]);

                    $result = $minutes6 - $minutes5;
                } else {

                    $minutes5 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                    $minutes6 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $result = 0;
                }

                $Regular1 = $result;
            } elseif ($get_clock_intime >= $overtime_end) {
                $leavetime = Carbon::parse($data->leave_jobtime);
                $clockout = Carbon::parse($data->clock_outtime);

                $Regular2 = $leavetime->diffInMinutes($clockout);

                $overTimeStart = ($ot_start[0] * 60.0 + $ot_start[1]);
                $clockInTime = ($clockintime[0] * 60.0 + $clockintime[1]);
                $regula3 = $overTimeStart - $clockInTime;

                $Regular1 = $Regular2 - $regula3;
            } elseif ($get_leave_jobtime >= $overtime_start && $get_arrive_jobtime >= $overtime_end) {

                $leavetime = Carbon::parse($data->leave_jobtime);
                $clockout = Carbon::parse($data->clock_outtime);

                $Regular2 = $leavetime->diffInMinutes($clockout);

                /*$minutes5 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $minutes6 = ($clockouttime[0] * 60.0 + $clockouttime[1]);*/

                $minutes22 = ($clockintime[0] * 60.0 + $clockintime[1]);
                $minutes23 = ($ot_end[0] * 60.0 + $ot_end[1]);

                //$Regular2 = $minutes6 - $minutes5;

                $Regular3 = $minutes23 - $minutes22;

                $Regular1 = $Regular2 + $Regular3;
            } elseif ($get_arrive_jobtime <= $overtime_end && $get_clock_intime < $overtime_end && $get_leave_jobtime >= $overtime_start) {
                $minutes22 = ($clockintime[0] * 60.0 + $clockintime[1]);
                $minutes23 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                $Regular3 = $minutes23 - $minutes22;

                $leavetime = Carbon::parse($data->leave_jobtime);
                $clockout = Carbon::parse($data->clock_outtime);

                $Regular2 = $leavetime->diffInMinutes($clockout);

                $Regular1 = $Regular2 + $Regular3;
            }

            if ($get_clock_intime < $overtime_end) {

                $TotalHourly = $Regular1 - 15;
            } else {

                $TotalHourly = $Regular1;
            }

            $RegularOvertime = $this->convertMinuteToHours($TotalHourly);
            //HourlyTime = all time worked between 7:00 am and ArriveJobTime  AND all time worked between LeaveJobTime and 3:00 pm.

            if ($get_arrive_jobtime <= $overtime_end) {

                if ($get_arrive_jobtime < $overtime_end && $get_leave_jobtime < $overtime_start && $get_clock_intime < $overtime_end && $get_clock_outtime < $overtime_start) {

                    if ($get_clock_outtime < $overtime_start && $get_leave_jobtime < $overtime_start) {

                        $leavetime = Carbon::parse($data->leave_jobtime);
                        $clockout = Carbon::parse($data->clock_outtime);

                        $total = $leavetime->diffInMinutes($clockout);
                    } else {

                        $minutes9 = ($ot_end[0] * 60.0 + $ot_end[1]);
                        $minutes10 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                        $total = $minutes9 - $minutes10;
                    }

                    $result = $total;
                } elseif ($get_arrive_jobtime < $overtime_end && $get_leave_jobtime < $overtime_start) {

                    $minutes15 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                    $minutes16 = ($ot_start[0] * 60.0 + $ot_start[1]);

                    $tt2 = $minutes16 - $minutes15;

                    $result = $tt2;
                } elseif ($get_arrive_jobtime <= $overtime_end && $get_clock_intime < $overtime_end && $get_leave_jobtime >= $overtime_start) {

                    $minutes9 = ($ot_end[0] * 60.0 + $ot_end[1]);
                    $minutes10 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                    $minutes15 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes16 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                    $tt1 = $minutes9 - $minutes10;
                    $tt2 = $minutes16 - $minutes15;

                    $result = $tt1 + $tt2;
                } else {

                    $minutes9 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes10 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $minutes15 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes16 = ($leaveyardtime[0] * 60.0 + $leaveyardtime[1]);

                    $tt1 = $minutes10 - $minutes9;
                    $tt2 = $minutes16 - $minutes15;

                    $result = $tt1 + $tt2;
                }

                $HourlyTime1 = $result;
            } elseif ($get_clock_intime >= $overtime_end) {

                $leavetime = Carbon::parse($data->leave_jobtime);
                $clockout = Carbon::parse($data->clock_outtime);

                $tt1 = $leavetime->diffInMinutes($clockout);
                $clocktime = Carbon::parse($data->clock_intime);
                $arrivetime = Carbon::parse($data->arrive_jobtime);

                $tt2 = $clocktime->diffInMinutes($arrivetime);

                /*$minutes9  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $minutes10 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                $minutes15  = ($clockintime[0] * 60.0 + $clockintime[1]);

                $minutes16  = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                $tt1 = $minutes10 - $minutes9;

                $tt2 = $minutes16 - $minutes15;*/

                $HourlyTime1 = $tt1 + $tt2 - $TotalHourly;
                // dd($day2);
                if ($day != $day2) {
                    $arriveTime = $arrivejobtime[0] * 60.0 + $arrivejobtime[1];
                    $leaveTime = $leavejobtime[0] * 60.0 + $leavejobtime[1];
                    $totalTime = $leaveTime - $arriveTime;
                    $OverTimeOnJob = $this->convertMinuteToHours($totalTime);
                    // dd($timeOne);
                    $OverTimeHourlyLP = $OverTimeOnJob;
                    $TimeHourlyLP = 0;
                }
            } elseif ($get_clock_intime <= $overtime_end && $get_leave_jobtime <= $overtime_start) {

                if ($get_leave_jobtime < $overtime_start && $get_clock_intime < $overtime_end && $get_clock_outtime < $overtime_start) {
                    $minutes11 = ($ot_end[0] * 60.0 + $ot_end[1]);
                    $minutes12 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                    $leavetime = Carbon::parse($data->leave_jobtime);
                    $clockout = Carbon::parse($data->clock_outtime);

                    $res1 = $minutes12 - $minutes11;

                    $res2 = $leavetime->diffInMinutes($clockout);

                    $tt2 = $res1 + $res2;
                } elseif ($get_leave_jobtime < $overtime_start && $get_clock_outtime < $overtime_start) {

                    $minutes11 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                    $minutes12 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $tt2 = $minutes12 - $minutes11;
                } elseif ($get_leave_jobtime < $overtime_start && $get_clock_outtime > $overtime_start) {

                    $minutes13 = ($ot_end[0] * 60.0 + $ot_end[1]);
                    $minutes14 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                    $result = $minutes14 - $minutes13;

                    $minutes11 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                    $minutes12 = ($ot_start[0] * 60.0 + $ot_start[1]);

                    $result2 = $minutes12 - $minutes11;

                    $tt2 = $result + $result2;
                } else {

                    $minutes11 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes12 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                    $tt3 = $minutes11 - $minutes12;

                    $minutes9 = ($ot_end[0] * 60.0 + $ot_end[1]);
                    $minutes10 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                    $tt1 = $minutes10 - $minutes9;

                    $tt2 = $tt3 + $tt1;
                }

                $HourlyTime1 = $tt2;
            } elseif ($get_clock_intime <= $overtime_end && $get_leave_jobtime >= $overtime_start) {
                $minutes9 = ($ot_end[0] * 60.0 + $ot_end[1]);
                $minutes10 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                $tt1 = $minutes10 - $minutes9;

                $HourlyTime1 = $tt1;
            } else {

                $minutes9 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                $minutes10 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                $HourlyTime1 = $minutes10 - $minutes9;
            }

            //$HourlyTime2 = $minutes12 - $minutes11;

            if ($get_clock_intime < $overtime_end) {

                $HourlyTime3 = $HourlyTime1 + 15;
            } else {

                $HourlyTime3 = $HourlyTime1;
            }

            $Hourly = $this->convertMinuteToHours($HourlyTime3);
        }

        //OvertimeOnJob = LeaveJobTime – ArriveJobTime

        if ($get_clock_intime > $overtime_start && $leavejobtime > $Midnight && $get_clock_outtime < $overtime_end) {

            $A_clockintime = Carbon::parse($data->clock_intime);
            $A_arrivejobtime = Carbon::parse($data->arrive_jobtime);

            $result = $A_clockintime->diffInMinutes($A_arrivejobtime);

            $MidRegularOvertime = $this->convertMinuteToHours(abs($result));

            $minutes31 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
            $minutes32 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

            $result2 = $minutes32 - $minutes31;

            $Onjobovertime = $result2;

            $JobOvertime = $this->convertMinuteToHours(abs($Onjobovertime));

            $minutes33 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
            $minutes34 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

            $result3 = $minutes34 - $minutes33;

            $doubletime_lp = $this->convertMinuteToHours(abs($result3));
            $minutes35 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
            $minutes36 = ($clockouttime[0] * 60.0 + $clockouttime[1]);
            $DBTime = $minutes36 - $minutes35;

            $OTime = $this->convertMinuteToHours(abs($DBTime));
            $MidRegularOvertime = $MidRegularOvertime + $doubletime_lp;
            if ($day == "Saturday" && $day2 == "Sunday") {
                $minutes35 = ($Midnight[0] * 60.0 + $Midnight[1]);
                $minutes36 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                $DBTime = $minutes36 - $minutes35;
                $OTime = $this->convertMinuteToHours(abs($DBTime));

                $minutes31 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes32 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $result2 = $minutes32 - $minutes31;

                $Onjobovertime = $result2;

                $JobOvertime = $this->convertMinuteToHours(abs($Onjobovertime));
                // dd($JobOvertime);
                $doubletime_lp = 0;
            }
        } elseif ($get_clock_intime > $overtime_start && $leavejobtime > $Midnight && $get_clock_outtime > $overtime_end) {

            $A_clockintime = Carbon::parse($data->clock_intime);
            $A_arrivejobtime = Carbon::parse($data->arrive_jobtime);

            $result = $A_clockintime->diffInMinutes($A_arrivejobtime);

            $MidRegularOvertime = $this->convertMinuteToHours(abs($result));

            $minutes31 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
            $minutes32 = ($Midnight2[0] * 60.0 + $Midnight2[1]);

            $result2 = $minutes32 - $minutes31;

            $Onjobovertime = $result2;

            $JobOvertime = $this->convertMinuteToHours(abs($Onjobovertime));

            $minutes33 = ($Midnight[0] * 60.0 + $Midnight[1]);
            $minutes34 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

            $result3 = $minutes34 - $minutes33;

            $doubletime_lp = $this->convertMinuteToHours(abs($result3));

            $minutes35 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
            $minutes36 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

            $DBTime = $minutes36 - $minutes35;

            $OTime = $this->convertMinuteToHours(abs($DBTime));
        } else {

            $arrivejobtimes = Carbon::parse($data->arrive_jobtime);
            $leavejobtimes = Carbon::parse($data->leave_jobtime);

            $Onjobovertime = $leavejobtimes->diffInMinutes($arrivejobtimes);

            $JobOvertime = $this->convertMinuteToHours(abs($Onjobovertime));

            $clockIN = Carbon::parse($data->clock_intime);
            $clockOUT = Carbon::parse($data->clock_outtime);

            $totaltime = $clockOUT->diffInMinutes($clockIN);

            $overtime = $this->convertMinuteToHours(abs($totaltime)) - $JobOvertime;

            //If clockout and clockin days are not same

            $Midnight = explode(':', "00:00:00");

            $minutes33 = ($Midnight[0] * 60.0 + $Midnight[1]);
            $minutes32 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

            $DBTime = $minutes32 - $minutes33;

            $OTime = $this->convertMinuteToHours(abs($DBTime));

            $MidRegularOvertime = $overtime - $OTime;
        }

        if ($day == "Saturday" && $day2 == "Saturday") {

            TimeLogs::where('id', $data->id)
                ->update(['overtime_hourly' => $overtime, 'overtime_hourly_lp4' => $JobOvertime]);
        } elseif ($day == "Saturday" && $day2 == "Sunday") {
            if (!isset($doubletime_lp)) {
                $doubletime_lp = 0;
            }

            $minutes35 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
            $minutes36 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
            $Plantovertime = $minutes35 - $minutes36;
            $JobOvertime = $this->convertMinuteToHours(abs($Plantovertime));

            $minutes33 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
            $minutes32 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

            $DBTime = $minutes32 - $minutes33;
            $OTime = $this->convertMinuteToHours(abs($DBTime));

            $minutes323 = ($Midnight[0] * 60.0 + $Midnight[1]);

            $DB2Time = $minutes33 - $minutes323;

            $doubletime_lp = $this->convertMinuteToHours(abs($DB2Time));

            $minutes34 = ($clockintime[0] * 60.0 + $clockintime[1]);
            $STime = abs($minutes34 - $minutes35);
            $SundayMidTime = $this->convertMinuteToHours(abs($STime));
            TimeLogs::where('id', $data->id)
                ->update([
                    'overtime_hourly' => $SundayMidTime,
                    'overtime_hourly_lp4' => $JobOvertime, 'double_time_hourly' => $OTime,
                    'lp4_doubletime' => $doubletime_lp,
                ]);
        } elseif ($holiday == "1") {

            TimeLogs::where('id', $data->id)
                ->update(['double_time_hourly' => $overtime, 'lp4_doubletime' => $JobOvertime]);
        } elseif ($day == "Sunday" && $day2 == "Sunday") {

            TimeLogs::where('id', $data->id)
                ->update(['double_time_hourly' => $overtime, 'lp4_doubletime' => $JobOvertime]);
        } elseif ($day == "Sunday" && $day2 == "Monday") {

            (new MidSundayCalculations)->calculations($data, $config, $mapping);
        } elseif ($get_clock_intime == $get_clock_outtime && $day == $day2) {

            TimeLogs::where('id', $data->id)
                ->update(['hourly' => '0.00', 'overtime_hourly' => '0.00', 'hourly_lp4' => '0.00', 'overtime_hourly_lp4' => '0.00']);
        } else {

            if ($get_clock_intime >= $overtime_start) {

                if (($day != "Friday" && $day2 != "Saturday") && $day != $day2 && $day2 != $day3 && $get_clock_outtime <= $overtime_end && $get_clock_intime >= $overtime_start) {
                    /* hourly */
                    $arrivejobtime = explode(':', $get_arrive_jobtime);
                    $arrivejobtime = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                    $overTimENd = $ot_end[0] * 60.0 + $ot_end[1];
                    $hourly1 = $overTimENd - $arrivejobtime;

                    $leavejobtime = explode(':', $get_leave_jobtime);
                    $minutes11 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes12 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                    $hourly2 = $minutes12 - $minutes11;

                    $hourly = $this->convertMinuteToHours(abs($hourly1 + $hourly2) + 15);

                    // $overtime_hourly
                    $clockout = ($clockouttime[0] * 60.0 + $clockouttime[1]);
                    $clockIn = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $time1 = $clockIn - $minutes11;
                    $time2 = $minutes11 - $clockout;
                    $RegularOvertime = $this->convertMinuteToHours(abs($time1 + $time2) - 15);

                    // hourly_lp2
                    $arrivejobtime = Carbon::parse($data->arrive_jobtime);
                    $leavejobtime = Carbon::parse($data->leave_jobtime);
                    $time3 = $leavejobtime->diffInMinutes($arrivejobtime);
                    $TimeOnJobHourly = $this->convertMinuteToHours(abs($time3));
                    $OverTimeOnJobHourly = 0;

                    TimeLogs::where('id', $data->id)
                        ->update(['overtime_hourly_lp4' => $OverTimeOnJobHourly, 'hourly_lp4' => $TimeOnJobHourly, 'hourly' => $hourly, 'overtime_hourly' => $RegularOvertime]);
                } else if (($day != "Friday" && $day2 != "Saturday") && $day != $day2) {

                    $arrivejobtime = explode(':', $get_arrive_jobtime);
                    $arrivejobtime = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                    $minutes35 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                    $result = abs($arrivejobtime - $minutes35);

                    $minutes36 = ($Midnight[0] * 60.0 + $Midnight[1]);
                    $overTimeStart = $ot_end[0] * 60.0 + $ot_end[1];
                    $result2 = abs($minutes36 - $overTimeStart);

                    $OTimeOnJob = abs($result2 + $result);
                    $OverTimeOnJobHourly = $this->convertMinuteToHours($OTimeOnJob);

                    $otEnd = $ot_start[0] * 60.0 + $ot_start[1];
                    $leave = $leaveyardtime[0] * 60.0 + $leaveyardtime[1];

                    $arrivejobtime = Carbon::parse($data->arrive_jobtime);
                    $leavejobtime = Carbon::parse($data->leave_jobtime);

                    $OverTimeOnJob = $leavejobtime->diffInMinutes($arrivejobtime);
                    $TimeOnJobHourly = $this->convertMinuteToHours($OverTimeOnJob) - $OverTimeOnJobHourly;

                    $leavejobtime = explode(':', $get_leave_jobtime);
                    $minutes11 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes12 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                    $arrivejobtime = explode(':', $get_arrive_jobtime);
                    $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes6 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                    $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $re1 = abs($minutes6 - $minutes5);
                    $re2 = abs($minutes8 - $minutes7);

                    if ($get_clock_intime <= $overtime_start) {
                        $RegularOvertime = $this->convertMinuteToHours($re1 + $re2 - 15);
                        $tt3 = $minutes11 - $minutes12 + 15;
                    } else {
                        $RegularOvertime = $this->convertMinuteToHours($re1 + $re2);
                        $tt3 = $minutes11 - $minutes12;
                    }
                    $Hourly = $this->convertMinuteToHours($tt3);
                    TimeLogs::where('id', $data->id)
                        ->update(['overtime_hourly_lp4' => $OverTimeOnJobHourly, 'hourly_lp4' => $TimeOnJobHourly, 'hourly' => $Hourly, 'overtime_hourly' => $RegularOvertime]);
                } else {

                    TimeLogs::where('id', $data->id)->update(['hourly' => $Hourly, 'overtime_hourly' => $RegularOvertime, 'overtime_hourly_lp4' => $OverTimeHourlyLP]);
                }
            } else if (($day != "Friday" && $day2 != "Saturday") && $day != $day2 && $get_clock_intime >= $overtime_end && $get_clock_outtime <= $overtime_start) {

                $arrivejobtime = explode(':', $get_arrive_jobtime);
                $arrivejobtime = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes35 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                $result = abs($arrivejobtime - $minutes35);

                $minutes36 = ($Midnight[0] * 60.0 + $Midnight[1]);
                $overTimeStart = $ot_end[0] * 60.0 + $ot_end[1];
                $result2 = abs($minutes36 - $overTimeStart);

                $OTimeOnJob = abs($result2 + $result);
                $OverTimeOnJobHourly = $this->convertMinuteToHours($OTimeOnJob);

                $otEnd = $ot_start[0] * 60.0 + $ot_start[1];
                $leave = $leaveyardtime[0] * 60.0 + $leaveyardtime[1];

                $arrivejobtime = Carbon::parse($data->arrive_jobtime);
                $leavejobtime = Carbon::parse($data->leave_jobtime);

                $OverTimeOnJob = $leavejobtime->diffInMinutes($arrivejobtime);
                $TimeOnJobHourly = $this->convertMinuteToHours($OverTimeOnJob) - $OverTimeOnJobHourly;

                $leavejobtime = explode(':', $get_leave_jobtime);
                $minutes11 = ($ot_start[0] * 60.0 + $ot_start[1]);
                $minutes12 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $arrivejobtime = explode(':', $get_arrive_jobtime);
                $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                $minutes6 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
                $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                $re1 = abs($minutes6 - $minutes7);
                $re2 = abs($minutes8 - $minutes7);

                if ($get_clock_intime <= $overtime_start) {
                    // $RegularOvertime = $this->convertMinuteToHours($re1 + $re2 - 15);
                    // $tt3 = $minutes11 - $minutes12 + 15;
                    $RegularOvertime = $this->convertMinuteToHours($re1 + $re2);
                    $tt3 = $minutes11 - $minutes12;
                } else {
                    $RegularOvertime = $this->convertMinuteToHours($re1 + $re2);
                    $tt3 = $minutes11 - $minutes12;
                }
                $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
                $t4 = abs($minutes5 - $minutes7);
                $Hourly = $this->convertMinuteToHours($tt3 + $t4);
                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly_lp4' => $OverTimeOnJobHourly, 'hourly_lp4' => $TimeOnJobHourly, 'hourly' => $Hourly, 'overtime_hourly' => $RegularOvertime]);
            } else if ($day != $day2 && $day != "Friday" && $day2 != "Saturday" && $get_clock_intime <= $overtime_end && $get_clock_outtime <= $overtime_end) {

                // Hourly
                $overTimeEnd = ($ot_end[0] * 60.0 + $ot_end[1]); /* 7am */
                $arrivejobtime = explode(':', $get_arrive_jobtime);
                $arrivejobtime = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $hourly1 = $arrivejobtime - $overTimeEnd;

                $leave = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                $overTimeStart = ($ot_start[0] * 60.0 + $ot_start[1]); /* 3pm */
                $hourly2 = $overTimeStart - $leave;
                $Hourly = $hourly1 + $hourly2;
                $Hourly = $this->convertMinuteToHours($Hourly);
                // totalOvertime

                // totalOvertime
                $clockoutTime = ($clockouttime[0] * 60.0 + $clockouttime[1]);
                $MidNighte = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                $MidNighte0 = ($Midnight[0] * 60.0 + $Midnight[1]);
                $clockinTime = ($clockintime[0] * 60.0 + $clockintime[1]);
                if ($get_leave_jobtime < $overtime_start) {
                    $regularOt1 = $MidNighte - $overTimeStart;
                    $regularOt2 = $clockoutTime - $MidNighte0;

                } else {
                    $regularOt1 = $MidNighte - $leave;
                    $regularOt2 = $clockoutTime - $MidNighte0;
                }
                $regular1 = abs($regularOt1) + abs($regularOt2);
                $regular2 = $overTimeEnd - $clockinTime;
                $totalOvertime = $this->convertMinuteToHours($regular1 + $regular2);
                // TimeOnJob
                // TimeHourlyLP
                $arrivejobtime = Carbon::parse($data->arrive_jobtime);
                $leavejobtime = Carbon::parse($data->leave_jobtime);
                $OverTimeOnJob = $leavejobtime->diffInMinutes($arrivejobtime);
                $TimeOnJob = $this->convertMinuteToHours($OverTimeOnJob);
                // plant_overtime_lp2
                $OverTimeOnJobHourly = 0;
                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly_lp4' => $OverTimeOnJobHourly, 'hourly_lp4' => $TimeOnJob, 'hourly' => $Hourly, 'overtime_hourly' => $totalOvertime]);

            } else if (($day != "Friday" && $day2 != "Saturday") && $day != $day2 && $get_clock_intime <= $overtime_end && $get_clock_outtime <= $overtime_start && $clockIndate == $arriveYardDate && $get_clock_outtime > $overtime_end) {
                $minutes11 = ($ot_start[0] * 60.0 + $ot_start[1]);
                $minutes12 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $arrivejobtime = explode(':', $get_arrive_jobtime);
                $minutes6 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                $minutes7 = ($ot_end[0] * 60.0 + $ot_end[1]);
                $hourly1 = $minutes7 - $minutes5;
                $hourly2 = $minutes11 - $minutes12;
                $Hourly = $this->convertMinuteToHours($hourly1 + $hourly2);
                // overtime_hourly
                $minutes12 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $overtime1 = $minutes7 - $minutes5;
                $MidNighte = ($Midnight[0] * 60.0 + $Midnight[1]);
                $overtime2 = $minutes11 - $MidNighte;
                $totalOvertime = $this->convertMinuteToHours($overtime2 + $overtime1);
                // plant_hourly_lp2

                $arrivejobtime = Carbon::parse($data->arrive_jobtime);
                $leavejobtime = Carbon::parse($data->leave_jobtime);
                $OverTimeOnJob = $leavejobtime->diffInMinutes($arrivejobtime);
                $TimeOnJob = $this->convertMinuteToHours($OverTimeOnJob);
                // plant_overtime_lp2
                $OverTimeOnJobHourly = 0;
                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly_lp4' => $OverTimeOnJobHourly, 'hourly_lp4' => $TimeOnJob, 'hourly' => $Hourly, 'overtime_hourly' => $totalOvertime]);
            } else if (($day != "Friday" && $day2 != "Saturday") && $day != $day2 && $get_clock_intime <= $overtime_end && $get_clock_outtime <= $overtime_start) {

                $minutes11 = ($ot_end[0] * 60.0 + $ot_end[1]);
                $minutes12 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                $Hourly = $minutes12 - $minutes11 + 15;
                $Hourly = $this->convertMinuteToHours($Hourly);

                /* overtiem 1.5x */
                $minutes7 = ($ot_end[0] * 60.0 + $ot_end[1]);
                $clockin = ($clockintime[0] * 60.0 + $clockintime[1]);

                $minutes8 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                $clockout = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                $result = abs($minutes7 - $clockin);
                $result2 = abs($minutes8 - $clockout);
                $res = abs($result + $result2 - 15);
                $totalOvertime = $this->convertMinuteToHours($res);
                //  $TimeOnJobHourly
                $overTimeStart = ($ot_start[0] * 60.0 + $ot_start[1]);
                $TotalTimeOnJob = $overTimeStart - $minutes12;
                $TimeOnJob = $this->convertMinuteToHours($TotalTimeOnJob);

                /* plantovertime */
                $leave = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $mid1 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                $mid2 = ($Midnight[0] * 60.0 + $Midnight[1]);
                // 3 - 12 am
                $OverTimeOnJobHourly1 = $mid1 - $overTimeStart;
                //   12 - leave job
                $OverTimeOnJobHourly2 = $leave - $mid2;
                $plantime = $OverTimeOnJobHourly1 + $OverTimeOnJobHourly2;
                $OverTimeOnJobHourly = $this->convertMinuteToHours($plantime);

                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly_lp4' => $OverTimeOnJobHourly, 'hourly_lp4' => $TimeOnJob, 'hourly' => $Hourly, 'overtime_hourly' => $totalOvertime]);

            } else {

                if ($day != $day2 && $day == "Friday" && $day2 == "Saturday" && $get_clock_intime >= $overtime_end && $get_clock_outtime <= $overtime_start) {

                    $arrivejobtime_s = Carbon::parse($data->arrive_jobtime);
                    $leavejobtime_s = Carbon::parse($data->leave_jobtime);
                    $OverTimeOnJob = $leavejobtime_s->diffInMinutes($arrivejobtime_s);

                    $TotalOverTimeOnJob = $OverTimeOnJob;

                    $OverTimeHourlyLP = $this->convertMinuteToHours($TotalOverTimeOnJob);

                    $clockin = Carbon::parse($data->clock_intime);

                    $Sum1 = $clockin->diffInMinutes($arrivejobtime_s);
                    $clockout = Carbon::parse($data->clock_outtime);
                    $Sum2 = $clockout->diffInMinutes($leavejobtime_s);
                    $TotalHourly = $Sum1 + $Sum2;
                    $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $HourlyTime = $minutes7 - $minutes5;

                    $TotalHourly = $TotalHourly - $HourlyTime;
                    $Hourly = $this->convertMinuteToHours($HourlyTime);

                    $RegularOvertime = $this->convertMinuteToHours($TotalHourly);
                    /* here */
                    TimeLogs::where('id', $data->id)
                        ->update(['hourly' => $Hourly, 'overtime_hourly' => $RegularOvertime, 'hourly_lp4' => $TimeHourlyLP, 'overtime_hourly_lp4' => $OverTimeHourlyLP]);
                } else if (($day == "Friday" && $day2 == "Saturday") && $day != $day2 && $day != $day2 && $get_clock_intime <= $overtime_end && $get_clock_outtime <= $overtime_start) {
                    $minutes11 = ($ot_end[0] * 60.0 + $ot_end[1]);
                    $clockin = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes12 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                    $Hourly = $minutes12 - $minutes11 + 15;
                    $Hourly = $this->convertMinuteToHours($Hourly);

                    /* overtiem 1.5x */
                    $minutes7 = ($ot_end[0] * 60.0 + $ot_end[1]);
                    $clockin = ($clockintime[0] * 60.0 + $clockintime[1]);

                    $minutes8 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                    $clockout = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $result = abs($minutes7 - $clockin);
                    $result2 = abs($minutes8 - $clockout);
                    $res = abs($result + $result2 - 15);
                    $totalOvertime = $this->convertMinuteToHours($res);

                    /* hourly LP4 */
                    $overTimeStart = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $TimeHourlyLP = $overTimeStart - $minutes12;
                    $TimeHourlyLP = $this->convertMinuteToHours($TimeHourlyLP);

                    /* overtimeHourlyLp4 */
                    $mid1 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                    $mid2 = ($Midnight[0] * 60.0 + $Midnight[1]);

                    $time1 = $mid1 - $overTimeStart;
                    $time2 = $minutes8 - $mid2;
                    $plantime = $time1 + $time2;
                    $OverTimeHourlyLP = $this->convertMinuteToHours($plantime);

                    TimeLogs::where('id', $data->id)
                        ->update(['hourly' => $Hourly, 'overtime_hourly' => $totalOvertime, 'hourly_lp4' => $TimeHourlyLP, 'overtime_hourly_lp4' => $OverTimeHourlyLP]);
                } else {

                    TimeLogs::where('id', $data->id)
                        ->update(['hourly' => $Hourly, 'overtime_hourly' => $RegularOvertime, 'hourly_lp4' => $TimeHourlyLP, 'overtime_hourly_lp4' => $OverTimeHourlyLP]);
                }
            }
        }
    }
}
