<?php

namespace App\Http\Controllers\Calculations;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use Exception;

use App\User;
use App\Breaks;
use App\TimeLogs;
use App\ConfigMapping;
use App\JobConfiguration;

class MidSundayCalculations extends Controller
{

    public function convertMinuteToHours($minutes)
    {
        return $minutes / 60;
    }

    public function calculations($data,$config,$mapping){

        $holiday             = $data->holiday;
        $day                 = Carbon::parse($data->clock_intime)->format('l');
        $day2                = Carbon::parse($data->clock_outtime)->format('l');

        $overtime_start      = $config->start_overtime;
        $overtime_end        = $config->end_overtime;

        $get_clock_intime    = Carbon::parse($data->clock_intime)->format('H:i:s');
        $get_leave_yardtime  = Carbon::parse($data->leave_yardtime)->format('H:i:s');
        $get_arrive_jobtime  = Carbon::parse($data->arrive_jobtime)->format('H:i:s');
        $get_leave_jobtime   = Carbon::parse($data->leave_jobtime)->format('H:i:s');
        $get_arrive_yardtime = Carbon::parse($data->arrive_yardtime)->format('H:i:s');
        $get_clock_outtime   = Carbon::parse($data->clock_outtime)->format('H:i:s');
        $arriveYardDate = Carbon::parse($data->arrive_yardtime)->format('Y-m-d');
        $clockIndate = Carbon::parse($data->clock_intime)->format('Y-m-d');
        $leaveYardDate = Carbon::parse($data->leave_yardtime)->format('Y-m-d');

        $ot_end              = explode(':', $overtime_end);
        $ot_start            = explode(':', $overtime_start);
        $clockintime         = explode(':', $get_clock_intime);
        $leaveyardtime       = explode(':', $get_leave_yardtime);
        $arrivejobtime       = explode(':', $get_arrive_jobtime);
        $leavejobtime        = explode(':', $get_leave_jobtime);
        $arriveyardtime      = explode(':', $get_arrive_yardtime);
        $clockouttime        = explode(':', $get_clock_outtime);

        $Midnight = explode(':', "00:00:00");
        $Midnight2 = explode(':', "24:00:00");

        if($get_leave_jobtime < $overtime_end && $leavejobtime > $Midnight && $get_clock_outtime > $overtime_end){

        $minutes3  = ($clockintime[0] * 60.0 + $clockintime[1]);
        $arriveJobTime = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
        $leaveJobTime = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes4  = ($Midnight2[0] * 60.0 + $Midnight2[1]);

        $S_DoubleTime = $arriveJobTime - $minutes3;

        $S_LPDoubleTime = $arriveJobTime - $minutes4;


        /* Hourly LP2-1/2  */
        // $minutes3  = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
        // $minutes4  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

        $midnight_2  = ($Midnight[0] * 60.0 + $Midnight[1]);
        $S_LPOverTime = $midnight_2 - $leaveJobTime;


        }else{

        $S_clockintime   = Carbon::parse($data->clock_intime);
        $S_arrivejobtime = Carbon::parse($data->arrive_jobtime);

        $leaveJobTImes = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $midnight_2  = ($Midnight2[0] * 60.0 + $Midnight2[1]);
        $midnightOvertime = $midnight_2 - $leaveJobTImes;

        $S_DoubleTime  = $S_arrivejobtime->diffInMinutes($S_clockintime) + $midnightOvertime;



        $minutes1  = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
        $minutes2  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $S_LPDoubleTime = $minutes1 - $minutes2;
        $minutes3  = ($Midnight[0] * 60.0 + $Midnight[1]);
        $minutes4  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $S_LPOverTime = $minutes3 - $minutes4;
        if($this->convertMinuteToHours(abs($S_LPDoubleTime)) > 12){
             $minutes3  = ($Midnight2[0] * 60.0 + $Midnight2[1]);
             $clockIn = ($clockintime[0] * 60.0 + $clockintime[1]);
             $S_LPDoubleTime = $minutes1 - $minutes3;

             $S_DoubleTime = $minutes1 - $clockIn;
             $MTime = 0;
         }
        }
        $SundayDoubleTime  = $this->convertMinuteToHours(abs($S_DoubleTime));

        $SundayLPDoubleTime = $this->convertMinuteToHours(abs($S_LPDoubleTime));

        $SundayLPOvertime = $this->convertMinuteToHours(abs($S_LPOverTime));
        if($get_clock_intime > $overtime_start && $get_leave_jobtime < $overtime_end && $get_clock_outtime < $overtime_end){

        $minutes5  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes6  = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $S_Overtime    = $minutes5 - $minutes6;

        }elseif($get_clock_outtime < $overtime_end && $get_leave_jobtime < $overtime_end && $get_clock_outtime < $overtime_end){

        $minutes5  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes6  = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $S_Overtime    = $minutes5 - $minutes6;

        }elseif($get_clock_outtime < $overtime_end && $get_leave_jobtime < $overtime_end){

        $minutes5  = ($arriveyardtime[0] * 60.0 + $arriveyardtime[1]);
        $minutes6  = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $S_Overtime    = $minutes5 - $minutes6;

        }elseif($get_leave_jobtime < $overtime_end && $leavejobtime > $Midnight && $get_clock_outtime > $overtime_end){

        $minutes5  = ($Midnight[0] * 60.0 + $Midnight[1]);
        // $minutes6  = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

        // $result    = $minutes6 - $minutes5;

        $minutes7  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        // $minutes8  = ($ot_end[0] * 60.0 + $ot_end[1]);
//
        // $result1    = $minutes8 - $minutes7;

        $S_Overtime    = $minutes7 - $minutes5;


        }elseif($get_leave_jobtime < $overtime_end){

        $minutes5  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes6  = ($ot_end[0] * 60.0 + $ot_end[1]);

        $S_Overtime    = $minutes5 - $minutes6;

        }else if( $get_clock_outtime < $overtime_end){

               $minutes6  = ($clockouttime[0] * 60.0 + $clockouttime[1]);
               $midnight_2  = ($Midnight[0] * 60.0 + $Midnight[1]);
        $S_Overtime    =$minutes6  -$midnight_2 ;

        } else {
                    $S_Overtime   = 0;

        }

        $SundayOvertime = $this->convertMinuteToHours(abs($S_Overtime));

        if($get_clock_outtime > $overtime_end && $get_leave_jobtime < $overtime_end){

        $minutes7  = ($ot_end[0] * 60.0 + $ot_end[1]);
        // $minutes8  = ($clockouttime[0] * 60.0 + $clockouttime[1]);
            $leaveJobTime = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        // $MTime = $minutes7 - $minutes8;
        $MTime = $leaveJobTime - $minutes7;


        }elseif($get_clock_outtime > $overtime_end && $get_leave_jobtime > $overtime_end){

        $minutes7  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes8  = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $MTime = $minutes7 - $minutes8;

        }elseif($get_clock_intime > $overtime_start && $get_leave_jobtime < $overtime_end && $get_clock_outtime < $overtime_end){

        $MTime = 0;

        }elseif($get_clock_intime < $overtime_start && $get_leave_yardtime > $overtime_start && $get_leave_jobtime < $overtime_end && $get_clock_outtime < $overtime_end){

        $MTime = 0;

        }elseif($get_clock_outtime < $overtime_end && $get_leave_jobtime < $overtime_end){

        $minutes7  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes8  = ($arriveyardtime[0] * 60.0 + $arriveyardtime[1]);

        $MTime = $minutes7 - $minutes8;

        $minutes1  = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
        $minutes2  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $S_LPDoubleTime = $minutes1 - $minutes2;
        if($this->convertMinuteToHours(abs($S_LPDoubleTime)) > 12){
                 $MTime = 0;
        }

        } else if($get_clock_outtime < $overtime_end){
            $MTime = 0;
            $SundayLPOvertime = 0;
        }else{

        $minutes9   = ($Midnight[0] * 60.0 + $Midnight[1]);
        $minutes10  = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $MTime = $minutes9 - $minutes10;
         }
        $MidTime  = $this->convertMinuteToHours(abs($MTime));
 if( ($get_clock_intime <= $overtime_end && $get_clock_outtime <= $overtime_start)){

        if($clockIndate == $arriveYardDate)
        {
            /* hourly  */
            $MTime =  15 ;
            $MidTime = $this->convertMinuteToHours(abs($MTime));
            /* overtime_hourly */
            $minutes8 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
              $mid1 = ($Midnight[0] * 60.0 + $Midnight[1]);
              $mid2 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
            $clockout = ($clockouttime[0] * 60.0 + $clockouttime[1]);
            $result2 = abs($mid1 - $clockout);
            $S_Overtime = abs( $result2 - 15);
            $SundayOvertime = $this->convertMinuteToHours(abs($S_Overtime));

            // plant_overtime
            $SundayLPOvertime = 0;
             // double_time_hourly
            $arrive = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
            $clockin = ($clockintime[0] * 60.0 + $clockintime[1]);
            $leave = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
            $time2 = $mid2 -$leave;
            $time1 = ($arrive - $clockin) + $time2;
            $SundayDoubleTime = $this->convertMinuteToHours($time1);
                  //   'plant db time '=> $SundayDoubleTime,
                  $time4 = $leave  - $arrive ;
                    $SundayLPDoubleTime = $this->convertMinuteToHours($time4);


        }else {
            /* hourly  */
            $MTime =  15 ;
            $MidTime = $this->convertMinuteToHours(abs($MTime));

            /* overtime_hourly */
            $minutes8 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
            $clockout = ($clockouttime[0] * 60.0 + $clockouttime[1]);
            $result2 = abs($minutes8 - $clockout);
            $S_Overtime = abs( $result2 - 15);
            $SundayOvertime = $this->convertMinuteToHours(abs($S_Overtime));
            // plant_overtime
            $mid1 = ($Midnight[0] * 60.0 + $Midnight[1]);
                   $SundayLPOvertime = $minutes8-  $mid1;
                   $SundayLPOvertime = $this->convertMinuteToHours($SundayLPOvertime);
                   // plant_doubletime
                   $arrive = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                   $clockin = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $time1 = $arrive - $clockin;
                    $SundayDoubleTime = $this->convertMinuteToHours($time1);
                        //   'double_time_hourly'=> $SundayDoubleTime,
                    $SundayLPDoubleTime = 0;
                    $mid2 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                    // plant_overtime
                            $time1 = $mid2 - $arrive;
            $SundayLPDoubleTime = $this->convertMinuteToHours($time1);
        }


        }
        if($get_leave_jobtime < $overtime_end && $leavejobtime > $Midnight && $get_clock_outtime > $overtime_end && $get_clock_intime >= $overtime_end && $clockIndate != $arriveYardDate)

        {
            // hourly

            $overTimeStart = ($ot_start[0] * 60.0 + $ot_start[1]);
            $overTimeEnd = ($ot_end[0] * 60.0 + $ot_end[1]);
            $hourlyTime =( $overTimeStart - $overTimeEnd) +15 ;
            $MidTime = $this->convertMinuteToHours(abs($hourlyTime));
            // overtiem_hourly
            $minutes8 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
            $result2 = abs($overTimeEnd - $minutes8);
            $clockOutTime = ($clockouttime[0] * 60.0 + $clockouttime[1]);
            $result1 = abs($overTimeStart - $clockOutTime);
            $result = abs($result1 + $result2) -15;
            $SundayOvertime = $this->convertMinuteToHours(abs($result));

            // double time
            $clockINTime = ($clockintime[0] * 60.0 + $clockintime[1]);
            $arriveJobTime = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
            $time1 = $arriveJobTime - $clockINTime;
            $SundayDoubleTime = $this->convertMinuteToHours($time1);

            // PTH = plant_doubletime_hourly = 0

            // plant over tiem $SundayLPOvertime
            $midNight = ($Midnight[0] * 60.0 + $Midnight[1]);
            $plantOverTime  = $minutes8 - $midNight;
            $SundayLPOvertime = $this->convertMinuteToHours($plantOverTime);

            // plant Double time  = SundayLPDoubleTime
             $minutes4  = ($Midnight2[0] * 60.0 + $Midnight2[1]);
            $S_LPDoubleTime = $minutes4 - $arriveJobTime;
            $SundayLPDoubleTime = $this->convertMinuteToHours($S_LPDoubleTime);

        }
        //LP2Commercial
        if($data->job_type_id == "16" || $data->job_type_id == "20"){
              if($day== 'Sunday' && $get_clock_intime > $overtime_start && $get_clock_outtime <= $overtime_end) {
                     // hourly
            $MidTime =0;
             /* overtime_hourly */
             $arriveJobTime = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
             $mid1 = ($Midnight[0] * 60.0 + $Midnight[1]);
            $minutes8 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
            $clockout = ($clockouttime[0] * 60.0 + $clockouttime[1]);
            $result2 = abs($minutes8 - $clockout);
             $time2 = abs($arriveJobTime - $mid1);
              $SundayOvertime = $this->convertMinuteToHours($result2 + $time2);
                     // double time
            $clockINTime = ($clockintime[0] * 60.0 + $clockintime[1]);
              $mid2 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
            $time1 = abs($mid2 - $clockINTime);
            $SundayLPDoubleTime = $this->convertMinuteToHours($time1);
                // plant_time_hourly
            $arrive = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
            $time25 = abs($minutes8 - $arrive);
            $SundayLPOvertime = $this->convertMinuteToHours($time25);
            $SundayDoubleTime = 0;
          TimeLogs::where('id',$data->id)
        ->update([
            'double_time_hourly'=> $SundayLPDoubleTime,
                'plant_doubletime'=>$SundayDoubleTime,
            'lp2_doubletime'=>$SundayDoubleTime,
            'overtime_hourly_lp2'=>$SundayLPOvertime,
            'overtime_hourly'=> $SundayOvertime,
            'hourly'=> $MidTime
        ]);

        }else {
              TimeLogs::where('id',$data->id)
        ->update([
            'double_time_hourly'=> $SundayDoubleTime,
            'lp2_doubletime'=>$SundayLPDoubleTime,
            'overtime_hourly_lp2'=>$SundayLPOvertime,
            'overtime_hourly'=> $SundayOvertime,
            'hourly'=> $MidTime
        ]);

        }
        //LP4Commercial
        }elseif($data->job_type_id == "19" || $data->job_type_id == "24"){
                if($day== 'Sunday' && $get_clock_intime > $overtime_start && $get_clock_outtime <= $overtime_end) {
                     // hourly
            $MidTime =0;
             /* overtime_hourly */
             $arriveJobTime = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
             $mid1 = ($Midnight[0] * 60.0 + $Midnight[1]);
            $minutes8 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
            $clockout = ($clockouttime[0] * 60.0 + $clockouttime[1]);
            $result2 = abs($minutes8 - $clockout);
             $time2 = abs($arriveJobTime - $mid1);
              $SundayOvertime = $this->convertMinuteToHours($result2 + $time2);
                     // double time
            $clockINTime = ($clockintime[0] * 60.0 + $clockintime[1]);
              $mid2 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
            $time1 = abs($mid2 - $clockINTime);
            $SundayLPDoubleTime = $this->convertMinuteToHours($time1);
                // plant_time_hourly
            $arrive = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
            $time25 = abs($minutes8 - $arrive);
            $SundayLPOvertime = $this->convertMinuteToHours($time25);
            $SundayDoubleTime = 0;
             TimeLogs::where('id',$data->id)
        ->update([
            'double_time_hourly'=> $SundayLPDoubleTime,
           'plant_doubletime'=>$SundayDoubleTime,
            'overtime_hourly_lp4'=>$SundayLPOvertime,
            'overtime_hourly'=> $SundayOvertime,
            'hourly'=> $MidTime
        ]);
        }else {
             TimeLogs::where('id',$data->id)
        ->update([
            'double_time_hourly'=> $SundayDoubleTime,
            'lp4_doubletime'=>$SundayLPDoubleTime,
            'overtime_hourly_lp4'=>$SundayLPOvertime,
            'overtime_hourly'=> $SundayOvertime,
            'hourly'=> $MidTime
        ]);
        }


        //Industrial
        }elseif($data->job_type_id == "14" || $data->job_type_id == "15"){
            if($day== 'Sunday' && $get_clock_intime > $overtime_start && $get_clock_outtime <= $overtime_end) {
               // hourly
            $MidTime =0;
             /* overtime_hourly */
             $arriveJobTime = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
             $mid1 = ($Midnight[0] * 60.0 + $Midnight[1]);
            $minutes8 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
            $clockout = ($clockouttime[0] * 60.0 + $clockouttime[1]);
            $result2 = abs($minutes8 - $clockout);
             $time2 = abs($arriveJobTime - $mid1);
              $SundayOvertime = $this->convertMinuteToHours($result2 + $time2);
                   // double time
            $clockINTime = ($clockintime[0] * 60.0 + $clockintime[1]);
              $mid2 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
            $time1 = abs($mid2 - $clockINTime);
            $SundayLPDoubleTime = $this->convertMinuteToHours($time1);

            // plant_time_hourly
            $arrive = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
            $time25 = abs($minutes8 - $arrive);
            $SundayLPOvertime = $this->convertMinuteToHours($time25);
            $SundayDoubleTime = 0;
        TimeLogs::where('id',$data->id)
        ->update([
            'double_time_hourly'=> $SundayLPDoubleTime,
            'plant_doubletime'=>$SundayDoubleTime,
            'plant_overtime'=>$SundayLPOvertime,
            'overtime_hourly'=> $SundayOvertime,
            'hourly'=> $MidTime
        ]);
            }else {
                    TimeLogs::where('id',$data->id)
        ->update([
            'double_time_hourly'=> $SundayDoubleTime,
            'plant_doubletime'=>$SundayLPDoubleTime,
            'plant_overtime'=>$SundayLPOvertime,
            'overtime_hourly'=> $SundayOvertime,
            'hourly'=> $MidTime
        ]);
            }


        //LP2Industrial
    }elseif($data->job_type_id == "22" || $data->job_type_id == "28"){

          if($day== 'Sunday' && $get_clock_intime > $overtime_start && $get_clock_outtime <= $overtime_end) {
                     // hourly
            $MidTime =0;
             /* overtime_hourly */
             $arriveJobTime = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
             $mid1 = ($Midnight[0] * 60.0 + $Midnight[1]);
            $minutes8 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
            $clockout = ($clockouttime[0] * 60.0 + $clockouttime[1]);
            $result2 = abs($minutes8 - $clockout);
             $time2 = abs($arriveJobTime - $mid1);
              $SundayOvertime = $this->convertMinuteToHours($result2 + $time2);
                     // double time
            $clockINTime = ($clockintime[0] * 60.0 + $clockintime[1]);
              $mid2 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
            $time1 = abs($mid2 - $clockINTime);
            $SundayLPDoubleTime = $this->convertMinuteToHours($time1);
                // plant_time_hourly
            $arrive = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
            $time25 = abs($minutes8 - $arrive);
            $SundayLPOvertime = $this->convertMinuteToHours($time25);
            $SundayDoubleTime = 0;


        TimeLogs::where('id',$data->id)
        ->update([
            'double_time_hourly'=>$SundayLPDoubleTime,
            'plant_doubletime'=>$SundayDoubleTime,
            'lp2_plant_doubletime'=>$SundayDoubleTime,
            'plant_overtime_lp2'=>$SundayLPOvertime,
            'overtime_hourly'=> $SundayOvertime,
            'hourly'=> $MidTime
        ]);
          }else {

              TimeLogs::where('id',$data->id)
              ->update([
                  'double_time_hourly'=> $SundayDoubleTime,
                  'lp2_plant_doubletime'=>$SundayLPDoubleTime,
                  'plant_overtime_lp2'=>$SundayLPOvertime,
                  'overtime_hourly'=> $SundayOvertime,
                  'hourly'=> $MidTime
              ]);
          }

        //LP4Industrial
        }elseif($data->job_type_id == "18" || $data->job_type_id == "23"){
            if($day== 'Sunday' && $get_clock_intime > $overtime_start && $get_clock_outtime <= $overtime_end) {
                     // hourly
            $MidTime =0;
             /* overtime_hourly */
             $arriveJobTime = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
             $mid1 = ($Midnight[0] * 60.0 + $Midnight[1]);
            $minutes8 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
            $clockout = ($clockouttime[0] * 60.0 + $clockouttime[1]);
            $result2 = abs($minutes8 - $clockout);
             $time2 = abs($arriveJobTime - $mid1);
              $SundayOvertime = $this->convertMinuteToHours($result2 + $time2);
                     // double time
            $clockINTime = ($clockintime[0] * 60.0 + $clockintime[1]);
              $mid2 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
            $time1 = abs($mid2 - $clockINTime);
            $SundayLPDoubleTime = $this->convertMinuteToHours($time1);
                // plant_time_hourly
            $arrive = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
            $time25 = abs($minutes8 - $arrive);
            $SundayLPOvertime = $this->convertMinuteToHours($time25);
            $SundayDoubleTime = 0;
                 TimeLogs::where('id',$data->id)
            ->update([
                        'double_time_hourly'=>$SundayLPDoubleTime,
                'lp4_plant_doubletime'=>$SundayDoubleTime,
                'plant_overtime_lp4'=>$SundayLPOvertime,
                'overtime_hourly'=> $SundayOvertime,
                'hourly'=> $MidTime
            ]);

            }else {
                     TimeLogs::where('id',$data->id)
                ->update([
                    'double_time_hourly'=> $SundayDoubleTime,
                    'lp4_plant_doubletime'=>$SundayLPDoubleTime,
                    'plant_overtime_lp4'=>$SundayLPOvertime,
                    'overtime_hourly'=> $SundayOvertime,
                    'hourly'=> $MidTime
                ]);
            }


        }
    }
}

