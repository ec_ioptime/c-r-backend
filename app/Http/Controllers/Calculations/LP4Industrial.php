<?php

namespace App\Http\Controllers\Calculations;

use App\Http\Controllers\Calculations\MidSundayCalculations;
use App\Http\Controllers\Controller;
use App\TimeLogs;
use Carbon\Carbon;

class LP4Industrial extends Controller
{

    public function convertMinuteToHours($minutes)
    {
        return $minutes / 60;
    }

    public function calculations($data, $config, $mapping)
    {

        $holiday = $data->holiday;
        $day = Carbon::parse($data->clock_intime)->format('l');
        $day2 = Carbon::parse($data->clock_outtime)->format('l');

        $overtime_start = $config->start_overtime;
        $overtime_end = $config->end_overtime;
        $day3 = Carbon::parse($data->leave_yardtime)->format('l');

        $get_clock_intime = Carbon::parse($data->clock_intime)->format('H:i:s');
        $get_leave_yardtime = Carbon::parse($data->leave_yardtime)->format('H:i:s');
        $get_arrive_jobtime = Carbon::parse($data->arrive_jobtime)->format('H:i:s');
        $get_leave_jobtime = Carbon::parse($data->leave_jobtime)->format('H:i:s');
        $get_arrive_yardtime = Carbon::parse($data->arrive_yardtime)->format('H:i:s');
        $get_clock_outtime = Carbon::parse($data->clock_outtime)->format('H:i:s');
        $arriveYardDate = Carbon::parse($data->arrive_yardtime)->format('Y-m-d');
        $clockIndate = Carbon::parse($data->clock_intime)->format('Y-m-d');
        $ot_end = explode(':', $overtime_end);
        $ot_start = explode(':', $overtime_start);
        $clockintime = explode(':', $get_clock_intime);
        $leaveyardtime = explode(':', $get_leave_yardtime);
        $arrivejobtime = explode(':', $get_arrive_jobtime);
        $leavejobtime = explode(':', $get_leave_jobtime);
        $arriveyardtime = explode(':', $get_arrive_yardtime);
        $clockouttime = explode(':', $get_clock_outtime);

        $Midnight = explode(':', "00:00:00");
        $Midnight2 = explode(':', "24:00:00");

        //OvertimeOnJob = LeaveJobTime – ArriveJobTime

        $arrivejobtimes = Carbon::parse($data->arrive_jobtime);
        $leavejobtimes = Carbon::parse($data->leave_jobtime);

        $Onjobovertime = $leavejobtimes->diffInMinutes($arrivejobtimes);

        $JobOvertime = $this->convertMinuteToHours(abs($Onjobovertime));

        $clockIN = Carbon::parse($data->clock_intime);
        $clockOUT = Carbon::parse($data->clock_outtime);

        $totaltime = $clockOUT->diffInMinutes($clockIN);

        $overtime = $this->convertMinuteToHours(abs($totaltime)) - $JobOvertime;

        //If clockout and clockin days are not same

        $minutes33 = ($Midnight[0] * 60.0 + $Midnight[1]);
        $minutes32 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $DBTime = $minutes32 - $minutes33;

        $OTime = $this->convertMinuteToHours(abs($DBTime));

        $MidRegularOvertime = $overtime - $OTime;

        //If ALL TIME logged between ClockInTime and ClockOutTime is Between 3:00 pm and 7:00 am then
        //AllOvertime = True Goto IndustrialAllOvertime

        if ($get_clock_intime > $overtime_start && $get_arrive_jobtime > $overtime_start && $leavejobtime > $Midnight && $get_clock_outtime > $overtime_end && $day = "Saturday" && $day2 = "Sunday") {

            $minutes33 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
            $minutes32 = ($Midnight2[0] * 60.0 + $Midnight2[1]);

            $Plantovertime = $minutes32 - $minutes33;

            $PumpOvertime = $this->convertMinuteToHours(abs($Plantovertime));

            $minutes34 = ($clockintime[0] * 60.0 + $clockintime[1]);
            $minutes35 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

            $DBTime = $minutes34 - $minutes35;

            $OTime1 = $this->convertMinuteToHours(abs($DBTime));

            $MidRegularOvertime = $OTime1;

            $minutes36 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
            $minutes37 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

            $DBTime1 = $minutes37 - $minutes36;

            $OTime = $this->convertMinuteToHours(abs($DBTime1));

            $minutes38 = ($Midnight[0] * 60.0 + $Midnight[1]);
            $minutes39 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

            $DBTime3 = $minutes39 - $minutes38;

            $PLDTime = $this->convertMinuteToHours(abs($DBTime3));

            TimeLogs::where('id', $data->id)
                ->update(['overtime_hourly' => $MidRegularOvertime,
                    'plant_overtime_lp4' => $PumpOvertime, 'double_time_hourly' => $OTime,
                    'plant_doubletime' => $PLDTime]);

        } elseif ($get_clock_intime >= $overtime_start) {
            $arrivejobtime = Carbon::parse($data->arrive_jobtime);
            $leavejobtime = Carbon::parse($data->leave_jobtime);

            $OverTimeOnJob = $leavejobtime->diffInMinutes($arrivejobtime);

            $TotalOverTimeOnJob = $OverTimeOnJob;

            $OvertimeOnJob = $this->convertMinuteToHours($TotalOverTimeOnJob);

            //TimeOnJob = (ClockOutTime – ClockInTime ) - OvertimeOnJob
            $clockin = Carbon::parse($data->clock_intime);
            $clockout = Carbon::parse($data->clock_outtime);

            $OnJob = $clockin->diffInMinutes($clockout);

            $TimeOnJob = $OnJob - $TotalOverTimeOnJob;
            if (($get_clock_intime >= $overtime_start)) {
                $regularTime = 0;
                $TimeOnJob = $TimeOnJob - 15;

                $regularTime = $regularTime + 15;
                $regularTime = $this->convertMinuteToHours($regularTime);
            }
            $TimeOnJob = $this->convertMinuteToHours($TimeOnJob);

            if ($day == "Saturday" && $day2 == "Saturday") {

                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly' => $overtime, 'plant_overtime_lp4' => $JobOvertime]);

            } elseif ($day == "Saturday" && $day2 == "Sunday") {
                $arrivejobtimeEx = explode(':', $get_arrive_jobtime);
                $leavejobtimeEx = explode(':', $get_leave_jobtime);
                //  Double-Time Hourly = clock out - leave job time
                $leaveJobTime = ($leavejobtimeEx[0] * 60.0 + $leavejobtimeEx[1]);
                $clockOutTime = ($clockouttime[0] * 60.0 + $clockouttime[1]);
                $OTime = $this->convertMinuteToHours($clockOutTime - $leaveJobTime);
                // over time hourly 1.5x   = clock in - arrive job time
                $clockOutTime = ($clockintime[0] * 60.0 + $clockintime[1]);
                $arriveJobTime = ($arrivejobtimeEx[0] * 60.0 + $arrivejobtimeEx[1]);
                $MidRegularOvertime = $this->convertMinuteToHours($arriveJobTime - $clockOutTime);
                // plant overtime lp2-1/2 = 12AM - arrive Job time
                $MidNighte = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                $JobOvertime = $this->convertMinuteToHours($MidNighte - $arriveJobTime);
                // dd( $MidRegularOvertime,$JobOvertime,$OTime );
                // lp2_plant_doubletime = 12AM - leave job time
                $MidNighte1 = ($Midnight[0] * 60.0 + $Midnight[1]);
                $lp4_plant_doubletime = $this->convertMinuteToHours($leaveJobTime - $MidNighte1);
                // dd( $lp4_plant_doubletime);
                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly' => $MidRegularOvertime,
                        'plant_overtime_lp4' => $JobOvertime, 'double_time_hourly' => $OTime, 'lp4_plant_doubletime' => $lp4_plant_doubletime]);

            } elseif ($holiday == "1") {

                TimeLogs::where('id', $data->id)
                    ->update(['double_time_hourly' => $overtime, 'lp4_plant_doubletime' => $JobOvertime]);

            } elseif ($day == "Sunday" && $day2 == "Sunday") {

                TimeLogs::where('id', $data->id)
                    ->update(['double_time_hourly' => $overtime, 'lp4_plant_doubletime' => $JobOvertime]);

            } elseif ($day == "Sunday" && $day2 == "Monday") {

                (new MidSundayCalculations)->calculations($data, $config, $mapping);

            } else if (($day != "Friday" && $day2 != "Saturday") && $day != $day2 && $day2 != $day3 && $get_clock_outtime <= $overtime_end && $get_clock_intime >= $overtime_start) {
                /* hourly */
                $arrivejobtime = explode(':', $get_arrive_jobtime);
                $arrivejobtime = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $overTimENd = $ot_end[0] * 60.0 + $ot_end[1];
                $hourly1 = $overTimENd - $arrivejobtime;

                $leavejobtime = explode(':', $get_leave_jobtime);
                $minutes11 = ($ot_start[0] * 60.0 + $ot_start[1]);
                $minutes12 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                $hourly2 = $minutes12 - $minutes11;

                $regularTime = $this->convertMinuteToHours(abs($hourly1 + $hourly2) + 15);

                // $overtime_hourly
                $clockout = ($clockouttime[0] * 60.0 + $clockouttime[1]);
                $clockIn = ($clockintime[0] * 60.0 + $clockintime[1]);
                $time1 = $clockIn - $minutes11;
                $time2 = $minutes11 - $clockout;
                $TimeOnJob = $this->convertMinuteToHours(abs($time1 + $time2) - 15);

                // hourly_lp2
                $arrivejobtime = Carbon::parse($data->arrive_jobtime);
                $leavejobtime = Carbon::parse($data->leave_jobtime);
                $time3 = $leavejobtime->diffInMinutes($arrivejobtime);
                $TimeOnJobHourly = $this->convertMinuteToHours(abs($time3));
                $OverTimeOnJobHourly = 0;
                TimeLogs::where('id', $data->id)
                    ->update(['plant_overtime_lp4' => $TimeOnJobHourly, 'overtime_hourly' => $TimeOnJob, "hourly" => $regularTime]);
            } else {

                TimeLogs::where('id', $data->id)
                    ->update(['plant_overtime_lp4' => $OvertimeOnJob, 'overtime_hourly' => $TimeOnJob, "hourly" => $regularTime]);

            }

        } elseif ($get_clock_intime < $overtime_end && $get_clock_outtime < $overtime_end && $get_arrive_jobtime < $overtime_end && $get_leave_jobtime < $overtime_end) {

            $arrivejobtime = Carbon::parse($data->arrive_jobtime);
            $leavejobtime = Carbon::parse($data->leave_jobtime);
            $OverTimeOnJob = $leavejobtime->diffInMinutes($arrivejobtime);

            $TotalOverTimeOnJob = $OverTimeOnJob;

            $OvertimeOnJob = $this->convertMinuteToHours($TotalOverTimeOnJob);

            //TimeOnJob = (ClockOutTime – ClockInTime ) - OvertimeOnJob
            $clockin = Carbon::parse($data->clock_intime);
            $clockout = Carbon::parse($data->clock_outtime);

            $OnJob = $clockin->diffInMinutes($clockout);

            $TimeOnJob = $OnJob - $TotalOverTimeOnJob;

            $TimeOnJob = $this->convertMinuteToHours($TimeOnJob);
            if ($day == "Saturday" && $day2 == "Saturday") {

                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly' => $overtime, 'plant_overtime_lp4' => $JobOvertime]);

            } elseif ($day == "Saturday" && $day2 == "Sunday") {

                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly' => $MidRegularOvertime,
                        'plant_overtime_lp4' => $JobOvertime, 'double_time_hourly' => $OTime]);

            } elseif ($holiday == "1") {

                TimeLogs::where('id', $data->id)
                    ->update(['double_time_hourly' => $overtime, 'lp4_plant_doubletime' => $JobOvertime]);

            } elseif ($day == "Sunday" && $day2 == "Sunday") {

                TimeLogs::where('id', $data->id)
                    ->update(['double_time_hourly' => $overtime, 'lp4_plant_doubletime' => $JobOvertime]);

            } elseif ($day == "Sunday" && $day2 == "Monday") {

                (new MidSundayCalculations)->calculations($data, $config, $mapping);

            } else {

                TimeLogs::where('id', $data->id)
                    ->update(['plant_overtime_lp4' => $OvertimeOnJob, 'overtime_hourly_lp4' => $TimeOnJob]);

            }

        } elseif ($get_clock_intime >= $overtime_end && $get_clock_outtime <= $overtime_start && $day == $day2) {

            $arrivejob = Carbon::parse($data->arrive_jobtime);
            $leavejob = Carbon::parse($data->leave_jobtime);

            $Total = $leavejob->diffInMinutes($arrivejob);

            $TotalTimeOnJob = $Total;

            $TimeOnJob = $this->convertMinuteToHours($TotalTimeOnJob);

            if ($get_clock_intime > $overtime_end) {

                $minutes1 = ($clockintime[0] * 60.0 + $clockintime[1]);
                $minutes2 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

            } else {

                $minutes1 = ($ot_end[0] * 60.0 + $ot_end[1]);
                $minutes2 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

            }

            $Time1 = $minutes2 - $minutes1;

            if ($get_clock_outtime < $overtime_start) {

                $minutes3 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                $minutes4 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

            } else {

                $minutes3 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                $minutes4 = ($ot_start[0] * 60.0 + $ot_start[1]);

            }

            $Time2 = $minutes4 - $minutes3;

            $TotalHourlyTime = $Time1 + $Time2;

            $HourlyTime = $this->convertMinuteToHours($TotalHourlyTime);

            if ($day == "Saturday" && $day2 == "Saturday") {

                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly' => $overtime, 'plant_overtime_lp4' => $JobOvertime]);

            } elseif ($day == "Saturday" && $day2 == "Sunday") {

                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly' => $MidRegularOvertime,
                        'plant_overtime_lp4' => $JobOvertime, 'double_time_hourly' => $OTime]);

            } elseif ($holiday == "1") {

                TimeLogs::where('id', $data->id)
                    ->update(['double_time_hourly' => $overtime, 'lp4_plant_doubletime' => $JobOvertime]);

            } elseif ($day == "Sunday" && $day2 == "Sunday") {

                TimeLogs::where('id', $data->id)
                    ->update(['double_time_hourly' => $overtime, 'lp4_plant_doubletime' => $JobOvertime]);

            } elseif ($day == "Sunday" && $day2 == "Monday") {

                (new MidSundayCalculations)->calculations($data, $config, $mapping);

            } else {
                ;
                /* if no overtime */
                if ($get_clock_intime >= $overtime_end && $get_clock_outtime <= $overtime_start) {

                    TimeLogs::where('id', $data->id)
                        ->update(['hourly' => $HourlyTime, 'planttime_hourly_lp4' => $TimeOnJob]);
                } else {

                    TimeLogs::where('id', $data->id)
                        ->update(['hourly_lp4' => $HourlyTime, 'planttime_hourly_lp4' => $TimeOnJob]);
                }

            }

        } else {

            if ($get_arrive_jobtime <= $overtime_end && $get_leave_jobtime <= $overtime_start) {
                $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes2 = ($ot_end[0] * 60.0 + $ot_end[1]);

                $OverTimeOnJob = $minutes2 - $minutes1;

                //True
            } elseif ($get_arrive_jobtime <= $overtime_end && $get_leave_jobtime >= $overtime_start) {
                $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes2 = ($ot_end[0] * 60.0 + $ot_end[1]);

                $minutes21 = ($ot_start[0] * 60.0 + $ot_start[1]);
                $minutes22 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $Time1 = $minutes2 - $minutes1;
                $Time2 = $minutes22 - $minutes21;

                $OverTimeOnJob = $Time1 + $Time2;

                //True
            } elseif ($get_clock_intime <= $overtime_end && $get_arrive_jobtime > $overtime_end &&
                $leavejobtime < $Midnight) {

                $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes2 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $Time1 = $minutes2 - $minutes1;

                $OverTimeOnJob = $Time1;

            } elseif ($get_clock_intime <= $overtime_end && $get_arrive_jobtime > $overtime_end &&
                $leavejobtime > $Midnight) {
                // OverTimeOnJob =  time actually spent on the job that is worked between 3:00 pm and 7:00 am AND is logged between ArriveJobTime and LeaveJobTime
                if ($get_arrive_jobtime > $overtime_start) {

                    /* if leave job time is before 7AM */
                    if ($get_leave_jobtime <= $overtime_end) {
                        // over time = leave job time - arrive job time
                        $OverTimeOnJob = $leavejobtime[0] * 60.0 + $leavejobtime[1] - $arrivejobtime[0] * 60.0 - $arrivejobtime[1];
                    } else {
                        //over time = 7AM - arrive job time
                        $OverTimeOnJob2 = date("g", strtotime($get_arrive_jobtime)) - date("g", strtotime($overtime_end));
                        $OverTimeOnJob = (str_replace('-', '', $OverTimeOnJob2)+"12") * 60;
                    }

                } else {

                    // if arrive job is not in overtime start AND leave job time is in between overtime start and end
                    // for calculatine Plant Overtime LP4
                    if ($get_arrive_jobtime < $overtime_start || $get_leave_jobtime < $overtime_end) {

                        //  JobOvertimeOne = time from 3PM to 12AM
                        $MidNighte = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                        $overTimeStart = $ot_start[0] * 60.0 + $ot_start[1];
                        $leaveJobTime = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                        $JobOvertimeOne = $MidNighte - $overTimeStart;

                        //  JobOvertimeOne = time from 12PM to LEAVE JOB TIME
                        $MidNighte1 = ($Midnight[0] * 60.0 + $Midnight[1]);
                        $JobOvertimeOTwo = $leaveJobTime - $MidNighte1;
                        $OverTimeOnJob = $JobOvertimeOne + $JobOvertimeOTwo;

                    } else {

                        $OverTimeOnJob = 0;
                    }
                }

// dd( $OverTimeOnJob);
                // $minutes1 = ($ot_start[0] * 60.0 + $ot_start[1]);
                // $minutes2 = ($Midnight2[0] * 60.0 + $Midnight2[1]);

                // $minutes21 = ($Midnight[0] * 60.0 + $Midnight[1]);
                // $minutes22 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                // $Time1 = $minutes2 - $minutes1;
                // $Time2 = $minutes22 - $minutes21;

                // $OverTimeOnJob = $Time1 + $Time2;
                // dd( $OverTimeOnJob );

            } elseif ($get_clock_intime > $overtime_end && $get_arrive_jobtime > $overtime_end &&
                $leavejobtime > $Midnight && $get_leave_jobtime < $overtime_end && $get_clock_outtime > $overtime_end) {

                $minutes1 = ($ot_start[0] * 60.0 + $ot_start[1]);
                $minutes2 = ($Midnight2[0] * 60.0 + $Midnight2[1]);

                $minutes21 = ($Midnight[0] * 60.0 + $Midnight[1]);
                $minutes22 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $Time1 = $minutes2 - $minutes1;
                $Time2 = $minutes22 - $minutes21;

                $OverTimeOnJob = $Time1 + $Time2;
                if ($day != $day2) {
                    $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                    $minutes2 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                    $Time1 = $minutes2 - $minutes1;

                    $arrivejob = Carbon::parse($data->arrive_jobtime);
                    $leavejob = Carbon::parse($data->leave_jobtime);

                    $OverTimeOnJob = $leavejob->diffInMinutes($arrivejob);

                }
            } elseif ($get_clock_intime > $overtime_end && $get_arrive_jobtime > $overtime_end &&
                $leavejobtime > $Midnight) {

                $minutes1 = ($ot_start[0] * 60.0 + $ot_start[1]);
                $minutes2 = ($Midnight2[0] * 60.0 + $Midnight2[1]);

                $minutes21 = ($Midnight[0] * 60.0 + $Midnight[1]);
                $minutes22 = ($ot_end[0] * 60.0 + $ot_end[1]);

                $Time1 = $minutes2 - $minutes1;
                $Time2 = $minutes22 - $minutes21;

                $OverTimeOnJob = $Time1 + $Time2;
            } elseif ($get_arrive_jobtime >= $overtime_end && $get_leave_jobtime <= $overtime_start) {

                $OverTimeOnJob = 0;

                //True
            } elseif ($get_arrive_jobtime <= $overtime_start && $get_leave_jobtime >= $overtime_start) {

                $minutes1 = ($ot_start[0] * 60.0 + $ot_start[1]);
                $minutes2 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $OverTimeOnJob = $minutes2 - $minutes1;

                //True
            } elseif ($get_arrive_jobtime >= $overtime_start && $get_leave_jobtime >= $overtime_start) {

                $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes2 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $OverTimeOnJob = $minutes2 - $minutes1;

                //True
            } else {

                $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes2 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $OverTimeOnJob = $minutes2 - $minutes1;

            }

            //OverTimeOnJob = time actually spent on the job that is worked between 3:00 pm and 7:00 am AND is logged between ArriveJobTime and LeaveJobTime

            $TotalOverTimeOnJob = $OverTimeOnJob;
            $OverTimeOnJobHourly = $this->convertMinuteToHours($TotalOverTimeOnJob);
            if ($get_arrive_jobtime <= $overtime_end && $get_clock_intime < $overtime_end && $get_leave_jobtime >= $overtime_start) {
                //(LeaveJobTime – ArriveJobTime ) – OvertimeOnJob
                $minutes3 = ($ot_end[0] * 60.0 + $ot_end[1]);
                $minutes4 = ($ot_start[0] * 60.0 + $ot_start[1]);

                $result = $minutes4 - $minutes3;

            } elseif ($get_clock_intime <= $overtime_end && $get_arrive_jobtime > $overtime_end &&
                $leavejobtime < $Midnight) {

                $minutes3 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes4 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $Time1 = $minutes4 - $minutes3;

                $result = $Time1;

            } elseif ($get_clock_intime <= $overtime_end && $get_arrive_jobtime > $overtime_end &&
                $leavejobtime > $Midnight) {
                $minutes3 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes4 = ($ot_start[0] * 60.0 + $ot_start[1]);

                $result = $minutes4 - $minutes3;

            } elseif ($get_clock_intime > $overtime_end && $get_arrive_jobtime > $overtime_end &&
                $leavejobtime > $Midnight) {

                $minutes3 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes4 = ($Midnight2[0] * 60.0 + $Midnight2[1]);

                $minutes27 = ($Midnight[0] * 60.0 + $Midnight[1]);
                $minutes28 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $Time1 = $minutes4 - $minutes3;
                $Time2 = $minutes28 - $minutes27;

                $result = $Time1 + $Time2;
                if ($day != $day2) {

                    $result = 0;
                }

            } else {

                //(LeaveJobTime – ArriveJobTime ) – OvertimeOnJob
                $minutes3 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes4 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                $result = $minutes4 - $minutes3;

            }
            $TimeOnJob = $result;
            // PLANT TIME HOURLY LP4
            $TimeOnJobHourly = $this->convertMinuteToHours($TimeOnJob);
            //RegularOvertime = all time worked between 3:00 pm and 7:00 am that is NOT logged between ArriveJobTime and LeaveJobTime

            if ($get_leave_jobtime <= $overtime_start && $get_arrive_jobtime <= $overtime_end) {

                if ($get_leave_jobtime < $overtime_start) {

                    $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes6 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                    $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $re1 = $minutes6 - $minutes5;
                    $re2 = $minutes8 - $minutes7;

                    if ($get_clock_outtime < $overtime_start && $get_leave_jobtime < $overtime_start) {

                        $result = $re1;

                    } else {

                        $result = $re1 + $re2;

                    }

                } else {

                    $leavetime = Carbon::parse($data->leave_jobtime);
                    $clockout = Carbon::parse($data->clock_outtime);

                    $result = $leavetime->diffInMinutes($clockout);

                }

                $Regular1 = $result;

            } elseif ($get_leave_jobtime <= $overtime_start && $get_arrive_jobtime >= $overtime_end && $get_clock_outtime >= $overtime_start) {

                if ($get_clock_intime < $overtime_end && $get_leave_jobtime < $overtime_start && $get_clock_outtime >= $overtime_start) {

                    $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes6 = ($ot_end[0] * 60.0 + $ot_end[1]);

                    $result = $minutes6 - $minutes5;
                    $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $result2 = $minutes8 - $minutes7;

                    $Regular1 = $result + $result2;

                } elseif ($get_clock_intime < $overtime_end && $get_arrive_yardtime < $overtime_start && $get_clock_outtime >= $overtime_start) {
                    $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes6 = ($ot_end[0] * 60.0 + $ot_end[1]);

                    $Regular1 = $minutes6 - $minutes5;

                } elseif ($get_clock_outtime <= $overtime_start) {

                    $minutes5 = ($clockouttime[0] * 60.0 + $clockouttime[1]);
                    $minutes6 = ($ot_start[0] * 60.0 + $ot_start[1]);

                    $Regular1 = $minutes6 - $minutes5;

                    {
                        $minutes3 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                        $minutes6 = ($ot_start[0] * 60.0 + $ot_start[1]);
                        $time1 = $minutes3 - $minutes6;

                        $minutes28 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                        $minutes7 = ($ot_end[0] * 60.0 + $ot_end[1]);
                        $time2 = $minutes7 - $minutes28;
                        $Regular1 = $time1 + $time2;
                    }
                } elseif ($get_leave_jobtime < $overtime_start && $get_clock_outtime > $overtime_start) {

                    if ($get_clock_intime < $overtime_end && $get_leave_jobtime < $overtime_start) {

                        $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                        $minutes6 = ($ot_end[0] * 60.0 + $ot_end[1]);

                        $Reg1 = $minutes6 - $minutes5;

                        $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
                        $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                        $Reg2 = $minutes8 - $minutes7;

                        $Regular1 = $Reg1 + $Reg2;

                    } else {

                        $minutes5 = ($clockouttime[0] * 60.0 + $clockouttime[1]);
                        $minutes6 = ($ot_start[0] * 60.0 + $ot_start[1]);

                        $Regular1 = $minutes5 - $minutes6;
                    }

                } else {

                    $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes6 = ($ot_end[0] * 60.0 + $ot_end[1]);

                    $Reg1 = $minutes6 - $minutes5;

                    $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $Reg2 = $minutes8 - $minutes7;

                    $Regular1 = $Reg1 + $Reg2;

                }

            } elseif ($get_leave_jobtime <= $overtime_start && $get_arrive_jobtime <= $overtime_end) {

                $minutes5 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                $minutes6 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                $Regular1 = 0;

            } elseif ($get_leave_jobtime <= $overtime_start && $get_arrive_jobtime >= $overtime_end) {
                if ($get_clock_intime > $overtime_end && $get_arrive_jobtime > $overtime_end &&
                    $leavejobtime > $Midnight && $get_leave_jobtime < $overtime_end && $get_clock_outtime > $overtime_end) {

                    $minutes5 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                    $minutes6 = ($ot_end[0] * 60.0 + $ot_end[1]);

                    $result = $minutes6 - $minutes5;

                } elseif ($get_clock_intime > $overtime_end && $get_clock_outtime < $overtime_start && $get_arrive_jobtime > $overtime_end && $get_leave_jobtime < $overtime_start) {

                    $result = 0;

                } elseif ($get_leave_jobtime <= $overtime_start) {

                    // all time worked between 3:00 pm and 7:00 am that is NOT logged between
                    // ArriveJobTime and LeaveJobTime
                    $leaveJobTime = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                    $overtimeEnd = ($ot_end[0] * 60.0 + $ot_end[1]);

                    $result = $overtimeEnd - $leaveJobTime;
                    if ($day != $day2) {
                        $minutes7 = ($ot_end[0] * 60.0 + $ot_end[1]);
                        $minutes8 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                        $result = abs($minutes8 - $minutes7);

                    }

                } else {

                    $minutes5 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                    $minutes6 = ($clockouttime[0] * 60.0 + $clockouttime[1]);
                    $result = 0;

                }

                $Regular1 = $result;

            } elseif ($get_clock_intime >= $overtime_end) {

                $leavetime = Carbon::parse($data->leave_jobtime);
                $clockout = Carbon::parse($data->clock_outtime);

                $Regular2 = $leavetime->diffInMinutes($clockout);
                $Regular1 = $Regular2;

            } elseif ($get_leave_jobtime >= $overtime_start && $get_arrive_jobtime >= $overtime_end) {

                $leavetime = Carbon::parse($data->leave_jobtime);
                $clockout = Carbon::parse($data->clock_outtime);

                $Regular2 = $leavetime->diffInMinutes($clockout);

                /*$minutes5 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                $minutes6 = ($clockouttime[0] * 60.0 + $clockouttime[1]);*/

                $minutes22 = ($clockintime[0] * 60.0 + $clockintime[1]);
                $minutes23 = ($ot_end[0] * 60.0 + $ot_end[1]);

                //$Regular2 = $minutes6 - $minutes5;

                $Regular3 = $minutes23 - $minutes22;

                $Regular1 = $Regular2 + $Regular3;

            } elseif ($get_arrive_jobtime <= $overtime_end && $get_clock_intime < $overtime_end && $get_leave_jobtime >= $overtime_start) {

                $minutes22 = ($clockintime[0] * 60.0 + $clockintime[1]);
                $minutes23 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                $Regular3 = $minutes23 - $minutes22;

                $leavetime = Carbon::parse($data->leave_jobtime);
                $clockout = Carbon::parse($data->clock_outtime);

                $Regular2 = $leavetime->diffInMinutes($clockout);

                $Regular1 = $Regular2 + $Regular3;

            }

            if ($get_clock_intime < $overtime_end) {

                $TotalHourly = $Regular1 - 15;

            } else {

                $TotalHourly = $Regular1;

            }
            $RegularOvertime = $this->convertMinuteToHours($TotalHourly);
            //HourlyTime = all time worked between 7:00 am and ArriveJobTime  AND all time worked between LeaveJobTime and 3:00 pm.

            if ($get_arrive_jobtime <= $overtime_end) {

                if ($get_arrive_jobtime < $overtime_end && $get_leave_jobtime < $overtime_start && $get_clock_intime < $overtime_end && $get_clock_outtime < $overtime_start) {

                    if ($get_clock_outtime < $overtime_start && $get_leave_jobtime < $overtime_start) {

                        $leavetime = Carbon::parse($data->leave_jobtime);
                        $clockout = Carbon::parse($data->clock_outtime);

                        $total = $leavetime->diffInMinutes($clockout);

                    } else {

                        $minutes9 = ($ot_end[0] * 60.0 + $ot_end[1]);
                        $minutes10 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                        $total = $minutes9 - $minutes10;

                    }

                    $result = $total;

                } elseif ($get_arrive_jobtime < $overtime_end && $get_leave_jobtime < $overtime_start) {

                    $minutes15 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                    $minutes16 = ($ot_start[0] * 60.0 + $ot_start[1]);

                    $tt2 = $minutes16 - $minutes15;

                    $result = $tt2;

                } elseif ($get_arrive_jobtime <= $overtime_end && $get_clock_intime < $overtime_end && $get_leave_jobtime >= $overtime_start) {

                    $minutes9 = ($ot_end[0] * 60.0 + $ot_end[1]);
                    $minutes10 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                    $minutes15 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes16 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                    $tt1 = $minutes9 - $minutes10;
                    $tt2 = $minutes16 - $minutes15;

                    $result = $tt1 + $tt2;

                } else {

                    $minutes9 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes10 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $minutes15 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes16 = ($leaveyardtime[0] * 60.0 + $leaveyardtime[1]);

                    $tt1 = $minutes10 - $minutes9;
                    $tt2 = $minutes16 - $minutes15;

                    $result = $tt1 + $tt2;

                }

                $HourlyTime1 = $result;

            } elseif ($get_clock_intime >= $overtime_end) {

                $leavetime = Carbon::parse($data->leave_jobtime);
                $clockout = Carbon::parse($data->clock_outtime);

                $tt1 = $leavetime->diffInMinutes($clockout);

                $clocktime = Carbon::parse($data->clock_intime);
                $arrivetime = Carbon::parse($data->arrive_jobtime);

                $tt2 = $clocktime->diffInMinutes($arrivetime);

                /*$minutes9  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                $minutes10 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                $minutes15  = ($clockintime[0] * 60.0 + $clockintime[1]);
                $minutes16  = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                $tt1 = $minutes10 - $minutes9;

                $tt2 = $minutes16 - $minutes15;*/

                $HourlyTime1 = $tt1 + $tt2 - $TotalHourly;

            } elseif ($get_clock_intime <= $overtime_end && $get_leave_jobtime <= $overtime_start) {

                if ($get_leave_jobtime < $overtime_start && $get_clock_intime < $overtime_end && $get_clock_outtime < $overtime_start) {

                    $minutes11 = ($ot_end[0] * 60.0 + $ot_end[1]);
                    $minutes12 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                    $leavetime = Carbon::parse($data->leave_jobtime);
                    $clockout = Carbon::parse($data->clock_outtime);

                    $res1 = $minutes12 - $minutes11;

                    $res2 = $leavetime->diffInMinutes($clockout);

                    $tt2 = $res1 + $res2;

                } elseif ($get_leave_jobtime < $overtime_start && $get_clock_outtime < $overtime_start) {

                    $minutes11 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                    $minutes12 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $tt2 = $minutes12 - $minutes11;

                } elseif ($get_leave_jobtime < $overtime_start && $get_clock_outtime > $overtime_start) {


                    $minutes13 = ($ot_end[0] * 60.0 + $ot_end[1]);
                    $minutes14 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                    $result = $minutes14 - $minutes13;

                    $minutes11 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                    $minutes12 = ($ot_start[0] * 60.0 + $ot_start[1]);

                    $result2 = $minutes12 - $minutes11;

                    $tt2 = $result + $result2;

                } else {

                    $minutes11 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes12 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                    $tt3 = $minutes11 - $minutes12;

                    $minutes9 = ($ot_end[0] * 60.0 + $ot_end[1]);
                    $minutes10 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                    $tt1 = $minutes10 - $minutes9;

                    $tt2 = $tt3 + $tt1;

                }
                $HourlyTime1 = $tt2;

            } elseif ($get_clock_intime <= $overtime_end && $get_leave_jobtime >= $overtime_start) {

                $minutes9 = ($ot_end[0] * 60.0 + $ot_end[1]);
                $minutes10 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                $tt1 = $minutes10 - $minutes9;

                $HourlyTime1 = $tt1;

            } else {

                $minutes9 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                $minutes10 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                $HourlyTime1 = $minutes10 - $minutes9;

            }

            //$HourlyTime2 = $minutes12 - $minutes11;

            if ($get_clock_intime < $overtime_end) {

                $HourlyTime3 = $HourlyTime1 + 15;

            } else {

                $HourlyTime3 = $HourlyTime1;

            }

            $Hourly = $this->convertMinuteToHours($HourlyTime3);

            if ($day == "Saturday" && $day2 == "Saturday") {

                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly' => $overtime, 'plant_overtime_lp4' => $JobOvertime]);

            } elseif ($day == "Saturday" && $day2 == "Sunday") {

                $minutes33 = ($clockintime[0] * 60.0 + $clockintime[1]);
                $minutes32 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                $minutes35 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                $midnight_time = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                $midnight_time2 = ($Midnight[0] * 60.0 + $Midnight[1]);

                $DBTime = $minutes32 - $minutes33;

                $arrivejobtimes = Carbon::parse($data->arrive_jobtime);
                $leavejobtimes = Carbon::parse($data->leave_jobtime);

                $Onjobovertime = $minutes32 - $midnight_time;

                $JobOvertime = $this->convertMinuteToHours(abs($Onjobovertime));

                $overtime2 = $midnight_time2 - $minutes35;
                $OTime2 = $this->convertMinuteToHours(abs($overtime2));
                $OTime = abs($OTime2 - $OTime);

                $MidRegularOvertime = $this->convertMinuteToHours(abs($DBTime));
                TimeLogs::where('id', $data->id)
                    ->update(['overtime_hourly' => $MidRegularOvertime,
                        'plant_overtime_lp4' => $JobOvertime, 'double_time_hourly' => $OTime, "lp4_plant_doubletime" => $OTime2]);

            } elseif ($holiday == "1") {

                TimeLogs::where('id', $data->id)
                    ->update(['double_time_hourly' => $overtime, 'lp4_plant_doubletime' => $JobOvertime]);

            } elseif ($day == "Sunday" && $day2 == "Sunday") {

                TimeLogs::where('id', $data->id)
                    ->update(['double_time_hourly' => $overtime, 'lp4_plant_doubletime' => $JobOvertime]);

            } elseif ($day == "Sunday" && $day2 == "Monday") {

                (new MidSundayCalculations)->calculations($data, $config, $mapping);

            } elseif ($get_clock_intime == $get_clock_outtime && $day == $day2) {

                TimeLogs::where('id', $data->id)
                    ->update(['plant_overtime_lp4' => '0.00', 'planttime_hourly_lp4' => '0.00', 'hourly' => '0.00', 'overtime_hourly' => '0.00', 'hourly_lp4' => '0.00', 'overtime_hourly_lp4' => '0.00']);

            } else if ($get_clock_intime <= $overtime_end && $get_arrive_jobtime > $overtime_end &&
                $leavejobtime > $Midnight && $day2 === $day) {
                // to fix invoice 2020

                TimeLogs::where('id', $data->id)
                    ->update(['plant_overtime_lp4' => 0, 'planttime_hourly_lp4' => $JobOvertime, 'hourly' => $Hourly, 'overtime_hourly' => $RegularOvertime]);
            } else {

                if ($day != $day2 && $day == "Friday" && $day2 == "Saturday" && $get_clock_intime >= $overtime_end && $get_clock_outtime <= $overtime_start) {

                    $arrivejobtime = Carbon::parse($data->arrive_jobtime);
                    $leavejobtime = Carbon::parse($data->leave_jobtime);

                    $OverTimeOnJob = $leavejobtime->diffInMinutes($arrivejobtime);

                    $TotalOverTimeOnJob = $OverTimeOnJob;

                    $OvertimeOnJob = $this->convertMinuteToHours($TotalOverTimeOnJob);

                    //TimeOnJob = (ClockOutTime – ClockInTime ) - OvertimeOnJob
                    $clockin = Carbon::parse($data->clock_intime);
                    $clockout = Carbon::parse($data->clock_outtime);

                    $OnJob = $clockin->diffInMinutes($clockout);

                    $TimeOnJob = $OnJob - $TotalOverTimeOnJob;

                    $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $regularTime = $minutes7 - $minutes5;
                    $TimeOnJob = $TimeOnJob - $regularTime;
                    $regularTime = $this->convertMinuteToHours($regularTime);
                    $TimeOnJob = $this->convertMinuteToHours($TimeOnJob);

                    TimeLogs::where('id', $data->id)
                        ->update(['plant_overtime_lp4' => $OvertimeOnJob, 'overtime_hourly' => $TimeOnJob, "hourly" => $regularTime]);

                } else if (($day == "Friday" && $day2 == "Saturday") && $day != $day2 && $day != $day2 && $get_clock_intime <= $overtime_end && $get_clock_outtime <= $overtime_start) {
                    $minutes11 = ($ot_end[0] * 60.0 + $ot_end[1]);
                    $minutes12 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                    $Hourly = $minutes12 - $minutes11 + 15;
                    $Hourly = $this->convertMinuteToHours($Hourly);

                    /* overtiem 1.5x */
                    $minutes7 = ($ot_end[0] * 60.0 + $ot_end[1]);
                    $clockin = ($clockintime[0] * 60.0 + $clockintime[1]);

                    $minutes8 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                    $clockout = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $result = abs($minutes7 - $clockin);
                    $result2 = abs($minutes8 - $clockout);
                    $res = abs($result + $result2 - 15);
                    $RegularOvertime = $this->convertMinuteToHours($res);
                    TimeLogs::where('id', $data->id)
                        ->update(['plant_overtime_lp4' => $OverTimeOnJobHourly, 'planttime_hourly_lp4' => $TimeOnJobHourly, 'hourly' => $Hourly, 'overtime_hourly' => $RegularOvertime]);
                } else if ($day != $day2 && $day != "Friday" && $day2 != "Saturday" && $get_clock_intime <= $overtime_end && $get_clock_outtime <= $overtime_end) {
                    // Hourly
                    $overTimeEnd = ($ot_end[0] * 60.0 + $ot_end[1]); /* 7am */
                    $arrivejobtime = explode(':', $get_arrive_jobtime);
                    $arrivejobtime = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                    $hourly1 = $arrivejobtime - $overTimeEnd;

                    $leave = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                    $overTimeStart = ($ot_start[0] * 60.0 + $ot_start[1]); /* 3pm */
                    $hourly2 = $overTimeStart - $leave;
                    $Hourly = $hourly1 + $hourly2;
                    $Hourly = $this->convertMinuteToHours($Hourly);
                    // RegularOvertime
                    $clockoutTime = ($clockouttime[0] * 60.0 + $clockouttime[1]);
                    $MidNighte = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                    $MidNighte0 = ($Midnight[0] * 60.0 + $Midnight[1]);
                    $clockinTime = ($clockintime[0] * 60.0 + $clockintime[1]);
                    if ($get_leave_jobtime < $overtime_start) {
                        $regularOt1 = $MidNighte - $overTimeStart;
                        $regularOt2 = $clockoutTime - $MidNighte0;
                    } else {
                        $regularOt1 = $MidNighte - $leave;
                        $regularOt2 = $clockoutTime - $MidNighte0;
                    }
                    $regular1 = abs($regularOt1) + abs($regularOt2);
                    $regular2 = $overTimeEnd - $clockinTime;
                    $RegularOvertime = $this->convertMinuteToHours($regular1 + $regular2);
                    // TimeOnJobHourly
                    $arrivejobtime = Carbon::parse($data->arrive_jobtime);
                    $leavejobtime = Carbon::parse($data->leave_jobtime);
                    $OverTimeOnJob = $leavejobtime->diffInMinutes($arrivejobtime);
                    $TimeOnJobHourly = $this->convertMinuteToHours($OverTimeOnJob);

                    $OverTimeOnJobHourly = 0;
                    TimeLogs::where('id', $data->id)
                        ->update(['plant_overtime_lp4' => $OverTimeOnJobHourly, 'planttime_hourly_lp4' => $TimeOnJobHourly, 'hourly' => $Hourly, 'overtime_hourly' => $RegularOvertime]);
                } else if (($day != "Friday" && $day2 != "Saturday") && $day != $day2 && $get_clock_intime <= $overtime_end && $get_clock_outtime <= $overtime_start && $clockIndate == $arriveYardDate && $get_clock_outtime > $overtime_end) {

                    $minutes11 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes12 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                    $arrivejobtime = explode(':', $get_arrive_jobtime);
                    $minutes6 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                    $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes7 = ($ot_end[0] * 60.0 + $ot_end[1]);
                    $hourly1 = $minutes7 - $minutes5;
                    $hourly2 = $minutes11 - $minutes12;
                    $Hourly = $this->convertMinuteToHours($hourly1 + $hourly2);
                    // overtime_hourly
                    $minutes12 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                    $overtime1 = $minutes7 - $minutes5;
                    $MidNighte = ($Midnight[0] * 60.0 + $Midnight[1]);
                    $overtime2 = $minutes11 - $MidNighte;
                    $RegularOvertime = $this->convertMinuteToHours($overtime2 + $overtime1);
                    // plant_hourly_lp2

                    $arrivejobtime = Carbon::parse($data->arrive_jobtime);
                    $leavejobtime = Carbon::parse($data->leave_jobtime);
                    $OverTimeOnJob = $leavejobtime->diffInMinutes($arrivejobtime);
                    $TimeOnJobHourly = $this->convertMinuteToHours($OverTimeOnJob);
                    // plant_overtime_lp2
                    $OverTimeOnJobHourly = 0;
                    TimeLogs::where('id', $data->id)
                        ->update(['plant_overtime_lp4' => $OverTimeOnJobHourly, 'planttime_hourly_lp4' => $TimeOnJobHourly, 'hourly' => $Hourly, 'overtime_hourly' => $RegularOvertime]);
                } else if (($day != "Friday" && $day2 != "Saturday") && $day != $day2 && $get_clock_intime <= $overtime_end && $get_clock_outtime <= $overtime_start) {
                    /* hourly */
                    $minutes11 = ($ot_end[0] * 60.0 + $ot_end[1]);
                    $minutes12 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

                    $Hourly = $minutes12 - $minutes11 + 15;
                    $Hourly = $this->convertMinuteToHours($Hourly);

                    /* overtiem 1.5x */
                    $minutes7 = ($ot_end[0] * 60.0 + $ot_end[1]);
                    $clockin = ($clockintime[0] * 60.0 + $clockintime[1]);

                    $minutes8 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
                    $clockout = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $result = abs($minutes7 - $clockin);
                    $result2 = abs($minutes8 - $clockout);
                    $res = abs($result + $result2 - 15);
                    $RegularOvertime = $this->convertMinuteToHours($res);
                    TimeLogs::where('id', $data->id)
                        ->update(['plant_overtime_lp4' => $OverTimeOnJobHourly, 'planttime_hourly_lp4' => $TimeOnJobHourly, 'hourly' => $Hourly, 'overtime_hourly' => $RegularOvertime]);

                } else {

                    $arrivejobtime = explode(':', $get_arrive_jobtime);
                    $arrivejobtime = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                    $minutes35 = ($Midnight2[0] * 60.0 + $Midnight2[1]);
                    $result = abs($arrivejobtime - $minutes35);

                    $minutes36 = ($Midnight[0] * 60.0 + $Midnight[1]);
                    $overTimeStart = $ot_end[0] * 60.0 + $ot_end[1];
                    $result2 = abs($minutes36 - $overTimeStart);

                    $OTimeOnJob = abs($result2 + $result);
                    $OverTimeOnJobHourly = $this->convertMinuteToHours($OTimeOnJob);

                    $otEnd = $ot_start[0] * 60.0 + $ot_start[1];
                    $leave = $leaveyardtime[0] * 60.0 + $leaveyardtime[1];

                    $arrivejobtime = Carbon::parse($data->arrive_jobtime);
                    $leavejobtime = Carbon::parse($data->leave_jobtime);

                    $OverTimeOnJob = $leavejobtime->diffInMinutes($arrivejobtime);
                    $TimeOnJobHourly = $this->convertMinuteToHours($OverTimeOnJob) - $OverTimeOnJobHourly;

                    $leavejobtime = explode(':', $get_leave_jobtime);
                    $minutes11 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes12 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

                    $arrivejobtime = explode(':', $get_arrive_jobtime);
                    $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
                    $minutes6 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
                    $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
                    $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

                    $re1 = abs($minutes6 - $minutes5);
                    $re2 = abs($minutes8 - $minutes7);

                    if ($get_clock_intime <= $overtime_start) {
                        $RegularOvertime = $this->convertMinuteToHours($re1 + $re2 - 15);
                        $tt3 = $minutes11 - $minutes12 + 15;

                    } else {
                        $RegularOvertime = $this->convertMinuteToHours($re1 + $re2);
                        $tt3 = $minutes11 - $minutes12;

                    }
                    $Hourly = $this->convertMinuteToHours($tt3);

                    TimeLogs::where('id', $data->id)
                        ->update(['plant_overtime_lp4' => $OverTimeOnJobHourly, 'planttime_hourly_lp4' => $TimeOnJobHourly, 'hourly' => $Hourly, 'overtime_hourly' => $RegularOvertime]);
                }

            }
        }
    }
}
