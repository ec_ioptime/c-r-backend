<?php

namespace App\Http\Controllers\Calculations;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\TimeLogs;
use App\ConfigMapping;
use App\JobConfiguration;
use App\Breaks;
use Exception;
use Carbon\Carbon;

class Test extends Controller
{

    public function convertMinuteToHours($minutes)
    {
        return $minutes / 60;
    }

    public function calculations($data,$config,$mapping){

        $holiday = $data->holiday;
        $day     = Carbon::parse($data->clock_intime)->format('l');
                
        $overtime_start = $config->start_overtime;
        $overtime_end   = $config->end_overtime;

        $get_clock_intime    = Carbon::parse($data->clock_intime)->format('H:i:s');
        $get_leave_yardtime  = Carbon::parse($data->leave_yardtime)->format('H:i:s');
        $get_arrive_jobtime  = Carbon::parse($data->arrive_jobtime)->format('H:i:s');
        $get_leave_jobtime   = Carbon::parse($data->leave_jobtime)->format('H:i:s');
        $get_arrive_yardtime = Carbon::parse($data->arrive_yardtime)->format('H:i:s');
        $get_clock_outtime   = Carbon::parse($data->clock_outtime)->format('H:i:s');

        $ot_end           = explode(':', $overtime_end);
        $ot_start         = explode(':', $overtime_start);
        $clockintime      = explode(':', $get_clock_intime);
        $leaveyardtime    = explode(':', $get_leave_yardtime);
        $arrivejobtime    = explode(':', $get_arrive_jobtime);
        $leavejobtime     = explode(':', $get_leave_jobtime);
        $arriveyardtime   = explode(':', $get_arrive_yardtime);
        $clockouttime     = explode(':', $get_clock_outtime);

        if($get_clock_intime >= $overtime_start){

        $arrivejobtime  = Carbon::parse($data->arrive_jobtime);
        $leavejobtime   = Carbon::parse($data->leave_jobtime);

        $OverTimeOnJob = $leavejobtime->diffInMinutes($arrivejobtime);

        $TotalOverTimeOnJob = $OverTimeOnJob;

        $OverTimeHourlyLP = $this->convertMinuteToHours($TotalOverTimeOnJob);

        $clockin    = Carbon::parse($data->clock_intime);

        $Sum1 =  $clockin->diffInMinutes($arrivejobtime);

        $clockout  = Carbon::parse($data->clock_outtime);

        $Sum2 =  $clockout->diffInMinutes($leavejobtime);

        $TotalHourly = $Sum1 + $Sum2 - 15;

        $RegularOvertime = $this->convertMinuteToHours($TotalHourly);

        $HourlyTime = 15;

        $Hourly = $this->convertMinuteToHours($HourlyTime);

        }else{

        //True
        if($get_arrive_jobtime <= $overtime_end && $get_leave_jobtime <= $overtime_start){

        $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
        $minutes2 = ($ot_end[0] * 60.0 + $ot_end[1]);

        $OverTimeOnJob = $minutes2 - $minutes1;

        //True
        }elseif($get_arrive_jobtime <= $overtime_end && $get_leave_jobtime >= $overtime_start){

        $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
        $minutes2 = ($ot_end[0] * 60.0 + $ot_end[1]);

        $minutes21 = ($ot_start[0] * 60.0 + $ot_start[1]);
        $minutes22 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

        $Time1 = $minutes2 - $minutes1;
        $Time2 = $minutes22 - $minutes21;

        $OverTimeOnJob = $Time1 + $Time2;
        
        //True
        }elseif($get_arrive_jobtime >= $overtime_end && $get_leave_jobtime <= $overtime_start){

        $OverTimeOnJob = 0;

        //True
        }elseif($get_arrive_jobtime <= $overtime_start && $get_leave_jobtime >= $overtime_start){

        $minutes1 = ($ot_start[0] * 60.0 + $ot_start[1]);
        $minutes2 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

        $OverTimeOnJob = $minutes2 - $minutes1;

        //True
        }elseif($get_arrive_jobtime >= $overtime_start && $get_leave_jobtime >= $overtime_start){

        $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
        $minutes2 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

        $OverTimeOnJob = $minutes2 - $minutes1;
        
        //True
        }else{

        $minutes1 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
        $minutes2 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

        $OverTimeOnJob = $minutes2 - $minutes1;

        }

        //OverTimeOnJob = time actually spent on the job that is worked between 3:00 pm and 7:00 am AND is logged between ArriveJobTime and LeaveJobTime

        $TotalOverTimeOnJob = $OverTimeOnJob;

        $OverTimeHourlyLP = $this->convertMinuteToHours($TotalOverTimeOnJob);

        //(LeaveJobTime – ArriveJobTime ) – OvertimeOnJob

        $minutes3 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);
        $minutes4 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

        $TimeOnJob = $minutes4 - $minutes3; 

        $TimeHourlyLP = $this->convertMinuteToHours($TimeOnJob) - $OverTimeHourlyLP;

        //RegularOvertime = all time worked between 3:00 pm and 7:00 am that is NOT logged between ArriveJobTime and LeaveJobTime

        if($get_leave_jobtime <= $overtime_start && $get_arrive_jobtime <= $overtime_end){

        if($get_leave_jobtime < $overtime_start){

        $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
        $minutes6 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

        $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
        $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $re1 = $minutes6 - $minutes5;
        $re2 = $minutes8 - $minutes7;
        
        $result = $re1 + $re2;

        }else{

        $minutes5 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes6 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $result = $minutes6 - $minutes5;

        }

        $Regular1 = $result;

        }elseif($get_leave_jobtime <= $overtime_start && $get_arrive_jobtime >= $overtime_end && $get_clock_outtime >= $overtime_start){

        if($get_clock_outtime <= $overtime_start){
            
        $minutes5 = ($clockouttime[0] * 60.0 + $clockouttime[1]);
        $minutes6 = ($ot_start[0] * 60.0 + $ot_start[1]);

        $Regular1 = $minutes6 - $minutes5;

        }elseif($get_leave_jobtime < $overtime_start && $get_clock_outtime > $overtime_start){

        $minutes5 = ($clockouttime[0] * 60.0 + $clockouttime[1]);
        $minutes6 = ($ot_start[0] * 60.0 + $ot_start[1]);

        $Regular1 = $minutes5 - $minutes6;

        }else{

        $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
        $minutes6 = ($ot_end[0] * 60.0 + $ot_end[1]);

        $Reg1 = $minutes6 - $minutes5;

        $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
        $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $Reg2 = $minutes8 - $minutes7;

        $Regular1 = $Reg1 + $Reg2;

        }

        }
        elseif($get_leave_jobtime <= $overtime_start && $get_arrive_jobtime <= $overtime_end){

        $minutes5 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes6 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $Regular1 = 0;

        }elseif($get_leave_jobtime <= $overtime_start && $get_arrive_jobtime >= $overtime_end){

        if($get_clock_intime > $overtime_end && $get_clock_outtime < $overtime_start && $get_arrive_jobtime > $overtime_end && $get_leave_jobtime < $overtime_start){

        $result = 0;



        }elseif($get_leave_jobtime <= $overtime_start){

        $minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
        $minutes6 = ($ot_end[0] * 60.0 + $ot_end[1]);

        $result = $minutes6 - $minutes5;

        }else{

        $minutes5 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes6 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $result = 0;

        }

        $Regular1 = $result;

        }elseif($get_clock_intime >= $overtime_end){

        $minutes5 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes6 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $Regular2 = $minutes6 - $minutes5;

        $Regular1 = $Regular2;

        }elseif($get_leave_jobtime >= $overtime_start && $get_arrive_jobtime >= $overtime_end){

        $minutes5 = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes6 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $minutes22 = ($clockintime[0] * 60.0 + $clockintime[1]);
        $minutes23 = ($ot_end[0] * 60.0 + $ot_end[1]);

        $Regular2 = $minutes6 - $minutes5;
        $Regular3 = $minutes23 - $minutes22;

        $Regular1 = $Regular2 + $Regular3;

        }

        /*$minutes5 = ($clockintime[0] * 60.0 + $clockintime[1]);
        $minutes6 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);*//*
        $minutes7 = ($ot_start[0] * 60.0 + $ot_start[1]);
        $minutes8 = ($clockouttime[0] * 60.0 + $clockouttime[1]);*/
        //$Regular2 = $minutes8 - $minutes7;

        if($get_clock_intime < $overtime_end){

        $TotalHourly =  $Regular1 - 15;

        }else{

        $TotalHourly =  $Regular1;

        }

        $RegularOvertime = $this->convertMinuteToHours($TotalHourly);

        //HourlyTime = all time worked between 7:00 am and ArriveJobTime  AND all time worked between LeaveJobTime and 3:00 pm. 

        if($get_arrive_jobtime <= $overtime_end){

        if($get_arrive_jobtime < $overtime_end && $get_leave_jobtime < $overtime_start && $get_clock_intime < $overtime_end && $get_clock_outtime < $overtime_start){

        $minutes9   = ($ot_end[0] * 60.0 + $ot_end[1]);
        $minutes10  = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

        $result = $minutes9 - $minutes10;

        }elseif($get_arrive_jobtime < $overtime_end && $get_leave_jobtime < $overtime_start){

        $minutes15  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes16  = ($ot_start[0] * 60.0 + $ot_start[1]);

        $tt2 = $minutes16 - $minutes15;

        $result = $tt2;
    
        }else{

        $minutes9  = ($ot_start[0] * 60.0 + $ot_start[1]);
        $minutes10  = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $minutes15  = ($clockintime[0] * 60.0 + $clockintime[1]);
        $minutes16  = ($leaveyardtime[0] * 60.0 + $leaveyardtime[1]);

        $tt1 = $minutes10 - $minutes9;
        $tt2 = $minutes16 - $minutes15;

        $result =  $tt1 + $tt2;

        }

        $HourlyTime1 = $result;

        }elseif($get_clock_intime >= $overtime_end){

        $minutes9  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes10 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $minutes15  = ($clockintime[0] * 60.0 + $clockintime[1]);
        $minutes16  = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

        $tt1 = $minutes10 - $minutes9;
        $tt2 = $minutes16 - $minutes15;

        $HourlyTime1 = $tt1 + $tt2 - $TotalHourly;

        }elseif($get_clock_intime <= $overtime_end && $get_leave_jobtime <= $overtime_start){

        if($get_leave_jobtime < $overtime_start && $get_clock_intime < $overtime_end && $get_clock_outtime < $overtime_start){

        $minutes11  = ($ot_end[0] * 60.0 + $ot_end[1]);
        $minutes12  = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

        $minutes13  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes14  = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $res1 = $minutes12 - $minutes11;

        $res2 = $minutes14 - $minutes13;

        $tt2 = $res1 + $res2;

        }elseif($get_leave_jobtime < $overtime_start && $get_clock_outtime < $overtime_start){

        $minutes11  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes12  = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $tt2 = $minutes12 - $minutes11;

        }elseif($get_leave_jobtime < $overtime_start && $get_clock_outtime > $overtime_start){

        $minutes13  = ($ot_end[0] * 60.0 + $ot_end[1]);
        $minutes14  = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

        $result     = $minutes14 - $minutes13;

        $minutes11  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes12  = ($ot_start[0] * 60.0 + $ot_start[1]);

        $result2    = $minutes12 - $minutes11;

        $tt2 = $result + $result2;

        }else{

        $minutes11  = ($ot_start[0] * 60.0 + $ot_start[1]);
        $minutes12  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);

        $tt3 = $minutes11 - $minutes12;

        $minutes9  = ($ot_end[0] * 60.0 + $ot_end[1]);
        $minutes10 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

        $tt1 = $minutes10 - $minutes9;

        $tt2 = $tt3 + $tt1;

        }

        $HourlyTime1 = $tt2;

        }elseif($get_clock_intime <= $overtime_end && $get_leave_jobtime >= $overtime_start){

        $minutes9  = ($ot_end[0] * 60.0 + $ot_end[1]);
        $minutes10 = ($arrivejobtime[0] * 60.0 + $arrivejobtime[1]);

        $tt1 = $minutes10 - $minutes9;

        $HourlyTime1 = $tt1;

        }else{

        $minutes9  = ($leavejobtime[0] * 60.0 + $leavejobtime[1]);
        $minutes10 = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $HourlyTime1 = $minutes10 - $minutes9;

        }
        
        //$HourlyTime2 = $minutes12 - $minutes11;

        if($get_clock_intime < $overtime_end){

        $HourlyTime3 = $HourlyTime1 + 15;

        }else{

        $HourlyTime3 = $HourlyTime1;

        }

        $Hourly = $this->convertMinuteToHours($HourlyTime3);
        
        }

        //$split = true;
        $minutes14  = ($clockintime[0] * 60.0 + $clockintime[1]);
        $minutes15  = ($clockouttime[0] * 60.0 + $clockouttime[1]);

        $totaltime  = $minutes15 - $minutes14;

        $overtime = $this->convertMinuteToHours(abs($totaltime));
      
        if($day == "Saturday"){

        TimeLogs::where('id',$data->id)->update(['overtime_hourly'=> $overtime]);

        }elseif($day == "Sunday" || $holiday == "1"){

        TimeLogs::where('id',$data->id)->update(['double_time_hourly'=> $overtime]);
  
        }elseif($get_clock_intime == $get_clock_outtime){

        TimeLogs::where('id',$data->id)->update(['hourly'=> '0.00','overtime_hourly'=> '0.00','overtime_hourly_lp2'=> '0.00','overtime_hourly_lp2'=> '0.00']);

        }else{  

        if($get_clock_intime >= $overtime_start){

        TimeLogs::where('id',$data->id)->update(['hourly'=> $Hourly,'overtime_hourly'=> $RegularOvertime,'overtime_hourly_lp2'=> $OverTimeHourlyLP]);
        
        }else{

        TimeLogs::where('id',$data->id)->update(['hourly'=> $Hourly,'overtime_hourly'=> $RegularOvertime,'hourly_lp2'=> $TimeHourlyLP,'overtime_hourly_lp2'=> $OverTimeHourlyLP]);

          }
       }
    }
}
