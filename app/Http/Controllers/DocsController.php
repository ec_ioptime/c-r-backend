<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Schema;

use Illuminate\Support\Facades\DB;

use Illuminate\Database\Schema\Builder;

use Illuminate\Http\Request;
use App\User;
use App\Report;
use App\Property;
class DocsController extends Controller
{
    public function addModelData($exploded,$value)
    {
        $model =[];
        if($exploded[1]=='user'){
            $usercolumns = DB::getSchemaBuilder()->getColumnListing('users');
            $arr = [];
            foreach ($usercolumns as $key => $value) {
                $temp ="";
                if($value!='updated_at'
                    && $value != 'created_at'
                    && $value != 'deleted_at'
                    && $value != 'email_verified_at'
                    && $value != 'id'
                    && $value !="remember_token"
                    && $value != ","
                ){
                    if($value=="image"){
                        $temp=$temp.$value." : "."jpg, png";
                        array_push($arr,$temp);
                    }else{

                        $temp = $temp.$value." : ".DB::getSchemaBuilder()->getColumnType('users',$value);
                        array_push($arr,$temp);
                    }
                }


            }
            $model =['user'=>$arr];
        }
        if($exploded[1]=='property'){
            $usercolumns = DB::getSchemaBuilder()->getColumnListing('properties');
            $arr = [];
            foreach ($usercolumns as $key => $value) {
                $temp ="";
                if($value!='updated_at'
                    && $value != 'created_at'
                    && $value != 'deleted_at'
                    && $value != 'email_verified_at'
                    && $value != 'id'
                    && $value !="remember_token"
                    && $value != ","
                ){
                    if($value=="image"){
                        $temp=$temp.$value." : "."jpg, png";
                        array_push($arr,$temp);
                    }else{

                        $temp = $temp.$value." : ".DB::getSchemaBuilder()->getColumnType('properties',$value);
                        array_push($arr,$temp);
                    }
                }



            }
            $model =['property'=>$arr];
        }
        if($exploded[1]=='report'){
            $usercolumns = DB::getSchemaBuilder()->getColumnListing('reports');
            $arr = [];
            foreach ($usercolumns as $key => $value) {
                $temp ="";
                if($value!='updated_at'
                    && $value != 'created_at'
                    && $value != 'deleted_at'
                    && $value != 'email_verified_at'
                    && $value != 'id'
                    && $value !="remember_token"
                    && $value != ","
                ){
                    if($value=="image"){
                        $temp=$temp.$value." : "."jpg, png";
                        array_push($arr,$temp);
                    }else{

                        $temp = $temp.$value." : ".DB::getSchemaBuilder()->getColumnType('reports',$value);
                        array_push($arr,$temp);
                    }
                }


            }
            $model =['report'=>$arr];
            // $model =['report'=>Report::getAllAttributes()];
            // dd($model);
        }

        if(sizeof(explode("-",$exploded[1]))==2){
            if(explode("-",$exploded[1])[sizeof(explode("-",$exploded[1]))-1]=='user'){
                         $usercolumns = DB::getSchemaBuilder()->getColumnListing('users');
            $arr = [];
            foreach ($usercolumns as $key => $value) {
                $temp ="";
                if($value!='updated_at'
                    && $value != 'created_at'
                    && $value != 'deleted_at'
                    && $value != 'email_verified_at'
                    && $value != 'id'
                    && $value !="remember_token"
                    && $value != ","
                ){
                    if($value=="image"){
                        $temp=$temp.$value." : "."jpg, png";
                        array_push($arr,$temp);
                    }else{

                        $temp = $temp.$value." : ".DB::getSchemaBuilder()->getColumnType('users',$value);
                        array_push($arr,$temp);
                    }
                }


            }
            $model =['user'=>$arr];

                // $model =['user'=>User::getAllAttributes()];
            }
            if(explode("-",$exploded[1])[sizeof(explode("-",$exploded[1]))-1]=='property'){
                   $usercolumns = DB::getSchemaBuilder()->getColumnListing('properties');
            $arr = [];
            foreach ($usercolumns as $key => $value) {
                $temp ="";
                if($value!='updated_at'
                    && $value != 'created_at'
                    && $value != 'deleted_at'
                    && $value != 'email_verified_at'
                    && $value != 'id'
                    && $value !="remember_token"
                    && $value != ","
                ){
                    if($value=="image"){
                        $temp=$temp.$value." : "."jpg, png";
                        array_push($arr,$temp);
                    }else{

                        $temp = $temp.$value." : ".DB::getSchemaBuilder()->getColumnType('properties',$value);
                        array_push($arr,$temp);
                    }
                }


            }
            $model =['property'=>$arr];
                // $model =['property'=>Property::getAllAttributes()];
            }
            if(explode("-",$exploded[1])[sizeof(explode("-",$exploded[1]))-1]=='report'){
                            $usercolumns = DB::getSchemaBuilder()->getColumnListing('reports');
            $arr = [];
            foreach ($usercolumns as $key => $value) {
                $temp ="";
                if($value!='updated_at'
                    && $value != 'created_at'
                    && $value != 'deleted_at'
                    && $value != 'email_verified_at'
                    && $value != 'id'
                    && $value !="remember_token"
                    && $value != ","
                ){
                    if($value=="image"){
                        $temp=$temp.$value." : "."jpg, png";
                        array_push($arr,$temp);
                    }else{

                        $temp = $temp.$value." : ".DB::getSchemaBuilder()->getColumnType('reports',$value);
                        array_push($arr,$temp);
                    }
                }


            }
            $model =['report'=>$arr];
                // $model =['report'=>Report::getAllAttributes()];
            }
        }

        return $model;
    }



public function getTableColumns($table)
{
    return DB::getSchemaBuilder()->getColumnListing($table);

    // OR

    return Schema::getColumnListing($table);

}


    public function getDocs(Request $request){

        $app = app();
        $routes = $app->routes->getRoutes();
        $docs = [];
        // dd($routes);
         foreach ($routes as $route => $value) {
             $doc = [];
             $doc['model'] = [];
             $doc['body'] = [];

             $doc['method'] = $value->getActionMethod();
             $doc['parameters'] = "";
             $exploded = explode("/",$value->uri);

            if($value->getPrefix() != '_ignition' && $value->getPrefix() !== 'sanctum' && $value->getPrefix() =='api' ){

                if($exploded[1] =='upload-image' ){
                        $doc['body'] =["image[] : "."jpg, png","id[]: integer"];

                }
                if($value->methods[0]=='GET' && sizeof($exploded) ==2 ){
                    $doc['uri'] = $value->uri ;
                    $doc['methods'] = $value->methods ;
                    if($exploded[sizeof($exploded)-1]=='user'){
                        $doc['details'] = "get all users";
                    }else{
                        $doc['details'] = "get all ".$exploded[sizeof($exploded)-1];
                    }
                }

                if($value->methods[0]=='GET' && sizeof($exploded) ==3 ){
                    $doc['uri'] = $value->uri ;
                    $doc['methods'] = $value->methods ;
                    // $doc['model'] = $this->addModelData($exploded,$value);

                    $doc['details'] = "get ".$exploded[sizeof($exploded)-2]." by id " ;
                    $doc['parameters'] = "id" ;
                }
                if($value->methods[0]=='POST' && sizeof($exploded) ==3 ){
                    $doc['uri'] = $value->uri ;
                    $doc['methods'] = $value->methods ;
                    $this->addModelData($exploded,$value);
                     $doc['model'] = $this->addModelData($exploded,$value);
                    $doc['details'] = explode("-",$exploded[sizeof($exploded)-2])[0]." ".
                     explode("-",$exploded[sizeof($exploded)-2])[1].
                     " by id " ;
                    $doc['parameters'] = "id" ;
                }
                if($value->methods[0]=='POST' && sizeof($exploded) == 2 ){
                    $doc['uri'] = $value->uri ;
                    $doc['methods'] = $value->methods ;
                    $this->addModelData($exploded,$value);
                    $doc['model'] = $this->addModelData($exploded,$value);
                     if($exploded[1] =='signup'|| $exploded[1] == 'login'){
                         $doc['body'] = ['email:string','password:string'];
                     }
                    if($exploded[1] !='signup' && $exploded[1] !='upload-image' && $exploded[1] !='login' && $exploded[1] !='logout' ){
                        $doc['details'] = "Create ".$exploded[sizeof($exploded)-1]."  " ;
                    }else{
                        $doc['details'] = $exploded[sizeof($exploded)-1]."  " ;
                    }
                }
                if($value->methods[0]=='DELETE' && sizeof($exploded) == 3 ){
                    $doc['uri'] = $value->uri ;
                    $doc['methods'] = $value->methods ;
                    $doc['details'] = "DELETE ".$exploded[sizeof($exploded)-2]." by id  " ;
                    // $doc['model'] = $this->addModelData($exploded,$value);

                    $doc['parameters'] = "id" ;
                }
                array_push($docs,$doc);
            }
        }

        // ===============================================================================
        //
        //                              CLI
        //
        // ===============================================================================
        // get api/users
        // $properties[10]->detail = "get all users";


        $post = [];
        $post['1'] = [
            'name'=>'getproperty',
        // 'uri'=> '/api/users'
        'properties'=>[
            "name",
            "user_id"
            ]
        ];
        $post['2'] = ['name'=>'user',
        'properties'=>[
            'first_name',
            'last_name',
            'phone_number',
            'email',
            'address',
            'city',
            'state',
            'zip',
            'country',
            'date_of_inspection',
            'inspection_fee',
            'realtor1',
            'realtor2',
            'realtor1_email',
            'realtor2_email',
            'weather',
            'direction',
            'summary',
            'image'
            ]
        ];
        $post['3'] = ['name'=>'report',
        'properties'=>[
            'type',
            'section',
            'description',
            'images'
            ]
        ];
        $post['4'] = ['name'=>'api/login',
        'properties'=>[
            'email',
            'password',
        ],'return' => [
            'user', 'token'
            ]
        ];
        $post['5'] = ['name'=>'api/signup',
        'properties'=>[
            'email',
            'password',
            ]
        ];

        return view('docs')->with('docs',$docs)->with('post',$post);
    }
}