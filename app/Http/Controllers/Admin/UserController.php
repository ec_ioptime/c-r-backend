<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Route;
use Validator;
use Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\forgetPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;


class UserController extends Controller
{
    // ==========================================================================
    //
    //                                WEB
    //
    // ==========================================================================

    public function dashboard(Request $request){

        return view('admin.dashboard');
    }

    public function reset_password(Request $request){

        return view('admin.update-password');
    }

    public function index(Request $request){

        $users = User::where('status','1')->orwhere('status','2')->get();

        return view('admin.user.index',compact('users'));
    }

    public function terminated_users(Request $request){

        $users = User::where('status','0')->get();

        return view('admin.user.terminated',compact('users'));
    }

    public function create(Request $request){

        return view('admin.user.create');
    }

    public function store(Request $request){

        $user = $request->all();

        if($request->has('password')){

        $user['password'] = bcrypt($request->password);
        }

        User::create($user);

        return back()->with('status','Added Successfully');
    }

    public function update(Request $request,$id){

        $user = User::find($id);

        if($request->has('first_name')){

        $user->first_name = $request->first_name;

        }

        if($request->has('last_name')){

        $user->last_name = $request->last_name;
        
        }

        if($request->has('employeeId')){

        $user->employeeId = $request->employeeId;
        
        }

        if($request->has('email')){

        $user->email = $request->email;
        
        }

        if($request->has('phone_number')){

        $user->phone_number = $request->phone_number;
        
        }

        $user->save();

        return back()->with('status','Updated Successfully');
    }


     public function update_password(Request $request){

        if($request->has('password')){


        User::where('id',Auth::user()->id)->update(['password'=> bcrypt($request->password)]);

         }

        return back()->with('status','Password Reset Successfully');
    }

    /**
     * Forgot Password.
     *
     * @return \Illuminate\Http\Response
     */

    public function forgot(Request $request){

     try{

            $user = User::where('email','=', $request->email)->first();

            if($user){

                $num = 4;

                $random = rand(pow(10, $num-1), pow(10, $num)-1);

                $code = 'ioptime@'.$random;

                $digits = bcrypt($code);
            
                if($user->id!=0){
    
                    $admin = User::where('id',$user->id)->first();
                }

                User::where('id',$user->id)->update(['password'=> $digits]);

                Mail::to($request->email)->send(new forgetPassword($code,$admin));

                return back()->with('status','System generated password sent to email !');
            }

        } catch (Exception $e) {
            return back()->with('error','Something Went Wrong!');
        }
    }


    public function getUsersWeb(Request $request){
        return view('admin.users')->with('users',User::all());
    }
    public function createuserview(Request $request){
        return view('admin.user.create');
    }
    public function edituserview(Request $request,$id){
        return view('admin.user.edit')->with('user',User::find($id));
    }

    //CREATE USER
    public function createUserWeb(Request $request){

        $request['name'] = $request->first_name.' '.$request->last_name;
        if(!$request['password']){
            $request['password'] = Hash::make('12345');
        }

        $validator = Validator::make($request->all(),
        [
            'email'=>['required','email','unique:users'],
            'password'=>'required'
        ]);


        $errors = $validator->errors();
        if ($validator->fails()) {
            $response = [];
            $response2 = [];
            foreach ($errors->getMessages() as $item) {
                array_push($response, $item);
            }
            $i=0;
            foreach ($response as $re => $value) {
                $response2['errors'][$i]=$value[0];
                $i++;
            }
            // return response()->json($response2,200);
            dd($response2);
            return redirect('/users')->with('errors',$response2);
        }

        $user = new User($request->all());
        $r = $user->save();
        if($r){
            return redirect('/users')->with('user',$user);
        }else{
            return response('errors: Could not Save the data',500)->header('Content-Type','application/json');
        }
    }

    //UPDATE USER
    public function updateUserWeb(Request $request,$id){
        $user = User::find($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone_number = $request->phone_number;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->city = $request->city;
        if($request['password']){
            $request['password'] = Hash::make($request->password);
        }
        $user->state = $request->state;
        $user->zip = $request->zip;
        $user->country = $request->country;
        $user->date_of_inspection = $request->date_of_inspection;
        $user->inspection_fee = $request->inspection_fee;
        $user->realtor1 = $request->realtor1;
        $user->realtor2 = $request->realtor2;
        $user->realtor1_email = $request->realtor1_email;
        $user->realtor2_email = $request->realtor2_email;
        $user->weather = $request->weather;
        $user->direction = $request->direction;
        $user->summary = $request->summary;

        if($request->hasfile('image')) {
            $image_path = $user->image;
            if($image_path){
                $img_arr = explode('/',$image_path);
                $image_path = $img_arr[3].'/'.$img_arr[4];
                if(File::exists($image_path)) {
                    File::delete($image_path);
                }
            }
            $imageName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('/img'), $imageName);
            $request['imageurl'] = url('/img/' . $imageName);
            $user->image = $request->imageurl;
        }

       $r =  $user->save();
       if($r){
           return redirect('/users')->with('users',User::all());
        }else{
            return redirect()->back->with('error',"Could not update the urser");
        }
    }
    // ==========================================================================
    //
    //                                API
    //
    // ==========================================================================
    public function signup(Request $request){
        $response = array('response' => '', 'success'=>false);

        $validator = Validator::make($request->all(),
         [
            'email'=>['required','email','unique:users'],
            'password'=>'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response = [];
            $response2 = [];
            foreach ($errors->getMessages() as $item) {
                array_push($response, $item);
            }
            $i=0;
            foreach ($response as $re => $value) {
                // dd($value[0]);
                $response2['errors'][$i]=$value[0];
                $i++;
            }
            // dd();
            // dd();
            return response()->json($response2,200);
        }else{
           $user =  User::create([
                'email'=>$request->email,
                'name'=>$request->email,
                'password'=>Hash::make($request->password),
                ]);
            return response()->json($user, 200);
         }
    }

    public function getUserProperties(Request $request, $id){
        // $user = auth('sanctum')->user();
        // if ($user) {
        //     dd($user->currentAccessToken());
        // }
        $user = User::find($id);
        $props = $user->properties;
        // dd($props);
        return response()->json($props,200);
    }

    public function getLogin(Request $request){
        dd('lk');
        return response()->json(['error'=>'Please provide access token or login to get the access token'],401);
    }

    public function logout(Request $request){
        Auth::user()->tokens()->where('id', $id)->delete();
        return response()->json(['msg'=>'Logged Out!'],200);
    }

    // login
    public function index2(Request $request){
        $user = User::where('email',$request->email)->first();
        //print_r($data)
        if(!$user||!Hash::check($request->password, $user->password)){
            return response([
                'message'=>['These credentials do not match. Please try again']
            ],200);
        }
        $token = $user->createToken('aieye-token')->plainTextToken;
        $retsponse = [
            'user'=>$user,
            'token'=>$token
        ];
        return response($retsponse,200);
    }


    public function createUser(Request $request){
        dd('kl');
        $request['name'] = $request->first_name.' '.$request->last_name;
        if(!$request['password']){
            $request['password'] = Hash::make('12345');
        }
        $validator = Validator::make($request->all(),
        [
            'email'=>['required','email','unique:users'],
            'password'=>'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response = [];
            $response2 = [];
            foreach ($errors->getMessages() as $item) {
                array_push($response, $item);
            }
            $i=0;
            foreach ($response as $re => $value) {
                $response2['errors'][$i]=$value[0];
                $i++;
            }
            return response()->json($response2,200);
        }

        // dd($request->all());
        $user = new User($request->all());
        $r = $user->save();
        if($r){
            return response()->json($user, 200);
        }else{
            return response('Error: Could not Save the data',500)->header('Content-Type','application/json');
        }
    }
    public function updateUser(Request $request, $id){
        $request['name'] = $request->first_name.' '.$request->last_name;
        if(!$request['password']){
            $request['password'] = Hash::make('12345');
        }
        $validator = Validator::make($request->all(),
        [
            'email'=>['required','email','unique:users'],
            'password'=>'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response = [];
            $response2 = [];
            foreach ($errors->getMessages() as $item) {
                array_push($response, $item);
            }
            $i=0;
            foreach ($response as $re => $value) {
                $response2['errors'][$i]=$value[0];
                $i++;
            }
            return response()->json($response2,200);
        }

        // dd($request->all());
        $user = User::find($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone_number = $request->phone_number;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->zip = $request->zip;
        $user->country = $request->country;
        $user->date_of_inspection = $request->date_of_inspection;
        $user->inspection_fee = $request->inspection_fee;
        $user->realtor1 = $request->realtor1;
        $user->realtor2 = $request->realtor2;
        $user->realtor1_email = $request->realtor1_email;
        $user->realtor2_email = $request->realtor2_email;
        $user->weather = $request->weather;
        $user->direction = $request->direction;
        $user->summary = $request->summary;

        if($request->hasfile('image')) {
            $imageName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('/img'), $imageName);
            $request['imageurl'] = url('/img/' . $imageName);
            $user->image = $request->imageurl;
        }

        $r = $user->save();
        if($r){
            return response()->json($user, 200);
        }else{
            return response('Error: Could not Save the data',500)->header('Content-Type','application/json');
        }
    }

    public function getUsers(Request $request){
        return User::all();
    }


    public function getUser(Request $request,$id){
        return view('admin.user.edit')->with('user',User::findOrFail($id));
    }

    public function deleteUser(Request $request,$id){
        $user = User::findOrFail($id);

        $r = $user->delete();
        if($r){
            return response('User Deleted',200)->header('Content-Type','application/json');
        }else{
            return response('Error: Could not delete',500)->header('Content-Type','application/json');
        }
    }

     public function delete(Request $request,$id){

        if(Auth::user()->id == $id){

            return back()->with('error','Sorry! you can not delete your record');
        }else{

        $types = User::where('id',$id)->delete();

        }
        
        return back()->with('status','Deleted Successfully');
    }

        public function terminate(Request $request,$id){

        if(Auth::user()->id == $id){

            return back()->with('error','Sorry! you can not terminate admin');

        }else{

        $types = User::where('id',$id)->update(['status' => '0']);

       }

        return back()->with('status','User Terminated Successfully');
    }


    public function activate(Request $request,$id){

        $types = User::where('id',$id)->update(['status' => '1']);

        return back()->with('status','User Activated Successfully');
    }




}