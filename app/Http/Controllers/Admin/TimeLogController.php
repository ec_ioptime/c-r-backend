<?php

namespace App\Http\Controllers\Admin;

use App\Breaks;
use App\Http\Controllers\Controller;
use App\Http\Controllers\TimeCalculation;
use App\JobConfiguration;
use App\TimeLogs;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TimeLogController extends Controller
{
    /**
     * convert minutes to hours.
     * @return \Illuminate\Http\Response
     */
    public function convertMinuteToHours($minutes)
    {
        return $minutes / 60;
    }

    /**
     * Edit Timelogs.
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

        $types = TimeLogs::findOrFail($id);

        return view('admin.report.edit', compact('types'));
    }

    /**
     * Add new break times.
     * @return \Illuminate\Http\Response
     */
    public function addbreaks(Request $request)
    {
        $Breaks = new Breaks;

        $startedAt = date('Y-m-d H:i:s', strtotime($request->startedAt));
        $endedAt = date('Y-m-d H:i:s', strtotime($request->endedAt));

        $starttime = Carbon::parse($startedAt);
        $endtime = Carbon::parse($endedAt);

        $minutes = $starttime->diffInMinutes($endtime);

        $Breaks['total_time'] = $this->convertMinuteToHours($minutes);
        $Breaks['startedAt'] = $startedAt;
        $Breaks['endedAt'] = $endedAt;
        $Breaks['time_log_id'] = $request->time_log_id;

        $Breaks->save();

        return response()->json(['status' => '200', 'message' => 'success']);
    }

    /**
     * Update break times.
     * @return \Illuminate\Http\Response
     */
    public function updatebreaks(Request $request, $id)
    {
        $breaks = Breaks::findOrFail($id);

        $TimeLogs = TimeLogs::find($breaks->time_log_id);

        $startedAt = Carbon::parse(date('d-m-Y H:i:s', strtotime($request->startedAt)));
        $endedAt = Carbon::parse(date('d-m-Y H:i:s', strtotime($request->endedAt)));

        if ($request->has('startedAt')) {
            $breaks->startedAt = $startedAt;
        }

        if ($request->has('endedAt')) {
            $breaks->endedAt = $endedAt;
        }

        $minutes = $startedAt->diffInMinutes($endedAt);
        $breaks->total_time = $this->convertMinuteToHours($minutes);

        $breaks->save();

        return response()->json(['status' => '200', 'message' => 'success']);
    }

    /**
     * Delete Break Times.
     * @return \Illuminate\Http\Response
     */
    public function delete_breaks(Request $request, $id)
    {

        $BreakID = Breaks::findorfail($id);

        $TimeLogs = Timelogs::findorfail($BreakID->time_log_id);

        $this->reset_previous_timelogs($TimeLogs);

        $Breaks = Breaks::where('id', $id)->delete();

        (new TimeCalculation)->calculation($TimeLogs);

        return back()->with('status', 'Deleted Successfully');
    }

    /**
     * Logs View.
     * @return \Illuminate\Http\Response
     */
    public function userlogs(Request $request, $id)
    {

        $user_id = $id;
        $logs = TimeLogs::where('user_id', $id)->get();

        $hourly = TimeLogs::where('user_id', $id)->sum('hourly');
        $overtime_hourly = TimeLogs::where('user_id', $id)->sum('overtime_hourly');
        // dd(   $logs->sum('overtime_hourly')          );
        $planttime_hourly = TimeLogs::where('user_id', $id)->sum('planttime_hourly');
        $plant_overtime = TimeLogs::where('user_id', $id)->sum('plant_overtime');
        $hourly_lp2 = TimeLogs::where('user_id', $id)->sum('hourly_lp2');
        $overtime_hourly_lp2 = TimeLogs::where('user_id', $id)->sum('overtime_hourly_lp2');
        $hourly_lp4 = TimeLogs::where('user_id', $id)->sum('hourly_lp4');
        $overtime_hourly_lp4 = TimeLogs::where('user_id', $id)->sum('overtime_hourly_lp4');
        $plant_hourly_lp2 = TimeLogs::where('user_id', $id)->sum('plant_hourly_lp2');
        $plant_overtime_lp2 = TimeLogs::where('user_id', $id)->sum('plant_overtime_lp2');
        $planttime_hourly_lp4 = TimeLogs::where('user_id', $id)->sum('planttime_hourly_lp4');
        $plant_overtime_lp4 = TimeLogs::where('user_id', $id)->sum('plant_overtime_lp4');
        $lp2_doubletime = TimeLogs::where('user_id', $id)->sum('lp2_doubletime');
        $lp4_doubletime = TimeLogs::where('user_id', $id)->sum('lp4_doubletime');
        $plant_doubletime = TimeLogs::where('user_id', $id)->sum('plant_doubletime');
        $lp2_plant_doubletime = TimeLogs::where('user_id', $id)->sum('lp2_plant_doubletime');
        $lp4_plant_doubletime = TimeLogs::where('user_id', $id)->sum('lp4_plant_doubletime');
        $fixed_salary = TimeLogs::where('user_id', $id)->sum('fixed_salary');
        $double_time_hourly = TimeLogs::where('user_id', $id)->sum('double_time_hourly');
        $mechanic_callout = TimeLogs::where('user_id', $id)->sum('mechanic_callout');
        $vacation_pay_hourly = TimeLogs::where('user_id', $id)->sum('vacation_pay_hourly');
        $sick_pay_hourly = TimeLogs::where('user_id', $id)->sum('sick_pay_hourly');

        return view('admin.report.user-logs', compact('hourly', 'overtime_hourly', 'planttime_hourly', 'plant_overtime', 'hourly_lp2', 'overtime_hourly_lp2', 'hourly_lp4', 'overtime_hourly_lp4', 'plant_hourly_lp2', 'plant_overtime_lp2', 'planttime_hourly_lp4', 'plant_overtime_lp4', 'lp2_doubletime', 'lp4_doubletime', 'plant_doubletime', 'lp2_plant_doubletime', 'lp4_plant_doubletime', 'fixed_salary', 'double_time_hourly', 'mechanic_callout', 'vacation_pay_hourly', 'sick_pay_hourly', 'user_id'));
    }

    /**
     * User Logs View.
     * @return \Illuminate\Http\Response
     */
    public function calculate_userlogs(Request $request, $id)
    {

        $user_id = $id;

        $date1 = $request->from;
        $date2 = $request->to;

        $hourly = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])->sum('hourly');

        $overtime_hourly = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('overtime_hourly');
        $planttime_hourly = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('planttime_hourly');
        $plant_overtime = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('plant_overtime');
        $hourly_lp2 = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('hourly_lp2');
        $overtime_hourly_lp2 = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('overtime_hourly_lp2');
        $hourly_lp4 = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('hourly_lp4');
        $overtime_hourly_lp4 = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('overtime_hourly_lp4');
        $plant_hourly_lp2 = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('plant_hourly_lp2');
        $plant_overtime_lp2 = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('plant_overtime_lp2');
        $planttime_hourly_lp4 = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('planttime_hourly_lp4');
        $plant_overtime_lp4 = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('plant_overtime_lp4');
        $lp2_doubletime = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('lp2_doubletime');
        $lp4_doubletime = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('lp4_doubletime');
        $plant_doubletime = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('plant_doubletime');
        $lp2_plant_doubletime = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('lp2_plant_doubletime');
        $lp4_plant_doubletime = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('lp4_plant_doubletime');
        $fixed_salary = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('fixed_salary');
        $double_time_hourly = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('double_time_hourly');
        $mechanic_callout = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('mechanic_callout');
        $vacation_pay_hourly = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('vacation_pay_hourly');
        $sick_pay_hourly = TimeLogs::where('user_id', $id)->whereBetween('created_at', [$date1, $date2])
            ->sum('sick_pay_hourly');

        return view('admin.report.user-logs', compact('hourly', 'overtime_hourly', 'planttime_hourly', 'plant_overtime', 'hourly_lp2', 'overtime_hourly_lp2', 'hourly_lp4', 'overtime_hourly_lp4', 'plant_hourly_lp2', 'plant_overtime_lp2', 'planttime_hourly_lp4', 'plant_overtime_lp4', 'lp2_doubletime', 'lp4_doubletime', 'plant_doubletime', 'lp2_plant_doubletime', 'lp4_plant_doubletime', 'fixed_salary', 'double_time_hourly', 'mechanic_callout', 'vacation_pay_hourly', 'sick_pay_hourly', 'user_id'));
    }

    /**
     * Update TimeLogs.
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $TimeLogs = TimeLogs::find($id);

        //$this->check_breaktimes($id);
        //dd(date('Y-d-m\TH:i', strtotime($request->clock_intime)));

        if ($TimeLogs->job_type_id > 0) {

            $timeConfig = JobConfiguration::where('job_id', $TimeLogs->job_id)
                ->where('job_type_id', $TimeLogs->job_type_id)
                ->value('id');

        } else {

            $timeConfig = JobConfiguration::where('job_id', $TimeLogs->job_id)->value('id');

        }

        $config = JobConfiguration::findorfail($timeConfig);

        $overtime_start = $config->start_overtime;
        $overtime_end = $config->end_overtime;

        $get_arrive_yardtime = Carbon::parse($request->arrive_yardtime)->format('H:i:s');
        $get_clock_outtime = Carbon::parse($request->clock_outtime)->format('H:i:s');
        $arriveyardtime = explode(':', $get_arrive_yardtime);
        $clockouttime = explode(':', $get_clock_outtime);

        $arriveyard = Carbon::parse($request->arrive_yardtime);
        $clockout = Carbon::parse($request->clock_outtime);

        $result = $arriveyard->diffInMinutes($clockout);

        $Total = $this->convertMinuteToHours(abs($result));
        // dd($get_clock_outtime ,$overtime_end , $Total);
        // if ($get_clock_outtime < $overtime_end && $Total > 1) {

        //     return back()->with('status_warning', 'Check Washout Overtime');

        // }

        // if ($get_clock_outtime > $overtime_start && $Total > 1) {

        //     return back()->with('status_warning', 'Check Washout Overtime');

        // } else {

            $this->reset_previous_timelogs($TimeLogs);

            if ($request->has('job_id')) {
                $TimeLogs->job_id = $request->job_id;
            }

            if ($request->job_type_id == "value" || $request->job_type_id == null) {
                $TimeLogs->job_type_id = 0;
            } else {
                $TimeLogs->job_type_id = $request->job_type_id;
            }

            if ($request->has('clock_intime')) {
                $TimeLogs->clock_intime = date('Y-m-d H:i:s', strtotime($request->clock_intime));
            }

            if ($request->has('clock_outtime')) {
                $TimeLogs->clock_outtime = date('Y-m-d H:i:s', strtotime($request->clock_outtime));
            }

            if ($request->has('leave_yardtime')) {
                $TimeLogs->leave_yardtime = date('Y-m-d H:i:s', strtotime($request->leave_yardtime));
            }

            if ($request->has('arrive_jobtime')) {
                $TimeLogs->arrive_jobtime = date('Y-m-d H:i:s', strtotime($request->arrive_jobtime));
            }

            if ($request->has('leave_jobtime')) {
                $TimeLogs->leave_jobtime = date('Y-m-d H:i:s', strtotime($request->leave_jobtime));
            }

            if ($request->has('arrive_yardtime')) {
                $TimeLogs->arrive_yardtime = date('Y-m-d H:i:s', strtotime($request->arrive_yardtime));
            }

            if ($request->has('vacation_pay_hourly')) {
                $TimeLogs->vacation_pay_hourly = $request->vacation_pay_hourly;
            }

            if ($request->has('sick_pay_hourly')) {
                $TimeLogs->sick_pay_hourly = $request->sick_pay_hourly;
            }

            if ($request->has('notes')) {
                $TimeLogs->notes = $request->notes;
            }

            $TimeLogs->save();

            (new TimeCalculation)->calculation($TimeLogs);

            return back()->with('status', 'Updated Successfully');

        // }

    }

    /**
     * Delete Timelogs.
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {

        $TimeLogs = TimeLogs::where('id', $id)->delete();
        return back()->with('status', 'Deleted Successfully');
    }

    /**
     * Reset Timelogs.
     * @return \Illuminate\Http\Response
     */
    public function reset_previous_timelogs($TimeLogs)
    {
        $data = TimeLogs::findorfail($TimeLogs->id);

        TimeLogs::where('id', $data->id)->update([
            'mechanic_callout' => '0.00',
            'hourly' => '0.00',
            'overtime_hourly' => '0.00',
            'double_time_hourly' => '0.00',
            'fixed_salary' => '0.00',
            'hourly_lp2' => '0.00',
            'overtime_hourly_lp2' => '0.00',
            'hourly_lp4' => '0.00',
            'overtime_hourly_lp4' => '0.00',
            'planttime_hourly' => '0.00',
            'plant_overtime' => '0.00',
            'plant_hourly_lp2' => '0.00',
            'plant_overtime_lp2' => '0.00',
            'planttime_hourly_lp4' => '0.00',
            'plant_overtime_lp4' => '0.00',
            'lp2_doubletime' => '0.00',
            'lp4_doubletime' => '0.00',
            'plant_doubletime' => '0.00',
            'lp2_plant_doubletime' => '0.00',
            'lp4_plant_doubletime' => '0.00',
        ]);
    }

}
