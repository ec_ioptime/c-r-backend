<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Property;
use App\Client;
use Validator;
class PropertyController extends Controller
{
    public function createProperty(Request $request){
        $rules = array(
            "name"=>"required",
            "client_id"=>"required"
        );
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return view('admin.property.create')->with('clients',Client::all());
        }else{
            $property = new Property($request->all());
            $property->save();
            return redirect('/properties');
        }
    }

    public function updateProperty(Request $request,$id){
        $rules = array(
            "name"=>"required",
            "client_id"=>"required"
        );
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            // dd($validator);
            return view('admin.property.edit')->with('property',Property::findOrFail($id))
            ->with('clients',Client::all())
            ->with('status',$validator);

        }else{
            $property =  Property::findOrFail($id);
            $property->name = $request->name;
            $property->client_id = $request->client_id;
            $property->save();
            return redirect('/properties');
        }
    }

    public function getProperties(Request $request){
        return view('admin.properties')->with('properties',Property::all());
    }


    public function getProperty(Request $request,$id){
        return Property::findOrFail($id);
    }

    public function create(Request $request){
        return view('admin.property.create')->with('clients',Client::all());
    }

    public function EditProperty(Request $request,$id){
        return view('admin.property.edit')->with('property',Property::findorFail($id))->with('clients',Client::all());
    }


    public function DeleteProperty(Request $request,$id){
        $property = Property::findOrFail($id);
        $property->delete();
        // return response('Property Deleted',200)->header('Content-Type','application/json');
        // return view('admin.properties')->with('properties',Property::all());
        return redirect('/properties');

    }

    // ==========================================================================================
    //
    //                                  API
    //
    // ==========================================================================================
    public function getPropertyReports(Request $request, $id)
    {
        $property = Property::find($id);
        $reports = $property->reports;
        if($reports){
            return response()->json($reports,200);
        }else{
            return response()->json(['errors'=>['Could not get reports']],200);
        }
    }
    public function getPropertiesReports(Request $request)
    {
        $property = Property::with('reports')->all();
        // $reports = $property->reports;
        if($reports){
            return response()->json($reports,200);
        }else{
            return response()->json(['errors'=>['Could not get reports']],200);
        }
    }

    public function getPropertiesApi(Request $request){
        return response()->json(Property::all(),200);
    }
    public function getPropertyApi(Request $request,$id){
        return response()->json(Property::find($id),200);
    }

    public function deletePropertyApi(Request $request,$id){
        // dd($id);
        $property = Property::findOrFail($id);
        $property->delete();
        return response()->json(['json'=>'Property Deleted'],200)->header('Content-Type','application/json');
    }
     public function createPropertyApi(Request $request){
        // dd($request->all());
        $rules = array(
            "name"=>"required",
            "client_id"=>"required"
        );
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return response()->json(["error"=>"missing fields","detail"=>$validator->errors()],403);
        }else{
            $property = new Property($request->all());
            $property->save();
            return response()->json($property, 200);
        }
    }
     public function updatePropertyApi(Request $request,$id){
        // dd($request->all());
        $rules = array(
            "name"=>"required",
            "client_id"=>"required"
        );
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return response()->json(["error"=>"missing fields","detail"=>$validator->errors()],403);
        }else{
            $property = Property::find($id);
            $property->name = $request->name;
            $property->client_id = $request->client_id;
            $property->save();
            return response()->json($property, 200);
        }
    }
}