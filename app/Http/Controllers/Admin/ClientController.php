<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\File;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Client;

class ClientController extends Controller
{
     public function createClient(Request $request){
        $client = new Client();
        // $client = Client::find($id);
        $client->first_name = $request->first_name;
        $client->last_name = $request->last_name;
        $client->phone_number = $request->phone_number;
        $client->email = $request->email;
        $client->address = $request->address;
        $client->city = $request->city;
        $client->state = $request->state;
        $client->zip = $request->zip;
        $client->country = $request->country;
        $client->date_of_inspection = $request->date_of_inspection;
        $client->inspection_fee = $request->inspection_fee;
        $client->realtor1 = $request->realtor1;
        $client->realtor2 = $request->realtor2;
        $client->realtor1_email = $request->realtor1_email;
        $client->realtor2_email = $request->realtor2_email;
        $client->weather = $request->weather;
        $client->direction = $request->direction;
        $client->summary = $request->summary;
        // dd($request->file('image'));
        // storage_path('public/' . $filename)
        // $client->image = $request->file('image')->store('clients');

        if($request->hasfile('image')) {
            $imageName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('/img'), $imageName);
            $request['imageurl'] = url('/img/' . $imageName);
            $client->image = $request->imageurl;
        }


       $r =  $client->save();
       if($r){
           return view('admin.clients')->with('clients',Client::all());
        }else{
            return response()->json(["error"=>"something went wrong"],500);

        }
    }

    public function updateClient(Request $request,$id){
        $client = Client::find($id);
        $client->first_name = $request->first_name;
        $client->last_name = $request->last_name;
        $client->phone_number = $request->phone_number;
        $client->email = $request->email;
        $client->address = $request->address;
        $client->city = $request->city;
        $client->state = $request->state;
        $client->zip = $request->zip;
        $client->country = $request->country;
        $client->date_of_inspection = $request->date_of_inspection;
        $client->inspection_fee = $request->inspection_fee;
        $client->realtor1 = $request->realtor1;
        $client->realtor2 = $request->realtor2;
        $client->realtor1_email = $request->realtor1_email;
        $client->realtor2_email = $request->realtor2_email;
        $client->weather = $request->weather;
        $client->direction = $request->direction;
        $client->summary = $request->summary;

        if($request->hasfile('image')) {
            //   $image_path = "/images/filename.ext";  // Value is not URL but directory file path
            $image_path = $client->image;
            $img_arr = explode('/',$image_path);
            $image_path = $img_arr[3].'/'.$img_arr[4];
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
            $imageName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('/img'), $imageName);
            $request['imageurl'] = url('/img/' . $imageName);
            $client->image = $request->imageurl;
        }

       $r =  $client->save();
       if($r){
           return redirect('/clients')->with('clients',Client::all());
        }else{
            return response()->json(["error"=>"something went wrong"],500);
        }
    }

    public function test(Request $request){
        return response('test completed', 200);
    }

    public function getClient(Request $request,$id){
        return view('admin.client.edit')->with('client',Client::findOrFail($id));
    }

    public function getClients(Request $request){
        return view('admin.clients')->with('clients',Client::all());
    }

    public function deleteClient(Request $request,$id){
        $client = Client::findOrFail($id);
        $client->delete();
        return response('Client Deleted',200)->header('Content-Type','application/json');
    }

    // ============================================================================
    //
    //                      API FUNCTIONS
    //
    // ============================================================================
    public function getClientsApi(Request $request){
        return Client::all();
    }

    public function getClientApi(Request $request,$id){
        return Client::find($id);
    }

    public function deleteClientApi(Request $request,$id){
        $client =  Client::find($id);
        $client->delete();
        return response()->json(['json'=>'Client Deleted'],200)->header('Content-Type','application/json');
    }

    public function createClientApi(Request $request){
        $client = new Client();
        $client->first_name = $request->first_name;
        $client->last_name = $request->last_name;
        $client->phone_number = $request->phone_number;
        $client->email = $request->email;
        $client->address = $request->address;
        $client->city = $request->city;
        $client->state = $request->state;
        $client->zip = $request->zip;
        $client->country = $request->country;
        $client->date_of_inspection = $request->date_of_inspection;
        $client->inspection_fee = $request->inspection_fee;
        $client->realtor1 = $request->realtor1;
        $client->realtor2 = $request->realtor2;
        $client->realtor1_email = $request->realtor1_email;
        $client->realtor2_email = $request->realtor2_email;
        $client->weather = $request->weather;
        $client->direction = $request->direction;
        $client->summary = $request->summary;

        if($request->hasfile('image')) {
            $imageName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('/img'), $imageName);
            $request['imageurl'] = url('/img/' . $imageName);
            $client->image = $request->imageurl;
        }

       $r =  $client->save();
       if($r){
           return Client::all();
        }else{
            return response()->json(["error"=>"something went wrong"],500);

        }
    }

    public function updateClientApi(Request $request,$id){
        $client = Client::find($id);
        $client->first_name = $request->first_name;
        $client->last_name = $request->last_name;
        $client->phone_number = $request->phone_number;
        $client->email = $request->email;
        $client->address = $request->address;
        $client->city = $request->city;
        $client->state = $request->state;
        $client->zip = $request->zip;
        $client->country = $request->country;
        $client->date_of_inspection = $request->date_of_inspection;
        $client->inspection_fee = $request->inspection_fee;
        $client->realtor1 = $request->realtor1;
        $client->realtor2 = $request->realtor2;
        $client->realtor1_email = $request->realtor1_email;
        $client->realtor2_email = $request->realtor2_email;
        $client->weather = $request->weather;
        $client->direction = $request->direction;
        $client->summary = $request->summary;

        if($request->hasfile('image')) {
            //   $image_path = "/images/filename.ext";  // Value is not URL but directory file path
            $image_path = $client->image;
            $img_arr = explode('/',$image_path);
            $image_path = $img_arr[3].'/'.$img_arr[4];
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
            $imageName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('/img'), $imageName);
            $request['imageurl'] = url('/img/' . $imageName);
            $client->image = $request->imageurl;
        }

       $r =  $client->save();
       if($r){
           return Client::all();
        }else{
            return response()->json(["error"=>"something went wrong"],500);
        }
    }

}