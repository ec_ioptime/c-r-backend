<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Jobs;
use App\Types;
use Route;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;


class JobsController extends Controller
{
    
    public function index(Request $request){

        $jobs = Jobs::with('types')->get();

        return view('admin.jobs.index',compact('jobs'));
    }

    public function create(Request $request){

        return view('admin.jobs.create');
    }

    public function store(Request $request){

        $jobs = $request->all();

        $result = Jobs::create($jobs);

        /*if($request->has('name')){
        
        $types = new Types;
        $types->job_id = $result->id;
        $types->name = $request->name;
        $types->save();

        }
*/
        

        return back()->with('status','Added Successfully');
    }

    public function edit(Request $request,$id){

        $jobs = Jobs::findOrFail($id);

        return view('admin.jobs.edit',compact('jobs'));
    }

    public function update(Request $request,$id){

        $jobs = Jobs::find($id);
        $jobs->title = $request->title;
        if($request->has('on_site')){

        $jobs->on_site = $request->on_site;
        
        }
        $jobs->save();

        return back()->with('status','Updated Successfully');
    }
   

    public function delete(Request $request,$id){

        $jobs = Jobs::where('id',$id)->delete();
        Types::where('job_id',$id)->delete();

        return back()->with('status','Deleted Successfully');
    }


}