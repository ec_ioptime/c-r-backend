<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Types;
use App\Jobs;
use App\JobConfiguration;
use Route;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;


class TypesController extends Controller
{
    
    public function index(Request $request){

        $types = Types::with('jobs')->get();

        return view('admin.types.index',compact('types'));
    }

    public function create(Request $request){

        return view('admin.types.create');
    }

    public function store(Request $request){

        $types = $request->all();

        Types::create($types);

        return back()->with('status','Added Successfully');
    }

    public function edit(Request $request,$id){

        $types = Types::findOrFail($id);

        return view('admin.types.edit',compact('types'));
    }

    public function update(Request $request,$id){

        $types = Types::find($id);
        $types->name = $request->name;
        if($request->has('job_id')){
            $types->job_id = $request->job_id;
        }
        $types->save();

        return back()->with('status','Updated Successfully');
    }
   

    public function delete(Request $request,$id){

        $types = Types::where('id',$id)->delete();
        JobConfiguration::where('job_type_id',$id)->delete();

        return back()->with('status','Deleted Successfully');
    }


}