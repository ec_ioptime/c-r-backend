<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Session;
class DashboardController extends Controller
{
    public function register(){
        $users = User::all();
        return view('admin.register')->with('users',$users);
    }
    public function edit(Request $request, $id){
        $users = User::find($id);
        return view('admin.edit')->with('user',$users);
    }
    public function delete(Request $request, $id){
        $user = User::find($id);
        $user->delete();
        return redirect('register-roles')->with('status','user deleted!');
     }

    public function update(Request $request ,$id){
        $user = User::find($id);
        $user->usertype=$request->usertype;
        $user->save();
        return redirect('register-roles')->with('status','Role updated');
    }
}