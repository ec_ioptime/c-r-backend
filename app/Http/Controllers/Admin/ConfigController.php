<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Jobs;
use App\JobConfiguration;
use App\ConfigMapping;
use App\Types;
use Route;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;


class ConfigController extends Controller
{
    
    public function index(Request $request){

        $jobs = JobConfiguration::with('types')->orderby('created_at','desc')->get();
      
        return view('admin.conf.index',compact('jobs'));
    }

    public function create(Request $request){

        return view('admin.conf.create');
    }

    public function store(Request $request){

        $jobs = $request->all();

        if($request->job_type_id == "value" || $request->job_type_id == ""){
         
        $jobs['job_type_id'] = "0"; 

        }

        $result = JobConfiguration::create($jobs);

        $mapping = ConfigMapping::create(['config_id'=>$result->id,
        'mapped_to'=>$request->map_to]);

        return back()->with('status','Added Successfully');
    }

    public function data(Request $request){

        $jobs = Types::where('job_id',$request->id)->pluck('name','id')->filter();

        return response()->json($jobs);
    }

    public function edit(Request $request,$id){

        $jobs = JobConfiguration::findOrFail($id);

        return view('admin.conf.edit',compact('jobs'));
    }

    public function update(Request $request,$id){

        $jobs = JobConfiguration::find($id);

        if($request->has('job_type_id')){

        $jobs->job_type_id = $request->job_type_id;
        
        }
        if($request->has('start_time')){

        $jobs->start_time = $request->start_time;
        
        }
        if($request->has('end_time')){

        $jobs->end_time = $request->end_time;
        
        }
        if($request->has('overtime')){

        $jobs->overtime = $request->overtime;
        
        }

        if($request->has('monday')){

        $jobs->monday = $request->monday;
        
        }
        if($request->has('tuesday')){

        $jobs->tuesday = $request->tuesday;
        
        }
        if($request->has('wednesday')){

        $jobs->wednesday = $request->wednesday;
        
        }
        if($request->has('thursday')){

        $jobs->thursday = $request->thursday;
        
        }
        if($request->has('friday')){

        $jobs->friday = $request->friday;
        
        }
        if($request->has('saturday')){

        $jobs->saturday = $request->saturday;
        
        }
        if($request->has('sunday')){

        $jobs->sunday = $request->sunday;
        
        }
        if($request->has('start_overtime')){

        $jobs->start_overtime = $request->start_overtime;
        
        }
        if($request->has('end_overtime')){

        $jobs->end_overtime = $request->end_overtime;
        
        }
        if($request->has('overtime_limit')){

        $jobs->overtime_limit = $request->overtime_limit;
        
        }

        if($request->has('overtime_rate')){

        $jobs->overtime_rate = $request->overtime_rate;
        
        }
       
        $jobs->save();

        return back()->with('status','Updated Successfully');
    }
   

    public function delete(Request $request,$id){

        $jobs = JobConfiguration::where('id',$id)->delete();

        ConfigMapping::where('config_id',$id)->delete();

        return back()->with('status','Deleted Successfully');
    }


}