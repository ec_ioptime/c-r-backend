<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Report;
use App\ReportData;
use App\TimeLogs;
use App\Breaks;
use Carbon\Carbon;
use App\Http\Controllers\TimeCalculation;

class ReportController extends Controller
{

     /**
     * Main index.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        $reports = ReportData::get();

        return view('admin.report.index',compact('reports'));
    }

    /**
     * Create Report View.
     * @return \Illuminate\Http\Response
     */
    public function createReport(Request $request){

        return view('admin.report.create');
    }

    /**
     * Report Details.
     * @return \Illuminate\Http\Response
     */
    public function details(Request $request,$id){

        $reportdata = ReportData::findOrFail($id);

        $reports = Report::where('report_id',$id)->get();

        return view('reports',compact('reports','reportdata'));
    }

    /**
     * Further Details.
     * @return \Illuminate\Http\Response
     */
    public function further_details(Request $request,$id,$user_id){

        $reportdata = ReportData::findOrFail($id);

        $date1 = $reportdata->start_date;
        $date2 = $reportdata->end_date;

        $reports = TimeLogs::where('user_id',$user_id)
        ->whereBetween('created_at', [$date1, $date2])->get();

        $hourly = TimeLogs::where('user_id',$user_id)
        ->whereBetween('created_at', [$date1, $date2])->sum('hourly');

        $overtime_hourly      = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('overtime_hourly');

        $planttime_hourly     = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('planttime_hourly');

        $plant_overtime       = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('plant_overtime');

        $hourly_lp2           = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('hourly_lp2');

        $overtime_hourly_lp2  = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('overtime_hourly_lp2');

        $hourly_lp4           = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('hourly_lp4');

        $overtime_hourly_lp4  = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('overtime_hourly_lp4');

        $plant_hourly_lp2     = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('plant_hourly_lp2');

        $plant_overtime_lp2   = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('plant_overtime_lp2');

        $planttime_hourly_lp4 = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('planttime_hourly_lp4');

        $plant_overtime_lp4   = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('plant_overtime_lp4');

        $lp2_doubletime       = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('lp2_doubletime');

        $lp4_doubletime       = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('lp4_doubletime');

        $plant_doubletime     = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('plant_doubletime');

        $lp2_plant_doubletime = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('lp2_plant_doubletime');

        $lp4_plant_doubletime = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('lp4_plant_doubletime');

        $fixed_salary         = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('fixed_salary');

        $double_time_hourly   = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('double_time_hourly');

        $mechanic_callout     = TimeLogs::where('user_id',$user_id)
                                ->whereBetween('created_at', [$date1, $date2])
                                ->sum('mechanic_callout');

        return view('reportdetails',compact('reports','reportdata','hourly','overtime_hourly','planttime_hourly','plant_overtime','hourly_lp2','overtime_hourly_lp2','hourly_lp4','overtime_hourly_lp4','plant_hourly_lp2','plant_overtime_lp2','planttime_hourly_lp4','plant_overtime_lp4','lp2_doubletime','lp4_doubletime','plant_doubletime','lp2_plant_doubletime','lp4_plant_doubletime','fixed_salary','double_time_hourly','mechanic_callout','user_id'));
    }

    /**
     * Store Report.
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $user = $request->all();
        $date1 =Carbon::parse($request->from)->format('Y-m-d');
        $date2 = Carbon::parse($request->to)->format('Y-m-d');

        $pay_date = date('Y-m-d', strtotime($date2. ' +8 days'));

        $user['start_date'] = $date1;
        $user['end_date'] = $date2;
        $user['pay_date'] = $pay_date;

        $user['name'] = $request->name;

        $reportdata = ReportData::create($user);

        $data = $request->user_id;

        foreach ($data as $key => $id) {

        $report = new Report;

        $report->report_id = $reportdata->id;
        $report->user_id = $id;

        $report->hourly = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('hourly');

        $report->overtime_hourly      = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('overtime_hourly');

        $report->planttime_hourly     = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('planttime_hourly');

        $report->plant_overtime       = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('plant_overtime');

        $report->hourly_lp2        = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('hourly_lp2');

        $report->overtime_hourly_lp2  = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('overtime_hourly_lp2');

        $report->hourly_lp4          = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('hourly_lp4');

        $report->overtime_hourly_lp4  = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2]) ->sum('overtime_hourly_lp4');

        $report->plant_hourly_lp2     = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('plant_hourly_lp2');

        $report->plant_overtime_lp2   = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('plant_overtime_lp2');

        $report->planttime_hourly_lp4 = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('planttime_hourly_lp4');

        $report->plant_overtime_lp4   = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('plant_overtime_lp4');

        $report->lp2_doubletime       = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('lp2_doubletime');

        $report->lp4_doubletime       = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('lp4_doubletime');

        $report->plant_doubletime    = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('plant_doubletime');

        $report->lp2_plant_doubletime = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('lp2_plant_doubletime');

        $report->lp4_plant_doubletime = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('lp4_plant_doubletime');

        $report->fixed_salary         = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('fixed_salary');

        $report->double_time_hourly   = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('double_time_hourly');

        $report->mechanic_callout     = TimeLogs::where('user_id',$id)
        ->whereBetween('created_at', [$date1, $date2])->sum('mechanic_callout');

        $report->save();

        }

        return back()->with('status','Report Created Successfully');
    }

    /**
     * Get Logs.
     * @return \Illuminate\Http\Response
     */
    public function getlogs(Request $request){

        if($request->has('from') && $request->has('to')){

        $date1 = $request->from;
        $date2 = $request->to;

        $types = TimeLogs::where('status',0)->whereDate('created_at', '>=', $date1)
        ->whereDate('created_at', '<=', $date2)
        ->with('user','jobs','types')
        ->orderby('id','desc')
        ->get();

        }else{

        $types = TimeLogs::where('status',0)->orderby('id','desc')->with('user','jobs','types')->get();

        }

        return view('admin.reports',compact('types'));
    }

     /**
     * Approved Logs.
     * @return \Illuminate\Http\Response
     */
    public function approved(Request $request){

        if($request->has('from') && $request->has('to')){

        $date1 = $request->from;
        $date2 = $request->to;

        $types = TimeLogs::where('status',1)->whereDate('created_at', '>=', $date1)
        ->whereDate('created_at', '<=', $date2)
        ->with('user','jobs','types')
        ->orderby('id','desc')
        ->get();

        }else{

        $types = TimeLogs::where('status',1)->orderby('id','desc')->with('user','jobs','types')->get();



        }

        return view('admin.approvedlogs',compact('types'));
    }

    /**
     * Disapproved Logs.
     * @return \Illuminate\Http\Response
     */
    public function disapproved(Request $request){

        if($request->has('from') && $request->has('to')){

        $date1 = $request->from;
        $date2 = $request->to;

        $types = TimeLogs::where('status',2)->whereDate('created_at', '>=', $date1)
        ->whereDate('created_at', '<=', $date2)
        ->with('user','jobs','types')
        ->orderby('id','desc')
        ->get();

        }else{

        $types = TimeLogs::where('status',2)->orderby('id','desc')->with('user','jobs','types')->get();

        }

        return view('admin.disapprovedlogs',compact('types'));
    }


    /**
     * Store Approve logs.
     * @return \Illuminate\Http\Response
     */
    public function save_approve_logs(Request $request,$id){

        $types = TimeLogs::where('id',$request->id)->update(['status' => '1']);

        return back()->with('status','Approved Successfully');
    }


    /**
     * Store disapprove logs.
     * @return \Illuminate\Http\Response
     */

     public function save_disapprove_logs(Request $request,$id){

        $types = TimeLogs::where('id',$request->id)->update(['status' => '2']);

        return back()->with('status','Disapproved Successfully');
    }


    /**
     * Store Bulk Approve logs.
     * @return \Illuminate\Http\Response
     */
    public function bulkapprove(request $request)
    {
        TimeLogs::whereIn('id', $request->id)->update(['status' => 1]);

        return back()->with('status','Approved Successfully');
    }


    /**
     * Delete Bulk.
     * @return \Illuminate\Http\Response
     */

    public function bulkdelete(request $request)
    {
        TimeLogs::whereIn('id', $request->id)->delete();

        return back()->with('status','Deleted Successfully');
    }

    public function EditReport(Request $request,$id){

        $timelogs = TimeLogs::findOrFail($id);

        $clockin_location = json_decode($timelogs->clock_in_location);

        if(empty($clockin_location) || $clockin_location == null){

        $clock_in_lat = "";
        $clock_in_lng = "";

        }else{

        $clock_in_lat = $clockin_location->latitude;
        $clock_in_lng = $clockin_location->longitude;

        }

        $leaveyard_location = json_decode($timelogs->leave_yard_location);

        if(empty($leaveyard_location) || $leaveyard_location == null){

        $leaveyard_lat = "";
        $leaveyard_lng = "";

        }else{

        $leaveyard_lat = $leaveyard_location->latitude;
        $leaveyard_lng = $leaveyard_location->longitude;

        }

        $arrivejob_location = json_decode($timelogs->arrive_job_location);


        if(empty($arrivejob_location) || $arrivejob_location == null){

        $arrivejob_lat = "";
        $arrivejob_lng = "";

        }else{

        $arrivejob_lat = $arrivejob_location->latitude;
        $arrivejob_lng = $arrivejob_location->longitude;

        }

        $leavejob_location = json_decode($timelogs->leave_job_location);

        if(empty($leavejob_location) || $leavejob_location == null){

        $leavejob_lat = "";
        $leavejob_lng = "";

        }else{

        $leavejob_lat = $leavejob_location->latitude;
        $leavejob_lng = $leavejob_location->longitude;

        }

        $arriveyard_location = json_decode($timelogs->arrive_yard_location);


        if(empty($arriveyard_location) || $arriveyard_location == null){

        $arriveyard_lat = "";
        $arriveyard_lng = "";

        }else{

        $arriveyard_lat = $arriveyard_location->latitude;
        $arriveyard_lng = $arriveyard_location->longitude;

        }

        $clockout_location = json_decode($timelogs->clock_out_location);

        if(empty($clockout_location) || $clockout_location == null){

        $clockout_lat = "";
        $clockout_lng = "";

        }else{

        $clockout_lat = $clockout_location->latitude;
        $clockout_lng = $clockout_location->longitude;

        }
        return view('admin.report.edit',compact('timelogs','clock_in_lat','clock_in_lng','leaveyard_lat','leaveyard_lng','arrivejob_lat','arrivejob_lng','leavejob_lat','leavejob_lng','arriveyard_lat','arriveyard_lng','clockout_lat','clockout_lng'));
    }


    public function updatetimelogs(Request $request,$id){

        $TimeLogs = TimeLogs::find($id);

        if($request->has('clock_intime')){

        $TimeLogs->clock_intime = $request->clock_intime;

        }
        if($request->has('clock_outtime')){

        $TimeLogs->clock_outtime = $request->clock_outtime;

        }

        $TimeLogs->save();

        (new TimeCalculation)->calculation($TimeLogs);

        return back()->with('status','Updated Successfully');
    }

    public function getReport(Request $request,$id){
        return Report::findOrFail($id);
    }

    public function DeleteReport(Request $request,$id){

        $repordata = Report::where('report_id',$id)->delete();
        $report = ReportData::findOrFail($id);
        $report->delete();

        return  redirect()->back()->with('status','Report deleted Successfully!');
    }
}
