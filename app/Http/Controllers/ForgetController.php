<?php

namespace App\Http\Controllers;

use App\Mail\sendRestPasswordLink;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ForgetController extends Controller
{
      function showForm(){
          //show forget password email form

          return view('auth.passwords.email2');
      }
    public function sendPasswordResetToken(Request $request)
    {
        //send reset password link through email

        $user = User::where ('email', $request->email)->first();
        if ( !$user ) return redirect()->back()->with(array('error'=>'Email not found!'));

       /* //create a new token to be sent to the user.
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => str_random(60), //change 60 to any length you want
            'created_at' => Carbon::now()
        ]);
*/
        $tokenData = DB::table('password_resets')
            ->where('email', $request->email)->first();

        $token = $tokenData->token;
        $email = $request->email; // or $email = $tokenData->email;

        Mail::to($email)->send(new sendRestPasswordLink($token,$email));

        return redirect('password-reset')->with(array('status'=>'We have e-mailed your password reset link!'));

    }


    function showPasswordResetForm($token,$email){

          //show password reset form

        $tokenData = DB::table('password_resets')
            ->where('token', $token)->first();

        if ( !$tokenData ) return redirect('vendor/dashboard'); //redirect them anywhere you want if the token does not exist.
        return view('auth.passwords.reset-Password',compact('token','email'));
    }

    public function resetPassword(Request $request, $token)
    {



        $password = $request->password;
        $tokenData = DB::table('password_resets')
            ->where('token', $token)->first();

        $user = Vendor::where('email', $tokenData->email)->first();
        if ( !$user ) return redirect('/vendor/vendor-login'); //or wherever you want

        $user->password = Hash::make($password);
        $user->update(); //or $user->save();
        DB::table('password_resets')->where('email', $user->email)->delete();
        return redirect('/vendor/vendor-login')->with(['success'=>'Password Reset Successfully !']);

 }


}
