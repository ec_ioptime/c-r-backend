<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        // dd($request->expectsJson());
//          if( $request->is('api/*')){
//             //  return ['lkj'];
//             return redirect('login');
//             // return response(['Maintenance'], 503)->header('Content-Type', 'application/json')
// ;
//             // return response()->json(["error"=>"UnAuthenticated user, please provide an access token"], 401);
//         }else
        if (! $request->expectsJson()) {
            return route('login');
        }
    }
}