<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public $fillable = [
        'user_id',
        'report_id',
        'hourly',
        'overtime_hourly',
        'planttime_hourly',
        'plant_overtime',
        'hourly_lp2',
        'overtime_hourly_lp2',
        'hourly_lp4',
        'overtime_hourly_lp4',
        'plant_hourly_lp2',
        'plant_overtime_lp2',
        'planttime_hourly_lp4',
        'plant_overtime_lp4',
        'fixed_salary',
        'double_time_hourly',
        'lp2_doubletime',
        'lp4_doubletime',
        'plant_doubletime',
        'lp2_plant_doubletime',
        'lp4_plant_doubletime',
        'mechanic_callout',
    ];

    /**
     * User Model Linked
     */

    public function user(){

        return $this->belongsTo('App\User','user_id','id');
    }
}
