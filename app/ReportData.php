<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportData extends Model
{
    public $fillable = [ 
        'name',
        'start_date',
        'end_date',
        'pay_date'
    ];
}
