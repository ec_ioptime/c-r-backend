@extends('layouts.master')

@section('title')
   Edit Job Configuration
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Edit Job Configuration</h4>
            </div>
            <div class="card-body">
                @if ($message = Session::get('status'))
                    <div class="alert alert-success" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
                @endif
            <form method="POST" action="{{route('config.update',$jobs->id)}}" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <input name="_method" type="hidden" value="PATCH">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <select class="form-control" name="job_id" id="category_id" required="">
                            <option value="">Select Job Title</option>
                            @foreach(App\Jobs::get() as $data)
                              <option value="{{$data->id}}" {{$jobs->job_id == $data->id  ? 'selected' : ''}}>{{$data->title}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <select class="form-control selecs" name="job_type_id" id="mySelect" required="">
                            <option value="{{$jobs->job_type_id}}">@if($jobs->types)
                              {{$jobs->types->name}}
                              @else
                              --
                              @endif</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <select class="form-control" name="start_time" required="">
                            <option value="">Select Start Time</option>
                            @foreach(App\TimeList::get() as $data)
                              <option value="{{$data->name}}" {{$jobs->start_time == $data->name  ? 'selected' : ''}}>{{$data->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                    <div class="col-md-6">
                      <div class="form-group">
                          <select class="form-control" name="end_time" required="">
                            <option value="">Select End Time</option>
                            @foreach(App\TimeList::get() as $data)
                              <option value="{{$data->name}}" {{$jobs->end_time == $data->name  ? 'selected' : ''}}>{{$data->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>

                    

                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating"> OverTime</label>
                         <input type="hidden" value="0" style="width: 18px;" class="form-control" name="overtime"> 
                          <input type="checkbox" value="1"  name="overtime" style="width: 18px;"  class="form-control overtime" name="" onchange="valueChanged()" {{$jobs->overtime == 1  ? 'checked' : ''}}/> 
                        </div>
                      </div>
                    </div>

                    <div class="row time" style="{{$jobs->overtime == 1  ? 'checked' : 'display: none'}};">
                      <div class="col-md-1">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Monday</label>
                          <input type="hidden" value="0" style="width: 18px;" class="form-control" name="monday"> 
                          <input type="checkbox" value="1" name="monday" style="width: 18px;"  class="form-control" {{$jobs->monday == 1  ? 'checked' : ''}}/> 
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Tuesday</label>
                          <input type="hidden" value="0" style="width: 18px;" class="form-control" name="tuesday"> 
                          <input type="checkbox" value="1" name="tuesday" style="width: 18px;"  class="form-control" {{$jobs->tuesday == 1  ? 'checked' : ''}}/> 
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Wednesday</label>
                          <input type="hidden" value="0" style="width: 18px;" class="form-control" name="wednesday"> 
                          <input type="checkbox" value="1" name="wednesday" style="width: 18px;"  class="form-control" {{$jobs->wednesday == 1  ? 'checked' : ''}}/> 
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Thursday</label>
                          <input type="hidden" value="0" style="width: 18px;" class="form-control" name="thursday"> 
                          <input type="checkbox" value="1" name="thursday" style="width: 18px;"  class="form-control" {{$jobs->thursday == 1  ? 'checked' : ''}}/> 
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Friday</label>
                          <input type="hidden" value="0" style="width: 18px;" class="form-control" name="friday"> 
                          <input type="checkbox" value="1" name="friday" style="width: 18px;"  class="form-control"{{$jobs->friday == 1  ? 'checked' : ''}}/> 
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Saturday</label>
                          <input type="hidden" value="0" style="width: 18px;" class="form-control" name="saturday"> 
                          <input type="checkbox" value="1" name="saturday" style="width: 18px;"  class="form-control"{{$jobs->saturday == 1  ? 'checked' : ''}}/> 
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Sunday</label>
                          <input type="hidden" value="0" style="width: 18px;" class="form-control" name="sunday"> 
                          <input type="checkbox" value="1" name="sunday" style="width: 18px;"  class="form-control"{{$jobs->sunday == 1  ? 'checked' : ''}}/> 
                        </div>
                      </div>
                    </div>

                    <div class="row time" style="{{$jobs->overtime == 1  ? 'checked' : 'display: none'}};">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label"> Start OverTime</label><br>
                          <input type="time" class="form-control" value="{{$jobs->start_overtime}}" name="start_overtime">
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label"> End OverTime</label><br>
                          <input type="time" class="form-control" name="end_overtime" value="{{$jobs->end_overtime}}">
                        </div>
                      </div>
                    </div>

                    <div class="row time" style="{{$jobs->overtime == 1  ? 'checked' : 'display: none'}};">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">OverTime Limit</label>
                          <input type="text" class="form-control" name="overtime_rate" value="{{$jobs->overtime_rate}}">
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">OverTime Limit</label>
                          <input type="text" class="form-control" name="overtime_limit" value="{{$jobs->overtime_limit}}">
                        </div>
                      </div>
                    </div>
                    
                     <button type="submit" class="btn btn-primary">Update</button>
                    <div class="clearfix"></div>
                  </form>
            </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    setTimeout(function() {
    $('#alert').fadeOut('slow');
}, 2000); // <-- time in milliseconds


    function valueChanged()
    {
        if($('.overtime').is(":checked"))   
            $(".time").show();
        else
            $(".time").hide();
    }


var select = document.getElementById('category_id');
select.onchange = function(){
 var id = $(this).val();

test($(this).val());
};


function test(id) {

$.ajax({
method:"get",
url: "{{route('job.data','')}}/"+parseInt(id),
dataType: "json",
contentType: false, // high importance!
processData: false, // high importance!
success:function(result)
{       
if(result)
{
    
var $mySelect = $('#mySelect');
$("#mySelect").empty();
$("#mySelect").append(new Option("Select Job Type", "value"));
$.each(result,function(key,value){

var $option = $("<option/>", { value: key,text: value});

$mySelect.append($option);

});
$('.selecs').reset();
}
}});
    }



</script>
@endsection
