@extends('layouts.master')

@section('title')
    Configurations
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <span class="pull-right">
                <a href="{{route('config.create')}}" style="float: right;" class="btn btn-success btn-sm" ><i class="fa fa-plus"></i> Add New Job Configuration</a></span>
                <h4 class="card-title ">Job Configurations</h4>
            </div>
            <div class="card-body">
                 @if ($message = Session::get('status'))
                    <div class="alert alert-success" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="table-responsive">
                <table class="table datatables">
                    <thead class=" text-dark">
                    <th>Job Title</th>
                    <th>Job Type</th>
                    <th>StartTime</th>
                    <th>EndTime</th>
                    <th>OverTime</th>
                    <th>Start OverTime</th>
                    <th>End OverTime</th>
                    <th>OverTime Limit</th>
                    <th style="padding-left:35px !important">Action</th>
                    </thead>
                    <tbody>
                        @foreach ($jobs as $item)
                            <tr>
                                <td>@if($item->job_id)
                                   {{App\Jobs::where('id',$item->job_id)->value('title')}}
                                   @else
                                   --
                                   @endif</td>
                                <td>@if($item->types)
                                    {{$item->types->name}}
                                    @else
                                    --
                                    @endif</td>
                                <td>{{$item->start_time}}</td>
                                <td>{{$item->end_time}}</td>
                                @if($item->overtime=="0")
                                <td>Disabled</td>
                                @else
                                <td>Active</td>
                                @endif

                                @if($item->overtime=="0")
                                <td>N/A</td>
                                @else
                                <td>{{$item->start_overtime}}</td>
                                @endif

                                @if($item->overtime=="0")
                                <td>N/A</td>
                                @else
                                <td>{{$item->end_overtime}}</td>
                                @endif

                                @if($item->overtime_limit=="")
                                <td>N/A</td>
                                @else
                                <td>{{$item->overtime_limit}}</td>
                                @endif
                                    
                                    

                                <td  width="16%" class="text-primary">
                                    <a href="{{route('config.edit',$item->id)}}" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></a>
                                
                                    <a href="{{route('config.delete',$item->id)}}" onclick="return confirm('This will delete all data related to this job configuration')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    setTimeout(function() {
    $('#alert').fadeOut('slow');
}, 2000); // <-- time in milliseconds
</script>
@endsection
