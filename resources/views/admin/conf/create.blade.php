@extends('layouts.master')

@section('title')
   New Job Configuration
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">New Job Configuration</h4>
            </div>
            <div class="card-body">
                @if ($message = Session::get('status'))
                    <div class="alert alert-success" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
                @endif
            <form method="POST" action="{{route('config.store')}}" enctype="multipart/form-data">
                  {{csrf_field()}}
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <select class="form-control" name="job_id" id="job_id" required="">
                            <option value="">Select Job Title</option>
                            @foreach(App\Jobs::get() as $data)
                              <option value="{{$data->id}}">{{$data->title}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <select class="form-control" name="job_type_id" id="jobtypes" required="">
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <select class="form-control" name="start_time" required="">
                            <option value="">Select Start Time</option>
                            @foreach(App\TimeList::get() as $data)
                              <option value="{{$data->name}}">{{$data->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                    <div class="col-md-6">
                      <div class="form-group">
                          <select class="form-control" name="end_time" required="">
                            <option value="">Select End Time</option>
                            @foreach(App\TimeList::get() as $data)
                              <option value="{{$data->name}}">{{$data->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <select class="form-control" name="map_to" required="">
                            <option value="">Select Map To</option>
                              <option value="hourly">hourly</option>
                              <option value="planttime_hourly">planttime_hourly</option>
                              <option value="plant_overtime">plant_overtime</option>
                              <option value="hourly_lp2_1/2">hourly_lp2_1/2</option>
                              <option value="hourly_lp4">hourly_lp4</option>
                              <option value="plant_hourly_lp2_1/2">plant_hourly_lp2_1/2</option>
                              <option value="plant_overtime_lp2_1/2">plant_overtime_lp2_1/2</option>
                              <option value="planttime_hourly_lp4">planttime_hourly_lp4</option>
                              <option value="plant_overtime_lp4">plant_overtime_lp4</option>
                              <option value="fixed_salary">fixed_salary</option>
                              <option value="double_time_hourly">double_time_hourly</option>
                              <option value="mechanic_callout">mechanic_callout</option>
                              <option value="vacation_pay_hourly">vacation_pay_hourly</option>
                              <option value="sick_pay_hourly">sick_pay_hourly</option>
                              <option value="holiday">holiday</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating"> OverTime</label>
                          <input type="hidden" value="0" style="width: 18px;" class="form-control" name="overtime"> 
                          <input type="checkbox" value="1" name="overtime" style="width: 18px;"  class="form-control overtime" onchange="valueChanged()"/> 
                        </div>
                      </div>
                    </div>

                    <div class="row time" style="display: none;">
                      <div class="col-md-1">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Monday</label>
                          <input type="hidden" value="0" style="width: 18px;" class="form-control" name="monday"> 
                          <input type="checkbox" value="1" name="monday" style="width: 18px;"  class="form-control"/> 
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Tuesday</label>
                          <input type="hidden" value="0" style="width: 18px;" class="form-control" name="tuesday"> 
                          <input type="checkbox" value="1" name="tuesday" style="width: 18px;"  class="form-control"/> 
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Wednesday</label>
                          <input type="hidden" value="0" style="width: 18px;" class="form-control" name="wednesday"> 
                          <input type="checkbox" value="1" name="wednesday" style="width: 18px;"  class="form-control"/> 
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Thursday</label>
                          <input type="hidden" value="0" style="width: 18px;" class="form-control" name="thursday"> 
                          <input type="checkbox" value="1" name="thursday" style="width: 18px;"  class="form-control"/> 
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Friday</label>
                          <input type="hidden" value="0" style="width: 18px;" class="form-control" name="friday"> 
                          <input type="checkbox" value="1" name="friday" style="width: 18px;"  class="form-control"/> 
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Saturday</label>
                          <input type="hidden" value="0" style="width: 18px;" class="form-control" name="saturday"> 
                          <input type="checkbox" value="1" name="saturday" style="width: 18px;"  class="form-control"/> 
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Sunday</label>
                          <input type="hidden" value="0" style="width: 18px;" class="form-control" name="sunday"> 
                          <input type="checkbox" value="1" name="sunday" style="width: 18px;"  class="form-control"/> 
                        </div>
                      </div>
                    </div>

                    <div class="row time" style="display: none;">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label"> Start OverTime</label><br>
                          <input type="time" class="form-control" name="start_overtime">
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label"> End OverTime</label><br>
                          <input type="time" class="form-control" name="end_overtime">
                        </div>
                      </div>
                    </div>

                    <div class="row time" style="display: none;">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">OverTime Rate</label>
                          <input type="number" class="form-control" name="overtime_rate">
                        </div>
                      </div>
                    
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">OverTime Limit</label>
                          <input type="text" class="form-control" name="overtime_limit">
                        </div>
                      </div>
                    </div>
                    
                     <button type="submit" class="btn btn-primary">Add</button>
                    <div class="clearfix"></div>
                  </form>
            </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    setTimeout(function() {
    $('#alert').fadeOut('slow');
}, 2000); // <-- time in milliseconds


    function valueChanged()
    {
        if($('.overtime').is(":checked"))   
            $(".time").show();
        else
            $(".time").hide();
    }


var select = document.getElementById('job_id');
select.onchange = function(){
var id = $(this).val();



getjobs($(this).val());

};


function getjobs(id) {

$.ajax({
method:"get",
url: "{{route('job.data','')}}/"+parseInt(id),
dataType: "json",
contentType: false, // high importance!
processData: false, // high importance!
success:function(result)
{       
if(result)
{
    
var $jobtypes = $('#jobtypes');
$("#jobtypes").empty();
$("#jobtypes").append(new Option("Select Job Type", "value"));
$.each(result,function(key,value){

var $option = $("<option/>", { value: key,text: value});

$jobtypes.append($option);

});
$('.selecs').reset();
}
}});
    }



</script>
@endsection
