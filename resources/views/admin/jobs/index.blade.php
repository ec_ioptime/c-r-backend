@extends('layouts.master')

@section('title')
    Jobs
@endsection

@section('content')
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="card">
            <div class="card-header card-header-primary">
                <span class="pull-right">
                <a href="{{route('jobs.create')}}" style="float: right;" class="btn btn-success btn-sm" ><i class="fa fa-plus"></i> Add New Job Title</a></span>
                <h4 class="card-title ">Job Titles</h4>
            </div>
            <div class="card-body">
                 @if ($message = Session::get('status'))
                    <div class="alert alert-success" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="table-responsive">
                <table class="table datatables">
                    <thead class=" text-dark">
                   
                    <th>Job Title</th>
                    <!-- <th>Job Types</th> -->
                    <th style="padding-left:35px !important">Action</th>
                    </thead>
                    <tbody>
                        @foreach ($jobs as $item)
                            <tr >
                               
                                <td width="75%">
                                    {{$item->title}}
                                </td>
                                <!-- <td>@foreach(App\Types::where('job_id',$item->id)->get() as $data)
                                    <a href="#" class="btn btn-default btn-sm" style="background-color: #2b4fa5">{{$data->name}}</a>
                                     @endforeach</td> -->
                                    
                                    

                                <td class="text-primary">
                                    <a href="{{route('jobs.edit',$item->id)}}" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></a>
                                
                                    <a href="{{route('jobs.delete',$item->id)}}" onclick="return confirm('This will delete all data related to this job title')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    setTimeout(function() {
    $('#alert').fadeOut('slow');
}, 2000); // <-- time in milliseconds
</script>
@endsection
