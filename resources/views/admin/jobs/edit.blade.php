@extends('layouts.master')

@section('title')
    Update Job
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Update Job</h4>
            </div>
            <div class="card-body">
                @if ($message = Session::get('status'))
                    <div class="alert alert-success" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
                @endif
            <form method="POST" action="{{route('jobs.update',$jobs->id)}}" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <input name="_method" type="hidden" value="PATCH">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Job Title</label>
                          <input type="text"  name="title" value="{{$jobs->title}}" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating"> On Site</label>
                         <input type="hidden" value="0" style="width: 18px;" class="form-control" name="on_site"> 
                          <input type="checkbox" value="1"  name="on_site" style="width: 18px;"  class="form-control overtime" name="" {{$jobs->on_site == 1  ? 'checked' : ''}}/> 
                        </div>
                      </div>
                    </div>
                    

                     <button type="submit" class="btn btn-primary  ">Update</button>
                    <div class="clearfix"></div>
                  </form>
            </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    setTimeout(function() {
    $('#alert').fadeOut('slow');
}, 2000); // <-- time in milliseconds
</script>
@endsection
