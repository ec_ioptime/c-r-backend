@extends('layouts.master')

@section('title')
    Edit Property
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Edit Property</h4>
                {{-- <p class="card-category"> All registered users</p> --}}
                  @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            </div>
            <div class="card-body">
            <form method="POST" action="/update-property/{{$property->id}}" >
                  {{csrf_field()}}

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group p-2 mt-4 bmd-form-group is-filled">
                                <label class="bmd-label-floating" style="margin-top: -1px;">Name</label>
                                <input type="text" value="{{$property->name}}" name="name"   class="form-control">
                            </div>
                         </div>
                        <div class="col-md-6">
                            <div class="form-group p-2 mt-3 bmd-form-group is-filled">
                                <label class="bmd-label-floating">Client</label>
                                <select class="form-control"  name="client_id" id="client_id">
                                    <option >Select a client</option>
                                    @foreach ($clients as $item)
                                    <option selected="{{$property->client_id ==$item->id?'true':'false'}}" value="{{$item->id}}">{{$item->first_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                      </div>
                      <br><hr>
                     <button type="submit" class="btn btn-primary  ">Update</button>
                    <a href="{{ url()->previous() }}" class="btn btn-warning ">Cancel</a>
                    <div class="clearfix"></div>
                  </form>
            </div>
            </div>
        </div>
    </div>

@endsection
