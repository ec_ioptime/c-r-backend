@extends('layouts.master')

@section('title')
    Create Property
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Create Property</h4>
                {{-- <p class="card-category"> All registered users</p> --}}
                  @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            </div>
            <div class="card-body">
            <form method="POST" action="/create-property" >
                  {{csrf_field()}}

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group ">
                          <label class="bmd-label-floating" style="margin-top: -14px;">Name</label>
                          <input type="text" name="name" class="form-control mt-4 pb-3" style="margin-top: 43px !important;">
                        </div>
                      </div>
                      <div class="col-md-6 ">
                        <div class="form-group ">
                          <label class="bmd-label-floating">Client</label>
                            <select name="client_id" id="client_id" class="form-control">
                                <option >Select</option>
                                @foreach ($clients as $item)
                                <option value="{{$item->id}}" >{{$item->first_name}}</option>
                                @endforeach
                            </select>
                         </div>
                      </div>
                    </div>
                    <br><hr>
                    <button type="submit" class="btn btn-primary  ">Create</button>
                    <a href="{{ url()->previous() }}" class="btn btn-warning ">Cancel</a>
                    <div class="clearfix"></div>
                  </form>
            </div>
        </div>
    </div>
</div>

@endsection
