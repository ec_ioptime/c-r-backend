@extends('layouts.master')

@section('title')
    Create Client
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Create Client</h4>
            </div>
            <div class="card-body">
            <form method="POST" action="/client" enctype="multipart/form-data">
                  {{csrf_field()}}
                  {{-- {{method_field('PUT')}} --}}
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">First Name</label>
                          <input type="text"  name="first_name" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Last Name</label>
                          <input type="text"  name="last_name" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Phone Number</label>
                          <input type="text"  name="phone_number" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email address</label>
                          <input  name="email" type="email"  class="form-control">
                        </div>
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">City</label>
                                <input type="text"  name="city" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">State</label>
                                <input type="text"  name="state" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Address</label>
                                <input type="text"  name="address" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Country</label>
                            <input type="text"  name="country" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Zip</label>
                          <input type="text"  name="zip" class="form-control">
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label mb-3 ">Date of Inspection</label>
                            <input type="date"  name="date_of_inspection" class="mt-3 form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Inspection Fee</label>
                            <input type="number"  name="inspection_fee" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Realtor 1 (optional)</label>
                            <input type="text"  name="realtor1" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Realtor 2 (optional)</label>
                            <input type="text"  name="realtor2" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Realtor1 Email </label>
                            <input type="email"  name="realtor1_email" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Realtor2 Email </label>
                            <input type="email"  name="realtor2_email" class="form-control">
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Weather</label>
                            <input type="text"  name="weather" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Direction</label>
                            <input type="text"  name="direction" class="form-control">
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-6 mb-3">
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image" name="image" lang="es">
                                <label class="custom-file-label" for="image">
                                    <span class="btn btn-info">
                                        click to select image
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="bmd-label-floating">Summary</label>
                            <textarea type="text"  name="summary" class="form-control">
                            </textarea>
                        </div>
                    </div>
                </div>
                     <button type="submit" class="btn btn-primary  ">Create</button>
                    <button type="button" class="btn btn-warning ">Cancel</button>
                    <div class="clearfix"></div>
                  </form>
            </div>
            </div>
        </div>
    </div>

@endsection
