@extends('layouts.master')

@section('title')
    Edit Client
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Edit Client</h4>
                {{-- <p class="card-category"> All registered users</p> --}}
            </div>
            <div class="card-body">
            <form method="POST" action="/update-client/{{$client->id}}" enctype="multipart/form-data">
                  {{csrf_field()}}
                  {{-- {{method_field('PUT')}} --}}

                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">First Name</label>
                          <input type="text" value="{{$client->first_name}}" name="first_name" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Last Name</label>
                          <input type="text" value="{{$client->last_name}}" name="last_name" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Phone Number</label>
                          <input type="text" value="{{$client->phone_number}}" name="phone_number" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email address</label>
                          <input  name="email" type="email" value="{{$client->email}}" class="form-control">
                        </div>
                      </div>
                    <hr>
                    <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Address</label>
                          <input type="text" value="{{$client->address}}" name="address" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">City</label>
                          <input type="text" value="{{$client->city}}" name="city" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">State</label>
                          <input type="text" value="{{$client->state}}" name="state" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Country</label>
                          <input type="text" value="{{$client->country}}" name="country" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Zip</label>
                          <input type="text" value="{{$client->zip}}" name="zip" class="form-control">
                        </div>
                      </div>
                      <hr>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Date of Inspection</label>
                          <input type="date" value="{{$client->date_of_inspection}}" name="date_of_inspection" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Inspection Fee</label>
                          <input type="number" value="{{$client->inspection_fee}}" name="inspection_fee" class="form-control">
                        </div>
                      </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Realtor 1 (optional)</label>
                          <input type="text" value="{{$client->realtor1}}" name="realtor1" class="form-control">
                        </div>
                      </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Realtor 2 (optional)</label>
                          <input type="text" value="{{$client->realtor2}}" name="realtor2" class="form-control">
                        </div>
                      </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Realtor1 Email </label>
                          <input type="email" value="{{$client->realtor1_email}}" name="realtor1_email" class="form-control">
                        </div>
                      </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Realtor2 Email </label>
                          <input type="email" value="{{$client->realtor2_email}}" name="realtor2_email" class="form-control">
                        </div>
                      </div>
                      <hr>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Weather</label>
                          <input type="text" value="{{$client->weather}}" name="weather" class="form-control">
                        </div>
                      </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Direction</label>
                          <input type="text" value="{{$client->direction}}" name="direction" class="form-control">
                        </div>
                      </div>
                    <div class="col-md-12 mt-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Summary</label>
                          <textarea type="text"  name="summary" class="form-control">{{$client->summary}}</textarea>
                        </div>
                      </div>
                    <hr>
                    <div class="col-md-6 mb-3">
                        <div class="form-group">
                            <div class="custom-file">
                                <img  height="100px" src="{{ asset($client->image)}}" alt="">
                            </div>
                        </div>
                    </div>

                        <div class="col-md-6 mb-3">
                            <div class="form-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="image" name="image" lang="es">
                                    <label class="custom-file-label" for="image">
                                        <span class="btn btn-info">
                                            click to change image
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>

                      </div>
                      <br><hr>
                     <button type="submit" class="btn btn-primary  ">Update</button>
                    <a href="{{ url()->previous() }}" class="btn btn-warning ">Cancel</a>
                    <div class="clearfix"></div>
                  </form>
            </div>
            </div>
        </div>
    </div>

@endsection
