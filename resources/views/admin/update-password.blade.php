@extends('layouts.master')

@section('title')
    Reset Password
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Reset Password</h4>
            </div>
            <div class="card-body">
                @if ($message = Session::get('status'))
                    <div class="alert alert-success" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
                @endif
            <form method="POST" action="{{route('reset.password')}}" enctype="multipart/form-data">
                  {{csrf_field()}}
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">New Password</label>
                          <input type="password"  name="password" id="password" class="form-control" required="">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Confirm Password</label>
                          <input type="password" onkeyup="check()" name="confirm_password" id="confirm_password" class="form-control" required="">
                        </div>
                        <p id="message"></p>
                      </div>
                    </div>

                     <button type="submit" class="btn btn-primary" id="reset">Reset</button>
                    <div class="clearfix"></div>
                  </form>
            </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    setTimeout(function() {
    $('#alert').fadeOut('slow');
}, 2000); // <-- time in milliseconds

 var check = function() {

 if (document.getElementById('password').value =="" && document.getElementById('confirm_password').value =="") {
 
 document.getElementById('message').style.color = '';
 document.getElementById('message').innerHTML = '';
 document.getElementById("reset").disabled = false; 

 }else if(document.getElementById('confirm_password').value ==""){

  document.getElementById("reset").disabled = false; 

 } else if(document.getElementById('password').value == document.getElementById('confirm_password').value) {

 document.getElementById('message').style.color = 'green';
 document.getElementById('message').innerHTML = 'Matched';
 document.getElementById("reset").disabled = false;   
 }
 else{
  
 document.getElementById('message').style.color = 'red';
 document.getElementById('message').innerHTML = 'Not Matched';
 document.getElementById("reset").disabled = true;
  }
}

</script>
@endsection
