@extends('layouts.master')

@section('title')
    Properties
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Properties</h4>
                @if ($message = Session::get('status'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
                @endif
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                    <th  style="width:10%">
                        ID
                    </th>
                    <th style="width:30%">
                        Name
                    </th>
                    <th style="width:30%">
                        User
                    </th>
                    <th  style="width:15%; text-align:center;">
                        Delete
                    </th>
                    <th   style="width:15%; text-align:center;">
                        Edit
                    </th>
                    </thead>
                    <tbody>
                        @foreach ($properties as $item)
                            <tr >
                                <td>
                                    {{$item->id}}
                                </td>
                                <td>
                                    {{$item->name}}
                                </td>
                                <td>
                                   {{$item->user['first_name'].' '.$item->user['last_name']}}
                                </td>
                                <td class="text-primary text-center">
                                    <a href="/edit-property/{{$item->id}}" class="btn btn-info">EDIT</a>
                                </td>
                                <td class="text-warning text-center">
                                    <a href="/delete-property/{{$item->id}}" class="btn btn-warning">DELETE</a>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>

          </div>
@endsection

@section('scripts')

@endsection
