@extends('layouts.master')

@section('title')
    Clients
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Users</h4>
                <p class="card-category">
                    All registered users
                </p>
                @if ($message = Session::get('status'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
                @endif
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                        <th>
                            ID
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            User Type
                        </th>
                        <th>
                            Edit
                        </th>
                        {{-- <th>
                            Delete
                        </th> --}}
                    </thead>
                    <tbody>
                        @foreach ($users as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->email}}</td>
                            <td>{{$item->usertype}}</td>
                            <td class="text-primary">
                                <a href="/role-edit/{{$item->id}}" class="btn btn-info">EDIT</a>
                            </td>
                            {{-- <td class="text-warning">
                                <a href="/role-delete/{{$item->id}}" class="btn btn-warning">DELETE</a>
                            </td> --}}
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>

    </div>
@endsection

@section('scripts')

@endsection
