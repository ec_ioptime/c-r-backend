@extends('layouts.master')

@section('title')
Edit Report
@endsection
@section('content')

<style>
    .container .bmd-form-group .bmd-label-static {
        top: .35rem;
        left: 0;
        font-size: .875rem;
    }

    .container .bmd-form-group .form-control,
    .bmd-form-group input::placeholder,
    .bmd-form-group label {
        line-height: 1.1;
    }

    .form-group {
        position: relative;
        padding-top: 0.75rem;
    }

    .container .form-control,
    .is-focused .form-control {
        background-image: linear-gradient(0deg, #FF6E66 2px,
                rgba(156, 39, 176, 0) 0), linear-gradient(0deg, #d2d2d2 1px, hsla(0, 0%, 82%, 0) 0) !important;
    }

    .is-focused [class*=" bmd-label"],
    .is-focused [class^=bmd-label] {
        color: #FF6E66 !important;
    }

    .form-control {
        background: no-repeat bottom, 50% calc(100% - 1px);
        background-size: 0 100%, 100% 100%;
        border: 0;
        height: 36px;
        transition: background 0s ease-out;
        padding-left: 0;
        padding-right: 0;
        border-radius: 0;
        font-size: 14px !important;
    }

    /* dropdown */
    .dropdown-menu.bootstrap-datetimepicker-widget.open {
        opacity: 1;
        transform: scale(1);
        top: 0;
    }

    .bootstrap-datetimepicker-widget.dropdown-menu {
        padding: 4px;
        width: 19em;
    }

    .bootstrap-datetimepicker-widget .list-unstyled {
        margin: 0;
    }

    .sr-only,
    .bootstrap-datetimepicker-widget .btn[data-action="incrementHours"]::after,
    .bootstrap-datetimepicker-widget .btn[data-action="incrementMinutes"]::after,
    .bootstrap-datetimepicker-widget .btn[data-action="decrementHours"]::after,
    .bootstrap-datetimepicker-widget .btn[data-action="decrementMinutes"]::after,
    .bootstrap-datetimepicker-widget .btn[data-action="showHours"]::after,
    .bootstrap-datetimepicker-widget .btn[data-action="showMinutes"]::after,
    .bootstrap-datetimepicker-widget .btn[data-action="togglePeriod"]::after,
    .bootstrap-datetimepicker-widget .btn[data-action="clear"]::after,
    .bootstrap-datetimepicker-widget .btn[data-action="today"]::after,
    .bootstrap-datetimepicker-widget .picker-switch::after,
    .bootstrap-datetimepicker-widget table th.prev::after,
    .bootstrap-datetimepicker-widget table th.next::after {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0;
    }

    .bootstrap-datetimepicker-widget {
        list-style: none;
    }

    .bootstrap-datetimepicker-widget a:hover {
        box-shadow: none !important;
    }

    .bootstrap-datetimepicker-widget a .btn:hover {
        background-color: transparent;
    }

    .bootstrap-datetimepicker-widget.dropdown-menu {
        padding: 10px;
        width: 19em;
    }

    @media (min-width: 768px) {
        .bootstrap-datetimepicker-widget.dropdown-menu.timepicker-sbs {
            width: 38em;
        }
    }

    @media (min-width: 991px) {
        .bootstrap-datetimepicker-widget.dropdown-menu.timepicker-sbs {
            width: 38em;
        }
    }

    @media (min-width: 1200px) {
        .bootstrap-datetimepicker-widget.dropdown-menu.timepicker-sbs {
            width: 38em;
        }
    }

    .bootstrap-datetimepicker-widget.dropdown-menu.bottom:before,
    .bootstrap-datetimepicker-widget.dropdown-menu.bottom:after {
        right: auto;
        left: 12px;
    }

    .bootstrap-datetimepicker-widget.dropdown-menu.top {
        margin-top: auto;
        margin-bottom: 27px;
    }

    .bootstrap-datetimepicker-widget.dropdown-menu.top.open {
        margin-top: auto;
        margin-bottom: 27px;
    }

    .bootstrap-datetimepicker-widget.dropdown-menu.pull-right:before {
        left: auto;
        right: 6px;
    }

    .bootstrap-datetimepicker-widget.dropdown-menu.pull-right:after {
        left: auto;
        right: 7px;
    }

    .bootstrap-datetimepicker-widget .list-unstyled {
        margin: 0;
    }

    .bootstrap-datetimepicker-widget a[data-action] {
        padding: 0;
        margin: 0;
        border-width: 0;
        background-color: transparent;
        color: #9c27b0;
        box-shadow: none;
    }

    .bootstrap-datetimepicker-widget a[data-action]:hover {
        background-color: transparent;
    }

    .bootstrap-datetimepicker-widget a[data-action]:hover span {
        background-color: #eee;
        color: #FF6E66;
    }

    .bootstrap-datetimepicker-widget a[data-action]:active {
        box-shadow: none;
    }

    .bootstrap-datetimepicker-widget .timepicker-hour,
    .bootstrap-datetimepicker-widget .timepicker-minute,
    .bootstrap-datetimepicker-widget .timepicker-second {
        width: 40px;
        height: 40px;
        line-height: 40px;
        font-weight: 300;
        font-size: 1.125rem;
        margin: 0;
        border-radius: 50%;
    }

    .bootstrap-datetimepicker-widget button[data-action] {
        width: 38px;
        height: 38px;
        margin-right: 3px;
        padding: 0;
    }

    .bootstrap-datetimepicker-widget .btn[data-action="incrementHours"]::after {
        content: "Increment Hours";
    }

    .bootstrap-datetimepicker-widget .btn[data-action="incrementMinutes"]::after {
        content: "Increment Minutes";
    }

    .bootstrap-datetimepicker-widget .btn[data-action="decrementHours"]::after {
        content: "Decrement Hours";
    }

    .bootstrap-datetimepicker-widget .btn[data-action="decrementMinutes"]::after {
        content: "Decrement Minutes";
    }

    .bootstrap-datetimepicker-widget .btn[data-action="showHours"]::after {
        content: "Show Hours";
    }

    .bootstrap-datetimepicker-widget .btn[data-action="showMinutes"]::after {
        content: "Show Minutes";
    }

    .bootstrap-datetimepicker-widget .btn[data-action="togglePeriod"]::after {
        content: "Toggle AM/PM";
    }

    .bootstrap-datetimepicker-widget .btn[data-action="clear"]::after {
        content: "Clear the picker";
    }

    .bootstrap-datetimepicker-widget .btn[data-action="today"]::after {
        content: "Set the date to today";
    }

    .bootstrap-datetimepicker-widget .picker-switch {
        text-align: center;
        border-radius: 3px;
        font-size: 0.875rem;
    }

    .bootstrap-datetimepicker-widget .picker-switch::after {
        content: "Toggle Date and Time Screens";
    }

    .bootstrap-datetimepicker-widget .picker-switch td {
        padding: 0;
        margin: 0;
        height: auto;
        width: auto;
        line-height: inherit;
    }

    .bootstrap-datetimepicker-widget .picker-switch td span {
        line-height: 2.5;
        height: 2.5em;
        width: 100%;
        border-radius: 3px;
        margin: 2px 0px !important;
    }

    .bootstrap-datetimepicker-widget table {
        width: 100%;
        margin: 0;
    }

    .bootstrap-datetimepicker-widget table.table-condensed tr>td {
        text-align: center;
    }

    .bootstrap-datetimepicker-widget table td>div,
    .bootstrap-datetimepicker-widget table th>div {
        text-align: center;
    }

    .bootstrap-datetimepicker-widget table th {
        height: 20px;
        line-height: 20px;
        width: 20px;
        font-weight: 500;
    }

    .bootstrap-datetimepicker-widget table th.picker-switch {
        width: 145px;
    }

    .bootstrap-datetimepicker-widget table th.disabled,
    .bootstrap-datetimepicker-widget table th.disabled:hover {
        background: none;
        color: rgba(0, 0, 0, 0.12);
        cursor: not-allowed;
    }

    .bootstrap-datetimepicker-widget table th.prev span,
    .bootstrap-datetimepicker-widget table th.next span {
        border-radius: 3px;
        height: 27px;
        width: 27px;
        line-height: 28px;
        font-size: 12px;
        border-radius: 50%;
        text-align: center;
    }

    .bootstrap-datetimepicker-widget table th.prev::after {
        content: "Previous Month";
    }

    .bootstrap-datetimepicker-widget table th.next::after {
        content: "Next Month";
    }

    .bootstrap-datetimepicker-widget table th.dow {
        text-align: center;
        border-bottom: 1px solid rgba(0, 0, 0, 0.12);
        font-size: 12px;
        text-transform: uppercase;
        color: rgba(0, 0, 0, 0.87);
        font-weight: 400;
        padding-bottom: 5px;
        padding-top: 10px;
    }

    .bootstrap-datetimepicker-widget table thead tr:first-child th {
        cursor: pointer;
    }

    .bootstrap-datetimepicker-widget table thead tr:first-child th:hover span,
    .bootstrap-datetimepicker-widget table thead tr:first-child th.picker-switch:hover {
        background: #eee;
    }

    .bootstrap-datetimepicker-widget table td>div {
        border-radius: 3px;
        height: 54px;
        line-height: 54px;
        width: 54px;
        text-align: center;
    }

    .bootstrap-datetimepicker-widget table td.cw>div {
        font-size: .8em;
        height: 20px;
        line-height: 20px;
        color: #999;
    }

    .bootstrap-datetimepicker-widget table td.day>div {
        height: 30px;
        line-height: 30px;
        width: 30px;
        text-align: center;
        padding: 0px;
        border-radius: 50%;
        position: relative;
        z-index: -1;
        color: #3C4858;
        font-size: 0.875rem;
    }

    .bootstrap-datetimepicker-widget table td.minute>div,
    .bootstrap-datetimepicker-widget table td.hour>div {
        border-radius: 50%;
    }

    .bootstrap-datetimepicker-widget table td.day:hover>div,
    .bootstrap-datetimepicker-widget table td.hour:hover>div,
    .bootstrap-datetimepicker-widget table td.minute:hover>div,
    .bootstrap-datetimepicker-widget table td.second:hover>div {
        background: #eee;
        cursor: pointer;
    }

    .bootstrap-datetimepicker-widget table td.old>div,
    .bootstrap-datetimepicker-widget table td.new>div {
        color: #999;
    }

    .bootstrap-datetimepicker-widget table td.today>div {
        position: relative;
    }

    .bootstrap-datetimepicker-widget table td.today>div:before {
        content: '';
        display: inline-block;
        border: 0 0 7px 7px solid transparent;
        border-bottom-color: #ff6e66;
        border-top-color: rgba(0, 0, 0, 0.2);
        position: absolute;
        bottom: 4px;
        right: 4px;
    }

    .bootstrap-datetimepicker-widget table td.active>div,
    .bootstrap-datetimepicker-widget table td.active:hover>div {
        background-color: #ff6e66;
        color: #fff;
        box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(156, 39, 176, 0.4);
    }

    .bootstrap-datetimepicker-widget table td.active.today:before>div {
        border-bottom-color: #fff;
    }

    .bootstrap-datetimepicker-widget table td.disabled>div,
    .bootstrap-datetimepicker-widget table td.disabled:hover>div {
        background: none;
        color: rgba(0, 0, 0, 0.12);
        cursor: not-allowed;
    }

    .bootstrap-datetimepicker-widget table td span {
        display: inline-block;
        width: 40px;
        height: 40px;
        line-height: 40px;
        margin: 3px 3px;
        cursor: pointer;
        border-radius: 50%;
        text-align: center;
    }

    .bootstrap-datetimepicker-widget table td span:hover {
        background: #eee;
    }

    .bootstrap-datetimepicker-widget table td span.active {
        background-color: #9c27b0;
        color: #fff;
    }

    .bootstrap-datetimepicker-widget table td span.old {
        color: #999;
    }

    .bootstrap-datetimepicker-widget table td span.disabled,
    .bootstrap-datetimepicker-widget table td span.disabled:hover {
        background: none;
        color: rgba(0, 0, 0, 0.12);
        cursor: not-allowed;
    }

    .bootstrap-datetimepicker-widget .timepicker-picker span,
    .bootstrap-datetimepicker-widget .timepicker-hours span,
    .bootstrap-datetimepicker-widget .timepicker-minutes span {
        border-radius: 50% !important;
    }

    .bootstrap-datetimepicker-widget.usetwentyfour td.hour {
        height: 27px;
        line-height: 27px;
    }
</style>

<link rel="stylesheet"
    href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css"
    integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <!--  <div class="card-header card-header-primary">
                <h4 class="card-title ">Edit Report</h4>
                {{-- <p class="card-category"> All registered users</p> --}}
                  @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            </div> -->
            <div class="card-body" style="padding: 25px">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(session('status_error'))
                <div class="alert alert-danger" style="padding: 20px">
                    {{ session('status_error') }}
                </div>
                @endif

                @if(session('status'))
                <div class="alert alert-success" style="padding: 20px">
                    {{ session('status') }}
                </div>
                @endif

                @if(session('status_warning'))
                <div class="alert alert-warning" style="padding: 20px">
                    {{ session('status_warning') }}
                </div>
                @endif
                <form method="POST" action="{{route('logs.update',$timelogs->id)}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input name="_method" type="hidden" value="PATCH">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="form-control" name="job_id" id="job_id" required="">
                                    <option value="">Select Job Title</option>
                                    @foreach(App\Jobs::get() as $data)
                                    <option data-id="{{$data->on_site}}" value="{{$data->id}}"
                                        {{$timelogs->job_id == $data->id  ? 'selected' : ''}}>{{$data->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="form-control selecs" name="job_type_id" id="jobtypes">
                                    @foreach(App\Types::where('job_id',$timelogs->job_id)->get() as $data2)
                                    <option value="{{$data2->id}}"
                                        {{$timelogs->job_type_id == $data2->id  ? 'selected' : ''}}>
                                        {{$data2->name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    @if($timelogs->job_type_id == 0)
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label">
                                    <font color="#ff6f67">Clock-InTime</font>
                                </label>@if($clock_in_lat!="" && $clock_in_lng!="")
                                <button type="button"
                                    style="float: right;margin-bottom: -19px;background-color: transparent;border: none;"
                                    data-toggle="modal" data-lat="{{$clock_in_lat}}" data-lng="{{$clock_in_lng}}"
                                    onclick="getlatlng(this);" data-target="#myModal"><img
                                        src="https://cr.ioptime.com/public/img/map.png"></button>
                                @endif
                                <br>
                                <input type="text" class="form-control datetimepicker"
                                    value="@if($timelogs->clock_intime!=""){{date('m/d/Y\TH:i', strtotime($timelogs->clock_intime))}}@endif"
                                    name="clock_intime" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label">
                                    <font color="#ff6f67">Clock-OutTime</font>
                                </label>
                                @if($clockout_lat!="" && $clockout_lng!="")
                                <button type="button"
                                    style="float: right;margin-bottom: -19px;background-color: transparent;border: none;"
                                    data-toggle="modal" data-lat="{{$clockout_lat}}" data-lng="{{$clockout_lng}}"
                                    onclick="getlatlng(this);" data-target="#myModal"><img
                                        src="https://cr.ioptime.com/public/img/map.png"></button>
                                @endif
                                <br>
                                <input type="text" class="form-control datetimepicker"
                                    value="@if($timelogs->clock_outtime!=""){{date('m/d/Y\TH:i', strtotime($timelogs->clock_outtime))}}@endif"
                                    name="clock_outtime" class="form-control">
                            </div>
                        </div>
                    </div>

                    @else
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label">
                                    <font color="#ff6f67">Clock-InTime
                                    </font>
                                </label>
                                @if($clock_in_lat!="" && $clock_in_lng!="")
                                <button type="button"
                                    style="float: right;margin-bottom: -19px;background-color: transparent;border: none;"
                                    data-toggle="modal" data-lat="{{$clock_in_lat}}" data-lng="{{$clock_in_lng}}"
                                    onclick="getlatlng(this);" data-target="#myModal"><img
                                        src="https://cr.ioptime.com/public/img/map.png"></button>
                                @endif
                                <br>
                                <input type="text" class="form-control datetimepicker"
                                    value="@if($timelogs->clock_intime!=""){{date('m/d/Y\TH:i', strtotime($timelogs->clock_intime))}}@endif"
                                    name="clock_intime" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label">
                                    <font color="#ff6f67">Leave-YardTime</font>
                                </label>
                                @if($leaveyard_lat!="" && $leaveyard_lng!="")
                                <button type="button"
                                    style="float: right;margin-bottom: -19px;background-color: transparent;border: none;"
                                    data-toggle="modal" data-lat="{{$leaveyard_lat}}" data-lng="{{$leaveyard_lng}}"
                                    onclick="getlatlng(this);" data-target="#myModal"><img
                                        src="https://cr.ioptime.com/public/img/map.png"></button>
                                @endif
                                <br>
                                <input type="text" class="form-control datetimepicker"
                                    value="@if($timelogs->leave_yardtime!=""){{date('m/d/Y\TH:i', strtotime($timelogs->leave_yardtime))}}@endif"
                                    name="leave_yardtime" class="form-control">
                            </div>
                        </div>
                    </div>

                    @if($timelogs->jobs->on_site == "1")
                    @else
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label">
                                    <font color="#ff6f67">Arrive-JobTime </font>
                                </label>
                                @if($arrivejob_lat!="" && $arrivejob_lng!="")
                                <button type="button"
                                    style="float: right;margin-bottom: -19px;background-color: transparent;border: none;"
                                    data-toggle="modal" data-lat="{{$arrivejob_lat}}" data-lng="{{$arrivejob_lng}}"
                                    onclick="getlatlng(this);" data-target="#myModal"><img
                                        src="https://cr.ioptime.com/public/img/map.png"></button>
                                @endif
                                <br>
                                <input type="text" class="form-control datetimepicker"
                                    value="@if($timelogs->arrive_jobtime!=""){{date('m/d/Y\TH:i', strtotime($timelogs->arrive_jobtime))}}@endif"
                                    name="arrive_jobtime" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label">
                                    <font color="#ff6f67">Leave-JobTime </font>
                                </label>
                                @if($leavejob_lat!="" && $leavejob_lng!="")
                                <button type="button"
                                    style="float: right;margin-bottom: -19px;background-color: transparent;border: none;"
                                    data-toggle="modal" data-lat="{{$leavejob_lat}}" data-lng="{{$leavejob_lng}}"
                                    onclick="getlatlng(this);" data-target="#myModal"><img
                                        src="https://cr.ioptime.com/public/img/map.png"></button>
                                @endif
                                <br>
                                <input type="text" class="form-control datetimepicker"
                                    value="@if($timelogs->leave_jobtime!=""){{date('m/d/Y\TH:i', strtotime($timelogs->leave_jobtime))}}@endif"
                                    name="leave_jobtime" class="form-control">
                            </div>
                        </div>
                    </div>

                    @endif
                    @if($timelogs->jobs->on_site == "1")
                    @else

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label">
                                    <font color="#ff6f67">Arrive-YardTime </font>
                                </label>
                                @if($arriveyard_lat!="" && $arriveyard_lng!="")
                                <button type="button"
                                    style="float: right;margin-bottom: -19px;background-color: transparent;border: none;"
                                    data-toggle="modal" data-lat="{{$arriveyard_lat}}" data-lng="{{$arriveyard_lng}}"
                                    onclick="getlatlng(this);" data-target="#myModal"><img
                                        src="https://cr.ioptime.com/public/img/map.png"></button>
                                @endif
                                <br>
                                <input type="text" class="form-control datetimepicker"
                                    value="@if($timelogs->arrive_yardtime!=""){{date('m/d/Y\TH:i', strtotime($timelogs->arrive_yardtime))}}@endif"
                                    name="arrive_yardtime" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label">
                                    <font color="#ff6f67">Clock-OutTime</font>
                                </label>
                                @if($clockout_lat!="" && $clockout_lng!="")
                                <button type="button"
                                    style="float: right;margin-bottom: -19px;background-color: transparent;border: none;"
                                    data-toggle="modal" data-lat="{{$clockout_lat}}" data-lng="{{$clockout_lng}}"
                                    onclick="getlatlng(this);" data-target="#myModal"><img
                                        src="https://cr.ioptime.com/public/img/map.png"></button>
                                @endif
                                <br>
                                <input id="clockout" type="text" class="form-control datetimepicker"
                                    value="@if($timelogs->clock_outtime!=""){{date('m/d/Y\TH:i', strtotime($timelogs->clock_outtime))}}@endif"
                                    name="clock_outtime" class="form-control">
                            </div>
                        </div>
                    </div>

                    @endif
                    @endif

                    <!-- @if($timelogs->jobs->on_site == "1")
                  <a href="#" id="show2" data-toggle="modal" data-target="#promoModal2" class="btn btn-info btn-sm">Add Break Times</a>
                  @endif -->

                    @if(App\Breaks::where('time_log_id',$timelogs->id)->count() > 0)
                    <a href="#" id="show" class="btn btn-info btn-sm">View Break Times</a>
                    @endif
                    @if($timelogs->jobs->on_site)
                    <a href="#" id="addbreak" data-toggle="modal" data-target="#promoModal2"
                        class="btn btn-success btn-sm">Add Break Times</a>
                    @endif
                    <br>

                    <h4 class="breaks" style="display: none;">Total Break Time:
                        <b>{{round(App\Breaks::where('time_log_id',$timelogs->id)->sum('total_time'),2)}}</b></h4>

                    @foreach(App\Breaks::where('time_log_id',$timelogs->id)->get()->chunk(1) as $chunk)
                    <div class="row breaks" id="breaks" style="display: none;">
                        @foreach($chunk as $breaks)
                        <div class="col-md-5">
                            <div class="form-group">

                                <?php

                            $started_location = json_decode($breaks->startedAtLocation);
                            $ended_location = json_decode($breaks->endedAtLocation);

                            ?>

                                <label class="bmd-label">
                                    <font color="#ff6f67">Started At</font>
                                </label>

                                @if($started_location!="")
                                <button type="button"
                                    style="float: right;margin-bottom: -19px;background-color: transparent;border: none;"
                                    data-toggle="modal" data-lat="{{$started_location->latitude}}"
                                    data-lng="{{$started_location->longitude}}" onclick="getlatlng(this);"
                                    data-target="#myModal"><img
                                        src="https://cr.ioptime.com/public/img/map.png"></button>
                                @endif
                                <br>
                                <input type="text" class="form-control datetimepicker" id="startvalue"
                                    value="@if($breaks->startedAt!=""){{date('m/d/Y\TH:i', strtotime($breaks->startedAt))}}@endif"
                                    name="startedAt" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="bmd-label">
                                    <font color="#ff6f67">Ended At</font>
                                </label>
                                @if($ended_location!="")
                                <button type="button"
                                    style="float: right;margin-bottom: -19px;background-color: transparent;border: none;"
                                    data-toggle="modal" data-lat="{{$ended_location->latitude}}"
                                    data-lng="{{$ended_location->latitude}}" onclick="getlatlng(this);"
                                    data-target="#myModal"><img
                                        src="https://cr.ioptime.com/public/img/map.png"></button>
                                @endif
                                <br>
                                <input type="text" class="form-control datetimepicker" id="endvalue"
                                    value="@if($breaks->endedAt!=""){{date('m/d/Y\TH:i', strtotime($breaks->endedAt))}}@endif"
                                    name="endedAt" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <button type="button" onclick="test(this);" data-id="{{$breaks->id}}"
                                    startedAt="{{date('m/d/Y H:i A', strtotime($breaks->startedAt))}}"
                                    endedAt="{{date('m/d/Y H:i A', strtotime($breaks->endedAt))}}" data-toggle="modal"
                                    data-target="#promoModal" class="btn btn-success btn-sm" value="Apply" name=""
                                    style="margin-top: 24px;"><i class="fa fa-pencil"></i></button>
                                <a style="margin-top: 24px;" href="{{route('breaks.delete',$breaks->id)}}"
                                    onclick="return confirm('Are you sure you want to delete this break time?')"
                                    class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    @endforeach

                    <br>
                    <hr>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font size="3" color="#ff6f67">Hourly</font>
                                </label>
                                <input type="text" value="{{$timelogs->hourly}}" name="hourly" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font size="3" color="#ff6f67">OverTime (x1.5) Hourly</font>
                                </label>
                                <input type="text" value="{{$timelogs->overtime_hourly}}" name="overtime_hourly"
                                    class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font size="3" color="#ff6f67">Double-Time Hourly</font>
                                </label>
                                <input type="text" value="{{$timelogs->double_time_hourly}}" name="double_time_hourly"
                                    class="form-control">
                            </div>
                        </div>
                    </div>


                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font size="3" color="#ff6f67">Plant Time Hourly</font>
                                </label>
                                <input type="text" value="{{$timelogs->planttime_hourly}}" name="planttime_hourly"
                                    class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font size="3" color="#ff6f67">Plant OverTime</font>
                                </label>
                                <input type="text" value="{{$timelogs->plant_overtime}}" name="plant_overtime"
                                    class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font size="3" color="#ff6f67">Plant Double Time</font>
                                </label>
                                <input type="text" value="{{$timelogs->plant_doubletime}}" name="plant_doubletime"
                                    class="form-control">
                            </div>
                        </div>
                    </div>




                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font size="3" color="#ff6f67">Hourly LP2-1/2</font>
                                </label>
                                <input type="text" value="{{$timelogs->hourly_lp2}}" name="hourly_lp2"
                                    class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font size="3" color="#ff6f67">OverTime (x1.5) Hourly LP2-1/2</font>
                                </label>
                                <input type="text" value="{{$timelogs->overtime_hourly_lp2}}"
                                    name="overtime_hourly_lp2_1/2" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font size="3" color="#ff6f67">LP2 Double Time</font>
                                </label>
                                <input type="text" value="{{$timelogs->lp2_doubletime}}" name="lp2_doubletime"
                                    class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font size="3" color="#ff6f67">Hourly LP4</font>
                                </label>
                                <input type="text" value="{{$timelogs->hourly_lp4}}" name="hourly_lp4"
                                    class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font size="3" color="#ff6f67">OverTime (x1.5) Hourly LP4</font>
                                </label>
                                <input type="text" value="{{$timelogs->overtime_hourly_lp4}}" name="overtime_hourly_lp4"
                                    class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font size="3" color="#ff6f67">LP4 Double Time</font>
                                </label>
                                <input type="text" value="{{$timelogs->lp4_doubletime}}" name="lp4_doubletime"
                                    class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font size="3" color="#ff6f67">Plant Time Hourly LP2-1/2</font>
                                </label>
                                <input type="text" value="{{$timelogs->plant_hourly_lp2}}" name="plant_hourly_lp2"
                                    class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font size="3" color="#ff6f67">Plant OverTime LP2-1/2</font>
                                </label>
                                <input type="text" value="{{$timelogs->plant_overtime_lp2}}" name="plant_overtime_lp2"
                                    class="form-control">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font size="3" color="#ff6f67">LP2 Plant Double Time</font>
                                </label>
                                <input type="text" value="{{$timelogs->lp2_plant_doubletime}}"
                                    name="lp2_plant_doubletime" class="form-control">
                            </div>
                        </div>
                    </div>


                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="bmd-label-floating">
                                        <font size="3" color="#ff6f67">Plant Time Hourly LP4</font>
                                    </label>
                                    <input type="text" value="{{$timelogs->planttime_hourly_lp4}}"
                                        name="planttime_hourly_lp4" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="bmd-label-floating">
                                        <font size="3" color="#ff6f67">Plant OverTime LP4</font>
                                    </label>
                                    <input type="text" value="{{$timelogs->plant_overtime_lp4}}"
                                        name="plant_overtime_lp4" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="bmd-label-floating">
                                        <font size="3" color="#ff6f67">LP4 Plant Double Time</font>
                                    </label>
                                    <input type="text" value="{{$timelogs->lp4_plant_doubletime}}"
                                        name="lp4_plant_doubletime" class="form-control">
                                </div>
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="bmd-label-floating">
                                        <font size="3" color="#ff6f67">Mechanic Callout</font>
                                    </label>
                                    <input type="text" value="{{$timelogs->mechanic_callout}}" name="mechanic_callout"
                                        class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="bmd-label-floating">
                                        <font size="3" color="#ff6f67">Fixed Salary</font>
                                    </label>
                                    <input type="text" value="{{$timelogs->fixed_salary}}" name="fixed_salary"
                                        class="form-control">
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="bmd-label-floating">
                                        <font size="3" color="#479be0">Vacation Pay Hourly</font>
                                    </label>
                                    <input type="text" value="{{$timelogs->vacation_pay_hourly}}"
                                        name="vacation_pay_hourly" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="bmd-label-floating">
                                        <font size="3" color="#479be0">Sick Pay Hourly</font>
                                    </label>
                                    <input type="text" value="{{$timelogs->sick_pay_hourly}}" name="sick_pay_hourly"
                                        class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="bmd-label-floating">
                                        <font size="3" color="#479be0">Holiday</font>
                                    </label>
                                    <input type="text" value="{{$timelogs->holiday}}" name="holiday"
                                        class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">
                                        <font size="3" color="#479be0">Notes</font>
                                    </label>
                                    <textarea rows="5" class="form-control" name="notes">{{$timelogs->notes}}</textarea>
                                </div>
                            </div>
                        </div>

                        <br>
                        <hr>
                        <button type="submit" class="btn btn-primary"
                            style="color: #fff;background-color: #FF6E66;border-color: #FF6E66;">Save</button>
                        @if($timelogs->status=="0")
                        <a href="{{route('save.approve',$timelogs->id)}}" class="btn btn-success"
                            style="color: #fff;background-color: #5cb55f;border-color: #5cb55f;">Approve</a>
                        <a href="{{route('save.disapprove',$timelogs->id)}}" class="btn btn-danger"
                            style="color: #fff;background-color: #f4483b;border-color: #f4483b;">Disapprove</a>
                        @endif
                        @if($timelogs->status=="1")
                        <a href="{{route('save.disapprove',$timelogs->id)}}" class="btn btn-danger"
                            style="color: #fff;background-color: #f4483b;border-color: #f4483b;">Disapprove</a>
                        @endif
                        @if($timelogs->status=="2")
                        <a href="{{route('save.approve',$timelogs->id)}}" class="btn btn-success"
                            style="color: #fff;background-color: #5cb55f;border-color: #5cb55f;">Approve</a>
                        @endif
                        <!--  <button type="submit" class="btn btn-primary  ">Update</button> -->
                        <a href="{{ url()->previous() }}" class="btn btn-warning"
                            style="color: #fff;background-color: #fd683a;border-color: #fd683a;">Cancel</a>

                        <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->

<div class="modal fade" id="promoModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Break Times</h4>
            </div>
            <div class="modal-body">
                <form files="true" class="form-horizontal" id="contact_us" action="javascript:void(0)" method="POST"
                    enctype="multipart/form-data" role="form">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="PATCH">
                    <input type="hidden" class="form-control" name="breakid" id="breakid" />

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font color="#ff6f67">Started At</font>
                                </label><br>
                                <input type="text" autocomplete="off" class="form-control datetimepicker"
                                    onchange="startchange();" id="started" name="startedAt" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font color="#ff6f67">Ended At</font>
                                </label><br>
                                <input type="text" autocomplete="off" class="form-control datetimepicker"
                                    onchange="endchange();" id="ended" name="endedAt" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="alert alert-success message" style="padding: 20px;display: none;">
                        Updated! Please click on save to reset calculations.
                    </div>
                    <button type="submit" id="send_form" class="btn btn-success btn-sm"
                        style="float: right;">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="promoModal2" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Break Times</h4>
            </div>
            <div class="modal-body">
                <form files="true" class="form-horizontal" id="contact_us2" action="javascript:void(0)" method="POST"
                    enctype="multipart/form-data" role="form">
                    {{csrf_field()}}
                    <input type="hidden" class="form-control" name="time_log_id" value="{{$timelogs->id}}"
                        id="time_log_id" />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font color="#ff6f67">Started At</font>
                                </label><br>
                                <input type="text" autocomplete="off" class="form-control datetimepicker"
                                    name="startedAt" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">
                                    <font color="#ff6f67">Ended At</font>
                                </label><br>
                                <input type="text" autocomplete="off" class="form-control datetimepicker" name="endedAt"
                                    class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="alert alert-success message2" style="padding: 20px;display: none;">
                        Added Successfully! Please click on save to reset calculations.
                    </div>
                    <button type="submit" id="send_form2" class="btn btn-success btn-sm"
                        style="float: right;">Add</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div id="googleMap" style="width:100%;height:400px;"></div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>


<script>
    function newtest(elem) {

        var datetime = $(elem).data("id");
        alert(datetime);

    }

    function getlatlng(elem) {

        var latitude = $(elem).data("lat");
        var longitude = $(elem).data("lng");

        myMap(latitude, longitude);

    }

    function myMap($latitude, $longitude) {

        var Lat = $latitude;
        var Lng = $longitude;

        myCenter = new google.maps.LatLng(Lat, Lng);

        var mapOptions = {
            center: myCenter,
            zoom: 16,
            scrollwheel: true,
            draggable: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);

        var marker = new google.maps.Marker({
            position: myCenter,
        });
        marker.setMap(map);

        /*  var infowindow = new google.maps.InfoWindow({
          content:"Hello World!"
        });

        google.maps.event.addListener(marker, 'click', function() {
          infowindow.open(map,marker);
        });*/

    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzf7KnzVx3iLASRh25OP_bYgTpUD-dIW8&callback=myMap">
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $('#show').click(function () {
            $('.breaks').toggle("slide");
        });
    });

    /*function startchange() {
      var start = document.getElementById('started').value;
      document.getElementById("startvalue").value = start;
    }

    function endchange() {
      var end = document.getElementById('ended').value;
      document.getElementById("endvalue").value = end;
    }*/

    function test(elem) {

        var dataId = $(elem).data("id");
        var startedAt = $(elem).attr("startedAt");
        var endedAt = $(elem).attr("endedAt");

        document.getElementById('breakid').value = dataId;
        document.getElementById('started').value = startedAt;
        document.getElementById('ended').value = endedAt;

    }

    $(document).ready(function () {
        $('#send_form').click(function (e) {

            $('#send_form').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
            e.preventDefault();

            var form = document.forms.namedItem(
            "contact_us"); // high importance!, here you need change "yourformname" with the name of yourform
            var formData = new FormData(form); // high importance!
            var id = document.getElementById('breakid').value;
            $.ajax({
                url: "{{route('breaks.update','')}}/" + parseInt(id),
                method: 'post',
                data: formData,
                dataType: "json", // or html if you want...
                contentType: false, // high importance!
                processData: false, // high importance!
                success: function (data) {
                    $('#send_form').html('Update');
                    if (data.status == 200) {
                        $('.message').toggle("slide");
                    }

                    setTimeout(function () {
                        $('.message').toggle("hide");
                    }, 3000); // <-- time in milliseconds

                }
            });
        });
    });

    $(document).ready(function () {

        $('#send_form2').click(function (e) {

            $('#send_form2').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
            e.preventDefault();

            var form = document.forms.namedItem(
            "contact_us2"); // high importance!, here you need change "yourformname" with the name of yourform
            var formData = new FormData(form); // high importance!

            $.ajax({
                url: "{{route('breaks.add')}}",
                method: 'post',
                data: formData,
                dataType: "json", // or html if you want...
                contentType: false, // high importance!
                processData: false, // high importance!
                success: function (data) {
                    $('#send_form2').html('Add');
                    if (data.status == 200) {
                        $('.message2').toggle("slide");
                    }

                    setTimeout(function () {
                        $('.message2').toggle("hide");
                    }, 3000); // <-- time in milliseconds

                }
            });
        });
    });

    var select = document.getElementById('job_id');
    var jobsite = $('#job_id option:selected').attr('data-id');

    select.onchange = function () {
        var id = $(this).val();
        var data = $(this).find(':selected').attr('data-id');

        if (data != jobsite) {
            alert("Job type cannot be changed from operator to non-operator!").show();
        }

        getjobs($(this).val());

    };

    function getjobs(id) {

        $.ajax({
            method: "get",
            url: "{{route('job.data','')}}/" + parseInt(id),
            dataType: "json",
            contentType: false, // high importance!
            processData: false, // high importance!
            success: function (result) {
                if (result) {
                    var $jobtypes = $('#jobtypes');
                    $("#jobtypes").empty();
                    $("#jobtypes").append(new Option("Select Job Type", "value"));
                    $.each(result, function (key, value) {

                        var $option = $("<option/>", {
                            value: key,
                            text: value
                        });
                        $jobtypes.append($option);

                    });
                    $('.selecs').reset();
                }
            }
        });
    }

    $(document).ready(function () {
        $('body').bootstrapMaterialDesign();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY h:mm A',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });

        $('.datepicker').datetimepicker({
            format: 'MM/DD/YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });

        $('.timepicker').datetimepicker({
            //          format: 'H:mm',    // use this format if you want the 24hours timepicker

            format: 'h:mm A', //use this format if you want the 12hours timpiecker with AM/PM toggle
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
    });
</script>
@endsection
