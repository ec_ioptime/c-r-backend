@extends('layouts.master')

@section('title')
    User Logs
@endsection

@section('content')
    <div class="row">
      
        <div class="col-md-12">

          <form method="GET" action="{{route('calculate.logs',$user_id)}}" enctype="multipart/form-data">

                 <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Date From</font></label><br>
                          <input type="date" name="from" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Date To</font></label><br>
                          <input type="date"  name="to" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-2" style="padding-top: 13px;">
                        <div class="form-group">
                          <button type="submit" class="btn btn-primary">Find</button>
                        </div>
                      </div>
                    </div>

               </form>
            <div class="card">
           <!--  <div class="card-header card-header-primary">
                <h4 class="card-title ">Edit Report</h4>
                {{-- <p class="card-category"> All registered users</p> --}}
                  @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            </div> -->
            <div class="card-body" style="padding: 25px">
              @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif

                @if(session('status'))
                    <div class="alert alert-success" style="padding: 20px">
                        {{ session('status') }}
                    </div>
                @endif
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Hourly</font></label>
                          <input type="text" value="@if($hourly){{$hourly}}@endif" name="hourly" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">OverTime (x1.5) Hourly</font></label>
                          <input type="text" value="{{$overtime_hourly}}" name="overtime_hourly" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Plant Time Hourly</font></label>
                          <input type="text" value="{{$planttime_hourly}}" name="planttime_hourly" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Plant OverTime</font></label>
                          <input type="text" value="{{$plant_overtime}}" name="plant_overtime" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Hourly LP2-1/2</font></label>
                          <input type="text" value="{{$hourly_lp2}}" name="hourly_lp2" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">OverTime (x1.5) Hourly LP2-1/2</font></label>
                          <input type="text" value="{{$overtime_hourly_lp2}}" name="overtime_hourly_lp2" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Hourly LP4</font></label>
                          <input type="text" value="{{$hourly_lp4}}" name="hourly_lp4" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">OverTime (x1.5) Hourly LP4</font></label>
                          <input type="text" value="{{$overtime_hourly_lp4}}" name="overtime_hourly_lp4" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Plant Time Hourly LP2-1/2</font></label>
                          <input type="text" value="{{$plant_hourly_lp2}}" name="plant_hourly_lp2" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Plant OverTime LP2-1/2</font></label>
                          <input type="text" value="{{$plant_overtime_lp2}}" name="plant_overtime_lp2" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Plant Time Hourly LP4</font></label>
                          <input type="text" value="{{$planttime_hourly_lp4}}" name="planttime_hourly_lp4" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Plant OverTime LP4</font></label>
                          <input type="text" value="{{$plant_overtime_lp4}}" name="plant_overtime_lp4" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">LP2 Double Time</font></label>
                          <input type="text" value="{{$lp2_doubletime}}" name="lp2_doubletime" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">LP4 Double Time</font></label>
                          <input type="text" value="{{$lp4_doubletime}}" name="lp4_doubletime" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">LP2 Plant Double Time</font></label>
                          <input type="text" value="{{$lp2_plant_doubletime}}" name="lp2_plant_doubletime" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">LP4 Plant Double Time</font></label>
                          <input type="text" value="{{$lp4_plant_doubletime}}" name="lp4_plant_doubletime" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Plant Double Time</font></label>
                          <input type="text" value="{{$plant_doubletime}}" name="plant_doubletime" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Double-Time Hourly</font></label>
                          <input type="text" value="{{$double_time_hourly}}" name="double_time_hourly" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Fixed Salary</font></label>
                          <input type="text" value="{{$fixed_salary}}" name="fixed_salary" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Mechanic Callout</font></label>
                          <input type="text" value="{{$mechanic_callout}}" name="mechanic_callout" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#479be0">Vacation Pay Hourly</font></label>
                          <input type="text" value="{{$vacation_pay_hourly}}" name="vacation_pay_hourly" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#479be0">Sick Pay Hourly</font></label>
                          <input type="text" value="{{$sick_pay_hourly}}" name="sick_pay_hourly" class="form-control">
                        </div>
                      </div>
                    </div>

                    <!--  <button type="submit" class="btn btn-primary  ">Update</button> -->
                     <a href="{{route('user.index')}}" class="btn btn-warning ">Cancel</a>
                    <div class="clearfix"></div>
            </div>
            </div>
        </div>
    </div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $('#show').click(function() {
      $('.breaks').toggle("slide");
    });
});

/*function startchange() {
  var start = document.getElementById('started').value;
  document.getElementById("startvalue").value = start;
}

function endchange() {
  var end = document.getElementById('ended').value;
  document.getElementById("endvalue").value = end;
}*/

function test(elem) {

var dataId = $(elem).data("id");
var startedAt = $(elem).attr("startedAt");
var endedAt = $(elem).attr("endedAt");

document.getElementById('breakid').value = dataId;



document.getElementById('started').value = startedAt;

document.getElementById('ended').value = endedAt;
}


 //-----------------
$(document).ready(function(){
$('#send_form').click(function(e){

  $('#send_form').html('<i class="fa fa-circle-o-notch fa-spin"></i>');

  e.preventDefault();
  var form = document.forms.namedItem("contact_us"); // high importance!, here you need change "yourformname" with the name of yourform
  var formData = new FormData(form); // high importance!
  var id = document.getElementById('breakid').value;
  $.ajax({
  url: "{{route('breaks.update','')}}/"+parseInt(id),
  method: 'post',
  data: formData,
  dataType: "json", // or html if you want...
  contentType: false, // high importance!
  processData: false, // high importance!
  success: function(data){

  $('#send_form').html('Update');

  if(data.status==200){

    $('.message').toggle("slide");

  }    
    setTimeout(function() {
    $('.message').toggle("hide"); 
    }, 2000); // <-- time in milliseconds

  document.getElementById("contact_us").reset();
  }});
  });
});

var select = document.getElementById('job_id');
select.onchange = function(){
var id = $(this).val();

getjobs($(this).val());

};


function getjobs(id) {

$.ajax({
method:"get",
url: "{{route('job.data','')}}/"+parseInt(id),
dataType: "json",
contentType: false, // high importance!
processData: false, // high importance!
success:function(result)
{       
if(result)
{
    
var $jobtypes = $('#jobtypes');
$("#jobtypes").empty();
$("#jobtypes").append(new Option("Select Job Type", "value"));
$.each(result,function(key,value){

var $option = $("<option/>", { value: key,text: value});

$jobtypes.append($option);

});
$('.selecs').reset();
}
}});
}



</script>
@endsection
