@extends('layouts.master')

@section('title')
    Reports
@endsection

@section('content')
<style type="text/css">
input.larger {
width: 15px;
height: 15px;
}
</style>

<div class="row">
    <div class="col-md-12">
          <a href="{{route('reports.create')}}" class="btn btn-success" style="width: 160px">Create Report</a>
          <div class="card">
            <div class="card-body">
              @if ($message = Session::get('status'))
                    <div class="alert alert-success" id="alert" style="height: 55px;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
              @endif
              <!-- <form method="GET" action="{{url('/')}}" enctype="multipart/form-data">
                 <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Date From</font></label><br>
                          <input type="date" name="from" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Date To</font></label><br>
                          <input type="date"  name="to" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-2" style="padding-top: 13px;">
                        <div class="form-group">
                          <button type="submit" class="btn btn-primary">Find</button>
                        </div>
                      </div>
                    </div>
              </form>
              <hr> -->
              <form method="post" id="myform"  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="table-responsive">
                <table class="table " id="datatables2">
                    <thead class=" text-dark">
                    <tr>
                    <th>Report Name</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Pay Date</th>
                    <th>Created At</th>
                    <th >Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($reports as $data)
                      <tr>
                      <td>{{$data->name}}</td>
                      <td>{{$data->start_date}}</td>
                      <td>{{$data->end_date}}</td>
                      <td>{{$data->pay_date}}</td>
                      <td>{{date('m-d-Y', strtotime($data->created_at))}}</td>

                      <td ><!-- <a href="#"><img src="{{asset('assets/img/tick.png')}}"></a>
                        <a href="#"><img src="{{asset('assets/img/close.png')}}"></a> -->
                        <a href="{{route('details',$data->id)}}" target="_blank" class="btn btn-info btn-sm">View Details</a>
                        <a href="{{route('delete.report',$data->id)}}" onclick="return confirm('This will delete all data related to this Report')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                      </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                 </div>
              </form>
          </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
