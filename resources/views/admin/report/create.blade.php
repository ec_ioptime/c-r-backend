@extends('layouts.master')

@section('title')
    Create Report
@endsection

@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<style type="text/css">

  .btn, .btn.btn-default{
    background-color: #ffffff;
    border-color: #ffffff;
    color: #212529;
  }

  .bootstrap-select>.dropdown-toggle.bs-placeholder, .bootstrap-select>.dropdown-toggle.bs-placeholder:active, .bootstrap-select>.dropdown-toggle.bs-placeholder:focus, .bootstrap-select>.dropdown-toggle.bs-placeholder:hover {
    color: #212529;
}
}
</style>
 <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Create Report</h4>
                  @if (count($errors) > 0)

                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            </div>
            <div class="card-body">
              @if ($message = Session::get('status'))
                    <div class="alert alert-success" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
                @endif
            <form method="POST" action="{{route('reports.store')}}" enctype="multipart/form-data">
               {{csrf_field()}}
                 <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Report Title</font></label><br>
                          <input type="text" name="name" class="form-control" required>
                        </div>
                      </div>
                  </div>

                 <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Start Date</font></label><br>
                          <input type="text" id="stardate" placeholder="mm/dd/yyyy"   name="from" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">End Date</font></label><br>
                          <input type="text"  id="enddate" placeholder="mm/dd/yyyy"  name="to" class="form-control" required>
                        </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Users</font></label><br><br>
                          <select class="form-control selectpicker" title="Please Select User" data-live-search="true" name="user_id[]" multiple="">
                            @foreach(App\User::where('status',1)->get() as $data)
                            <option value="{{$data->id}}">{{$data->first_name}} {{$data->last_name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                  </div>
                   <div class="row">
                    <div class="col-md-2" style="padding-top: 13px;">
                        <div class="form-group">
                          <button type="submit" class="btn btn-primary">Create</button>
                        </div>
                      </div>
                    </div>
              </form>
            </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">


  <script type="text/javascript">
            var arr = [1, 2, 3, 4, 5, 6]
$("#stardate").datepicker({
    beforeShowDay: function(date){ return [date.getDay() == 1,""]}
});
$("#enddate").datepicker({
    beforeShowDay: function(date){ return [date.getDay() == 0,""]}
});

  </script>
@endsection


