@extends('layouts.master')

@section('title')
    Time-Logs
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12">

            <a href="{{url('/')}}" class="btn btn-default" style="width: 160px;background-color: #3850af;border-color: #3850af">Pending</a>
            <a href="{{route('approved.logs')}}" class="btn btn-success" style="width: 160px">Approved</a>
            <a href="{{route('disapproved.logs')}}" class="btn btn-danger" style="width: 160px;opacity: 70%">Disapproved</a>
            <div class="card">

            <div class="card-body">
                <form method="GET" action="{{route('disapproved.logs')}}" enctype="multipart/form-data">
                 <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Date From</font></label><br>
                          <input type="date" name="from" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Date To</font></label><br>
                          <input type="date"  name="to" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-2" style="padding-top: 13px;">
                        <div class="form-group">
                          <button type="submit" class="btn btn-primary">Find</button>
                        </div>
                      </div>
                    </div>
               </form>
               <hr>
                <div class="table-responsive">
                <table class="table datatables">
                    <thead class=" text-dark">
                    <th>Employee</th>
                    <th>Date</th>
                    <th>Job Title</th>
                    <th>Job Type</th>
                    <th>Invoice Number</th>
                    <th style="padding-left:35px !important">Action</th>
                    </thead>
                    <tbody>
                        @foreach($types as $data)
                        <tr>
                        <td>@if($data->user!="")
                            {{$data->user->first_name}} {{$data->user->last_name}}
                        @else
                        --
                        @endif</td>
                        <td>
                            {{date('d-m-Y', strtotime($data->created_at))}}
                        </td>
                        <td>@if($data->jobs!="")
                          {{$data->jobs->title}}
                        @else
                        --
                        @endif</td>
                        <td>@if($data->types!="")
                          {{$data->types->name}}
                            @else
                            --
                            @endif</td>
                        <td>@if($data->invoiceNumber!="")
                          {{$data->invoiceNumber}}
                            @else
                            --
                            @endif</td>

                        <td width="20%"><!-- <a href="#"><img src="{{asset('assets/img/tick.png')}}"></a>
                            <a href="#"><img src="{{asset('assets/img/close.png')}}"></a> -->
                            <a href="{{route('details.logs',$data->id)}}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                            <a href="{{route('logs.delete',$data->id)}}" onclick="return confirm('This will delete all data related to this timelog')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                       </td>
                    </tr>
                        @endforeach

                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>

          </div>
@endsection

@section('scripts')

@endsection
