@extends('layouts.master')

@section('title')
    Time-Logs
@endsection

@section('content')
<style type="text/css">
input.larger {
width: 15px;
height: 15px;
}
</style>

<div class="row">
    <div class="col-md-12">
          <a href="{{url('/')}}" class="btn btn-default" style="width: 160px;background-color: #3850af;border-color: #3850af;opacity: 70%">Pending </a>
          <a href="{{route('approved.logs')}}" class="btn btn-success" style="width: 160px">Approved</a>
          <a href="{{route('disapproved.logs')}}" class="btn btn-danger" style="width: 160px">Disapproved</a>
          <div class="card">
            <div class="card-body">
              @if ($message = Session::get('status'))
                    <div class="alert alert-success" id="alert" style="height: 55px;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
              @endif
              <form method="GET" action="{{url('/')}}" enctype="multipart/form-data">
                 <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Date From</font></label><br>
                          <input type="date" name="from" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating"><font color="#ff6f67">Date To</font></label><br>
                          <input type="date"  name="to" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-2" style="padding-top: 13px;">
                        <div class="form-group">
                          <button type="submit" class="btn btn-primary">Find</button>
                        </div>
                      </div>
                    </div>
              </form>
              <hr>
              <form method="post" id="myform"  enctype="multipart/form-data">
                {{ csrf_field() }}
                <input style="display: none;" id="approvedbtn" class="btn btn-success" type="submit" name="submit" value="Approve"/>

                <input style="display: none;" id="deletebtn" class="btn btn-danger" type="submit" name="submit" value="Delete"/>

                <div class="table-responsive">
                <table class="table datatables">
                    <thead class=" text-dark">
                    <tr>
                    <th width="8%"><input class="larger" type="checkbox" id="checkAll"></th>
                    <th>Employee</th>
                    <th>Date</th>
                    <th>Job Title</th>
                    <th>Job Type</th>
                    <th>Invoice Number</th>
                    <th style="padding-left:35px !important">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($types as $data)
                      <tr>
                      <td class="text-center"><input class="larger" name='id[]' type="checkbox" id="checkItem" value="{{$data->id}}" style="height: "></td>
                      <td>@if($data->user!="")
                            {{$data->user->first_name}} {{$data->user->last_name}}
                        @else
                        --
                        @endif</td>
                      <td>{{date('m-d-Y', strtotime($data->created_at))}}</td>
                      <td>@if($data->jobs!="")
                        	{{$data->jobs->title}}
                        @else
                        --
                        @endif
                      </td>
                      <td>@if($data->types!="")
                        	{{$data->types->name}}
                            @else
                            --
                            @endif
                      </td>
                      <td>@if($data->invoiceNumber!="")
                          {{$data->invoiceNumber}}
                            @else
                            --
                            @endif
                      </td>
                      <td width="15%"><!-- <a href="#"><img src="{{asset('assets/img/tick.png')}}"></a>
                        <a href="#"><img src="{{asset('assets/img/close.png')}}"></a> -->
                        <a href="{{route('details.logs',$data->id)}}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                        <a href="{{route('logs.delete',$data->id)}}" onclick="return confirm('This will delete all data related to this timelog')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                      </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                 </div>
              </form>
          </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script type="text/javascript">

$("#checkAll").click(function () {

$('input:checkbox').not(this).prop('checked', this.checked);

$.alert({
  title: 'Confirm',
  content: 'Do you want to delete or approve data',
  type: 'blue',
  typeAnimated: true,
  buttons: {

  approve: function () {

    $('#approvedbtn').toggle("slide");
    $('#deletebtn').hide();
    $("#myform").attr('action', '/reports/bulkapprove');

  },

  delete: function () {

    $('#deletebtn').toggle("slide");
    $('#approvedbtn').hide();
    $("#myform").attr('action', '/reports/bulkdelete');
  }
  }
});
});

</script>

@endsection
