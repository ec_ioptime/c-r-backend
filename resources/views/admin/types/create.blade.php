@extends('layouts.master')

@section('title')
    Add New Job
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Add New Job</h4>
            </div>
            <div class="card-body">
                @if ($message = Session::get('status'))
                    <div class="alert alert-success" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
                @endif
            <form method="POST" action="{{route('types.store')}}" enctype="multipart/form-data">
                  {{csrf_field()}}
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Enter Job Type</label>
                          <input type="text"  name="name" class="form-control" required="">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                          <select class="form-control"  name="job_id" required="">
                            <option value="">Select Job Title</option>
                            @foreach(App\Jobs::get() as $data)
                              <option value="{{$data->id}}">{{$data->title}}</option>
                              @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    

                     <button type="submit" class="btn btn-primary  ">Add</button>
                    <div class="clearfix"></div>
                  </form>
            </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    setTimeout(function() {
    $('#alert').fadeOut('slow');
}, 2000); // <-- time in milliseconds
</script>
@endsection
