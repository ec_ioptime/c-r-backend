@extends('layouts.master')

@section('title')
    Update Type
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Update Type</h4>
            </div>
            <div class="card-body">
                @if ($message = Session::get('status'))
                    <div class="alert alert-success" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
                @endif
            <form method="POST" action="{{route('types.update',$types->id)}}" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <input name="_method" type="hidden" value="PATCH">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Job Type</label>
                          <input type="text"  name="name" value="{{$types->name}}" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                          <select class="form-control" name="job_id">
                            @foreach(App\Jobs::get() as $data)
                              <option value="{{$data->id}}" {{$types->job_id == $data->id  ? 'selected' : ''}}>{{$data->title}}</option>
                              @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    

                     <button type="submit" class="btn btn-primary  ">Update</button>
                    <div class="clearfix"></div>
                  </form>
            </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    setTimeout(function() {
    $('#alert').fadeOut('slow');
}, 2000); // <-- time in milliseconds
</script>
@endsection
