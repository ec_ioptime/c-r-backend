@extends('layouts.master')

@section('title')
    Users
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{route('user.index')}}" class="btn btn-success" style="width: 160px;">Active </a>
          <a href="{{route('user.terminated')}}" class="btn btn-danger" style="width: 160px;opacity: 70%">Termindated</a>
            <div class="card">
            <div class="card-header card-header-primary">
                <span class="pull-right">
                <a href="{{route('user.create')}}" style="float: right;" class="btn btn-success btn-sm" ><i class="fa fa-plus"></i> Add New User</a></span>
                <h4 class="card-title ">Employees</h4>
                 
            </div>
            <div class="card-body">
                @if ($message = Session::get('status'))
                    <div class="alert alert-success" style="height: 55px;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger a2">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="table-responsive">
                <table class="table datatables">
                    <thead class="text-dark">
                    <th>
                        Quickbooks-ID
                    </th>
                    <th>
                        Name
                    </th>
                    <th>
                        Email
                    </th>
                    <th>Phone Number</th>
                    <th>Role</th>
                    <th>Logs</th>
                    <th style="padding-left:150px !important">
                        Action
                    </th>
                    </thead>
                    <tbody>
                        @foreach ($users as $item)
                            <tr >
                                <td>

                                    {{$item->employeeId}}
                                    
                                </td>
                                <td>
                                    {{$item->first_name}} {{$item->last_name}}
                                </td>
                                <td>{{$item->email}}</td>
                                <td>
                                    {{$item->phone_number}}
                                </td>
                                <td>
                                    @if($item->usertype=="admin")
                                    <span class="badge badge-info" style="background-color: #4856ea;border-color: #4856ea;color: #fff;padding: 6px;">&nbsp;ADMIN&nbsp;</span>
                                   
                                    @elseif($item->usertype=="user")
                                    <span class="badge badge-success" style="background-color: #47A44B;border-color: #47A44B;color: #fff;padding: 7px;">&nbsp;&nbsp;USER&nbsp;&nbsp;</span>

                                   
                                    @endif
                                </td>
                                
                                <td><a href="{{route('user.logs',$item->id)}}" style="background-color: #488096;border-color: #488096;" class="btn btn-success btn-sm">Logs</a></td>
                                <td class="text-primary">
                                   <a href="{{route('terminate',$item->id)}}" class="btn btn-danger btn-sm">Terminate</a>
                                   <a href="{{route('send.link',$item->email)}}" class="btn btn-success btn-sm">Reset Password</a>
                                    <a href="/user/{{$item->id}}" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></a>
                                    
                                    
                                    <a href="{{route('user.delete',$item->id)}}" onclick="return confirm('Are you sure you want to delete this user?')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    setTimeout(function() {
    $('.alert').fadeOut('slow');
}, 2000); // <-- time in milliseconds
</script>
@endsection
