@extends('layouts.master')

@section('title')
    Edit User
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Edit User</h4>
                {{-- <p class="card-category"> All registered users</p> --}}
            </div>
            <div class="card-body">
              @if ($message = Session::get('status'))
                    <div class="alert alert-success" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{ $message }}</p>
                    </div>
                @endif
            <form method="POST" action="{{route('user.update',$user->id)}}" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <input name="_method" type="hidden" value="PATCH">

                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">QuickBooks-ID</label>
                          <input type="text" value="{{$user->employeeId}}" name="employeeId" class="form-control">
                        </div>
                      </div>
                    </div>

                      <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">First Name</label>
                          <input type="text" value="{{$user->first_name}}" name="first_name" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Last Name</label>
                          <input type="text" value="{{$user->last_name}}" name="last_name" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Phone Number</label>
                          <input type="text" value="{{$user->phone_number}}" name="phone_number" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email address</label>
                          <input  name="email" type="email" value="{{$user->email}}" class="form-control">
                        </div>
                      </div>
                      </div>
                    
                     <button type="submit" class="btn btn-primary  ">Update</button>
                    
                    <div class="clearfix"></div>
                  </form>
            </div>
            </div>
        </div>
    </div>

@endsection
