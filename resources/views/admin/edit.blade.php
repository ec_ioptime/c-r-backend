@extends('layouts.master')

@section('title')
Edit
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Edit User Role</h4>
                {{-- <p class="card-category"> All registered users</p> --}}
            </div>
            <div class="card-body">
            <form method="POST" action="/role-register-update/{{$user->id}}">
                  {{csrf_field()}}
                  {{-- {{method_field('PUT')}} --}}

                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">name</label>
                          <input type="text" value="{{$user->name}}" disabled class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email address</label>
                          <input  disabled type="email" value="{{$user->email}}" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="role">Select Role</label>
                            <select name="usertype" id="usertype" class="form-control">
                                <option value="admin">Admin</option>
                                <option value="developer">Developer</option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary  ">Update Profile</button>
                    <a href="{{ url()->previous() }}" class="btn btn-warning ">Cancel</a>
                    {{-- <button type="button" class="btn btn-warning ">Cancel</button> --}}
                    <div class="clearfix"></div>
                  </form>
            </div>
            </div>
        </div>
    </div>

@endsection
