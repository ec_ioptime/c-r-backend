@extends('layouts.auth')
@section('title')
    Login
@endsection
@section('content')

@section('content')
<div class="container">
    <div class="row justify-cons tent-center">
        <div class="col-md-8">
         <!--    <a href="{{url('/login')}}" class="btn btn-sm btn-primary" style="float: left;">
                                    {{ __('Back to Login') }}
                                </a> -->
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}
                </div>

                <div class="card-body">
                     @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row pt-2">
                            <label for="email" class=" mb-4  pb-3 mt-2 pt-2 col-form-label text-md-right">{{ __('Email') }}</label>

                            <div class="col-md-12 mt-3">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0 text-center">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-block btn-primary">
                                    {{ __('Reset') }}
                                </button>
                                 <a href="{{url('/login')}}" class="btn btn-block btn-primary">
                                    {{ __('Login') }}
                                </a>

                                
                              
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
