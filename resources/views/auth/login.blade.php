@extends('layouts.auth')
@section('title')
    Login
@endsection
@section('content')

@section('content')
<div class="container">
    <div class="row justify-cons tent-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row pt-2">
                            <label for="email" class=" mb-4  pb-3 mt-2 pt-2 col-form-label text-md-left">{{ __('Email') }}</label>

                            <div class="col-md-12 mt-3">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row pt-2">
                            <label for="password" class="mb-4  pb-3  mt-2 pt-2 col-form-label text-md-left">{{ __('Password') }}</label>

                            <div class="col-md-12 mt-3">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                @error('password')
                                {{$message}}
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0 text-center">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-block btn-primary">
                                    {{ __('Login') }}
                                </button>
<br>
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link m-0 p-0 text-center" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
