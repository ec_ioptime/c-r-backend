
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/img/apple-touch-icon.png')}}">
  <link rel="icon" type="image/png" href="{{asset('assets/img/favicon2.png')}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Report Details
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<style type="text/css">
 .company-details {
    text-align: right
}
 .image-det {
    text-align: left
}

html,body{
    width: 100%;
    height: 100%;
    margin: 0; /* Space from this element (entire page) and others*/
    padding: 0; /*space from content and border*/
    border: solid red;
    border-width: thick;
    overflow:auto;
    overflow-x: hidden;
    display:block;
    box-sizing: border-box;
}
</style>
</head>

<body>
<div class="row" style="padding-top:20px ">
  <div class="col-md-12">
      <div class="row">
        <div class="col-md-5">
          <div class="image-det" style="padding: 20px">
             <h3>Employee Name </h3><h4>{{App\User::where('id',$user_id)->value('first_name')}} {{App\User::where('id',$user_id)->value('last_name')}}</h4>
          </div>
        </div>

        <div class="col-md-4">
          <div class="image-det" style="padding: 20px">
             <img src="{{asset('assets/img/logo2.png')}}" height="75px" alt="">
          </div>
        </div>

        <div class="col-md-3">
          <div class="col company-details" style="padding: 20px">
            <div><font size="4"><b>Report Title: </b></font><font size="4">{{$reportdata->name}}</font></div>
            <div><font size="4"><b>Start Date: </b></font><font size="4">{{$reportdata->start_date}}</font></div>
            <div><font size="4"><b>End Date: </b></font><font size="4">{{$reportdata->end_date}}</font></div>
            <div><font size="4"><b>Pay Date: </b></font><font size="4">{{$reportdata->pay_date}}</font></div>
          </div>
        </div>
      </div>
  </div>
</div>

<div class="wrapper" style="padding: 10px">
  <div class="">
    <table class="table" style="width:100%">
      <thead style="background-color: #ff6e66;color: #fff">
          <tr>
            <th>Invoice</th>
            <th width="8%">Job Date</th>
            <th>JobTitle</th>
            <th>JobType</th>
            <th>Reg Time</th>
            <th>Reg OT</th>
            <th>PT</th>
            <th>POT</th>
            <th>Reg Dbl</th>
            <th>PT Dbl</th>
            <th>RT 2-1/2</th>
            <th>OT 2-1/2</th>
            <th>PT 2-1/2</th>
            <th>POT 2-1/2</th>
            <th>LP2 Dbl</th>
            <th>LP2 PDT</th>
            <th>RT 4</th>
            <th>OT 4</th>
            <th>PT 4</th>
            <th>POT 4</th>
            <th>LP4 Dbl</th>
            <th>LP4 DBT</th>
            <th>FS</th>
            <th>MC</th>
            <th>Action</th>
          </tr>
      </thead>
    <tbody>
          @foreach($reports as $data)
             <tr>
                <td>{{$data->invoiceNumber}}</td>
                <td>{{date('m-d-Y', strtotime($data->created_at))}}</td>
                <td>@if($data->jobs!="")
                {{$data->jobs->title}}
                @else
                 --
                @endif
                </td>
                <td>@if($data->types!="")
                {{$data->types->name}}
                @else
                 --
                @endif
                </td>
                <td>{{$data->hourly}}</td>
                <td>{{$data->overtime_hourly}}</td>
                <td>{{$data->planttime_hourly}}</td>
                <td>{{$data->plant_overtime}}</td>
                <td>{{$data->double_time_hourly}}</td>
                <td>{{$data->plant_doubletime}}</td>
                <td>{{$data->hourly_lp2}}</td>
                <td>{{$data->overtime_hourly_lp2}}</td>
                <td>{{$data->plant_hourly_lp2}}</td>
                <td>{{$data->plant_overtime_lp2}}</td>
                <td>{{$data->lp2_doubletime}}</td>
                <td>{{$data->lp2_plant_doubletime}}</td>
                <td>{{$data->hourly_lp4}}</td>
                <td>{{$data->overtime_hourly_lp4}}</td>
                <td>{{$data->planttime_hourly_lp4}}</td>
                <td>{{$data->plant_overtime_lp4}}</td>
                <td>{{$data->lp4_doubletime}}</td>
                <td>{{$data->lp4_plant_doubletime}}</td>
                <td>{{$data->fixed_salary}}</td>
                <td>{{$data->mechanic_callout}}</td>
                <td><a target="_blank" href="{{route('details.logs',$data->id)}}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a></td>
              </tr>

            @endforeach

              <tr style="background-color: #eeeeee;">
                <td><b>--</b></td>
                <td><b>--</b></td>
                <td><b>--</b></td>
                <td><font size="3"><b>Totals</b></font></td>
                <td><font size="3"><b>{{$hourly}}</b></font></td>
                <td><font size="3"><b>{{$overtime_hourly}}</b></font></td>
                <td><font size="3"><b>{{$planttime_hourly}}</b></font></td>
                <td><font size="3"><b>{{$plant_overtime}}</b></font></td>
                <td><font size="3"><b>{{$double_time_hourly}}</b></font></td>
                <td><font size="3"><b>{{$plant_doubletime}}</b></font></td>
                <td><font size="3"><b>{{$hourly_lp2}}</b></font></td>
                <td><font size="3"><b>{{$overtime_hourly_lp2}}</b></font></td>
                <td><font size="3"><b>{{$plant_hourly_lp2}}</b></font></td>
                <td><font size="3"><b>{{$plant_overtime_lp2}}</b></font></td>
                <td><font size="3"><b>{{$lp2_doubletime}}</b></font></td>
                <td><font size="3"><b>{{$lp2_plant_doubletime}}</b></font></td>
                <td><font size="3"><b>{{$hourly_lp4}}</b></font></td>
                <td><font size="3"><b>{{$overtime_hourly_lp4}}</b></font></td>
                <td><font size="3"><b>{{$planttime_hourly_lp4}}</b></font></td>
                <td><font size="3"><b>{{$plant_overtime_lp4}}</b></font></td>
                <td><font size="3"><b>{{$lp4_doubletime}}</b></font></td>
                <td><font size="3"><b>{{$lp4_plant_doubletime}}</b></font></td>
                <td><font size="3"><b>{{$fixed_salary}}</b></font></td>
                <td><font size="3"><b>{{$mechanic_callout}}</b></font></td>
                <td><b>--</b></td>
              </tr>
          </tbody>
    </table>
  </div>
</div>

  <!--   Core JS Files   -->
  <script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
  <script src="{{asset('assets/js/core/popper.min.js')}}"></script>
  <script src="{{asset('assets/js/core/bootstrap-material-design.min.js')}}"></script>
  {{-- <script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script> --}}
  <!-- Plugin for the momentJs  -->
  <script src="{{asset('assets/js/plugins/moment.min.js')}}"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="{{asset('assets/js/plugins/sweetalert2.js')}}"></script>
  <!-- Forms Validations Plugin -->
  <script src="{{asset('assets/js/plugins/jquery.validate.min.js')}}"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="{{asset('assets/js/plugins/jquery.bootstrap-wizard.js')}}"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="{{asset('assets/js/plugins/bootstrap-selectpicker.js')}}"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="{{asset('assets/js/plugins/bootstrap-datetimepicker.min.js')}}"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="{{asset('assets/js/plugins/jquery.dataTables.min.js')}}"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="{{asset('assets/js/plugins/bootstrap-tagsinput.js')}}"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="{{asset('assets/js/plugins/jasny-bootstrap.min.js')}}"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="{{asset('assets/js/plugins/fullcalendar.min.js')}}"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="{{asset('assets/js/plugins/jquery-jvectormap.js')}}"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{asset('assets/js/plugins/nouislider.min.js')}}"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="{{asset('assets/js/plugins/arrive.min.js')}}"></script>
  <!--  Google Maps Plugin    -->
  {{-- <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> --}}
  <!-- Chartist JS -->
  <script src="{{asset('assets/js/plugins/chartist.min.js')}}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{asset('assets/js/material-dashboard.js?v=2.1.2')}}" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="{{asset('assets/demo/demo.js')}}"></script>

</body>
</html>
