@component('mail::message')
# Hello!
You are receiving this email because we received a password reset request for your account.

@component('mail::button', ['url' => url('reset-password/' . $token.'/'.$email), 'color' => 'primary'])
   Reset Password
@endcomponent

This password reset link will expire in 60 minutes.

If you did not request a password reset, no further action is required.

If you’re having trouble clicking the "Reset Password" button, copy and paste the URL below into your web browser:
{{url('reset-password/' . $token.'/'.$email)}}


Thanks,<br>
{{ config('app.name') }}
@endcomponent
