@extends('layouts.empty')

@section('title')
    API docs
@endsection

@section('content')
    <div class="row p-0">
             <div class="card">
                <div class="card-body">
                    <div class="row">
                        {{-- <thead class=" text-primary"> --}}
                        <div class="border col-md-3">
                            URI
                        </div>
                        {{-- <div>
                            TYPE
                        </div> --}}
                        <div class="border col-md-2 text-center" style="width: 10%; border-left:1px solid rgba(128,128,128,0.215);border-right: 1px solid rgba(128, 128, 128, 0.215);">
                            DETAIL
                        </div>
                         <div  class=" border col-md-2 border">FUNCTION </div >

                        <div  class=" border col-md-1 border"> METHODS </div>
                         <div  class=" border col-md-1 border" > PARAMETERS </div>
                         <div class="border border col-md-3" > BODY </div>

                    {{-- </thead> --}}
                        {{-- <tbody> --}}
                            @foreach ($docs as $item)
                                <div class="border col-md-3">
                                {{Request::root().'/'. $item['uri'] }}
                                </div>
                                <div class="border col-md-2" class="text-center border"  >

                                {{ $item['details']  }}
                                {{-- @endif --}}

                            </div>
                            <div  class="border col-md-2 border">{{ $item['method'] }}</div>
                            <div  class="border col-md-1 border">{{ $item['methods'][0] }}</div>
                            <div  class="border col-md-1  border text-center" >{{ $item['parameters']?$item['parameters']:'' }}</div>
                            <div   class="border col-md-3 border"
                                style=" position: relative; display:block;"
                                >

                                @if ($item['body'])
                                {{ json_encode( $item['body'] ) }}
                                @endif
                                @if ($item['model'])
                                {{ json_encode( $item['model'] ) }}
                                @endif
                             </div>
                                <hr>
                            @endforeach
                        {{-- </tbody> --}}
                    </div>
                </div>
        </div>
        {{-- <div class="border col-md-4">
            <div class="card">
                <div class="card-header">
                    <h4>POST METHOD OBJECTS: </h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">

                         <th class="border">
                            NAME
                        </th>
                        <th>
                            Properties
                        </th>
                    </thead>
                        <tbody>
                        @foreach ($post as $item)
                                 <tr >
                                    <div style="border-left:1px solid rgba(128,128,128,0.215);border-right: 1px solid rgba(128, 128, 128, 0.521);">
                                        {{ $item['name'] }}
                                    </td>
                                    <td>
                                        <ul>
                                            @foreach ($item['properties'] as $em)
                                                <li>
                                                    {{$em }}
                                                </li>
                                            @endforeach
                                        </ul>
                                    </td>
                                </tr>
                             @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> --}}
</div>


<!-- Button trigger modal -->



@endsection

@section('scripts')


<script>$(function () {

  $('[data-toggle="popover"]').popover()
})</script>
@endsection

