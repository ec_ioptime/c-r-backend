-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 03, 2022 at 05:06 AM
-- Server version: 10.3.32-MariaDB-log-cll-lve
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crprdpsp_cr`
--

-- --------------------------------------------------------

--
-- Table structure for table `breaks`
--

CREATE TABLE `breaks` (
  `id` int(10) NOT NULL,
  `time_log_id` int(11) NOT NULL,
  `startedAt` timestamp NULL DEFAULT NULL,
  `endedAt` timestamp NULL DEFAULT NULL,
  `startedAtLocation` varchar(255) DEFAULT NULL,
  `endedAtLocation` varchar(255) DEFAULT NULL,
  `total_time` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `breaks`
--

INSERT INTO `breaks` (`id`, `time_log_id`, `startedAt`, `endedAt`, `startedAtLocation`, `endedAtLocation`, `total_time`, `created_at`, `updated_at`) VALUES
(1, 148, '2021-06-29 15:30:00', '2021-06-29 16:15:00', '', '', '0.75', '2021-06-29 11:11:03', '2021-06-29 11:27:28'),
(2, 156, '2021-06-29 19:00:00', '2021-06-29 17:45:00', '', '', '1.25', '2021-06-29 13:53:32', '2021-06-29 13:53:32'),
(3, 157, '2021-06-29 12:45:00', '2021-06-29 13:00:00', '', '', '0.25', '2021-06-29 13:55:09', '2021-07-01 09:20:07'),
(6, 157, '2021-06-29 15:00:00', '2021-06-29 15:15:00', '', '', '0.25', '2021-07-01 06:25:16', '2021-07-01 06:25:16'),
(14, 157, '2021-06-29 17:00:00', '2021-06-29 17:30:00', '', '', '0.5', '2021-07-01 11:47:02', '2021-07-01 11:47:02'),
(15, 169, '2021-07-01 06:00:00', '2021-07-01 06:30:00', '', '', '0.5', '2021-07-01 17:46:42', '2021-07-02 04:53:03'),
(16, 160, '2021-07-01 18:02:00', '2021-07-01 18:49:00', '', '', '0.78333333333333', '2021-07-01 18:05:21', '2021-07-01 18:05:21'),
(17, 169, '2021-07-01 08:00:00', '2021-07-01 08:30:00', '', '', '0.5', '2021-07-02 04:53:31', '2021-07-02 04:53:51'),
(18, 160, '2021-07-01 13:00:00', '2021-07-01 13:30:00', '', '', '0.5', '2021-07-02 11:44:03', '2021-07-02 11:44:03'),
(19, 178, '2021-07-28 15:30:00', '2021-07-28 16:00:00', '', '', '0.5', '2021-07-28 17:23:34', '2021-07-28 17:25:02'),
(21, 178, '2021-07-28 17:30:00', '2021-07-28 18:00:00', '', '', '0.5', '2021-07-28 17:28:32', '2021-07-28 17:28:32'),
(22, 197, '2021-09-09 16:00:00', '2021-09-09 16:00:00', '{\"latitude\":37.0902,\"longitude\":95.7129}', '{\"latitude\":51.1657,\"longitude\":10.4515}', '0', '2021-09-13 05:38:57', '2021-09-13 05:38:57'),
(23, 197, '2021-09-09 16:00:00', '2021-09-09 16:00:00', '{\"latitude\":37.0902,\"longitude\":95.7129}', '{\"latitude\":36.2048,\"longitude\":138.2529}', '0', '2021-09-13 05:38:57', '2021-09-13 05:38:57'),
(24, 202, '2021-09-09 16:00:00', '2021-09-09 16:00:00', '{\"latitude\":33.57898877733148168545085354708135128021240234375,\"longitude\":73.018767207987906431299052201211452484130859375}', '{\"latitude\":33.57898877733148168545085354708135128021240234375,\"longitude\":73.018767207987906431299052201211452484130859375}', '0', '2021-09-14 04:42:15', '2021-09-14 04:42:15'),
(25, 202, '2021-09-09 16:00:00', '2021-09-09 16:00:00', '{\"latitude\":33.57898877733148168545085354708135128021240234375,\"longitude\":73.018767207987906431299052201211452484130859375}', '{\"latitude\":33.57898877733148168545085354708135128021240234375,\"longitude\":73.018767207987906431299052201211452484130859375}', '0', '2021-09-14 04:42:15', '2021-09-14 04:42:15'),
(30, 258, '2021-09-16 16:15:00', '2021-09-16 16:15:00', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '0', '2021-09-17 11:39:05', '2021-09-17 11:39:05'),
(31, 258, '2021-09-16 16:15:00', '2021-09-16 16:15:00', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '0', '2021-09-17 11:39:05', '2021-09-17 11:39:05'),
(32, 258, '2021-09-16 16:15:00', '2021-09-16 16:15:00', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '0', '2021-09-17 11:39:05', '2021-09-17 11:39:05'),
(33, 259, '2021-09-16 16:15:00', '2021-09-16 16:15:00', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '0', '2021-09-17 19:02:00', '2021-09-17 19:02:00'),
(34, 259, '2021-09-16 16:15:00', '2021-09-16 16:15:00', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '0', '2021-09-17 19:02:00', '2021-09-17 19:02:00'),
(35, 259, '2021-09-16 16:15:00', '2021-09-16 16:15:00', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '0', '2021-09-17 19:02:00', '2021-09-17 19:02:00'),
(36, 260, '2021-09-16 16:15:00', '2021-09-16 16:15:00', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '0', '2021-09-17 19:02:22', '2021-09-17 19:02:22'),
(37, 260, '2021-09-16 16:15:00', '2021-09-16 16:15:00', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '0', '2021-09-17 19:02:22', '2021-09-17 19:02:22'),
(38, 260, '2021-09-16 16:15:00', '2021-09-16 16:15:00', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '0', '2021-09-17 19:02:22', '2021-09-17 19:02:22'),
(39, 261, '2021-09-16 16:15:00', '2021-09-16 16:15:00', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '0', '2021-09-17 19:03:41', '2021-09-17 19:03:41'),
(40, 261, '2021-09-16 16:15:00', '2021-09-16 16:15:00', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '0', '2021-09-17 19:03:41', '2021-09-17 19:03:41'),
(41, 261, '2021-09-16 16:15:00', '2021-09-16 16:15:00', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '{\"latitude\":33.57896929609521663451232598163187503814697265625,\"longitude\":73.0187626986867002187864272855222225189208984375}', '0', '2021-09-17 19:03:41', '2021-09-17 19:03:41'),
(42, 262, '2021-09-17 19:45:00', '2021-09-17 19:45:00', '{\"latitude\":33.57886620000000021946107153780758380889892578125,\"longitude\":73.0187475999999975329046719707548618316650390625}', '{\"latitude\":33.57886620000000021946107153780758380889892578125,\"longitude\":73.0187475999999975329046719707548618316650390625}', '0', '2021-09-17 19:41:35', '2021-09-17 19:41:35'),
(43, 262, '2021-09-17 19:45:00', '2021-09-17 19:45:00', '{\"latitude\":33.57886620000000021946107153780758380889892578125,\"longitude\":73.0187475999999975329046719707548618316650390625}', '{\"latitude\":33.57886620000000021946107153780758380889892578125,\"longitude\":73.0187475999999975329046719707548618316650390625}', '0', '2021-09-17 19:41:35', '2021-09-17 19:41:35'),
(44, 373, '2021-12-16 16:15:00', '2021-12-16 17:30:00', '{\"latitude\":30.234508292079386393425011192448437213897705078125,\"longitude\":-93.16097436068076831361395306885242462158203125}', '{\"latitude\":30.234508292079386393425011192448437213897705078125,\"longitude\":-93.16097436068076831361395306885242462158203125}', '1.25', '2021-12-16 12:24:43', '2021-12-16 12:24:43'),
(47, 374, '2021-12-16 16:00:00', '2021-12-16 16:30:00', '{\"latitude\":30.234508292079386393425011192448437213897705078125,\"longitude\":-93.16097436068076831361395306885242462158203125}', '{\"latitude\":30.234508292079386393425011192448437213897705078125,\"longitude\":-93.16097436068076831361395306885242462158203125}', '0.5', '2021-12-16 12:45:57', '2021-12-16 12:45:57'),
(48, 405, '2022-01-31 10:00:00', '2022-01-31 10:00:00', '{\"latitude\":33.69784580000000318023012368939816951751708984375,\"longitude\":73.0490558999999990419382811523973941802978515625}', '{\"latitude\":33.69784580000000318023012368939816951751708984375,\"longitude\":73.0490558999999990419382811523973941802978515625}', '0', '2022-01-31 10:08:12', '2022-01-31 10:08:12'),
(49, 405, '2022-01-31 10:00:00', '2022-01-31 10:00:00', '{\"latitude\":33.69784580000000318023012368939816951751708984375,\"longitude\":73.0490558999999990419382811523973941802978515625}', '{\"latitude\":33.69784580000000318023012368939816951751708984375,\"longitude\":73.0490558999999990419382811523973941802978515625}', '0', '2022-01-31 10:08:12', '2022-01-31 10:08:12'),
(50, 405, '2022-01-31 10:00:00', '2022-01-31 10:00:00', '{\"latitude\":33.69784580000000318023012368939816951751708984375,\"longitude\":73.0490558999999990419382811523973941802978515625}', '{\"latitude\":33.69784580000000318023012368939816951751708984375,\"longitude\":73.0490558999999990419382811523973941802978515625}', '0', '2022-01-31 10:08:12', '2022-01-31 10:08:12'),
(51, 405, '2022-01-31 10:00:00', '2022-01-31 10:00:00', '{\"latitude\":33.69784580000000318023012368939816951751708984375,\"longitude\":73.0490558999999990419382811523973941802978515625}', '{\"latitude\":33.69784580000000318023012368939816951751708984375,\"longitude\":73.0490558999999990419382811523973941802978515625}', '0', '2022-01-31 10:08:12', '2022-01-31 10:08:12'),
(52, 405, '2022-01-31 10:00:00', '2022-01-31 10:00:00', '{\"latitude\":33.69784580000000318023012368939816951751708984375,\"longitude\":73.0490558999999990419382811523973941802978515625}', '{\"latitude\":33.6978543000000030360752134583890438079833984375,\"longitude\":73.0490508999999974548700265586376190185546875}', '0', '2022-01-31 10:08:12', '2022-01-31 10:08:12'),
(53, 407, '2022-01-31 10:15:00', '2022-01-31 10:15:00', '{\"latitude\":33.6978543000000030360752134583890438079833984375,\"longitude\":73.0490508999999974548700265586376190185546875}', '{\"latitude\":33.6978543000000030360752134583890438079833984375,\"longitude\":73.0490508999999974548700265586376190185546875}', '0', '2022-01-31 10:09:55', '2022-01-31 10:09:55'),
(54, 408, '2022-01-31 10:15:00', '2022-01-31 10:15:00', '{\"latitude\":33.6978543000000030360752134583890438079833984375,\"longitude\":73.0490508999999974548700265586376190185546875}', '{\"latitude\":33.6978543000000030360752134583890438079833984375,\"longitude\":73.0490508999999974548700265586376190185546875}', '0', '2022-01-31 10:11:17', '2022-01-31 10:11:17');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_inspection` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inspection_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `realtor1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `realtor2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `realtor1_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `realtor2_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weather` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direction` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `first_name`, `last_name`, `phone_number`, `email`, `address`, `city`, `state`, `zip`, `country`, `date_of_inspection`, `inspection_fee`, `realtor1`, `realtor2`, `realtor1_email`, `realtor2_email`, `weather`, `direction`, `summary`, `image`, `created_at`, `updated_at`) VALUES
(4, 'Palmer', 'Clayton', '+1 (474) 828-8608', 'leqexuq@mailinator.com', 'Et minim omnis dolor', 'Placeat et veritati', 'Dicta odit laboris e', '17904', 'Facilis culpa autem', '1974-05-17', '30', 'Ipsam quo maxime ven', 'Vitae sit quibusdam', 'dykihaniz@mailinator.com', 'paripunejo@mailinator.com', 'Dolorem quod ea duis', 'Provident in corpor', 'Consectetur tenetur', 'http://localhost:8000/public/img/1612958795.png', '2021-02-10 07:06:35', '2021-02-10 07:06:35'),
(5, 'Palmer', 'Clayton', '+1 (474) 828-8608', 'leqexuq@mailinator.com', 'Et minim omnis dolor', 'Placeat et veritati', 'Dicta odit laboris e', '17904', 'Facilis culpa autem', '1974-05-17', '30', 'Ipsam quo maxime ven', 'Vitae sit quibusdam', 'dykihaniz@mailinator.com', 'paripunejo@mailinator.com', 'Dolorem quod ea duis', 'Provident in corpor', 'Consectetur tenetur', 'http://localhost:8000/public/img/1612958856.png', '2021-02-10 07:07:36', '2021-02-10 07:07:36'),
(6, 'Palmer', 'Clayton', '+1 (474) 828-8608', 'leqexuq@mailinator.com', 'Et minim omnis dolor', 'Placeat et veritati', 'Dicta odit laboris e', '17904', 'Facilis culpa autem', '1974-05-17', '30', 'Ipsam quo maxime ven', 'Vitae sit quibusdam', 'dykihaniz@mailinator.com', 'paripunejo@mailinator.com', 'Dolorem quod ea duis', 'Provident in corpor', 'Consectetur tenetur', 'http://localhost:8000/public/img/1612958880.png', '2021-02-10 07:08:00', '2021-02-10 07:08:00'),
(7, 'Burton', 'Oneal', '+1 (859) 128-2405', 'netybiv@mailinator.com', 'Aliquam omnis aperia', 'Voluptatem aute ex q', 'Obcaecati aut conseq', '62689', 'Ex a dolorem aut cul', '2015-05-27', '59', 'Duis ut quo qui aute', 'Debitis rerum in eos', 'tesymusy@mailinator.com', 'cizitoryh@mailinator.com', 'Repellendus Aut tem', 'Qui nihil pariatur', 'Porro debitis qui pl', 'http://localhost:8000/public/img/1612958980.png', '2021-02-10 07:09:41', '2021-02-10 07:09:41'),
(8, 'Clinton', 'Medina', '+1 (666) 904-4719', 'bazyrid@mailinator.com', 'Ducimus ab distinct', 'Non soluta laboris r', 'Debitis aut laborum', '89428', 'Assumenda enim eos s', '1985-02-26', '35', 'Velit voluptas venia', 'Aut consequat Dolor', 'zewyvi@mailinator.com', 'sicibyciga@mailinator.com', 'Cillum quaerat ullam', 'Minus eaque quis acc', 'that the quick', 'http://localhost:8000/img/1612961526.png', '2021-02-10 07:14:45', '2021-02-10 07:52:07');

-- --------------------------------------------------------

--
-- Table structure for table `conf_mapping`
--

CREATE TABLE `conf_mapping` (
  `id` int(10) NOT NULL,
  `config_id` int(11) NOT NULL,
  `mapped_to` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conf_mapping`
--

INSERT INTO `conf_mapping` (`id`, `config_id`, `mapped_to`, `created_at`, `updated_at`) VALUES
(1, 1, 'mechanic_callout', '2021-04-19 16:18:22', '2021-04-19 16:18:22'),
(2, 2, 'hourly', '2021-04-19 16:36:01', '2021-04-19 16:36:01'),
(3, 3, 'fixed_salary', '2021-04-19 19:17:15', '2021-04-19 19:17:15'),
(4, 4, 'hourly', '2021-04-26 05:27:56', '2021-04-21 16:34:24'),
(5, 5, 'hourly', '2021-04-27 05:06:12', '2021-04-27 05:06:12'),
(6, 6, 'hourly', '2021-04-27 05:06:34', '2021-04-27 05:06:34'),
(7, 7, 'hourly', '2021-04-29 05:22:48', '2021-04-29 05:22:48'),
(9, 9, 'hourly_lp2_1/2', '2021-04-29 10:54:52', '2021-04-29 09:44:21'),
(10, 10, 'hourly_lp4', '2021-04-30 08:49:11', '2021-04-30 08:49:11'),
(11, 11, 'hourly', '2021-04-30 09:09:42', '2021-04-30 09:09:42'),
(12, 12, 'hourly_lp2_1/2', '2021-04-30 09:10:13', '2021-04-30 09:10:13'),
(13, 13, 'hourly_lp4', '2021-04-30 09:11:08', '2021-04-30 09:11:08'),
(17, 17, 'hourly', '2021-05-31 09:07:34', '2021-05-31 09:07:34'),
(18, 18, 'planttime_hourly', '2021-06-22 06:08:31', '2021-06-22 06:08:31'),
(20, 20, 'plant_hourly_lp2_1/2', '2021-07-01 10:20:53', '2021-07-01 10:20:53'),
(21, 21, 'planttime_hourly_lp4', '2021-07-02 05:07:57', '2021-07-02 05:07:57'),
(22, 22, 'planttime_hourly_lp4', '2021-07-28 06:37:39', '2021-07-28 06:37:39'),
(23, 23, 'planttime_hourly_lp4', '2021-07-28 06:39:46', '2021-07-28 06:39:46'),
(24, 24, 'planttime_hourly_lp4', '2021-07-28 06:41:19', '2021-07-28 06:41:19');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `on_site` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `title`, `on_site`, `created_at`, `updated_at`) VALUES
(13, 'Operator', '0', '2021-04-06 21:14:50', '2021-04-06 21:14:50'),
(14, 'Extra Man', '0', '2021-04-06 21:15:06', '2021-04-06 21:15:06'),
(15, 'Shop', '1', '2021-04-06 21:15:15', '2021-04-06 21:15:15'),
(16, 'Office', '1', '2021-04-06 21:15:22', '2021-04-06 21:15:22'),
(17, 'Mechanic Callout', '1', '2021-04-21 07:32:46', '2021-04-21 16:32:46'),
(18, 'Training', '1', '2021-04-06 21:15:43', '2021-04-06 21:15:43'),
(19, 'Sales', '1', '2021-04-09 11:54:28', '2021-04-09 15:54:28'),
(21, 'Mechanic', '1', '2021-04-21 16:33:10', '2021-04-21 16:33:10'),
(22, 'Developer', '0', '2021-05-31 10:43:35', '2021-05-31 09:43:35');

-- --------------------------------------------------------

--
-- Table structure for table `job_configuration`
--

CREATE TABLE `job_configuration` (
  `id` int(10) NOT NULL,
  `job_id` int(11) NOT NULL,
  `job_type_id` int(11) NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `overtime` varchar(255) NOT NULL DEFAULT '0',
  `monday` varchar(255) NOT NULL DEFAULT '0',
  `tuesday` varchar(255) NOT NULL DEFAULT '0',
  `wednesday` varchar(255) NOT NULL DEFAULT '0',
  `thursday` varchar(255) NOT NULL DEFAULT '0',
  `friday` varchar(255) NOT NULL DEFAULT '0',
  `saturday` varchar(255) NOT NULL DEFAULT '0',
  `sunday` varchar(255) NOT NULL DEFAULT '0',
  `start_overtime` time DEFAULT NULL,
  `end_overtime` time DEFAULT NULL,
  `overtime_rate` varchar(255) DEFAULT NULL,
  `overtime_limit` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_configuration`
--

INSERT INTO `job_configuration` (`id`, `job_id`, `job_type_id`, `start_time`, `end_time`, `overtime`, `monday`, `tuesday`, `wednesday`, `thursday`, `friday`, `saturday`, `sunday`, `start_overtime`, `end_overtime`, `overtime_rate`, `overtime_limit`, `created_at`, `updated_at`) VALUES
(1, 17, 0, 'clock_in', 'clock_out', '0', '0', '0', '0', '0', '0', '0', '0', NULL, NULL, NULL, NULL, '2021-04-19 16:18:22', '2021-04-19 16:18:22'),
(2, 18, 0, 'clock_in', 'clock_out', '0', '0', '0', '0', '0', '0', '0', '0', NULL, NULL, NULL, NULL, '2021-04-19 16:36:01', '2021-04-19 16:36:01'),
(3, 19, 0, 'clock_in', 'clock_out', '0', '0', '0', '0', '0', '0', '0', '0', NULL, NULL, NULL, NULL, '2021-04-19 19:17:15', '2021-04-19 19:17:15'),
(4, 21, 0, 'clock_in', 'clock_out', '0', '0', '0', '0', '0', '0', '0', '0', NULL, NULL, NULL, NULL, '2021-04-21 16:34:24', '2021-04-21 16:34:24'),
(5, 15, 0, 'clock_in', 'clock_out', '0', '0', '0', '0', '0', '0', '0', '0', NULL, NULL, NULL, NULL, '2021-04-27 05:06:12', '2021-04-27 05:06:12'),
(6, 16, 0, 'clock_in', 'clock_out', '0', '0', '0', '0', '0', '0', '0', '0', NULL, NULL, NULL, NULL, '2021-04-27 05:06:34', '2021-04-27 05:06:34'),
(7, 13, 25, 'clock_in', 'clock_out', '1', '1', '1', '1', '1', '1', '0', '0', '15:00:00', '07:00:00', NULL, NULL, '2021-05-03 10:14:55', '2021-05-03 09:14:55'),
(9, 13, 16, 'clock_in', 'clock_out', '1', '1', '1', '1', '1', '1', '0', '0', '15:00:00', '07:00:00', NULL, NULL, '2021-06-02 07:53:54', '2021-06-02 06:53:54'),
(10, 14, 19, 'clock_in', 'clock_out', '1', '1', '0', '0', '0', '0', '0', '0', '15:00:00', '07:00:00', NULL, NULL, '2021-05-05 11:02:56', '2021-05-05 10:02:56'),
(11, 14, 13, 'clock_in', 'clock_out', '1', '1', '1', '1', '1', '1', '0', '0', '15:00:00', '07:00:00', NULL, NULL, '2021-05-03 12:01:35', '2021-05-03 21:01:35'),
(12, 14, 20, 'clock_in', 'clock_out', '1', '1', '1', '1', '1', '1', '0', '0', '15:00:00', '07:00:00', NULL, NULL, '2021-05-03 12:02:06', '2021-05-03 21:02:06'),
(13, 13, 24, 'clock_in', 'clock_out', '1', '1', '1', '1', '1', '1', '0', '0', '15:00:00', '07:00:00', NULL, NULL, '2021-05-05 05:34:34', '2021-05-05 14:34:34'),
(17, 22, 29, 'arrive_job', 'leave_job', '1', '1', '1', '1', '1', '1', '0', '0', '09:00:00', '17:00:00', '100', '40', '2021-05-31 10:13:09', '2021-05-31 09:13:09'),
(18, 13, 14, 'clock_in', 'clock_out', '1', '1', '1', '1', '1', '1', '0', '0', '15:00:00', '07:00:00', NULL, NULL, '2021-06-23 05:48:17', '2021-06-23 04:48:17'),
(20, 13, 22, 'clock_in', 'clock_out', '1', '1', '1', '1', '1', '1', '0', '0', '15:00:00', '07:00:00', NULL, NULL, '2021-07-01 10:20:53', '2021-07-01 10:20:53'),
(21, 13, 18, 'clock_in', 'clock_out', '1', '1', '1', '1', '1', '1', '0', '0', '15:00:00', '07:00:00', NULL, NULL, '2021-07-02 05:07:57', '2021-07-02 05:07:57'),
(22, 14, 23, 'clock_in', 'clock_out', '1', '1', '1', '1', '1', '1', '0', '0', '15:00:00', '07:00:00', NULL, NULL, '2021-07-28 06:37:39', '2021-07-28 06:37:39'),
(23, 14, 28, 'clock_in', 'clock_out', '1', '1', '1', '1', '1', '1', '0', '0', '15:00:00', '07:00:00', NULL, NULL, '2021-07-28 06:39:46', '2021-07-28 06:39:46'),
(24, 14, 15, 'clock_in', 'clock_out', '1', '1', '1', '1', '1', '1', '0', '0', '15:00:00', '07:00:00', NULL, NULL, '2021-07-28 06:41:19', '2021-07-28 06:41:19');

-- --------------------------------------------------------

--
-- Table structure for table `job_types`
--

CREATE TABLE `job_types` (
  `id` int(10) NOT NULL,
  `job_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_types`
--

INSERT INTO `job_types` (`id`, `job_id`, `name`, `created_at`, `updated_at`) VALUES
(13, 14, 'Commercial', '2021-05-05 09:53:40', '2021-05-05 18:53:40'),
(14, 13, 'Industrial', '2021-04-06 21:18:21', '2021-04-06 21:18:21'),
(15, 14, 'Industrial', '2021-04-06 21:18:27', '2021-04-06 21:18:27'),
(16, 13, 'LP2 Commercial', '2021-04-06 21:18:47', '2021-04-06 21:18:47'),
(18, 13, 'LP4 Industrial', '2021-04-06 17:28:53', '2021-04-06 21:28:53'),
(19, 14, 'LP4 Commercial', '2021-04-06 21:19:29', '2021-04-06 21:19:29'),
(20, 14, 'LP2 Commercial', '2021-04-06 22:54:15', '2021-04-06 22:54:15'),
(22, 13, 'LP2 Industrial', '2021-04-27 18:06:31', '2021-04-27 18:06:31'),
(23, 14, 'LP4 Industrial', '2021-04-27 18:07:38', '2021-04-27 18:07:38'),
(24, 13, 'LP4 Commercial', '2021-04-27 18:08:00', '2021-04-27 18:08:00'),
(25, 13, 'Commercial', '2021-04-27 18:08:38', '2021-04-27 18:08:38'),
(28, 14, 'LP2 Industrial', '2021-04-27 18:11:46', '2021-04-27 18:11:46'),
(29, 22, 'Mobile', '2021-05-31 08:01:19', '2021-05-31 08:01:19');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_02_09_113543_update_users_table', 2),
(5, '2021_02_10_065333_create_clients_table', 3),
(6, '2021_02_10_065452_create_properties_table', 3),
(7, '2021_02_10_065558_create_reports_table', 3),
(8, '2021_02_10_070038_add_extra_fields_to_users_table', 3),
(9, '2021_02_10_072813_change_default_client_id_properties_table', 4),
(10, '2019_12_14_000001_create_personal_access_tokens_table', 5),
(11, '2021_02_15_093838_update_add_fields_users_table', 6),
(12, '2021_02_15_102147_update_pass_fields_users_table', 7),
(13, '2021_02_17_050950_update_properties_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ali@yopmail.com', '$2y$10$ohFuoTp7olJd9gdzg2GUje54rYXBoCTBwxiywWA1e/KpiAldF9qES', '2021-07-02 09:17:30'),
('contact@euromoonintl.com', '$2y$10$7USkY7RUzTyZbqkdO7LqCOoxBxiU4H2hjZ6Iv7.W4j/Vl9/QWs1ES', '2021-07-05 05:08:38'),
('new123@yopmail.com', '$2y$10$zOCsRRilw/jJEeMvFIhr6uG8GDHeALFT9lHjyjIKB9Hz63llfoFqS', '2021-08-04 05:38:37'),
('nc@yopmail.com', '$2y$10$E.SBIfRZEExKx3.JcAK4sO540FM01SaJKDGvjAjQMDQN6Me9k.ecG', '2021-10-22 05:42:51'),
('aazam229@yahoo.com', '$2y$10$SoQMFvtpiSGIleXLwtou9u1ak6O6acq8kIEyo5nxtTWgKc0zI/dSm', '2021-10-22 09:40:12'),
('aazam513@gmail.com', '$2y$10$T6N5hy/oefTCDMlv1pqmi.x8.akHQVypoq0.vK7xVWL8i3WfmlciG', '2021-12-16 14:57:55');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\User', 5, 'aieye-token', 'bb521d8386fd17ad2f2e78e075817632c9bd2ed25c360affb124b20b74ea00e0', '[\"*\"]', '2021-02-11 06:37:51', '2021-02-10 03:43:04', '2021-02-11 06:37:51'),
(2, 'App\\User', 5, 'aieye-token', '9c1a452a308cfb2e540581257d59760c3b933cc138c334ad16cbacc43985d1cb', '[\"*\"]', '2021-02-15 07:10:35', '2021-02-11 06:09:59', '2021-02-15 07:10:35'),
(3, 'App\\User', 5, 'aieye-token', 'dd828fc2148ae41cdb9c8ee1b81aebd4e43bf3f27f0369c68ba8fa87832dfd84', '[\"*\"]', NULL, '2021-02-15 03:00:28', '2021-02-15 03:00:28'),
(4, 'App\\User', 4, 'aieye-token', 'b60a0307df8b97bf561906e110f3617d083f702b2b5a8e50ac8c093f1a5c04a0', '[\"*\"]', NULL, '2021-02-17 11:04:06', '2021-02-17 11:04:06');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_inspection` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inspection_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `realtor1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `realtor2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `realtor1_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `realtor2_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direction` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weather` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `name`, `user_id`, `created_at`, `updated_at`, `first_name`, `last_name`, `city`, `state`, `zip`, `image`, `address`, `country`, `date_of_inspection`, `inspection_fee`, `realtor1`, `realtor2`, `realtor1_email`, `realtor2_email`, `direction`, `weather`, `summary`, `phone_number`, `email`) VALUES
(5, 'hrmanager', 3, '2021-02-10 03:10:47', '2021-02-11 05:21:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'sddsds 12', 12, '2021-02-11 06:29:59', '2021-02-11 06:32:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'sddsds', 12, '2021-02-11 06:29:59', '2021-02-11 06:29:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'sddsds', 4, '2021-02-11 06:30:00', '2021-02-11 06:30:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'sddsds', 4, '2021-02-11 06:30:00', '2021-02-11 06:30:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'sddsds', 4, '2021-02-11 06:30:01', '2021-02-11 06:30:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'sddsds', 4, '2021-02-11 06:30:01', '2021-02-11 06:30:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `report_id` int(11) NOT NULL,
  `user_id` int(10) NOT NULL,
  `hourly` double(10,2) NOT NULL DEFAULT 0.00,
  `overtime_hourly` double(10,2) NOT NULL DEFAULT 0.00,
  `planttime_hourly` double(10,2) NOT NULL DEFAULT 0.00,
  `plant_overtime` double(10,2) NOT NULL DEFAULT 0.00,
  `hourly_lp2` double(10,2) NOT NULL DEFAULT 0.00,
  `overtime_hourly_lp2` double(10,2) NOT NULL DEFAULT 0.00,
  `hourly_lp4` double(10,2) NOT NULL DEFAULT 0.00,
  `overtime_hourly_lp4` double(10,2) NOT NULL DEFAULT 0.00,
  `plant_hourly_lp2` double(10,2) NOT NULL DEFAULT 0.00,
  `plant_overtime_lp2` double(10,2) NOT NULL DEFAULT 0.00,
  `planttime_hourly_lp4` double(10,2) NOT NULL DEFAULT 0.00,
  `plant_overtime_lp4` double(10,2) NOT NULL DEFAULT 0.00,
  `fixed_salary` double(10,2) NOT NULL DEFAULT 0.00,
  `double_time_hourly` double(10,2) NOT NULL DEFAULT 0.00,
  `lp2_doubletime` double(10,2) NOT NULL DEFAULT 0.00,
  `lp4_doubletime` double(10,2) NOT NULL DEFAULT 0.00,
  `plant_doubletime` double(10,2) NOT NULL DEFAULT 0.00,
  `lp2_plant_doubletime` double(10,2) NOT NULL DEFAULT 0.00,
  `lp4_plant_doubletime` double(10,2) NOT NULL DEFAULT 0.00,
  `mechanic_callout` double(10,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `report_id`, `user_id`, `hourly`, `overtime_hourly`, `planttime_hourly`, `plant_overtime`, `hourly_lp2`, `overtime_hourly_lp2`, `hourly_lp4`, `overtime_hourly_lp4`, `plant_hourly_lp2`, `plant_overtime_lp2`, `planttime_hourly_lp4`, `plant_overtime_lp4`, `fixed_salary`, `double_time_hourly`, `lp2_doubletime`, `lp4_doubletime`, `plant_doubletime`, `lp2_plant_doubletime`, `lp4_plant_doubletime`, `mechanic_callout`, `created_at`, `updated_at`) VALUES
(3, 2, 20, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '2021-09-09 06:27:51', '2021-09-09 06:27:51'),
(4, 2, 35, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 10.25, '2021-09-09 06:27:51', '2021-09-09 06:27:51'),
(5, 2, 36, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '2021-09-09 06:27:51', '2021-09-09 06:27:51'),
(6, 3, 35, 6.87, 30.13, -0.37, 0.37, 3.00, 8.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4.00, 2.00, 0.00, 0.00, 0.00, 0.00, 10.25, '2021-09-09 06:51:23', '2021-09-09 06:51:23'),
(7, 3, 36, 16.75, 20.25, 0.00, 5.00, 0.00, 0.00, 0.00, 0.00, 8.00, 7.00, 0.00, 0.00, 0.00, 2.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '2021-09-09 06:51:23', '2021-09-09 06:51:23');

-- --------------------------------------------------------

--
-- Table structure for table `report_data`
--

CREATE TABLE `report_data` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `pay_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report_data`
--

INSERT INTO `report_data` (`id`, `name`, `start_date`, `end_date`, `pay_date`, `created_at`, `updated_at`) VALUES
(2, 'Test', '2021-09-01', '2021-09-08', '2021-09-16', '2021-09-09 06:27:51', '2021-09-09 06:27:51'),
(3, 'Testing', '2021-06-01', '2021-09-09', '2021-09-17', '2021-09-09 06:51:23', '2021-09-09 06:51:23');

-- --------------------------------------------------------

--
-- Table structure for table `times_list`
--

CREATE TABLE `times_list` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `times_list`
--

INSERT INTO `times_list` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'clock_in', '2021-04-05 07:59:13', '0000-00-00 00:00:00'),
(2, 'leave_yard', '2021-04-05 07:59:13', '0000-00-00 00:00:00'),
(3, 'arrive_job', '2021-04-05 07:59:38', '0000-00-00 00:00:00'),
(4, 'leave_job', '2021-04-05 07:59:38', '0000-00-00 00:00:00'),
(5, 'arrive_yard', '2021-04-05 08:00:28', '0000-00-00 00:00:00'),
(6, 'clock_out', '2021-04-05 08:00:28', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `time_logs`
--

CREATE TABLE `time_logs` (
  `id` int(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `job_type_id` int(11) NOT NULL,
  `clock_intime` timestamp NULL DEFAULT NULL,
  `leave_yardtime` timestamp NULL DEFAULT NULL,
  `arrive_jobtime` timestamp NULL DEFAULT NULL,
  `leave_jobtime` timestamp NULL DEFAULT NULL,
  `arrive_yardtime` timestamp NULL DEFAULT NULL,
  `clock_outtime` timestamp NULL DEFAULT NULL,
  `clock_in_location` varchar(255) DEFAULT NULL,
  `leave_yard_location` varchar(255) DEFAULT NULL,
  `arrive_job_location` varchar(255) DEFAULT NULL,
  `leave_job_location` varchar(255) DEFAULT NULL,
  `arrive_yard_location` varchar(255) DEFAULT NULL,
  `clock_out_location` varchar(255) DEFAULT NULL,
  `hourly` double(10,2) NOT NULL DEFAULT 0.00,
  `overtime_hourly` double(10,2) NOT NULL DEFAULT 0.00,
  `planttime_hourly` double(10,2) NOT NULL DEFAULT 0.00,
  `plant_overtime` double(10,2) NOT NULL DEFAULT 0.00,
  `hourly_lp2` double(10,2) NOT NULL DEFAULT 0.00,
  `overtime_hourly_lp2` double(10,2) NOT NULL DEFAULT 0.00,
  `hourly_lp4` double(10,2) NOT NULL DEFAULT 0.00,
  `overtime_hourly_lp4` double(10,2) NOT NULL DEFAULT 0.00,
  `plant_hourly_lp2` double(10,2) NOT NULL DEFAULT 0.00,
  `plant_overtime_lp2` double(10,2) NOT NULL DEFAULT 0.00,
  `planttime_hourly_lp4` double(10,2) NOT NULL DEFAULT 0.00,
  `plant_overtime_lp4` double(10,2) NOT NULL DEFAULT 0.00,
  `fixed_salary` double(10,2) NOT NULL DEFAULT 0.00,
  `double_time_hourly` double(10,2) NOT NULL DEFAULT 0.00,
  `lp2_doubletime` double(10,2) NOT NULL DEFAULT 0.00,
  `lp4_doubletime` double(10,2) NOT NULL DEFAULT 0.00,
  `plant_doubletime` double(10,2) NOT NULL DEFAULT 0.00,
  `lp2_plant_doubletime` double(10,2) NOT NULL DEFAULT 0.00,
  `lp4_plant_doubletime` double(10,2) NOT NULL DEFAULT 0.00,
  `mechanic_callout` double(10,2) NOT NULL DEFAULT 0.00,
  `vacation_pay_hourly` double(10,2) NOT NULL DEFAULT 0.00,
  `sick_pay_hourly` double(10,2) NOT NULL DEFAULT 0.00,
  `holiday` tinyint(1) NOT NULL DEFAULT 0,
  `invoiceNumber` varchar(255) DEFAULT NULL,
  `notes` varchar(5000) DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `time_logs`
--

INSERT INTO `time_logs` (`id`, `user_id`, `job_id`, `job_type_id`, `clock_intime`, `leave_yardtime`, `arrive_jobtime`, `leave_jobtime`, `arrive_yardtime`, `clock_outtime`, `clock_in_location`, `leave_yard_location`, `arrive_job_location`, `leave_job_location`, `arrive_yard_location`, `clock_out_location`, `hourly`, `overtime_hourly`, `planttime_hourly`, `plant_overtime`, `hourly_lp2`, `overtime_hourly_lp2`, `hourly_lp4`, `overtime_hourly_lp4`, `plant_hourly_lp2`, `plant_overtime_lp2`, `planttime_hourly_lp4`, `plant_overtime_lp4`, `fixed_salary`, `double_time_hourly`, `lp2_doubletime`, `lp4_doubletime`, `plant_doubletime`, `lp2_plant_doubletime`, `lp4_plant_doubletime`, `mechanic_callout`, `vacation_pay_hourly`, `sick_pay_hourly`, `holiday`, `invoiceNumber`, `notes`, `status`, `created_at`, `updated_at`) VALUES
(163, 35, 21, 0, '2021-06-30 06:30:00', NULL, NULL, NULL, NULL, '2021-06-30 06:30:00', NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2021-06-30 06:37:05', '2021-08-09 11:29:27'),
(164, 35, 18, 0, '2021-06-30 06:00:00', NULL, NULL, NULL, NULL, '2021-06-30 07:30:00', NULL, NULL, NULL, NULL, NULL, NULL, 1.50, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2021-06-30 07:12:04', '2021-08-09 11:29:27'),
(168, 35, 21, 0, '2021-07-01 04:30:00', NULL, NULL, NULL, NULL, '2021-07-01 04:30:00', NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, 'notes will be addedhere', '1', '2021-07-01 04:30:22', '2021-08-09 11:29:27'),
(357, 36, 13, 16, '2021-10-23 19:45:00', '2021-10-23 19:45:00', '2021-10-23 20:00:00', '2021-10-23 20:15:00', '2021-10-23 21:00:00', '2021-10-23 22:00:00', '{\"latitude\":29.92704070175991,\"longitude\":-91.73956040301522}', '{\"latitude\":29.98317250088297,\"longitude\":-91.8562145971507}', '{\"latitude\":30.114845242985183,\"longitude\":-91.94291734004447}', '{\"latitude\":30.254133982508108,\"longitude\":-92.01419091893906}', '{\"latitude\":30.245261247407065,\"longitude\":-92.63988819450931}', '{\"latitude\":30.482078946521703,\"longitude\":-92.69523016176106}', 0.00, 2.00, 0.00, 0.00, 0.00, 0.25, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '22', NULL, '1', '2021-10-23 22:03:00', '2021-11-06 05:41:56'),
(358, 58, 13, 18, '2021-10-25 21:45:00', '2021-10-25 21:45:00', '2021-10-25 21:45:00', '2021-10-25 21:45:00', '2021-10-25 21:45:00', '2021-10-25 21:45:00', '{\"latitude\":30.491724218951017,\"longitude\":-92.44715177606054}', '{\"latitude\":30.49166355284908,\"longitude\":-92.44730617859628}', '{\"latitude\":30.49166355284908,\"longitude\":-92.44730617859628}', '{\"latitude\":30.49166355284908,\"longitude\":-92.44730617859628}', '{\"latitude\":30.49166355284908,\"longitude\":-92.44730617859628}', '{\"latitude\":30.491724698832716,\"longitude\":-92.44738060989644}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '555', NULL, '1', '2021-10-25 21:47:41', '2021-11-06 05:41:56'),
(359, 58, 13, 14, '2021-10-26 17:30:00', '2021-10-26 17:30:00', '2021-10-26 17:30:00', '2021-10-26 17:30:00', '2021-10-26 17:30:00', '2021-10-26 17:30:00', '{\"latitude\":30.23469,\"longitude\":-93.1614439}', '{\"latitude\":30.23469,\"longitude\":-93.1614439}', '{\"latitude\":30.23469,\"longitude\":-93.1614439}', '{\"latitude\":30.23469,\"longitude\":-93.1614439}', '{\"latitude\":30.23469,\"longitude\":-93.1614439}', '{\"latitude\":30.23469,\"longitude\":-93.1614439}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '55', NULL, '1', '2021-10-26 17:29:41', '2021-11-06 05:41:56'),
(360, 58, 13, 16, '2021-10-26 17:30:00', '2021-10-26 17:30:00', '2021-10-26 17:30:00', '2021-10-26 17:30:00', '2021-10-26 17:30:00', '2021-10-26 17:30:00', '{\"latitude\":30.23469,\"longitude\":-93.1614439}', '{\"latitude\":30.23469,\"longitude\":-93.1614439}', '{\"latitude\":30.23469,\"longitude\":-93.1614439}', '{\"latitude\":30.23469,\"longitude\":-93.1614439}', '{\"latitude\":30.23469,\"longitude\":-93.1614439}', '{\"latitude\":30.23469,\"longitude\":-93.1614439}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '66', NULL, '1', '2021-10-26 17:31:14', '2021-11-06 05:41:56'),
(361, 58, 13, 14, '2021-10-26 19:30:00', '2021-10-26 19:30:00', '2021-10-26 19:45:00', '2021-10-26 20:00:00', '2021-10-26 20:00:00', '2021-10-26 20:15:00', '{\"latitude\":30.2346905,\"longitude\":-93.1614446}', '{\"latitude\":30.2346905,\"longitude\":-93.1614446}', '{\"latitude\":30.248703,\"longitude\":-92.9961525}', '{\"latitude\":30.248703,\"longitude\":-92.9961525}', '{\"latitude\":30.4825053,\"longitude\":-92.842428}', '{\"latitude\":30.4364336,\"longitude\":-92.775501}', 0.00, 0.50, 0.00, 0.25, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '0001', NULL, '1', '2021-10-26 20:12:38', '2021-11-06 05:41:56'),
(362, 36, 13, 16, '2021-10-26 19:30:00', '2021-10-26 19:30:00', '2021-10-26 19:45:00', '2021-10-26 20:00:00', '2021-10-26 20:00:00', '2021-10-26 20:15:00', '{\"latitude\":30.234658643631484,\"longitude\":-93.16134290778372}', '{\"latitude\":30.23189653371357,\"longitude\":-93.01301649630899}', '{\"latitude\":30.360326472199525,\"longitude\":-92.92050511570137}', '{\"latitude\":30.482033986627577,\"longitude\":-92.85101655320214}', '{\"latitude\":30.481685756026916,\"longitude\":-92.78006578610682}', '{\"latitude\":30.426336513845904,\"longitude\":-92.79217764140984}', 0.25, 0.25, 0.00, 0.00, 0.00, 0.25, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '0002', NULL, '1', '2021-10-26 20:12:46', '2022-01-13 11:17:34'),
(363, 58, 13, 24, '2021-10-27 09:30:00', '2021-10-27 09:45:00', '2021-10-27 10:00:00', '2021-10-27 10:15:00', '2021-10-27 10:30:00', '2021-10-27 10:45:00', '{\"latitude\":30.4829392,\"longitude\":-92.6942824}', '{\"latitude\":30.4996423,\"longitude\":-92.7806264}', '{\"latitude\":30.3663724,\"longitude\":-92.9188091}', '{\"latitude\":30.2534589,\"longitude\":-93.0133776}', '{\"latitude\":30.247552,\"longitude\":-93.014784}', '{\"latitude\":30.247552,\"longitude\":-93.014784}', 0.25, 0.75, 0.00, 0.00, 0.00, 0.00, 0.00, 0.25, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '444', NULL, '1', '2021-10-27 10:40:20', '2022-01-13 11:18:11'),
(364, 36, 13, 18, '2021-10-27 09:30:00', '2021-10-27 09:45:00', '2021-10-27 10:00:00', '2021-10-27 10:15:00', '2021-10-27 10:30:00', '2021-10-27 10:45:00', '{\"latitude\":30.482858858992742,\"longitude\":-92.69475372865425}', '{\"latitude\":30.481884763604526,\"longitude\":-92.85101694063549}', '{\"latitude\":30.36614870189728,\"longitude\":-92.91737546221624}', '{\"latitude\":30.253487415639917,\"longitude\":-93.01407211468836}', '{\"latitude\":30.23233103103026,\"longitude\":-93.11366370887785}', '{\"latitude\":30.23474782708123,\"longitude\":-93.16140560441944}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1.00, 0.00, 0.00, 0.00, 0.25, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '333', NULL, '1', '2021-10-27 10:40:45', '2021-11-06 05:41:56'),
(365, 58, 13, 25, '2021-10-28 09:15:00', '2021-10-28 09:45:00', '2021-10-28 09:45:00', '2021-10-28 10:00:00', '2021-10-28 10:15:00', '2021-10-28 10:30:00', '{\"latitude\":30.4829385,\"longitude\":-92.6942999}', '{\"latitude\":30.5297857,\"longitude\":-92.8170505}', '{\"latitude\":30.5297857,\"longitude\":-92.8170505}', '{\"latitude\":30.5297857,\"longitude\":-92.8170505}', '{\"latitude\":30.2482929,\"longitude\":-92.9983408}', '{\"latitude\":30.2482929,\"longitude\":-92.9983408}', 0.00, 1.25, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '555', NULL, '1', '2021-10-28 10:25:48', '2021-11-06 05:41:56'),
(366, 36, 13, 14, '2021-10-28 09:15:00', '2021-10-28 09:45:00', '2021-10-28 09:45:00', '2021-10-28 10:00:00', '2021-10-28 10:15:00', '2021-10-28 10:30:00', '{\"latitude\":30.482947924202758,\"longitude\":-92.69436713316729}', '{\"latitude\":30.481898271866353,\"longitude\":-92.85119432552752}', '{\"latitude\":30.366114620346746,\"longitude\":-92.91748913845491}', '{\"latitude\":30.253310608194496,\"longitude\":-93.01376423917925}', '{\"latitude\":30.23214172821759,\"longitude\":-93.11281394482198}', '{\"latitude\":30.237350161887438,\"longitude\":-93.16328653530006}', 0.00, 1.00, 0.00, 0.25, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '666', NULL, '1', '2021-10-28 10:25:56', '2021-11-06 05:41:56'),
(367, 35, 17, 0, '2021-11-06 05:45:00', NULL, NULL, NULL, NULL, '2021-11-06 05:45:00', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '\"\"', '\"\"', '\"\"', '\"\"', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, 'Gjj', '1', '2021-11-06 05:43:43', '2021-11-06 05:55:17'),
(368, 35, 13, 24, '2021-11-06 05:45:00', '2021-11-06 05:45:00', '2021-11-06 05:45:00', '2021-11-06 05:45:00', '2021-11-06 05:45:00', '2021-11-06 05:45:00', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '5685', NULL, '1', '2021-11-06 05:46:06', '2022-01-13 11:17:56'),
(369, 35, 19, 0, '2021-11-06 05:45:00', NULL, NULL, NULL, NULL, '2021-11-06 05:45:00', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '\"\"', '\"\"', '\"\"', '\"\"', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2021-11-06 05:46:43', '2021-11-06 05:55:17'),
(370, 35, 22, 29, '2021-11-06 05:45:00', '2021-11-06 05:45:00', '2021-11-06 05:45:00', '2021-11-06 05:45:00', '2021-11-06 05:45:00', '2021-11-06 05:45:00', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', 0.25, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '123456', NULL, '1', '2021-11-06 05:51:53', '2021-11-06 05:55:17'),
(371, 35, 14, 13, '2021-11-06 06:00:00', '2021-11-06 06:00:00', '2021-11-06 06:00:00', '2021-11-06 06:00:00', '2021-11-06 06:00:00', '2021-11-06 06:00:00', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', '{\"latitude\":30.1538747,\"longitude\":71.4951594}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '147258', NULL, '1', '2021-11-06 05:53:43', '2021-11-06 05:55:17'),
(373, 36, 13, 16, '2021-12-16 12:00:00', NULL, NULL, NULL, NULL, '2021-12-16 20:30:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '\"\"', '\"\"', '\"\"', '\"\"', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '0', '2021-12-16 12:24:43', '2021-12-24 11:32:03'),
(376, 36, 13, 25, '2021-12-16 11:00:00', '2021-12-16 14:00:00', '2021-12-16 15:00:00', '2021-12-16 18:00:00', '2021-12-16 19:00:00', '2021-12-17 13:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 9.25, 16.75, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '111', NULL, '0', '2021-12-16 14:39:06', '2022-01-12 09:56:24'),
(377, 36, 13, 25, '2021-12-16 11:00:00', '2021-12-16 14:00:00', '2021-12-16 15:00:00', '2021-12-16 18:00:00', '2021-12-16 19:00:00', '2021-12-16 20:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 8.25, 0.75, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '222', NULL, '0', '2021-12-16 14:45:55', '2022-01-12 09:55:48'),
(379, 36, 13, 25, '2021-12-19 23:00:00', '2021-12-20 00:00:00', '2021-12-20 01:00:00', '2021-12-20 11:00:00', '2021-12-20 12:00:00', '2021-12-20 13:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 1.00, 7.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 6.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '333', NULL, '0', '2021-12-16 16:05:53', '2021-12-16 16:10:50'),
(380, 36, 13, 16, '2021-12-16 10:00:00', '2021-12-16 13:00:00', '2021-12-16 14:00:00', '2021-12-16 17:00:00', '2021-12-16 20:00:00', '2021-12-16 21:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 5.25, 2.75, 0.00, 0.00, 3.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '444', NULL, '0', '2021-12-16 16:29:58', '2021-12-27 13:18:53'),
(381, 36, 13, 16, '2021-12-16 11:00:00', '2021-12-16 12:00:00', '2021-12-16 13:00:00', '2021-12-17 11:00:00', '2021-12-17 12:00:00', '2021-12-17 13:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 3.25, 0.75, 0.00, 0.00, 7.00, 15.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '555', NULL, '0', '2021-12-17 12:16:45', '2022-01-11 09:56:31'),
(382, 36, 13, 16, '2021-12-18 18:00:00', '2021-12-18 19:00:00', '2021-12-18 20:00:00', '2021-12-19 04:00:00', '2021-12-19 06:00:00', '2021-12-19 07:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 0.00, 3.00, 0.00, 0.00, 0.00, 8.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '666', NULL, '0', '2021-12-17 12:25:44', '2022-01-11 10:05:39'),
(384, 36, 13, 16, '2021-12-27 01:00:00', '2021-12-27 02:00:00', '2021-12-27 03:00:00', '2021-12-27 07:00:00', '2021-12-27 08:00:00', '2021-12-27 09:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 0.00, 2.00, 0.00, 0.00, 0.00, 2.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2.00, 2.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '777', NULL, '0', '2021-12-17 13:01:24', '2022-01-11 10:05:56'),
(385, 36, 13, 24, '2021-12-24 11:00:00', '2021-12-24 14:00:00', '2021-12-24 15:00:00', '2021-12-24 16:00:00', '2021-12-24 20:00:00', '2021-12-24 21:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 7.25, 1.75, 0.00, 0.00, 0.00, 0.00, 5.00, 20.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '888', NULL, '0', '2021-12-17 13:38:32', '2022-01-11 10:06:52'),
(386, 36, 13, 16, '2021-12-15 14:00:00', '2021-12-15 15:00:00', '2021-12-15 16:00:00', '2021-12-16 13:00:00', '2021-12-16 14:00:00', '2021-12-16 15:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 4.00, 0.00, 0.00, 0.00, 5.00, 16.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '999', NULL, '0', '2021-12-17 14:25:08', '2022-01-13 06:39:59'),
(387, 36, 13, 24, '2021-12-18 01:00:00', '2021-12-18 02:00:00', '2021-12-18 03:00:00', '2021-12-18 06:00:00', '2021-12-18 07:00:00', '2021-12-18 08:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 0.25, 3.75, 0.00, 0.00, 0.00, 0.00, 0.00, 3.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1010', NULL, '0', '2021-12-17 14:35:00', '2021-12-17 14:36:00'),
(389, 36, 13, 24, '2021-12-18 21:00:00', '2021-12-18 22:00:00', '2021-12-18 23:00:00', '2021-12-19 07:00:00', '2021-12-19 08:00:00', '2021-12-19 09:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 0.00, 2.00, 0.00, 0.00, 0.00, 0.00, 0.00, 6.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2.00, 0.00, 2.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1111', NULL, '0', '2021-12-17 14:52:41', '2022-01-12 11:23:11'),
(390, 36, 13, 14, '2021-12-20 09:00:00', '2021-12-20 13:15:00', '2021-12-20 14:00:00', '2021-12-20 17:00:00', '2021-12-20 21:00:00', '2021-12-20 22:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 5.25, 4.75, 3.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1212', NULL, '0', '2021-12-20 12:41:43', '2021-12-28 13:15:18'),
(391, 36, 13, 14, '2021-12-20 13:00:00', '2021-12-20 14:00:00', '2021-12-20 16:00:00', '2021-12-21 11:00:00', '2021-12-21 13:00:00', '2021-12-21 14:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 5.00, 1.00, 4.00, 15.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1313', NULL, '0', '2021-12-20 13:07:04', '2022-01-13 05:42:22'),
(392, 36, 13, 14, '2021-12-18 23:00:00', '2021-12-19 00:00:00', '2021-12-19 02:00:00', '2021-12-19 08:00:00', '2021-12-19 09:00:00', '2021-12-19 15:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 0.00, 3.00, 0.00, 3.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 7.00, 0.00, 0.00, 3.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1414', NULL, '0', '2021-12-20 13:11:46', '2022-01-13 06:57:20'),
(393, 36, 13, 24, '2021-12-20 02:00:00', '2021-12-20 03:00:00', '2021-12-20 08:00:00', '2021-12-20 11:00:00', '2021-12-20 12:00:00', '2021-12-20 13:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 1.00, 4.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1515', NULL, '0', '2021-12-20 14:32:53', '2022-01-13 07:23:08'),
(394, 36, 13, 18, '2021-12-20 10:00:00', '2021-12-20 13:00:00', '2021-12-20 14:00:00', '2021-12-20 16:00:00', '2021-12-20 20:00:00', '2021-12-20 21:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 6.25, 2.75, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1616', NULL, '0', '2021-12-20 14:45:41', '2021-12-28 13:17:12'),
(395, 36, 13, 22, '2021-12-20 12:00:00', '2021-12-20 13:00:00', '2021-12-20 14:00:00', '2021-12-21 11:00:00', '2021-12-21 13:00:00', '2021-12-21 14:00:00', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', '{\"latitude\":30.234508292079386,\"longitude\":-93.16097436068077}', 5.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 6.00, 15.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1717', NULL, '0', '2021-12-20 15:23:07', '2022-01-13 11:13:46'),
(396, 36, 13, 22, '2021-12-26 00:00:00', '2021-12-26 01:00:00', '2021-12-26 02:00:00', '2021-12-26 08:00:00', '2021-12-26 09:00:00', '2021-12-26 10:00:00', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', 0.00, -1.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 6.00, 0.00, 0.00, 0.00, 5.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1818', NULL, '0', '2021-12-21 11:55:56', '2022-01-13 07:45:41'),
(397, 36, 13, 22, '2021-12-27 01:00:00', '2021-12-27 02:00:00', '2021-12-27 03:00:00', '2021-12-27 07:00:00', '2021-12-27 08:00:00', '2021-12-27 09:00:00', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', 0.00, 2.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2.00, 0.00, 0.00, 0.00, 2.00, 0.00, 0.00, 0.00, 2.00, 0.00, 0.00, 0.00, 0.00, 0, '1919', NULL, '0', '2021-12-21 18:12:00', '2022-01-18 09:39:58'),
(398, 36, 13, 18, '2021-12-22 11:00:00', '2021-12-22 13:00:00', '2021-12-22 14:00:00', '2021-12-22 16:00:00', '2021-12-22 20:00:00', '2021-12-22 21:00:00', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', 6.25, 1.75, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 6.00, 20.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '2020', NULL, '0', '2021-12-22 13:30:15', '2022-01-13 11:47:47'),
(399, 36, 13, 18, '2021-12-22 12:00:00', '2021-12-22 14:00:00', '2021-12-22 16:00:00', '2021-12-23 10:00:00', '2021-12-23 12:00:00', '2021-12-23 14:00:00', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.234596127132875,\"longitude\":-93.16138975284339}', '{\"latitude\":30.234589532160264,\"longitude\":-93.16125786209459}', '{\"latitude\":30.23459299043077,\"longitude\":-93.16132702291333}', 8.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4.00, 14.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '2121', NULL, '0', '2021-12-22 14:04:47', '2022-01-18 09:53:38'),
(400, 36, 13, 18, '2021-12-19 02:00:00', '2021-12-19 03:00:00', '2021-12-19 04:00:00', '2021-12-19 07:00:00', '2021-12-19 09:00:00', '2021-12-19 10:00:00', '{\"latitude\":30.2345924945296,\"longitude\":-93.16131710551181}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3.00, 0.00, 5.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '2222', NULL, '0', '2021-12-22 14:24:01', '2021-12-28 13:41:34'),
(402, 36, 13, 18, '2021-12-26 19:00:00', '2021-12-26 21:00:00', '2021-12-26 23:00:00', '2021-12-27 09:00:00', '2021-12-27 10:00:00', '2021-12-27 11:00:00', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.234601473061158,\"longitude\":-93.16149666712452}', 0.00, 2.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4.00, 0.00, 4.00, 0.00, 0.00, 0.00, 0.00, 6.00, 0.00, 0.00, 0.00, 0, '2323', NULL, '0', '2021-12-22 19:02:42', '2021-12-28 13:29:51'),
(403, 36, 13, 25, '2021-12-16 11:00:00', '2021-12-16 14:00:00', '2021-12-16 15:00:00', '2021-12-16 18:00:00', '2021-12-16 19:00:00', '2021-12-17 13:00:00', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', '{\"latitude\":30.23450842208445,\"longitude\":-93.16097444992744}', 9.25, 16.75, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '0111', NULL, '0', '2021-12-27 12:10:14', '2022-01-13 11:45:20'),
(404, 35, 13, 16, '2022-01-31 10:00:00', '2022-01-31 10:00:00', '2022-01-31 10:00:00', '2022-01-31 10:00:00', '2022-01-31 10:00:00', '2022-01-31 10:00:00', '{\"latitude\":33.6978453,\"longitude\":73.049067}', '{\"latitude\":33.6978453,\"longitude\":73.049067}', '{\"latitude\":33.6978453,\"longitude\":73.049067}', '{\"latitude\":33.6978453,\"longitude\":73.049067}', '{\"latitude\":33.6978453,\"longitude\":73.049067}', '{\"latitude\":33.6978458,\"longitude\":73.0490559}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '569', 'gfdbvcuyt mnblkjoiu', '0', '2022-01-31 09:56:47', '2022-01-31 09:56:48'),
(405, 35, 16, 0, '2022-01-31 10:00:00', NULL, NULL, NULL, NULL, '2022-01-31 10:15:00', '{\"latitude\":33.6978458,\"longitude\":73.0490559}', '\"\"', '\"\"', '\"\"', '\"\"', '{\"latitude\":33.6978543,\"longitude\":73.0490509}', 0.25, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '0', '2022-01-31 10:08:12', '2022-01-31 10:08:12'),
(406, 35, 18, 0, '2022-01-31 10:15:00', NULL, NULL, NULL, NULL, '2022-01-31 10:15:00', '{\"latitude\":33.6978543,\"longitude\":73.0490509}', '\"\"', '\"\"', '\"\"', '\"\"', '{\"latitude\":33.6978543,\"longitude\":73.0490509}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '0', '2022-01-31 10:09:20', '2022-01-31 10:09:20'),
(407, 35, 18, 0, '2022-01-31 10:15:00', NULL, NULL, NULL, NULL, '2022-01-31 10:15:00', '{\"latitude\":33.6978543,\"longitude\":73.0490509}', '\"\"', '\"\"', '\"\"', '\"\"', '{\"latitude\":33.6978543,\"longitude\":73.0490509}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '0', '2022-01-31 10:09:55', '2022-01-31 10:09:55'),
(408, 35, 17, 0, '2022-01-31 10:15:00', NULL, NULL, NULL, NULL, '2022-01-31 10:15:00', '{\"latitude\":33.6978543,\"longitude\":73.0490509}', '\"\"', '\"\"', '\"\"', '\"\"', '{\"latitude\":33.6978543,\"longitude\":73.0490509}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '0', '2022-01-31 10:11:17', '2022-01-31 10:11:17'),
(409, 35, 14, 15, '2022-01-31 11:30:00', '2022-01-31 11:30:00', '2022-01-31 11:30:00', '2022-01-31 11:30:00', '2022-01-31 11:30:00', '2022-01-31 11:30:00', '{\"latitude\":33.6978492,\"longitude\":73.0490676}', '{\"latitude\":33.6978492,\"longitude\":73.0490676}', '{\"latitude\":33.6978492,\"longitude\":73.0490676}', '{\"latitude\":33.6978492,\"longitude\":73.0490676}', '{\"latitude\":33.6978492,\"longitude\":73.0490676}', '{\"latitude\":33.6978492,\"longitude\":73.0490676}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '256', NULL, '0', '2022-01-31 11:32:28', '2022-01-31 11:32:28'),
(410, 35, 15, 0, '2022-01-31 12:00:00', NULL, NULL, NULL, NULL, '2022-01-31 12:00:00', NULL, '\"\"', '\"\"', '\"\"', '\"\"', '{\"latitude\":33.697856,\"longitude\":73.0490548}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '0', '2022-01-31 11:57:38', '2022-01-31 11:57:38'),
(411, 35, 13, 18, '2022-01-31 19:00:00', '2022-01-31 19:00:00', '2022-01-31 19:00:00', '2022-01-31 19:00:00', '2022-01-31 19:00:00', '2022-01-31 19:00:00', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '258', NULL, '0', '2022-01-31 19:01:07', '2022-01-31 19:01:07'),
(412, 35, 14, 20, '2022-01-31 19:00:00', '2022-01-31 19:00:00', '2022-01-31 19:00:00', '2022-01-31 19:00:00', '2022-01-31 19:00:00', '2022-01-31 19:00:00', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '369', NULL, '0', '2022-01-31 19:02:07', '2022-01-31 19:02:07'),
(413, 35, 22, 29, '2022-01-31 19:00:00', '2022-01-31 19:00:00', '2022-01-31 19:00:00', '2022-01-31 19:00:00', '2022-01-31 19:00:00', '2022-01-31 19:00:00', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', '{\"latitude\":33.5733092,\"longitude\":73.0935148}', 0.25, -0.25, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '147', NULL, '0', '2022-01-31 19:03:18', '2022-01-31 19:03:18'),
(414, 35, 14, 23, '2022-02-01 05:00:00', '2022-02-01 05:00:00', '2022-02-01 05:00:00', '2022-02-01 05:00:00', '2022-02-01 05:00:00', '2022-02-01 05:00:00', '{\"latitude\":33.6978451,\"longitude\":73.0490609}', '{\"latitude\":33.6978451,\"longitude\":73.0490609}', '{\"latitude\":33.6978451,\"longitude\":73.0490609}', '{\"latitude\":33.6978451,\"longitude\":73.0490609}', '{\"latitude\":33.6978451,\"longitude\":73.0490609}', '{\"latitude\":33.6978451,\"longitude\":73.0490609}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '456', NULL, '0', '2022-02-01 04:55:02', '2022-02-01 04:55:02'),
(415, 35, 13, 25, '2022-02-03 09:00:00', '2022-02-03 09:00:00', '2022-02-03 09:00:00', '2022-02-03 09:00:00', '2022-02-03 09:00:00', '2022-02-03 09:00:00', '{\"latitude\":33.6978579,\"longitude\":73.0490467}', '{\"latitude\":33.6978579,\"longitude\":73.0490467}', '{\"latitude\":33.6978579,\"longitude\":73.0490467}', '{\"latitude\":33.6978579,\"longitude\":73.0490467}', '{\"latitude\":33.6978579,\"longitude\":73.0490467}', '{\"latitude\":33.6978379,\"longitude\":73.0490572}', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '111', 'Hrudhd', '0', '2022-02-03 09:06:55', '2022-02-03 09:06:55');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employeeId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_id` int(11) DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `usertype` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `employeeId`, `job_id`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `usertype`, `first_name`, `last_name`, `status`, `image`, `phone_number`) VALUES
(20, '0', NULL, 'contact@ioptime.com', NULL, '$2y$10$WvRfKgRSpUBX10xWnBHX.u2lAQL6DZrCwMAVM8GfWXesbHjCZQYxu', NULL, '2021-04-06 15:27:20', '2021-12-27 06:41:45', 'admin', 'C', 'R', '2', NULL, '+92321648226'),
(35, 'EC123', NULL, 'ec.ioptime@gmail.com', NULL, '$2y$10$WvRfKgRSpUBX10xWnBHX.u2lAQL6DZrCwMAVM8GfWXesbHjCZQYxu', NULL, '2021-04-12 16:44:12', '2021-12-27 06:41:45', 'user', 'EC', 'Ioptime', '1', NULL, '0325687942'),
(36, '001', NULL, 'krfrasier@hotmail.com', NULL, '$2y$10$hFhOkrAhEpLGXJ1k0IYVEujDNkvyU0LSMq4QtXnEytd5/Df7nv2LO', NULL, '2021-04-16 21:54:43', '2021-12-27 06:41:45', 'admin', 'Kenneth', 'Frasier', '1', NULL, '13372750928'),
(56, 'eee22', NULL, 'aazam513@gmail.com', NULL, '$2y$10$WvRfKgRSpUBX10xWnBHX.u2lAQL6DZrCwMAVM8GfWXesbHjCZQYxu', 'wzlBaqHzO99ew4IvZfyGIJzLUwcBfQg2JrePoXmIuyKVXeEozwoEZZfC4P49', '2021-09-14 04:33:48', '2021-12-27 06:41:45', 'user', 'ali', 'azam', '1', NULL, '0118119191'),
(57, 'nc', NULL, 'nc@yopmail.com', NULL, '$2y$10$Eu3tlAVwClgt1HL2jyq2K.Y7gAETT7LZQ.Avm8Jhu3azmuStRY.Xa', NULL, '2021-09-15 15:41:41', '2021-12-27 06:41:45', 'user', 'Nouman', 'Sakhawat', '1', NULL, '03055536641'),
(58, '007', NULL, 'tigerreaux@gmail.com', NULL, '$2y$10$U4W.ADJLsn/XNWxs0sZiNesw/Hp7GI2RQ5ukJNW.Alxji3Sj8xA/O', NULL, '2021-09-23 16:03:43', '2021-12-27 06:41:45', 'user', 'James', 'Bond', '1', NULL, '337.275.0928'),
(59, '123456', NULL, 'stacyga410@gmail.com', NULL, '$2y$10$fPxggJmR2MfscGpRv.mOguBwkgQVGwqWcp75NbCD764W2DtxeXe/C', NULL, '2021-09-23 16:39:47', '2021-12-27 06:41:45', 'user', 'Stacy', 'Armstrong', '1', NULL, '337.707.7683'),
(61, '000', NULL, 'irrankhan2@gmail.com', NULL, '$2y$10$3bkwssfVD8a6p07tuXSaTeEsf9dlm8.5W9kHdHZNFYuvsIkhj7sje', NULL, '2021-10-22 09:09:18', '2021-12-27 06:41:45', 'user', 'test', 'Parveen', '0', NULL, '03005195622'),
(65, '001', NULL, 'testioptime@zohomail.com', NULL, '$2y$10$O4kx34dyDsvXJdxnfLtHyuY94qlxNsmDCc3YSH8ssCdlI5x5l/kHu', NULL, '2021-10-25 05:07:26', '2021-12-27 06:41:45', 'user', 'reset  test', 'qq', '1', NULL, '0340'),
(66, '001', NULL, 'iqraparveen970@yahoo.com', NULL, '$2y$10$e05q6CXk4TClETK51koo9OCUrnisFT2mNqobiiFkkjmaFs0eWyv3C', NULL, '2021-10-25 05:12:58', '2021-12-27 06:41:45', 'user', 'Iqra', 'tESTRESET', '1', NULL, '0300'),
(67, '002', NULL, 'testreset@yopmail.com', NULL, '$2y$10$waSki0VYpUHJNA3Hu611veECFAEjXOx3DxmvTOpFdGfrB35rfciJG', NULL, '2021-10-25 05:19:21', '2021-12-27 06:41:45', 'user', 'QA', 'Reset', '1', NULL, '090078601'),
(70, '.', NULL, 'testabcd@yopmail.com', NULL, '$2y$10$5yDpd2WqEucEJ2Vrlb6uU.lAhrOXbDqO8l1bWS6Z1g.fq8fSUi5Gq', NULL, '2021-10-25 09:22:27', '2021-12-27 06:41:45', 'admin', '.', 'cc', '1', NULL, '034');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `breaks`
--
ALTER TABLE `breaks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conf_mapping`
--
ALTER TABLE `conf_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_configuration`
--
ALTER TABLE `job_configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_types`
--
ALTER TABLE `job_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_data`
--
ALTER TABLE `report_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `times_list`
--
ALTER TABLE `times_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_logs`
--
ALTER TABLE `time_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `breaks`
--
ALTER TABLE `breaks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `conf_mapping`
--
ALTER TABLE `conf_mapping`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `job_configuration`
--
ALTER TABLE `job_configuration`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `job_types`
--
ALTER TABLE `job_types`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `report_data`
--
ALTER TABLE `report_data`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `times_list`
--
ALTER TABLE `times_list`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `time_logs`
--
ALTER TABLE `time_logs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=416;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
