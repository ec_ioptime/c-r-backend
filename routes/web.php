<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);
Route::get('/clear', function() {

    Artisan::call('config:clear');

    return "Cleared!";

});
    Route::get('/docs','DocsController@getDocs');
    Route::get('login', 'Admin\UserController@getLogin');
    Route::group(['middleware' => ['auth','admin']],function(){

    //Route::get('dashboard', 'Admin\UserController@dashboard')->name('dashboard');

    // ===============================================================================
    //                            USERS
    // ===============================================================================
    Route::get('users', 'Admin\UserController@index')->name('user.index');
    Route::get('user/terminate', 'Admin\UserController@terminated_users')->name('user.terminated');
    Route::get('user-terminate/{id}', 'Admin\UserController@terminate')->name('terminate');
    Route::get('user-activate/{id}', 'Admin\UserController@activate')->name('activate');
    Route::get('createuser', 'Admin\UserController@createuserview');
    Route::get('user/{id}', 'Admin\UserController@edituserview');
    Route::post('user', 'Admin\UserController@createUserWeb');
    Route::get('user/delete/{id}', 'Admin\UserController@delete')->name('user.delete');
    Route::post('update-user/{id}', 'Admin\UserController@updateUserWeb');

    Route::get('users/create', 'Admin\UserController@create')->name('user.create');
    Route::post('users/store', 'Admin\UserController@store')->name('user.store');
    Route::patch('user/update/{id}', 'Admin\UserController@update')->name('user.update');

    Route::get('reset/password', 'Admin\UserController@reset_password')->name('reset.view');
    Route::post('password/update', 'Admin\UserController@update_password')->name('reset.password');

    Route::get('send/resetpassword/{email}', 'Admin\UserController@forgot')->name('send.link');

    // ===============================================================================
    //                            PROPERTIES
    // ===============================================================================
    Route::get('createproperties', 'Admin\PropertyController@create');
    Route::get('edit-property/{id}', 'Admin\PropertyController@EditProperty');
    Route::get('delete-property/{id}', 'Admin\PropertyController@DeleteProperty');
    Route::get('properties', 'Admin\PropertyController@getProperties');
    Route::post('update-property/{id}', 'Admin\PropertyController@updateProperty');
    Route::post('create-property', 'Admin\PropertyController@createProperty');


    // API --------------------------------------------------------------------------
     Route::get('property/{id}', 'Admin\PropertyController@getPropertyApi');
    Route::post('property', 'Admin\PropertyController@createPropertyApi');
    Route::delete('property/{id}', 'Admin\PropertyController@deletePropertyApi');


    Route::post('/reports/bulkapprove', 'Admin\ReportController@bulkapprove')->name('bulk.approve');
    Route::post('/reports/bulkdelete', 'Admin\ReportController@bulkdelete')->name('bulk.delete');

    // ===============================================================================
    //                            JOBS
    // ===============================================================================
    Route::get('jobs', 'Admin\JobsController@index')->name('jobs.index');
    Route::get('jobs/create', 'Admin\JobsController@create')->name('jobs.create');
    Route::post('jobs/store', 'Admin\JobsController@store')->name('jobs.store');
    Route::get('jobs/edit/{id}', 'Admin\JobsController@edit')->name('jobs.edit');
    Route::patch('jobs/update/{id}', 'Admin\JobsController@update')->name('jobs.update');
    Route::get('jobs/delete/{id}', 'Admin\JobsController@delete')->name('jobs.delete');

    // ===============================================================================
    //                            JOB-TYPES
    // ===============================================================================
    Route::get('types', 'Admin\TypesController@index')->name('types.index');
    Route::get('types/create', 'Admin\TypesController@create')->name('types.create');
    Route::post('types/store', 'Admin\TypesController@store')->name('types.store');
    Route::get('types/edit/{id}', 'Admin\TypesController@edit')->name('types.edit');
    Route::patch('types/update/{id}', 'Admin\TypesController@update')->name('types.update');
    Route::get('types/delete/{id}', 'Admin\TypesController@delete')->name('types.delete');


    // ===============================================================================
    //                            JOB-TYPES
    // ===============================================================================
    Route::get('jobconfig', 'Admin\ConfigController@index')->name('config.index');
    Route::get('jobconfig/create', 'Admin\ConfigController@create')->name('config.create');
    Route::post('jobconfig/store', 'Admin\ConfigController@store')->name('config.store');
    Route::get('jobconfig/edit/{id}', 'Admin\ConfigController@edit')->name('config.edit');
    Route::patch('jobconfig/update/{id}', 'Admin\ConfigController@update')->name('config.update');
    Route::get('jobconfig/delete/{id}', 'Admin\ConfigController@delete')->name('config.delete');

    Route::get('job/data/{id}', 'Admin\ConfigController@data')->name('job.data');

    Route::get('/approved', 'Admin\ReportController@approved')->name('approved.logs');
    Route::get('/disapproved', 'Admin\ReportController@disapproved')->name('disapproved.logs');

    Route::get('/save/approved/{id}', 'Admin\ReportController@save_approve_logs')->name('save.approve');
    Route::get('/save/disapproved/{id}', 'Admin\ReportController@save_disapprove_logs')->name('save.disapprove');

    Route::get('edit-logs/{id}', 'Admin\ReportController@EditReport')->name('details.logs');
    Route::patch('update-logs/{id}', 'Admin\TimeLogController@update')->name('logs.update');
    Route::get('timelogs/delete/{id}', 'Admin\TimeLogController@delete')->name('logs.delete');

    Route::get('breaks/delete/{id}', 'Admin\TimeLogController@delete_breaks')->name('breaks.delete');
    Route::patch('update-breaks/{id}', 'Admin\TimeLogController@updatebreaks')->name('breaks.update');
    Route::post('add-breaks', 'Admin\TimeLogController@addbreaks')->name('breaks.add');

    Route::get('logs/{id}', 'Admin\TimeLogController@userlogs')->name('user.logs');
    Route::get('calculate-logs/{id}', 'Admin\TimeLogController@calculate_userlogs')->name('calculate.logs');

    // ===============================================================================
    //                            REPORTS
    // ===============================================================================
    Route::get('createreport', function(){
        return view('admin.report.create');
    });

    Route::get('/reports','Admin\ReportController@index')->name('reports.index');

    Route::get('reports/create', 'Admin\ReportController@createReport')->name('reports.create');
    Route::post('store-report', 'Admin\ReportController@store')->name('reports.store');
    Route::get('report-details/{id}', 'Admin\ReportController@details')->name('details');

    Route::get('report-details/extra/{id}/{user_id}', 'Admin\ReportController@further_details')->name('further.details');

    Route::get('edit-report/{id}', 'Admin\ReportController@EditReport');
    Route::post('update-report/{id}', 'Admin\ReportController@UpdateReport');
    Route::get('delete-report/{id}', 'Admin\ReportController@DeleteReport')->name('delete.report');

    // API --------------------------------------------------------------------------
    Route::get('report/{id}', 'Admin\ReportController@getReport');
    Route::delete('report/{id}', 'Admin\ReportController@deleteReport');
    Route::get('/','Admin\ReportController@getlogs');
    Route::get('/register-roles', 'Admin\DashboardController@register');
    Route::get('/role-edit/{id}', 'Admin\DashboardController@edit');
    Route::post('/role-register-update/{id}', 'Admin\DashboardController@update');
    Route::get('/role-delete/{id}', 'Admin\DashboardController@delete');
});

Route::get('/register',function(){
    return view('auth.register');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>['auth']],function(){
 //Route::get('/{wildcard}','Admin\ReportController@getReports');
});
