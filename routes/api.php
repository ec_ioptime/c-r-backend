<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('upload-image', 'ImageController@upload');
Route::post('signup', 'Admin\UserController@signup');
Route::post('login', 'Admin\UserController@index');
Route::post('logout', 'Admin\UserController@logout');

Route::get('jobs', 'ApiController@get_jobs');
Route::get('job/types', 'ApiController@get_jobtypes');


Route::post('/login','ApiController@userlogin');
Route::post('/forgot','ApiController@forgot');

Route::post('/reset/password','ApiController@reset');

Route::post('/test','ApiController@timelogs');

Route::post('/testlogs','ApiController@testlogs');

Route::post('/newtest','ApiController@newtest');

Route::post('/timelogs','ApiController@post_timelogs');

Route::group(['middleware'=>'auth:sanctum'],function(){

    // ===============================================================================
    //                            CLIENTS
    // ===============================================================================
    // Route::get('test', 'Admin\ClientController@test');
    // Route::get('clients', 'Admin\ClientController@getClientsApi');
    // Route::get('client/{id}', 'Admin\ClientController@getClientApi');
    // Route::post('client', 'Admin\ClientController@createClient');
    // Route::delete('delete-client/{id}', 'Admin\ClientController@deleteClientApi');

    // ===============================================================================
    //                            USERS
    // ===============================================================================
    Route::get('users', 'Admin\UserController@getUsers');
    Route::get('user/{id}', 'Admin\UserController@getUser');
    Route::post('user', 'Admin\UserController@createUser');
    Route::post('update-user/{id}', 'Admin\UserController@updateUser');
    Route::delete('user/{id}', 'Admin\UserController@deleteUser');

    // get properties of a user
    Route::get('user-properties/{id}','Admin\UserController@getUserProperties');

    // ===============================================================================
    //                            PROPERTIES
    // ===============================================================================
    Route::get('properties', 'Admin\PropertyController@getPropertiesApi');
    Route::get('property/{id}', 'Admin\PropertyController@getPropertyApi');
    Route::post('property', 'Admin\PropertyController@createPropertyApi');
    Route::post('update-property/{id}', 'Admin\PropertyController@updatePropertyApi');
    Route::delete('property/{id}', 'Admin\PropertyController@deletePropertyApi');

    //get reports of a property
    Route::get('property-reports/{id}','Admin\PropertyController@getPropertyReports');
    Route::get('properties-with-reports','Admin\PropertyController@getPropertiesReports');
    // ===============================================================================
    //                            REPORTS
    // ===============================================================================

    Route::get('reports', 'Admin\ReportController@getProperts');
    Route::get('report/{id}', 'Admin\ReportController@getReport');
    Route::post('report', 'Admin\ReportController@createReport');
    Route::delete('report/{id}', 'Admin\ReportController@deleteReport');
});