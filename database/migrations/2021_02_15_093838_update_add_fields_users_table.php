<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddFieldsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function (Blueprint $table){
            $table->string('country')->nullable();
            $table->string('date_of_inspection')->nullable();
            $table->string('inspection_fee')->nullable();
            $table->string('realtor1')->nullable();
            $table->string('realtor2')->nullable();
            $table->string('realtor1_email')->nullable();
            $table->string('realtor2_email')->nullable();
            $table->string('direction')->nullable();
            $table->string('weather')->nullable();
            $table->string('summary')->nullable();
            $table->string('phone_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn('country', 191)->default('NULL');
        $table->dropColumn('date_of_inspection', 191)->default('NULL');
        $table->dropColumn('inspection_fee', 191)->default('NULL');
        $table->dropColumn('realtor1', 191)->default('NULL');
        $table->dropColumn('realtor2', 191)->default('NULL');
        $table->dropColumn('realtor1_email', 191)->default('NULL');
        $table->dropColumn('realtor2_email', 191)->default('NULL');
        $table->dropColumn('direction', 191)->default('NULL');
        $table->dropColumn('weather', 191)->default('NULL');
        $table->dropColumn('summary', 191)->default('NULL');
        $table->dropColumn('phone_number', 191)->default('NULL');
        // $table->dropColumn('image');
        // $table->dropColumn('address', 191)->default('NULL');

    }
}